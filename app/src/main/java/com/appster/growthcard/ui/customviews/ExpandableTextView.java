package com.appster.growthcard.ui.customviews;


/**
 * Created by navdeep on 12/04/16.
 */

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.utils.MySpannable;

public class ExpandableTextView extends TextView {
    private static final int DEFAULT_TRIM_LENGTH = 200;
    private static final String ELLIPSIS = "Read More";
    private CharSequence originalText;
    private CharSequence trimmedText;
    private BufferType bufferType;
    private boolean trim = true;
    private int trimLength;

    public ExpandableTextView(Context context) {
        this(context, null);
    }

    public ExpandableTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView);
        this.trimLength = typedArray.getInt(R.styleable.ExpandableTextView_trimLength, DEFAULT_TRIM_LENGTH);
        typedArray.recycle();

        setMovementMethod(LinkMovementMethod.getInstance());

    }

    private void setText() {
        super.setText(getDisplayableText(), bufferType);
    }

    private CharSequence getDisplayableText() {
        return trim ? trimmedText : originalText;
    }

    public void collapseText() {
        trim = true;
        setText();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        originalText = Html.fromHtml(text.toString());
        trimmedText = getTrimmedText(text);
        bufferType = BufferType.SPANNABLE;
        setText();
    }

    private CharSequence getTrimmedText(CharSequence text) {
        if (originalText != null && originalText.length() > trimLength) {

            SpannableStringBuilder ssb = new SpannableStringBuilder(originalText, 0, trimLength + 1).append(ELLIPSIS);
            ssb.setSpan(new MySpannable(false) {

                @Override
                public void onClick(View widget) {

                    trim = !trim;
                    setText();
                    requestFocusFromTouch();
                }
            }, ssb.toString().indexOf(ELLIPSIS), ssb.toString().indexOf(ELLIPSIS) + ELLIPSIS.length(), 0);

            ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getContext(), android.R.color.holo_green_dark)), ssb.toString().indexOf(ELLIPSIS), ssb.toString().indexOf(ELLIPSIS) + ELLIPSIS.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return ssb;
        } else {
            return originalText;
        }
    }

    public CharSequence getOriginalText() {
        return originalText;
    }

    public void setTrimLength(int trimLength) {
        this.trimLength = trimLength;
        trimmedText = getTrimmedText(originalText);
        setText();
    }

    public int getTrimLength() {
        return trimLength;
    }
}