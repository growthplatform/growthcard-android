package com.appster.growthcard.ui.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.EditProfileReq;
import com.appster.growthcard.network.requests.LogOutReq;
import com.appster.growthcard.network.response.EditProfileResponse;
import com.appster.growthcard.network.response.GenericResponse;
import com.appster.growthcard.network.response.SignInResponse;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.regex.Pattern;

import id.zelory.compressor.Compressor;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshu on 25/02/16.
 */
public class SettingActivity extends RootActivity implements View.OnClickListener {

    private static final int RESULT_LOAD_IMG = 1;
    private static final int CAMERA_RESULT = 2;
    private static final int PICK_IMAGE_FROM_CAMERA = 123;
    private static final String INTENT_PICK_TYPE_AS_IMAGE = "image/*";
    private static final int PICK_IMAGE_FROM_GALLERY = 133;
    private Button btntoolDone;
    private ImageView ivUser;
    private ImageView ivClick;
    private TextView tvWorkdays;
    private RelativeLayout rel_termNconditions;
    private RelativeLayout rel_viewTutorial;
    private RelativeLayout rel_feedback;
    private RelativeLayout rel_logout;
    private TextView tvAccountSettings;
    private TextView tvEmailWord;
    private TextView tvFirstNameWord;
    private TextView tvLastNameWord;
    private TextView tvWorkdaysWord;
    private TextView tvPrivacySettings;
    private TextView tvTermNconditions;
    private TextView tvViewTutorial;
    private TextView tvFeedback;
    private TextView tvLogout;
    private TextView tvEmail;
    private TextView tvFirstName;
    private TextView tvLastName;
    private String mImgDecodableString;
    private RelativeLayout rel_workdays;
    private AppPrefrences appPref;
    private Bitmap bitmap;


    public static String updatedWorkDays = "";
    private File imageFile;
    private File photo;
    private Uri imageCameraUri;
    private TextView tvChangePassword;
    private RelativeLayout relChangePassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        appPref = new AppPrefrences(SettingActivity.this);
        initialization();
        registerForContextMenu(ivClick);

        ivClick.setOnClickListener(this);
        ivUser.setOnClickListener(this);
        rel_termNconditions.setOnClickListener(this);
        rel_viewTutorial.setOnClickListener(this);
        rel_workdays.setOnClickListener(this);
        rel_feedback.setOnClickListener(this);
        btntoolDone.setOnClickListener(this);
        rel_logout.setOnClickListener(this);
        relChangePassword.setOnClickListener(this);

        tvEmail.setText(appPref.getEmail());
        tvFirstName.setText(appPref.getFirstName());
        tvLastName.setText(appPref.getLastName());

    }

    private void initialization() {

        btntoolDone = (Button) findViewById(R.id.toolBtnDone);
        ivUser = (ImageView) findViewById(R.id.img_showimage);
        ivClick = (ImageView) findViewById(R.id.img_clickimage);

        tvWorkdays = (TextView) findViewById(R.id.txtWorkdays);
        tvEmailWord = (TextView) findViewById(R.id.txtEmailWord);
        tvAccountSettings = (TextView) findViewById(R.id.txtAccountSettings);
        tvFirstNameWord = (TextView) findViewById(R.id.txtFirstNameWord);
        tvLastNameWord = (TextView) findViewById(R.id.txtLastNameWord);
        tvWorkdaysWord = (TextView) findViewById(R.id.txtWorkdaysWord);
        tvPrivacySettings = (TextView) findViewById(R.id.txtPrivacySettings);
        tvViewTutorial = (TextView) findViewById(R.id.txtViewTutorial);
        tvTermNconditions = (TextView) findViewById(R.id.txtTermNconditions);
        tvFeedback = (TextView) findViewById(R.id.txtFeedback);
        tvLogout = (TextView) findViewById(R.id.txtLogout);
        tvEmail = (TextView) findViewById(R.id.txtEmail);
        tvFirstName = (TextView) findViewById(R.id.txtFirstName);
        tvLastName = (TextView) findViewById(R.id.txtLastName);
        tvChangePassword = (TextView) findViewById(R.id.txtChangePassword);
        relChangePassword = (RelativeLayout) findViewById(R.id.relChangePassword);

        rel_termNconditions = (RelativeLayout) findViewById(R.id.rel_termNconditions);
        rel_viewTutorial = (RelativeLayout) findViewById(R.id.rel_viewTutorial);
        rel_feedback = (RelativeLayout) findViewById(R.id.rel_feedback);
        rel_logout = (RelativeLayout) findViewById(R.id.rel_logout);
        rel_workdays = (RelativeLayout) findViewById(R.id.rel_workdays);

        photo = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + "Pic.jpg");
        imageCameraUri = Uri.fromFile(photo);
        Utils.setTextStyle(tvAccountSettings, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(tvPrivacySettings, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(tvEmailWord, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvFirstNameWord, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvLastNameWord, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvWorkdaysWord, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvTermNconditions, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvViewTutorial, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvFeedback, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvLogout, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvEmail, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvWorkdays, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvEmail, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvFirstName, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvLastName, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvWorkdays, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvChangePassword, getApplication(), Utils.REGULAR);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        Utils.setTextStyle(btntoolDone, getApplication(), Utils.MEDIUM);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        updatedWorkDays = appPref.getWorkDays();
        Utils.loadUrlImage(appPref.getProfileImage(), ivUser);
        setData();

    }

    @Override
    protected void onResume() {
        super.onResume();

        setData();
    }

    private void setData() {

        if (!updatedWorkDays.equals(appPref.getWorkDays())) {
            btntoolDone.setVisibility(View.VISIBLE);
        }

        String workDays = "";
        for (int i = 0; i < updatedWorkDays.length(); i++) {
            if (updatedWorkDays.charAt(i) == '1') {

                if (workDays.equals(""))
                    workDays = Utils.days[i].substring(0, 3);
                else
                    workDays = workDays + "," + Utils.days[i].substring(0, 3);
            }

        }
        tvWorkdays.setText(workDays);

    }

    @Override
    public void onBackPressed() {

        if (!updatedWorkDays.equals(appPref.getWorkDays()) || !tvEmail.getText().toString().trim().equals(appPref.getEmail().trim()) || !tvFirstName.getText().toString().trim().equals(appPref.getFirstName().trim()) || !tvLastName.getText().toString().trim().equals(appPref.getLastName().trim())) {
            new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.NORMAL_TYPE)
                    .setTitleText(getString(R.string.alert))
                    .showCancelButton(true)
                    .setCancelText(getString(R.string.cancel))
                    .setContentText(getString(R.string.settings_not_saved))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                        }
                    })
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismiss();
                            finish();
                        }
                    })
                    .show();

        } else
            finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_showimage:
                selectImage();

                break;
            case R.id.img_clickimage:
                selectImage();

                break;

            case R.id.rel_viewTutorial:
                Intent viewtutorial = new Intent(this, TutorialActivity.class);
                viewtutorial.putExtra("FROM_SETTING", true);

                startActivity(viewtutorial);
                break;

            case R.id.rel_termNconditions:
                Intent termsNconditions = new Intent(this, TermsActivity.class);
                termsNconditions.putExtra("FROM_SETTING", true);
                startActivity(termsNconditions);
                break;

            case R.id.rel_workdays:
                Intent workdays = new Intent(this, WorkDaysActivity.class);
                workdays.putExtra("FROM_SETTING", true);
                workdays.putExtra("WORKDAYS", updatedWorkDays);

                startActivity(workdays);
                break;

            case R.id.rel_feedback:
                Intent feedback = new Intent(this, FeedbackActivity.class);
                feedback.putExtra("FROM_SETTING", true);
                startActivity(feedback);
                break;

            case R.id.toolBtnDone:

                if (TextUtils.isEmpty(tvEmail.getText().toString().trim())) {
                    Utils.showResMsg(tvEmail, getString(R.string.empty_mail));
                    return;
                }

                if (!isEmailValid(tvEmail.getText().toString().trim())) {
                    Utils.showResMsg(tvEmail, getString(R.string.error_invalid_email));
                    return;
                }

                if (tvFirstName.getText().toString().trim().isEmpty()) {
                    Utils.showResMsg(tvFirstName, getString(R.string.please_enter_firstname));
                    return;
                }

                if (tvLastName.getText().toString().trim().isEmpty()) {
                    Utils.showResMsg(tvFirstName, getString(R.string.please_enter_lastname));
                    return;
                }

                editProfile();
                break;

            case R.id.relChangePassword:

                startActivity(new Intent(this, ChangePasswordActivity.class));
                break;

            case R.id.rel_logout:

                new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.NORMAL_TYPE)
                        .setTitleText(getString(R.string.logout))
                        .showCancelButton(true)
                        .setCancelText(getString(R.string.no))
                        .setConfirmText(getString(R.string.yes))
                        .setContentText(getString(R.string.logout_message))
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismiss();

                                logoutApi();

                            }
                        })
                        .show();
                break;


        }


    }

    private void logoutApi() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SettingActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<GenericResponse> ip = service.logOut(getLogoutPayload());


        ip.enqueue(new ErrorCallback.MyCallback<GenericResponse>() {
            @Override
            public void success(Response<GenericResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (FilterActivity.filterData != null)
                    FilterActivity.filterData.clear();

                NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nMgr.cancelAll();
                appPref.logOut();
                finish();

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SettingActivity.this, true);


    }

    private LogOutReq getLogoutPayload() {

        LogOutReq req = new LogOutReq();
        req.setDeviceId(Utils.getDeviceId(SettingActivity.this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());


        return req;
    }

    private void editProfile() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SettingActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<EditProfileResponse> ip = service.updateUserProfile(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<EditProfileResponse>() {
            @Override
            public void success(Response<EditProfileResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                appPref.setWorkDays(updatedWorkDays);
                appPref.setEmail(tvEmail.getText().toString().trim());
                appPref.setFirstName(tvFirstName.getText().toString().trim());
                appPref.setLastName(tvLastName.getText().toString().trim());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.done))
                                .setContentText(getString(R.string.settings_updated))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();

                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SettingActivity.this, true);


    }

    private void editImage() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SettingActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        RequestBody userId =
                RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(appPref.getUserId()));
        RequestBody userToken =
                RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(appPref.getUserToken()));
        RequestBody deviceId =
                RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(Utils.getDeviceId(SettingActivity.this)));

        ErrorCallback.MyCall<SignInResponse> ip = service.uploadProfilePhoto(getPhotoRequestBody(), deviceId, userToken, userId);

        ip.enqueue(new ErrorCallback.MyCallback<SignInResponse>() {
            @Override
            public void success(Response<SignInResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());

                appPref.setProfileImage(response.body().getResult().getProfileImage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        new SweetAlertDialog(SettingActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText(getString(R.string.done))
                                .setContentText(getString(R.string.image_uploaded))
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        ivUser.setImageBitmap(bitmap);

                                        sweetAlertDialog.dismiss();
                                        //finish();
                                    }
                                })
                                .show();

                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SettingActivity.this, true);


    }

    private EditProfileReq getPayload() {
        EditProfileReq req = new EditProfileReq();
        req.setDeviceId(Utils.getDeviceId(SettingActivity.this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId() + "");
        req.setWorkDay(updatedWorkDays);
        if (!tvEmail.getText().toString().trim().equals(appPref.getEmail().trim())) {
            req.setEmail(tvEmail.getText().toString());
        }

        if (!tvFirstName.getText().toString().trim().equals(appPref.getFirstName().trim())) {
            req.setFirstName(tvFirstName.getText().toString());
        }

        if (!tvLastName.getText().toString().trim().equals(appPref.getLastName().trim())) {
            req.setLastName(tvLastName.getText().toString());
        }


        return req;
    }


    private RequestBody getPhotoRequestBody() {
        RequestBody requestBody = null;
        if (imageFile != null) {
            requestBody = RequestBody.create(MediaType.parse(getString(R.string.multipart_form_data)), imageFile);
        }
        return requestBody;
    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (permsRequestCode) {

            case 200:

                boolean audioAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                break;

        }

    }

    private void selectImage() {
        String[] perms = {
                getString(R.string.android_permission_READ_EXTERNAL_STORAGE),
                getString(R.string.android_permission_WRITE_EXTERNAL_STORAGE),
                getString(R.string.android_permission_CAMERA)
        };

        int permsRequestCode = 200;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(perms, permsRequestCode);
        }
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.select_from_gallery),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        builder.setTitle(getString(R.string.select_image));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, imageCameraUri);
                    startActivityForResult(intent, PICK_IMAGE_FROM_CAMERA);
                } else if (items[item].equals(getString(R.string.select_from_gallery))) {
                    Intent intent = new Intent();
                    intent.setType(INTENT_PICK_TYPE_AS_IMAGE);
                    intent.setAction(Intent.ACTION_PICK);
                    startActivityForResult(Intent.createChooser(intent, getString(R.string.select_image)), PICK_IMAGE_FROM_GALLERY);
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK_IMAGE_FROM_GALLERY && resultCode == RESULT_OK) {
            bitmap = null;
            int rotateImageDegrees = 0;
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String filePath = cursor.getString(columnIndex);
            cursor.close();
            rotateImageDegrees = getCameraPhotoOrientation(SettingActivity.this, selectedImage, filePath);
          //  bitmap = compressImage(SettingActivity.this, filePath);
            File file = new File(filePath);

            bitmap = Compressor.getDefault(this).compressToBitmap(file);




            if (bitmap != null) {
                bitmap = rotateImage(bitmap, rotateImageDegrees);
            }
            editImage();
        }
        if (requestCode == PICK_IMAGE_FROM_CAMERA && resultCode == RESULT_OK) {
            int rotateImageDegrees;
            Uri selectedImage = imageCameraUri;
            InputStream is = null;
            bitmap=null;
            try {
                is = getContentResolver().openInputStream(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            String picturePath = selectedImage.toString();
            imageFile = new File(picturePath);
            rotateImageDegrees = getCameraPhotoOrientationCamera(SettingActivity.this, selectedImage);
         //   bitmap = compressImage(SettingActivity.this, imageFile.getPath());
            bitmap = Compressor.getDefault(this).compressToBitmap(imageFile);
/*
            BitmapFactory.Options options = new BitmapFactory.Options();

            // by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
            // you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true;
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getPath(), options);

            ByteArrayOutputStream bytearrayoutputstream = new ByteArrayOutputStream();


            bitmap.compress(Bitmap.CompressFormat.JPEG,40,bytearrayoutputstream);

            byte[] BYTE;

            BYTE = bytearrayoutputstream.toByteArray();

            bitmap = BitmapFactory.decodeByteArray(BYTE,0,BYTE.length);*/




            if (bitmap != null) {
                bitmap = rotateImage(bitmap, rotateImageDegrees);
            }
            editImage();
        }
    }

    public Bitmap compressImage(FragmentActivity context, String filePath) {

        Bitmap scaledBitmap = null;
        String filename = null;

        try {
            BitmapFactory.Options options = new BitmapFactory.Options();

            // by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
            // you try the use the bitmap here, you will get null.
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;

            if (actualHeight == 0) {
                actualHeight = bmp.getHeight();
            }
            if (actualWidth == 0) {
                actualWidth = bmp.getWidth();
            }

            float maxHeight = 800.0f;
            float maxWidth = 800.0f;

            System.out.println("heig actualWidth" + actualWidth);
            System.out.println("heig actualHeight" + actualHeight);
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            // width and height values are set maintaining the aspect ratio of the image

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;
                }
            }

            // setting inSampleSize value allows to load a scaled down version of the original image

            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

            // inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false;

            // this options allow android to claim the bitmap memory if it runs low on memory
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                // load the bitmap from its path
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

            // check the rotation of the image and display it properly
            ExifInterface exif;
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);

            FileOutputStream out = null;
            filename = filePath;
            out = new FileOutputStream(filename);

            // write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.PNG, 80, out);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return scaledBitmap;

    }

    private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }

    private Bitmap rotateImage(Bitmap img, int degree) {

        Matrix matrix = new Matrix();
        Bitmap rotatedImg = img;
       /* if (degree != 90 && degree!=270) {
            matrix.postRotate(degree);
            rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        }*/
        return rotatedImg;
    }

    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    public int getCameraPhotoOrientationCamera(Context context, Uri imageUri) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            imageFile = photo;
            ExifInterface exif = new ExifInterface(photo.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    private boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }


}
