package com.appster.growthcard.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.CreatePostReq;
import com.appster.growthcard.network.response.GenericResponse;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 22/03/16.
 */
public class NewPostActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btntoolDone;
    private EditText etPost;
    private RelativeLayout relpost;
    private Button btnMyTeam;
    private Button btnMyDepartment;
    private Button btnAll;
    private int flag = 0;
    private AppPrefrences appPref;
    private ImageButton btnClosed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);
        appPref = new AppPrefrences(this);

        initilize();


        btntoolDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etPost.getWindowToken(), 0);

                if (etPost.getText().toString().isEmpty()) {
                    Utils.showResMsg(etPost, getString(R.string.enter_message));

                } else {

                    relpost.setVisibility(View.VISIBLE);
                    etPost.setFocusable(false);
                    etPost.setFocusableInTouchMode(false);

                }
            }
        });

        etPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etPost.setFocusable(true);
                etPost.setFocusableInTouchMode(true);
            }
        });
    }

    private void initilize() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btntoolDone = (Button) findViewById(R.id.toolBtnDone);
        etPost = (EditText) findViewById(R.id.edtPost);
        relpost = (RelativeLayout) findViewById(R.id.relpost);
        btnMyTeam = (Button) findViewById(R.id.btnMyTeam);
        btnMyDepartment = (Button) findViewById(R.id.btnMyDepartment);
        btnAll = (Button) findViewById(R.id.btnAll);
        btnClosed = (ImageButton) findViewById(R.id.btnClosed);

        btnAll.setOnClickListener(this);
        btnMyDepartment.setOnClickListener(this);
        btnMyTeam.setOnClickListener(this);
        btnClosed.setOnClickListener(this);

        Utils.setTextStyleButton(btntoolDone, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(etPost, getApplication(), Utils.REGULAR);


        setSupportActionBar(toolbar);

        btntoolDone.setText(getResources().getString(R.string.publish));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_close));

        }


    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btnAll:
                flag = 3;

                hitApi();

                break;

            case R.id.btnMyDepartment:
                flag = 2;
                hitApi();
                break;

            case R.id.btnMyTeam:
                flag = 1;

                hitApi();
                break;

            case R.id.btnClosed:
                relpost.setVisibility(View.GONE);
                break;
        }

    }

    private void hitApi() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);

        ErrorCallback.MyCall<GenericResponse> ip = service.post(getPayload(etPost.getText().toString().trim(), flag));

        ip.enqueue(new ErrorCallback.MyCallback<GenericResponse>() {
            @Override
            public void success(Response<GenericResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                finish();


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, this, true);


    }

    public CreatePostReq getPayload(String message, int flag) {


        CreatePostReq req = new CreatePostReq();
        req.setDeviceId(Utils.getDeviceId(this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());
        req.setContent(message);
        req.setFlag(flag);
        req.setDepartmentId(appPref.getDepartmentId());


        return req;
    }

    @Override
    public void onBackPressed() {
        if (relpost.getVisibility() == View.VISIBLE) {
            relpost.setVisibility(View.GONE);
        } else {
            finish();
        }
    }
}
