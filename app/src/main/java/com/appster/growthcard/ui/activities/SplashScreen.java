package com.appster.growthcard.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.notifications.RegistrationIntentService;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Path;

public class SplashScreen extends AppCompatActivity implements Runnable {


    private static final long SPLASH_TIME_OUT = 2000;
    private Handler handler = new Handler();
    private AppPrefrences mAppPrefrences;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "ActivitySplash";
    private static final String FROM_SPLASH = "from_splash";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash_screen);


        mAppPrefrences = new AppPrefrences(SplashScreen.this);

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);
        }


    }


    BroadcastReceiver mRegistrationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            boolean sentToken = mAppPrefrences.isGCMRegistrationStatus();
            if (sentToken) {
                handler.postDelayed(SplashScreen.this, SPLASH_TIME_OUT);
            } else {
//                    mInformationTextView.setText("Error");
                Toast.makeText(SplashScreen.this, "error", Toast.LENGTH_SHORT).show();
                handler.postDelayed(SplashScreen.this, SPLASH_TIME_OUT);

            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver, new IntentFilter(Utils.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        handler.removeCallbacks(this);
    }

    @Override
    public void run() {
        //testApiCall();

        if (mAppPrefrences.getUserId() != 0 && !mAppPrefrences.getUserToken().equals("")) {
            if (!mAppPrefrences.isWorkDaysSelected()) {
                Intent i = new Intent(SplashScreen.this, WorkDaysActivity.class);
                i.putExtra(FROM_SPLASH, true);

                startActivity(i);
                finish();
            } else {

                if (mAppPrefrences.getRole() == 1) {
                    Intent i = new Intent(SplashScreen.this, ManagerMainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Intent i = new Intent(SplashScreen.this, SubMainActivity.class);
                    startActivity(i);
                    finish();
                }
            }
        } else if (mAppPrefrences.getLoginPath() == getResources().getInteger(R.integer.LoginPath)) {

            Intent i = new Intent(SplashScreen.this, TutorialActivity.class);
            i.putExtra(FROM_SPLASH, true);
            startActivity(i);
            finish();
        }

    }


    ////////////////////////////SAMPLE////////////////////////////////////////////////////////


    public static final String API_URL = "https://api.github.com";


    public static class Contributor {
        public final String login;
        public final int contributions;

        public Contributor(String login, int contributions) {
            this.login = login;
            this.contributions = contributions;
        }
    }

    public interface GitHub {
        @GET("/repos/{owner}/{repo}/contributors")
        ErrorCallback.MyCall<List<Contributor>> contributors(
                @Path("owner") String owner,
                @Path("repo") String repo);
    }


    private void testApiCall() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        GitHub service = retrofit.create(GitHub.class);
        ErrorCallback.MyCall<List<Contributor>> ip = service.contributors("square", "retrofit");
        ip.enqueue(new ErrorCallback.MyCallback<List<Contributor>>() {
            @Override
            public void success(Response<List<Contributor>> response) {
                Log.d("Tag", "SUCCESS! " + response.body().get(0).login + " " + response.body().get(0).contributions);
            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SplashScreen.this, false);

    }

    private Object createPayload(Object object) {
        ///manipulate data
        //set variables
        return object;
    }
}
