package com.appster.growthcard.ui.customviews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.GestureDetector;

/**
 * Created by navdeep on 07/04/16.
 */
public class HorizontalRecyclerView extends RecyclerView {
    private GestureDetector mGestureDetector;

    public HorizontalRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFadingEdgeLength(0);
    }


}
