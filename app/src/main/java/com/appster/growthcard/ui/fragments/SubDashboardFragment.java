package com.appster.growthcard.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.appster.growthcard.ui.activities.EADetailsActivity;
import com.appster.growthcard.ui.activities.FilterActivity;
import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.adapter.FeedListAdapterNew;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.interfaces.OnFragmentInteractionListener;
import com.appster.growthcard.ui.customviews.ScrollViewList;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.interfaces.ScrollViewListener;
import com.appster.growthcard.network.requests.FeedsReq;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.response.EADetailResponse;
import com.appster.growthcard.network.response.FeedsResponse;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersTouchListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SubDashboardFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private View view;
    private RecyclerView recPastEA;
    private LinearLayoutManager mLayoutManager;
    private FeedListAdapterNew mAdapter;
    private AppPrefrences appPref;
    private ImageView ivUserGraph;
    private EADetailResponse.Result eaDetailResponse;
    private TextView tvProfileName;
    private TextView tvUserDesignation;
    private LinearLayout currentEAParent;
    private RelativeLayout feedTopView;
    private List<String> mDummyData = new ArrayList<>();
    private int currentPage = 1;
    private int pageCount = 1;
    private FeedsResponse.RecentAnnouncement recentAnnouncement;
    private ArrayList feedList = new ArrayList();
    //    private ScrollViewList scrollView;
//    private NestedScrollView nestScroll;
       private TextView tvEmptyGraphView;
    private ImageView ivEmpty;
    private SwipeRefreshLayout swipeContainer;
    private TextView emptyView;


    public static SubDashboardFragment newInstance(String param1, String param2) {
        SubDashboardFragment fragment = new SubDashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SubDashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appPref = new AppPrefrences(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);


        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sub_fragment_feeds, null);
        setHasOptionsMenu(true);
        findViews();

        getEADetails();

        getFeedData();

        return view;
    }


    private void findViews() {

//        scrollView = (ScrollViewList) view.findViewById(R.id.scroller);
//        nestScroll = (NestedScrollView) view.findViewById(R.id.nest_scroller);
        recPastEA = (RecyclerView) view.findViewById(R.id.recPastEA);
        emptyView = (TextView) view.findViewById(R.id.empty_view);
        ivUserGraph = (ImageView) view.findViewById(R.id.user_graph_image);
        tvProfileName = (TextView) view.findViewById(R.id.name);
        tvUserDesignation = (TextView) view.findViewById(R.id.designation);
        ivEmpty = (ImageView) view.findViewById(R.id.empty_image_view);
        tvEmptyGraphView = (TextView) view.findViewById(R.id.empty_graph_view);

        currentEAParent = (LinearLayout) view.findViewById(R.id.current_ea_parent);
        feedTopView = (RelativeLayout) view.findViewById(R.id.feed_top_view);
        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                currentPage = 1;
                pageCount = 1;

                flag1 = 1;
                flag2 = 0;

                feedList.clear();



                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                    mAdapter = null;
                }
                getEADetails();
                getFeedData();
            }
        });


        Utils.setTextStyle(tvProfileName, getActivity(), Utils.MEDIUM);
        Utils.setTextStyle(tvUserDesignation, getActivity(), Utils.REGULAR);

        mLayoutManager = new LinearLayoutManager(getActivity());
        recPastEA.setLayoutManager(mLayoutManager);


        recPastEA.setNestedScrollingEnabled(false);
        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        if (appPref.geteaTitle().equalsIgnoreCase(""))
            ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_feeds);
        else
            ((RootActivity) getActivity()).getSupportActionBar().setTitle(appPref.geteaTitle());


        ((TextView) view.findViewById(R.id.empty_view)).setVisibility(View.INVISIBLE);
        ((TextView) view.findViewById(R.id.empty_view)).setText(R.string.no_data_available);


        tvProfileName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        tvUserDesignation.setText(appPref.getDesignation());
        currentEAParent.setOnClickListener(this);
        ivEmpty.setOnClickListener(this);
              tvEmptyGraphView.setOnClickListener(this);
        Utils.loadUrlImage(appPref.getProfileImage(), ivUserGraph);
        //  Inflate the layout for this fragment

        if (appPref.getEAId() == 0)
            currentEAParent.setVisibility(View.VISIBLE);
        //  ((TextView) view.findViewById(R.id.empty_graph_view)).setText(R.string.no_ea_assigned);


        recPastEA.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
//                nestScroll.animate()
//                        .translationYBy(dy / 2)
//                        .translationY(0)
//                        .setDuration(getResources().getInteger(android.R.integer.config_mediumAnimTime));


            }
        });


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.feed_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.feed_filter:

                startActivity(new Intent(getActivity(), FilterActivity.class));

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.current_ea_parent:
            case R.id.empty_graph_view:
            case R.id.empty_image_view:

                if (appPref.getEAId() != 0) {
                    Intent i = new Intent(getActivity(), EADetailsActivity.class);
                    i.putExtra("isCurrent", true);
                    i.putExtra("EAid", appPref.getEAId());
                    i.putExtra("USER", appPref.getUserId());
                    i.putExtra("TEAM", appPref.getTeamId());

                    startActivity(i);
                }

                break;
        }
    }

    public void refreshData() {
        getEADetails();
        currentPage = 1;
        getFeedData();
    }


    private void getEADetails() {
        if (appPref.getEAId() != 0) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(NetworkUtil.addHeader(getActivity())) ///to add header
                    .build();
            GCApi service = retrofit.create(GCApi.class);
            ErrorCallback.MyCall<EADetailResponse> ip = service.getEADetails(getEADetailPayload());

            ip.enqueue(new ErrorCallback.MyCallback<EADetailResponse>() {
                @Override
                public void success(final Response<EADetailResponse> response) {
                    Log.d("Tag", "SUCCESS! " + response.message());


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (response.body().getResult() != null) {


                                eaDetailResponse = response.body().getResult();


                                if (response.body().getResult().getGraph() != null) {

                                    Utils.setChart(getActivity(), currentEAParent, response.body().getResult().getGraph(), appPref.getRole() == 2);
                                }

                                appPref.setEATitle(response.body().getResult().getEffectiveActionTitle());

                                if (appPref.geteaTitle().equalsIgnoreCase("")) {
                                    ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_feeds);
                                } else {
                                    ((RootActivity) getActivity()).getSupportActionBar().setTitle(appPref.geteaTitle());

                                    if (Utils.isEAUpdated) {
                                        ((SubMainActivity) getActivity()).updateOverlay();
                                        Utils.isEAUpdated = false;

                                    }
                                }


                            } else {
                                ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_feeds);

                            }
                            if(mAdapter!=null){
                                if(eaDetailResponse!=null){
                                    Log.d("ea-assigned", "assigned");
                                    feedTopView.setVisibility(View.GONE);
                                }
                                mAdapter.notifyDataSetChanged();
                            }

                        }


                    });


                }

                @Override
                public void error(String errorMessage) {
                    Log.d("Tag", "Error! " + errorMessage);

                }


            }, getActivity(), false);
        }
        else {
            feedTopView.setVisibility(View.VISIBLE);
        }

    }

    private SendCommentsReq getEADetailPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(appPref.getEAId());
        req.setSubordinateId(appPref.getUserId());
        req.setFlag(1);

        return req;
    }


    private boolean isLoading = false;

    private void getFeedData() {

        swipeContainer.setRefreshing(false);
        if (appPref.getTeamId() != 0) {

            isLoading = true;
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(NetworkUtil.addHeader(getActivity())) ///to add header
                    .build();
            GCApi service = retrofit.create(GCApi.class);

            ErrorCallback.MyCall<FeedsResponse> ip = service.getFeeds(getFeedsPayload());

            ip.enqueue(new ErrorCallback.MyCallback<FeedsResponse>() {
                @Override
                public void success(final Response<FeedsResponse> response) {
                    Log.d("Tag", "SUCCESS! " + response.message());


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            try {
                                ((TextView) view.findViewById(R.id.empty_view)).setVisibility(View.INVISIBLE);
//                            scrollView.setVisibility(View.VISIBLE);
                                if (response.body().getResult().getResult().getFeed() != null && response.body().getResult().getResult().getFeed().size() > 0) {
                                    currentPage = response.body().getResult().getPageNo();
                                    pageCount = response.body().getResult().getTotalPages();
                                    if (currentPage == 1) {
                                        feedList.clear();

                                        feedList = (ArrayList) response.body().getResult().getResult().getFeed();

                                    } else if (currentPage > 1) {
                                        feedList.addAll(response.body().getResult().getResult().getFeed());
                                    }

                                    recentAnnouncement = response.body().getResult().getResult().getRecentAnnouncement();
                                    ((SubMainActivity) getActivity()).updateRecentAnnouncement(recentAnnouncement);
                                    fillData();
                                } else {
                                    if (response.body().getResult().getResult().getRecentAnnouncement() != null) {

                                        recentAnnouncement = response.body().getResult().getResult().getRecentAnnouncement();
                                        ((SubMainActivity) getActivity()).updateRecentAnnouncement(recentAnnouncement);

                                        fillData();

                                    } else {
                                        feedList.clear();
                                        fillData();
                                    }
                                }
                                isLoading = false;
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });


                }

                @Override
                public void error(String errorMessage) {
                    Log.d("Tag", "Error! " + errorMessage);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            isLoading = false;
                            pageCount = 1;
                            currentPage = 1;

                            if (feedList != null) {
                                feedList.clear();
                                fillData();
                            }
                        }
                    });


                }


            }, getActivity(), currentPage == 1);
        }
    }

    private void fillData() {


        if (currentPage == 1) {

            if (feedList.size() == 0) {
                if (eaDetailResponse == null) {
                    Log.d("ea-null", "EA-NULL");
                    emptyView.setVisibility(View.VISIBLE);
                } else {
                    Log.d("feedList-null", "feedList-NULL");

                    feedTopView.setVisibility(View.VISIBLE);
                    Utils.setChart(getActivity(), currentEAParent, eaDetailResponse.getGraph(), appPref.getRole() == 2);

                }
                if (mAdapter != null) {
                    mAdapter.notifyDataSetChanged();
                }
                return;
            }

            StickyRecyclerHeadersDecoration headersDecor;

            if (recentAnnouncement != null && recentAnnouncement.getAnnouncementId() != appPref.getRecentAnnouncementId()) {
                Log.d("recentAnnouncement", "present");

                appPref.setRecentAnnouncementId(recentAnnouncement.getAnnouncementId());
                mAdapter = new FeedListAdapterNew(getActivity(), eaDetailResponse, recPastEA, feedList, recentAnnouncement);
                recPastEA.setAdapter(mAdapter);
                headersDecor = new StickyRecyclerHeadersDecoration(mAdapter);
                recPastEA.addItemDecoration(headersDecor);
            } else {

                Log.d("recentAnnouncement", "absent");

                mAdapter = new FeedListAdapterNew(getActivity(), eaDetailResponse, recPastEA, feedList, null);
                recPastEA.setAdapter(mAdapter);
                headersDecor = new StickyRecyclerHeadersDecoration(mAdapter);
                recPastEA.addItemDecoration(headersDecor);

            }

            if (mAdapter != null) {
                StickyRecyclerHeadersTouchListener touchListener =
                        new StickyRecyclerHeadersTouchListener(recPastEA, headersDecor);
                touchListener.setOnHeaderClickListener(
                        new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
                            @Override
                            public void onHeaderClick(View header, int position, long headerId) {
                                if (appPref.getEAId() != 0) {
                                    if (position == 0) {
                                        Intent i = new Intent(getActivity(), EADetailsActivity.class);
                                        i.putExtra("isCurrent", true);
                                        i.putExtra("EAid", appPref.getEAId());
                                        i.putExtra("USER", appPref.getUserId());
                                        i.putExtra("TEAM", appPref.getTeamId());

                                        startActivity(i);
                                    }
                                }
                            }
                        });

                recPastEA.addOnItemTouchListener(touchListener);
            }

//            scrollView.setScrollViewListener(new ScrollViewListener() {
//                @Override
//                public void onScrollChanged(ScrollViewList scrollView, int x, int y, int oldx, int oldy) {
//                    View view = (View) scrollView.getChildAt(scrollView.getChildCount() - 1);
//                    int diff = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
//
//                    // if diff is zero, then the bottom has been reached
//                    if (diff == 0) {
//
//                        if (currentPage != pageCount && !isLoading) {
//
//                            currentPage++;
//                            getFeedData();
//
//                            System.out.println("load");
//                        }
//                    }
//                }
//            });


        } else if (mAdapter != null) {

            if (recentAnnouncement != null && recentAnnouncement.getAnnouncementId() != appPref.getRecentAnnouncementId()) {

                appPref.setRecentAnnouncementId(recentAnnouncement.getAnnouncementId());
                mAdapter.updateRecentAnnouncement(recentAnnouncement);
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();
            } else {
                mAdapter.updateRecentAnnouncement(null);
                mAdapter.notifyDataSetChanged();
                mAdapter.setLoaded();
            }


        }


    }

    public static int flag1 = 1;
    public static int flag2 = 0;
    public static boolean isUpdated = false;

    private FeedsReq getFeedsPayload() {

        FeedsReq feedsReq = new FeedsReq();

        feedsReq.setUserToken(appPref.getUserToken());
        feedsReq.setDeviceId(Utils.getDeviceId(getActivity()));
        feedsReq.setTeamId(appPref.getTeamId());
        feedsReq.setUserId(appPref.getUserId());
        feedsReq.setPageNo(currentPage);
        feedsReq.setCompanyId(appPref.getCompanyId());
        feedsReq.setFlag1(flag1);
        feedsReq.setFlag2(flag2);//> 0 ? 1 : 0);


        return feedsReq;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isUpdated) {
            isUpdated = false;
            pageCount = 0;
            currentPage = 1;
            feedList.clear();
//            scrollView.setVisibility(View.GONE);

            getFeedData();
        }

        if (Utils.isNotification)
            Utils.isNotification = false;


        if (Utils.isEACleared) {


            if (mAdapter != null && feedList != null) {
                feedList.clear();
                currentPage = 1;

                mAdapter.notifyDataSetChanged();
            }

           /* if (currentEAParent.getChildCount() > 2) {
                currentEAParent.removeViewAt(2);
            }*/
            findViews();

            Utils.isEACleared = false;


        }

        if (Utils.isEAUpdated) {

            findViews();

            if (appPref.getEAId() != 0) {
                getEADetails();
            }
            if (appPref.getTeamId() != 0)
                currentPage = 1;
            getFeedData();

        }
    }
}