package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.widget.TextView;

import com.appster.growthcard.notifications.GcmMessageHandler;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;

/**
 * Created by navdeep on 05/04/16.
 */
public class NotifyDialogActivity extends RootActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isAnimation = false;

        super.onCreate(savedInstanceState);

        setContentView(R.layout.notify_dialog);
        AppPrefrences appPref = new AppPrefrences(NotifyDialogActivity.this);
        String message;
        if (getIntent().getStringExtra(GcmMessageHandler.NOTIFICATION_MSG) != null) {
            message = getIntent().getStringExtra(GcmMessageHandler.NOTIFICATION_MSG);
        } else {
            message = getString(R.string.daily_notification_message).replace("XX", appPref.getFirstName());
        }
        Snackbar.make((TextView) findViewById(R.id.dummy), message, Snackbar.LENGTH_LONG).setCallback(new Snackbar.Callback() {
            @Override
            public void onDismissed(Snackbar snackbar, int event) {
                //see Snackbar.Callback docs for event details
                finish();
            }
            @Override
            public void onShown(Snackbar snackbar) {
            }
        }).show();
        if (getIntent().getStringExtra(GcmMessageHandler.NOTIFICATION_MSG) != null) {
            if (getIntent().getStringExtra(GcmMessageHandler.NOTIFICATION_TYPE).equalsIgnoreCase(getString(R.string.notification_type_7))) {
                Utils.isEACleared = true;
            }
            if (getIntent().getStringExtra(GcmMessageHandler.NOTIFICATION_TYPE).equalsIgnoreCase(getString(R.string.notification_type_2))) {
                Utils.isEAUpdated = true;
            }
            Utils.isNotification = true;
        }


    }

}
