package com.appster.growthcard.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.adapter.EffectiveActionListAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.CustomEAReq;
import com.appster.growthcard.network.requests.EAListReq;
import com.appster.growthcard.network.response.EAListResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshu on 01/03/16.
 */
public class EffectiveActionListActivity extends RootActivity {


    private RecyclerView rvEffectiveActionlist;
    private Button btnCreateEffectiveAction;
    private LinearLayoutManager mLayoutManager;
    private Button btntooldone;
    private EffectiveActionListAdapter mAdapter;
    private TextView tvChoosePredefinedList;
    private RelativeLayout relOverlay;
    private int topheight;
    private Toolbar toolbar;
    private EditText etEffectiveAction;
    private Button btnAddEffectiveAction;

    private TextView tvEaCount;

    private boolean loading = true;
    int pastVisiblesItems, visibleItemCount, totalItemCount;
    private AppPrefrences appPref;
    private int currentPage = 1, pageCount = 0;
    private ArrayList<EAListResponse.EffectiveAction> eaList;
    private boolean isEdit;
    private boolean isAdded;
    private Users user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_effectiveaction_list);

        user = getIntent().getParcelableExtra("USER");
        appPref = new AppPrefrences(EffectiveActionListActivity.this);
        initiate();
        isEdit = getIntent().getBooleanExtra(getString(R.string.is_editting), false);
        topheight = tvChoosePredefinedList.getMeasuredHeight() + toolbar.getMeasuredHeight() + btnCreateEffectiveAction.getMeasuredHeight();
        btnCreateEffectiveAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etEffectiveAction.requestFocus();
                if (etEffectiveAction != null) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.toggleSoftInputFromWindow(etEffectiveAction.getWindowToken(), InputMethodManager.SHOW_FORCED, 0);
                }
                relOverlay.setVisibility(View.VISIBLE);


            }
        });

        relOverlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (etEffectiveAction != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etEffectiveAction.getWindowToken(), 0);
                }
                etEffectiveAction.setText("");
                relOverlay.setVisibility(View.GONE);
            }
        });

        btnAddEffectiveAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etEffectiveAction != null) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etEffectiveAction.getWindowToken(), 0);
                }

                if (TextUtils.isEmpty(etEffectiveAction.getText().toString())) {

                    Utils.showResMsg(etEffectiveAction, getString(R.string.invalid_ea));
                    return;

                }
                addEA();

            }
        });

        etEffectiveAction.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tvEaCount.setText(Utils.EA_CHAR_LIMIT - etEffectiveAction.getText().length() + " " + getString(R.string.left));
            }
            @Override
            public void afterTextChanged(Editable s) {


            }
        });
    }

    private void fillData() {

        if (currentPage == 1) {
            mAdapter = new EffectiveActionListAdapter(EffectiveActionListActivity.this, eaList, rvEffectiveActionlist);
            rvEffectiveActionlist.setAdapter(mAdapter);
            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != pageCount) {

                        eaList.add(null);
                        mAdapter.notifyItemInserted(eaList.size() - 1);

                        currentPage++;
                        getList();

                        System.out.println("load");
                    }
                }
            });

            mAdapter.setListener(new EffectiveActionListAdapter.EffectiveActionListener() {
                @Override
                public void onAction(int position) {


                    Intent effectiveActionIntent = new Intent(EffectiveActionListActivity.this, AssignCommentActivity.class);
                    Utils.selectedEA = eaList.get(position);
                    effectiveActionIntent.putExtra(getString(R.string.is_editting), false);
                    effectiveActionIntent.putExtra("USER", user);
                    startActivity(effectiveActionIntent);

                }
            });

        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();


        }


    }

    private void initiate() {

        rvEffectiveActionlist = (RecyclerView) findViewById(R.id.rvEffectiveActionlist);
        btnCreateEffectiveAction = (Button) findViewById(R.id.btnCreateEffectiveAction);
        tvChoosePredefinedList = (TextView) findViewById(R.id.txtChoosePredefinedList);
        relOverlay = (RelativeLayout) findViewById(R.id.relOverlay);
        etEffectiveAction = (EditText) findViewById(R.id.edtEffectiveAction);
        tvEaCount = (TextView) findViewById(R.id.char_left);
        btntooldone = (Button) findViewById(R.id.toolBtnDone);
        btnAddEffectiveAction = (Button) findViewById(R.id.btnAddEffectiveAction);
        tvEaCount.setText(Utils.EA_CHAR_LIMIT + " " + getString(R.string.left));
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btntooldone.setVisibility(View.GONE);

        Utils.applyFontForToolbarTitle(EffectiveActionListActivity.this, toolbar);
        Utils.setTextStyle(btnCreateEffectiveAction, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(tvChoosePredefinedList, getApplication(), Utils.LIGHT);
        Utils.setTextStyle(etEffectiveAction, getApplication(), Utils.ITALIC);
        Utils.setTextStyle(btnAddEffectiveAction, getApplication(), Utils.MEDIUM);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getList();

        mLayoutManager = new LinearLayoutManager(this);
        rvEffectiveActionlist.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    private void getList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(EffectiveActionListActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<EAListResponse> ip = service.getEAList(getEAListPayload());

        ip.enqueue(new ErrorCallback.MyCallback<EAListResponse>() {
            @Override
            public void success(final Response<EAListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getEffectiveActions().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            etEffectiveAction.setText("");
                            if (currentPage == 1) {
                                eaList = response.body().getResult().getEffectiveActions();
                            } else if (currentPage > 1) {

                                eaList.remove(eaList.size() - 1);
                                mAdapter.notifyItemRemoved(eaList.size());

                                eaList.addAll(response.body().getResult().getEffectiveActions());
                            }


                            fillData();
                        }
                    });

                }


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }
        }, EffectiveActionListActivity.this, currentPage == 1);

    }

    private void addEA() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(EffectiveActionListActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<EAListResponse> ip = service.addCustomEA(getCustomEAPayload());

        ip.enqueue(new ErrorCallback.MyCallback<EAListResponse>() {
            @Override
            public void success(final Response<EAListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                if (response.body().getResult() != null && response.body().getResult().getEffectiveActions().size() > 0) {

                    currentPage = 1;// response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            relOverlay.setVisibility(View.GONE);
                            etEffectiveAction.setText("");
                            if (currentPage == 1) {
                                if (eaList != null)
                                    eaList.clear();
                                eaList = response.body().getResult().getEffectiveActions();
                                isAdded = true;
                            }
                            fillData();
                        }
                    });
                }

            }
            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        relOverlay.setVisibility(View.GONE);
                    }
                });
            }
        }, EffectiveActionListActivity.this, true);
    }

    private EAListReq getEAListPayload() {
        EAListReq req = new EAListReq();
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId() + "");
        req.setDesignationId(user.getDesignationId());
        req.setPageNo(currentPage);
        req.setDeviceId(Utils.getDeviceId(EffectiveActionListActivity.this));
        return req;
    }

    private CustomEAReq getCustomEAPayload() {
        CustomEAReq req = new CustomEAReq();
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId() + "");
        req.setDesignationId(user.getDesignationId());
        req.setEffectiveActionName(etEffectiveAction.getText().toString());
        req.setDeviceId(Utils.getDeviceId(EffectiveActionListActivity.this));
        return req;
    }

    private BroadcastReceiver finishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                EffectiveActionListActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(finishActivity, new IntentFilter(Utils.FINISH_SUB_LIST));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

}
