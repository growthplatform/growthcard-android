package com.appster.growthcard.ui.activities;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.adapter.NavigationDrawerAdapter;
import com.appster.growthcard.model.NavItems;
import com.appster.growthcard.ui.fragments.ManagerDashFragment;
import com.appster.growthcard.utils.SlidingLayer;

import java.util.ArrayList;


public class SampleMainActivity extends RootActivity implements View.OnTouchListener {

    private SlidingLayer mSlidingLayer, slidingLayer2;
    private TextView swipeText;
    private Button buttonClose2;
    private Button buttonClose;
    private ArrayList<NavItems> mNavItems = new ArrayList<>();
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout mDrawerPane;
    private LinearLayoutManager mLayoutManager;
    private NavigationDrawerAdapter adapter;
    private String[] nav_array;
    private ActionBarDrawerToggle mDrawerToggle;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_main);
        initUI();
        setupData();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonClose:
                mSlidingLayer.closeLayer(true);
                break;
            case R.id.buttonClose2:
                slidingLayer2.closeLayer(true);
        }
    }


    protected void initUI() {
        mSlidingLayer = (SlidingLayer) findViewById(R.id.slidingLayer1);
        slidingLayer2 = (SlidingLayer) findViewById(R.id.slidingLayer2);
        buttonClose2 = (Button) findViewById(R.id.buttonClose2);
        buttonClose = (Button) findViewById(R.id.buttonClose);
        swipeText = (TextView) findViewById(R.id.swipeText);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout1);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);
        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.slider);
        mDrawerList = (ListView) findViewById(R.id.navList);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    protected void setupData() {

        nav_array = getResources().getStringArray(R.array.subordinate_navigation_menu);
        for (int i = 0; i < nav_array.length; i++) {
            mNavItems.add(new NavItems(nav_array[i]));
        }
        adapter = new NavigationDrawerAdapter(mNavItems, SampleMainActivity.this);
        mDrawerList.setAdapter(adapter);
        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.menu_profile_header, mDrawerList, false);
        mDrawerList.addHeaderView(header, null, false);
        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                selectItemFromDrawer(position);
            }


        });
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerToggle = new ActionBarDrawerToggle(SampleMainActivity.this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        mSlidingLayer.setVisibility(View.GONE);
        slidingLayer2.setVisibility(View.GONE);

        mSlidingLayer.setChangeStateOnTap(false);
        mSlidingLayer.setStickTo(SlidingLayer.STICK_TO_LEFT);
        slidingLayer2.setStickTo(SlidingLayer.STICK_TO_RIGHT);
        mSlidingLayer.setOffsetDistance(48);
        slidingLayer2.setOffsetDistance(48);
        slidingLayer2.setChangeStateOnTap(false);

        slidingLayer2.setOnTouchListener(this);
        mSlidingLayer.setOnTouchListener(this);

        mSlidingLayer.setOnInteractListener(new SlidingLayer.OnInteractListener() {
            @Override
            public void onOpen() {
                Log.d("mSlidingLayer ", "onopen");
                slidingLayer2.setVisibility(View.GONE);
                mSlidingLayer.setVisibility(View.VISIBLE);
                slidingLayer2.closeLayer(true);
            }
            @Override
            public void onShowPreview() {
                Log.d("mSlidingLayer ", "show preview");
            }
            @Override
            public void onClose() {
                Log.d("mSlidingLayer ", "onclose");
                slidingLayer2.setVisibility(View.VISIBLE);
                mSlidingLayer.setVisibility(View.VISIBLE);


            }

            public void onOpened() {
                Log.d("mSlidingLayer ", "onopened");


            }

            @Override
            public void onPreviewShowed() {
                Log.d("mSlidingLayer ", "onpreviewshowed");
            }

            @Override
            public void onClosed() {
                Log.d("mSlidingLayer ", "onclosed");
                //slidingLayer2.setSlidingEnabled(true);


            }

            @Override
            public void onMoved(float percentMoved) {

            }
        });

        slidingLayer2.setOnInteractListener(new SlidingLayer.OnInteractListener() {
            @Override
            public void onOpen() {
                Log.d("slidingLayer2 ", "onopen");
                mSlidingLayer.setVisibility(View.GONE);
                slidingLayer2.setVisibility(View.VISIBLE);
                mSlidingLayer.closeLayer(true);
            }

            @Override
            public void onShowPreview() {
                Log.d("slidingLayer2 ", "show preview");
            }

            @Override
            public void onClose() {

                Log.d("slidingLayer2 ", "onclose");
                mSlidingLayer.setVisibility(View.VISIBLE);
                slidingLayer2.setVisibility(View.VISIBLE);
            }

            @Override
            public void onOpened() {
                Log.d("slidingLayer2 ", "onopened");
            }

            @Override
            public void onPreviewShowed() {
                Log.d("slidingLayer2 ", "onpreviewshowed");
            }

            @Override
            public void onClosed() {
                Log.d("slidingLayer2 ", "onclosed");
            }

            @Override
            public void onMoved(float percentMoved) {

            }
        });

        buttonClose.setOnClickListener(this);
        buttonClose2.setOnClickListener(this);


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if (v.getId() == R.id.slidingLayer2) {
            mSlidingLayer.setSlidingEnabled(false);
            slidingLayer2.setSlidingEnabled(true);
        } else if (v.getId() == R.id.slidingLayer1) {
            slidingLayer2.setSlidingEnabled(false);
            mSlidingLayer.setSlidingEnabled(true);
        }
        return true;
    }

    public void selectItemFromDrawer(int position) {
        Fragment fragment = new ManagerDashFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle
        // If it returns true, then it has handled
        // the nav drawer indicator touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;

        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }
}
