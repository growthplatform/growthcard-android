package com.appster.growthcard.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.appster.growthcard.R;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.adapter.PostListAdapter;
import com.appster.growthcard.adapter.SubPastEAAdapter;
import com.appster.growthcard.adapter.TeamMemberAdapter;
import com.appster.growthcard.model.Graph;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.PastEAListReq;
import com.appster.growthcard.network.requests.PostListReq;
import com.appster.growthcard.network.requests.SubProfileReq;
import com.appster.growthcard.network.response.PastEAListResponse;
import com.appster.growthcard.network.response.PostListResponse;
import com.appster.growthcard.network.response.TeamMemberResponse;
import com.appster.growthcard.ui.activities.ManagerMainActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.CustomlayoutManager;
import com.appster.growthcard.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by navdeep on 16/03/16.
 */
public class TeamMemberFragment extends Fragment implements ProfileFragment.offsetChangedListener, View.OnClickListener {

    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.6f;
    private static final int ALPHA_ANIMATIONS_DURATION = 1000;
    private AppPrefrences appPref;
    private static final String USER = "user";
    private static final String GRAPH = "graph";
    private static final String WORKDAYS = "workdays";
    private static final String ARG_PARAM2 = "param2";
    private Users user;
    private ArrayList<Graph> graphVO;
    private RecyclerView feedRecyclerView;
    private RecyclerView teamRecyclerView;
    private int pageNo = 1;
    private int totalPage = 1;
    private Retrofit retrofit;
    private List<Users> teamMembers = new ArrayList<>();
    private TeamMemberAdapter mAdapter;
    private PostListAdapter mPostAdapter;
    private ProgressBar progressBar;
    private TextView teamMembersTextView;
    private TextView teamMembersEmpty;
    float x1 = 0, x2 = 0, y1 = 0, y2 = 0, dx = 0, dy = 0;
    String direction = "";
    private ViewFlipper viewFlipper;
    private TextView userName, userDesignation;
    private ImageView userImage;
    private LinearLayout graphParent;
    private TextView workdays;
    private View sepHead;
    private View sepFoot;
    private String userWorkdays;
    private TextView eaHeader;
    private RecyclerView pastEaRecyclerView;
    private boolean mIsTheViewsVisible;
    private RelativeLayout teamMemberLayout;
    private RelativeLayout profileParent;
    private RelativeLayout graphBehindView;
    private SubPastEAAdapter mPastAdapter;
    private ArrayList<PastEAListResponse.PastEffectiveAction> eaList;
    private int currentPage = 1;
    private int pageCount = 0;
    private ArrayList postData;
    private RelativeLayout teamMemberParent;


    public static TeamMemberFragment newInstance(Users param1, ArrayList<Graph> param2, String workdays) {
        TeamMemberFragment fragment = new TeamMemberFragment();
        Bundle args = new Bundle();
        args.putParcelable(USER, param1);
        args.putParcelableArrayList(GRAPH, param2);
        args.putString(WORKDAYS, workdays);
        fragment.setArguments(args);
        return fragment;
    }

    public TeamMemberFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            user = getArguments().getParcelable(USER);
            graphVO = getArguments().getParcelableArrayList(GRAPH);
            userWorkdays = getArguments().getString(WORKDAYS);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_team_mem, null);

        appPref = new AppPrefrences(getActivity());
        findViews(view);
        if (getActivity() instanceof SubMainActivity || getActivity() instanceof ManagerMainActivity) {
            if (appPref.getTeamId() != 0)
                getTeamMembers();
            else {
                progressBar.setVisibility(View.INVISIBLE);
                teamMembersEmpty.setVisibility(View.VISIBLE);
                teamMembersEmpty.setText(R.string.no_team_assigned);
            }
        } else {
            getTeamMembers();
        }

        hitApi(2);
        return view;
    }

    public void findViews(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        viewFlipper = (ViewFlipper) view.findViewById(R.id.pager);
        viewFlipper.setDisplayedChild(0);
        teamMembersTextView = (TextView) view.findViewById(R.id.team_members);
        teamMembersEmpty = (TextView) view.findViewById(R.id.team_members_empty);
        userName = (TextView) view.findViewById(R.id.name);
        userDesignation = (TextView) view.findViewById(R.id.designation);
        userImage = (ImageView) view.findViewById(R.id.user_graph_image);
        graphParent = (LinearLayout) view.findViewById(R.id.current_ea_parent);
        workdays = (TextView) view.findViewById(R.id.workdays);
        sepHead = (View) view.findViewById(R.id.sep_head);
        sepFoot = (View) view.findViewById(R.id.sep_foot);
        teamMemberLayout = (RelativeLayout) view.findViewById(R.id.team_member_layout);
        profileParent = (RelativeLayout) view.findViewById(R.id.profile_name_parent);
        sepFoot = (View) view.findViewById(R.id.sep_foot);
        graphBehindView = (RelativeLayout) view.findViewById(R.id.behindView);
        teamMemberParent = (RelativeLayout) view.findViewById(R.id.team_members_parent);

        feedRecyclerView = (RecyclerView) view.findViewById(R.id.feedList);
        teamRecyclerView = (RecyclerView) view.findViewById(R.id.team_members_list);
        pastEaRecyclerView = (RecyclerView) view.findViewById(R.id.past_ea_list);

        workdays.setVisibility(View.GONE);
        sepFoot.setVisibility(View.GONE);
        sepHead.setVisibility(View.GONE);

        graphParent.setOnClickListener(this);


        Utils.setTextStyle(userName,

                getActivity(), Utils

                        .MEDIUM);
        Utils.setTextStyle(userDesignation,

                getActivity(), Utils

                        .REGULAR);
        Utils.setTextStyle(teamMembersTextView,

                getActivity(), Utils

                        .MEDIUM);
        Utils.setTextStyle(teamMembersEmpty,

                getActivity(), Utils

                        .REGULAR);
        Utils.setTextStyle(workdays,

                getActivity(), Utils

                        .REGULAR);


        if (user != null)

        {
            userName.setText(user.getUser().getFirstName() + " " + user.getUser().getLastName());

            if (user.getDesignation() != null)
                userDesignation.setText(user.getDesignation().getDesignationTitle());

            Utils.loadUrlImage(user.getUser().getProfileImage(), userImage);
        }

        if (graphVO != null)
            Utils.setChart(

                    getActivity(), graphParent, graphVO, appPref

                            .

                                    getRole()

                            == 2);

        displayWorkDays();


        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) teamRecyclerView.getLayoutParams();
        // calculate height of RecyclerView based on child count
        params.height = Utils.dpToPx(getActivity(), 150);

        // set height of RecyclerView
        teamRecyclerView.setLayoutParams(params);
     //   teamRecyclerView.setNestedScrollingEnabled(false);
//        teamRecyclerView.requestDisallowInterceptTouchEvent(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        feedRecyclerView.setLayoutManager(linearLayoutManager);
        feedRecyclerView.setNestedScrollingEnabled(false);



    }

    public void handleFragmentsView(float percentage) {

        try {
            if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
                if (mIsTheViewsVisible) {
                    startAlphaAnimation(teamMemberLayout, ALPHA_ANIMATIONS_DURATION, View.GONE);
                    startAlphaAnimation(profileParent, ALPHA_ANIMATIONS_DURATION, View.GONE);
                    startAlphaAnimation(graphParent, ALPHA_ANIMATIONS_DURATION, View.GONE);
                    mIsTheViewsVisible = false;
                }

            } else {

                if (!mIsTheViewsVisible) {
                    startAlphaAnimation(teamMemberLayout, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                    startAlphaAnimation(profileParent, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                    startAlphaAnimation(graphParent, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                    mIsTheViewsVisible = true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startAlphaAnimation(final View v, long duration, final int visibility) {
        Animation alphaAnimation = (visibility == View.VISIBLE)
                ? AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_down)
                : AnimationUtils.loadAnimation(getActivity().getApplicationContext(), R.anim.slide_up);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (visibility == View.GONE)
                    v.setVisibility(View.GONE);
                else
                    v.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    public void displayWorkDays() {

        if (userWorkdays != null) {
            String workDayStr = "";
            for (int i = 0; i < userWorkdays.length(); i++) {
                if (userWorkdays.charAt(i) == '1') {

                    if (workDayStr.equals(""))
                        workDayStr = Utils.days[i].substring(0, 3);
                    else
                        workDayStr = workDayStr + "  " + Utils.days[i].substring(0, 3);
                }

            }

            workdays.setText(workDayStr);
            workdays.setVisibility(View.VISIBLE);
            sepFoot.setVisibility(View.VISIBLE);
            sepHead.setVisibility(View.VISIBLE);
        }
    }

    public void toggleList(int position) {

        viewFlipper.setDisplayedChild(position);
        if (eaList != null) {

        } else {
            if (position == 1)
                getPastEAList();

        }

    }

    private void getTeamMembers() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity()))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<TeamMemberResponse> ip = service.getTeamMembers(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<TeamMemberResponse>() {
            @Override
            public void success(final Response<TeamMemberResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());


                if (response.body().getResult() != null &&  response.body().getResult().getResult().getMembers().size() > 0 ) {

                    pageNo = response.body().getResult().getResult().getPageNo();
                    totalPage = response.body().getResult().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);
                            teamRecyclerView.setVisibility(View.VISIBLE);
                            if (pageNo == 1) {
                                teamMembers = response.body().getResult().getResult().getMembers();
                            } else if (pageNo > 1) {

                                teamMembers.remove(teamMembers.size() - 1);
                                mAdapter.notifyItemRemoved(teamMembers.size());

                                teamMembers.addAll(response.body().getResult().getResult().getMembers());
                            }


                            fillData();
                        }
                    });

                } else if (teamMembers.size() == 0) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressBar.setVisibility(View.GONE);

                            teamRecyclerView.setVisibility(View.GONE);

                            teamMembersEmpty.setVisibility(View.VISIBLE);
                        }
                    });
                }



            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        progressBar.setVisibility(View.GONE);
                        if (teamMembers.size() == 0) {
                            teamRecyclerView.setVisibility(View.GONE);

                            teamMembersEmpty.setVisibility(View.VISIBLE);
                        }


                    }
                });

            }


        }, getActivity(), false);

    }

    private SubProfileReq getPayload() {

        SubProfileReq req = new SubProfileReq();
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());

        req.setCompanyId(appPref.getCompanyId());

        if (getActivity() instanceof SubMainActivity || getActivity() instanceof ManagerMainActivity)
            req.setSubordinateId(appPref.getUserId());
        else
            req.setSubordinateId(user.getUser().getUserId());

        if (getActivity() instanceof SubMainActivity || getActivity() instanceof ManagerMainActivity)
            req.setTeamId(appPref.getTeamId());
        else
            req.setTeamId(user.getTeamId());

        req.setPageNo(pageNo);


        return req;

    }


    private void fillData() {


        teamMembersTextView.setText(getString(R.string.team_members) + " (" + teamMembers.size() + ")");
        if (teamMembers.size() == 0) {

            teamMembersEmpty.setVisibility(View.VISIBLE);
            teamRecyclerView.setVisibility(View.GONE);
            return;
        }
        if (pageNo == 1) {

            CustomlayoutManager layoutManager
                    = new CustomlayoutManager(getActivity(), CustomlayoutManager.HORIZONTAL, false);

            teamRecyclerView.setLayoutManager(layoutManager);
            mAdapter = new TeamMemberAdapter(teamMembers, getActivity(), teamRecyclerView);
            teamRecyclerView.setAdapter(mAdapter);

            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (pageNo != totalPage) {

                        teamMembers.add(null);
                        mAdapter.notifyItemInserted(teamMembers.size() - 1);

                        pageNo++;
                        getTeamMembers();

                        System.out.println("load");
                    }
                }
            });


        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();


        }


    }

    @Override
    public void onOffsetChanged(float percent) {
    }


    private int eaCurrentPage = 1, eaPageCount = 1;

    private void getPastEAList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<PastEAListResponse> ip = service.getPastEAList(getPastEAListPayload());

        ip.enqueue(new ErrorCallback.MyCallback<PastEAListResponse>() {
            @Override
            public void success(final Response<PastEAListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getPastEffectiveAction().size() > 0) {

                    eaCurrentPage = response.body().getResult().getPageNo();
                    eaPageCount = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (eaCurrentPage == 1) {
                                eaList = response.body().getResult().getPastEffectiveAction();
                            } else if (eaCurrentPage > 1) {

                                eaList.remove(eaList.size() - 1);
                                mPastAdapter.notifyItemRemoved(eaList.size());

                                eaList.addAll(response.body().getResult().getPastEffectiveAction());
                            }


                            fillPastEAData();
                        }
                    });

                }


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), eaCurrentPage == 1);


    }

    private PastEAListReq getPastEAListPayload() {
        PastEAListReq req = new PastEAListReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setSubordinateId(user.getUser().getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setPageNo(eaCurrentPage);
        req.setFlag(2);
        return req;
    }


    private void fillPastEAData() {


        if (eaCurrentPage == 1) {

            mPastAdapter = new SubPastEAAdapter(getActivity(), pastEaRecyclerView, eaList, graphBehindView, user);
            pastEaRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            pastEaRecyclerView.setAdapter(mPastAdapter);

            mPastAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (eaCurrentPage != eaPageCount) {

                        eaList.add(null);
                        mPastAdapter.notifyItemInserted(eaList.size() - 1);

                        eaCurrentPage++;
                        getPastEAList();

                        System.out.println("load");
                    }
                }
            });


        } else if (mPastAdapter != null) {

            mPastAdapter.notifyDataSetChanged();
            mPastAdapter.setLoaded();


        }

    }

    private void hitApi(int flag) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);

        ErrorCallback.MyCall<PostListResponse> ip = service.getpost(getPayload(flag));

        ip.enqueue(new ErrorCallback.MyCallback<PostListResponse>() {
            @Override
            public void success(final Response<PostListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getPost().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                postData = (ArrayList) response.body().getResult().getPost();
                            } else if (currentPage > 1) {

                                postData.remove(postData.size() - 1);
                                mPostAdapter.notifyItemRemoved(postData.size());

                                postData.addAll(response.body().getResult().getPost());
                            }


                            fillPostData();
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), currentPage == 1);


    }

    private void fillPostData() {

        if (currentPage == 1) {


            mPostAdapter = new PostListAdapter(postData, feedRecyclerView, getActivity(), 0, teamMemberLayout);
            feedRecyclerView.setAdapter(mPostAdapter);


            mPostAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != pageCount) {

                        postData.add(null);
                        mPostAdapter.notifyItemInserted(postData.size() - 1);

                        currentPage++;
                        hitApi(2);

                        System.out.println("load");
                    }
                }
            });


            //}

        } else if (mPostAdapter != null) {

            mPostAdapter.notifyDataSetChanged();
            mPostAdapter.setLoaded();


        }


    }


    public PostListReq getPayload(int flag) {


        PostListReq req = new PostListReq();
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setSubordinateId(user != null ? user.getUser().getUserId() : appPref.getUserId());
        req.setCompanyId(appPref.getCompanyId());
        req.setPageNo(currentPage);
        req.setFlag(flag);


        return req;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.current_ea_parent:
//                Intent i=new Intent(getActivity(), EADetailsActivity.class);
//                i.putExtra("isCurrent",true);
//                i.putExtra("EAid",user.getEffectiveActionId());
//                i.putExtra("USER",user.getUser().getUserId());
//                getActivity().startActivity(i);

                break;
        }
    }
}
