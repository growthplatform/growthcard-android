package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;

import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.network.requests.TermsReq;
import com.appster.growthcard.network.response.TermsResponse;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TermsActivity extends RootActivity {

    private Toolbar toolbar;
    private WebView webView;
    private Button btnAccept;
    private Button btnDecline;
    private boolean mFromSetting = false;
    private LinearLayout llAcceptDecline;
    private AppPrefrences appPref;
    private Retrofit retrofit;
    private static final String FROM_SPLASH = "from_splash";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);
        appPref = new AppPrefrences(TermsActivity.this);

        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(TermsActivity.this))
                .build();


        initUI();
        setupData();


    }

    protected void initUI() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        webView = (WebView) findViewById(R.id.termsWebView);
        btnAccept = (Button) findViewById(R.id.positive_btn);
        btnDecline = (Button) findViewById(R.id.negative_btn);
        llAcceptDecline = (LinearLayout) findViewById(R.id.buttonPanel);
        llAcceptDecline.setVisibility(View.GONE);
        Utils.setTextStyleButton(btnAccept, TermsActivity.this, Utils.MEDIUM);
        Utils.setTextStyleButton(btnDecline, TermsActivity.this, Utils.MEDIUM);

        mFromSetting = getIntent().getBooleanExtra("FROM_SETTING", false);

        if (mFromSetting) {
            llAcceptDecline.setVisibility(View.GONE);
        }

        btnAccept.setOnClickListener(this);
        btnDecline.setOnClickListener(this);

    }

    protected void setupData() {

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        getTerms();

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.positive_btn:
                acceptTerms();
                break;

            case R.id.negative_btn:

                if (getIntent().getBooleanExtra(FROM_SPLASH, false)) {
                    Intent i = new Intent(TermsActivity.this, LoginActivity.class);
                    startActivity(i);
                }
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {

        if (getIntent().getBooleanExtra(FROM_SPLASH, false)) {
            Intent i = new Intent(TermsActivity.this, LoginActivity.class);
            startActivity(i);
        }
        finish();
    }

    private void getTerms() {


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<ResponseBody> ip = service.getTerms();
        ip.enqueue(new ErrorCallback.MyCallback<ResponseBody>() {
            @Override
            public void success(final Response<ResponseBody> response) {
                Log.d("Tag", "SUCCESS! " + response.body());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        webView.getSettings().setJavaScriptEnabled(true);
                        try {
                            webView.loadDataWithBaseURL("", response.body().string(), "text/html", "UTF-8", "");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, TermsActivity.this, true);

    }

    private void acceptTerms() {


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<TermsResponse> ip = service.updateTerms(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<TermsResponse>() {
            @Override
            public void success(final Response<TermsResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());

                appPref.setTermsAccepted(true);
                appPref.setLoginPath(getResources().getInteger(R.integer.WorkdaysPath));
//                startActivity(new Intent(TermsActivity.this, TutorialActivity.class));
                finish();


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, TermsActivity.this, true);

    }

    private TermsReq getPayload() {
        TermsReq termsReq = new TermsReq();
        termsReq.setDeviceId(Utils.getDeviceId(TermsActivity.this));
        termsReq.setUserId(appPref.getUserId());
        termsReq.setUserToken(appPref.getUserToken());


        return termsReq;

    }

}
