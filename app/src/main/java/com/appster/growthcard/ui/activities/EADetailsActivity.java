package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.appster.growthcard.R;
import com.appster.growthcard.adapter.DetailViewCommentAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.model.Comment;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.response.CommentListResponse;
import com.appster.growthcard.network.response.EADetailResponse;
import com.appster.growthcard.network.response.SendCommentsResponse;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class EADetailsActivity extends RootActivity {

    private RelativeLayout topLayout;
    private RecyclerView commentRecylclerView;
    private LinearLayoutManager mLayoutManager;
    private DetailViewCommentAdapter mDetailViewCommentAdapter;
    private Button btnTooldone;
    private AppPrefrences appPref;
    private RelativeLayout commentBox;
    private EditText etComment;
    private Button btnSendComment;
    private ArrayList<Comment> commentList;
    private int commentCurrentPage = 1;
    private int commentPageCount = 1;
    private int mActionBarSize;
    private int eaId;
    private int userId;
    private EADetailResponse.Result result;
    private boolean flag;
    private int teamId;
    private String startDate;
    private String endDate;
    private int pastEAid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eadetails);

        eaId = getIntent().getIntExtra("EAid", 0);
        flag = getIntent().getBooleanExtra("isCurrent", false);
        userId = getIntent().getIntExtra("USER", 0);
        pastEAid = getIntent().getIntExtra("pastEAid", 0);
        appPref = new AppPrefrences(EADetailsActivity.this);
        pastEAid = getIntent().getIntExtra("pastEAid", 0);


        findViews();
        getEADetails();
    }

    private void findViews() {
        commentRecylclerView = (RecyclerView) findViewById(R.id.comment_list);
        topLayout = (RelativeLayout) findViewById(R.id.top_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btnTooldone = (Button) findViewById(R.id.toolBtnDone);
        btnTooldone.setVisibility(View.INVISIBLE);
        setSupportActionBar(toolbar);

        commentBox = (RelativeLayout) findViewById(R.id.comment_box);
        etComment = (EditText) findViewById(R.id.comment_edit);
        btnSendComment = (Button) findViewById(R.id.send_comment);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Utils.setTextStyle(etComment, EADetailsActivity.this, Utils.REGULAR);
        Utils.setTextStyle(btnSendComment, EADetailsActivity.this, Utils.MEDIUM);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.getBackground().setAlpha(0);

        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (etComment.getText().toString().trim().length() > 0) {
                    btnSendComment.setTextColor(ContextCompat.getColor(EADetailsActivity.this, R.color.button_green));
                    btnSendComment.setAlpha(1);

                } else {
                    btnSendComment.setTextColor(ContextCompat.getColor(EADetailsActivity.this, R.color.text_workdays));
                    btnSendComment.setAlpha(0.3f);


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        btnSendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(etComment.getText().toString().trim()))
                    sendComment();
            }
        });

        if (userId == appPref.getUserId() && eaId == appPref.getEAId() && flag) {
                commentBox.setVisibility(View.VISIBLE);
          } else {
            commentBox.setVisibility(View.GONE);

        }


    }


    private SendCommentsReq getSendCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(EADetailsActivity.this));
        req.setSubordinateId(userId);
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(eaId);
        req.setContent(etComment.getText().toString());
        req.setCompnayId(appPref.getCompanyId());
        req.setPastEffectiveActionId(pastEAid);

        return req;
    }

    private void sendComment() {

        if (appPref.getEAId() != eaId) {

            new SweetAlertDialog(EADetailsActivity.this, SweetAlertDialog.NORMAL_TYPE)
                    .setTitleText("Alert!")
                    .setContentText("Manager has assigned you a new Effective Action!")
                    .setConfirmText("OK")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            EADetailsActivity.this.finish();
                        }
                    })
                    .show();

            return;
        }

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(EADetailsActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SendCommentsResponse> ip = service.sendComment(getSendCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SendCommentsResponse>()

                   {
                       @Override
                       public void success(final Response<SendCommentsResponse> response) {
                           Log.d("Tag", "SUCCESS! " + response.message());

                           if (response.body().getResult() != null) {


                               runOnUiThread(new Runnable() {
                                   @Override
                                   public void run() {
                                       if (commentList != null && commentList.size() != 0) {
                                           commentList.add(0, response.body().getResult());

                                       } else {
                                           commentCurrentPage = 1;
                                           commentPageCount = 1;
                                           commentList = new ArrayList<Comment>();
                                           commentList.add(0, response.body().getResult());

                                       }

                                       etComment.setText("");
                                       fillCommentData(false);
                                   }
                               });

                           }

                       }

                       @Override
                       public void error(String errorMessage) {
                           Log.d("Tag", "Error! " + errorMessage);

                       }


                   }

                , EADetailsActivity.this, true);


    }

    private void getCommentList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(EADetailsActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<CommentListResponse> ip = service.getCommentsList(getCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<CommentListResponse>() {
            @Override
            public void success(final Response<CommentListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getComment().size() > 0) {

                    commentCurrentPage = response.body().getResult().getPageNo();
                    commentPageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (commentCurrentPage == 1) {
                                commentList = response.body().getResult().getComment();
                            } else if (commentCurrentPage > 1) {

                                commentList.remove(commentList.size() - 1);
                                mDetailViewCommentAdapter.notifyItemRemoved(commentList.size());

                                commentList.addAll(response.body().getResult().getComment());
                            }


                            fillCommentData(false);
                        }
                    });

                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLayoutManager = new LinearLayoutManager(EADetailsActivity.this);
                            commentRecylclerView.setLayoutManager(mLayoutManager);
                            commentList.add(null);
                            mDetailViewCommentAdapter = new DetailViewCommentAdapter(EADetailsActivity.this, commentList, commentRecylclerView, true, result);
                            commentRecylclerView.setAdapter(mDetailViewCommentAdapter);
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, EADetailsActivity.this, true);


    }

    private SendCommentsReq getCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(EADetailsActivity.this));
        req.setSubordinateId(userId);
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(eaId);
        req.setPageNo(commentCurrentPage);
        req.setPastEffectiveActionId(pastEAid);

        return req;
    }


    private void fillCommentData(boolean isUpdated) {

        if (commentList.size() == 0) {
            mLayoutManager = new LinearLayoutManager(EADetailsActivity.this);
            commentRecylclerView.setLayoutManager(mLayoutManager);

            mDetailViewCommentAdapter = new DetailViewCommentAdapter(EADetailsActivity.this, commentList, commentRecylclerView, true, result);
            commentRecylclerView.setAdapter(mDetailViewCommentAdapter);


            return;

        }


        if (commentCurrentPage == 1) {

            mLayoutManager = new LinearLayoutManager(EADetailsActivity.this);
            commentRecylclerView.setLayoutManager(mLayoutManager);

            mDetailViewCommentAdapter = new DetailViewCommentAdapter(EADetailsActivity.this, commentList, commentRecylclerView, true, result);
            commentRecylclerView.setAdapter(mDetailViewCommentAdapter);

//            final TypedArray styledAttributes = getTheme().obtainStyledAttributes(
//                    new int[]{android.R.attr.actionBarSize});
//            mActionBarSize = (int) styledAttributes.getDimension(0, 0);
//            styledAttributes.recycle();


            mDetailViewCommentAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (commentCurrentPage != commentPageCount) {

                        commentList.add(null);
                        mDetailViewCommentAdapter.notifyItemInserted(commentList.size() - 1);

                        commentCurrentPage++;
                        getCommentList();

                        System.out.println("load");
                    }
                }
            });

            mLayoutManager.scrollToPositionWithOffset(0, 20);

        } else if (mDetailViewCommentAdapter != null) {

            mDetailViewCommentAdapter.notifyDataSetChanged();
            mDetailViewCommentAdapter.setLoaded();


        }


//        if (isUpdated) {
//            commentRecylclerView.smoothScrollToPosition(commentList.size() - 1);
////            commentRecylclerView.getLayoutManager().smoothScrollToPosition(commentRecylclerView, null, mDetailViewCommentAdapter.getItemCount() - 1);
//        }


    }


    private void getEADetails() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(EADetailsActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<EADetailResponse> ip = service.getEADetails(getEADetailPayload());

        ip.enqueue(new ErrorCallback.MyCallback<EADetailResponse>() {
            @Override
            public void success(final Response<EADetailResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {


                        if (getIntent().getBooleanExtra("NEW_EA", false)) {
                            appPref.setEATitle(response.body().getResult().getEffectiveActionTitle());
                        }

                        result = response.body().getResult();
                        commentList = new ArrayList<Comment>();
                        //  fillCommentData(false);
                        getCommentList();

                    }


                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, EADetailsActivity.this, true);


    }

    private SendCommentsReq getEADetailPayload() {
        SendCommentsReq req = new SendCommentsReq();
        int getFlag=1;
        if(pastEAid>0){
            getFlag=2;
        }

        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(EADetailsActivity.this));
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(eaId);
        req.setFlag(getFlag);
        req.setSubordinateId(userId);
        req.setPastEffectiveActionId(pastEAid);

        return req;
    }


}
