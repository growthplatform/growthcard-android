package com.appster.growthcard.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appster.growthcard.R;
import com.appster.growthcard.ui.activities.NewAnnoucementActivity;
import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.adapter.AnnoucementListAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.AnnoucementListReq;
import com.appster.growthcard.network.response.AnnoucementListResponse;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AnnoucementListFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private RecyclerView recAnnoucement;
    private LinearLayoutManager mLayoutManager;
    private AnnoucementListAdapter mAdapter;
    private FloatingActionButton fab;
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private int currentPage = 1;
    private int pageCount = 1;
    private ArrayList<AnnoucementListResponse.Announcement> annoucementData = new ArrayList<AnnoucementListResponse.Announcement>();

    public AnnoucementListFragment() {
    }

    public static AnnoucementListFragment newInstance(String param1, String param2) {
        AnnoucementListFragment fragment = new AnnoucementListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        appPref = new AppPrefrences(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_annoucement_list, container, false);
        initilize(view);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        pageCount = 1;
        getList();
    }

    private void initilize(View view) {
        recAnnoucement = (RecyclerView) view.findViewById(R.id.recAnnoucement);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recAnnoucement.setLayoutManager(mLayoutManager);

        if (((RootActivity) getActivity()).getSupportActionBar() != null) {
            ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
            ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_announcements);
        }

        fab.setOnClickListener(this);

    }

    private void getList() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity()))
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<AnnoucementListResponse> ip = service.annoucementList(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<AnnoucementListResponse>() {
            @Override
            public void success(final Response<AnnoucementListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                if (response.body().getResult() != null && response.body().getResult().getAnnouncements().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                annoucementData = (ArrayList) response.body().getResult().getAnnouncements();
                            } else if (currentPage > 1) {

                                annoucementData.remove(annoucementData.size() - 1);
                                mAdapter.notifyItemRemoved(annoucementData.size());

                                annoucementData.addAll(response.body().getResult().getAnnouncements());
                            }
                            fillData();
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), currentPage == 1);


    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:

                Intent intent = new Intent(getActivity(), NewAnnoucementActivity.class);
                startActivity(intent);

                break;
        }
    }

    public AnnoucementListReq getPayload() {
        AnnoucementListReq req = new AnnoucementListReq();
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());
        req.setRole(appPref.getRole());
        req.setPageNo(currentPage);
        return req;
    }


    private void fillData() {

        if (currentPage == 1) {


            mAdapter = new AnnoucementListAdapter(getActivity(), recAnnoucement, annoucementData);
            recAnnoucement.setAdapter(mAdapter);


            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != pageCount) {

                        annoucementData.add(null);
                        mAdapter.notifyItemInserted(annoucementData.size() - 1);

                        currentPage++;
                        getList();

                        System.out.println("load");
                    }
                }
            });


            //}

        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();


        }


    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }


}
