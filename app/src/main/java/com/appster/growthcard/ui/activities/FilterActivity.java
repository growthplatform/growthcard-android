package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.adapter.FilterGridAdapter;
import com.appster.growthcard.ui.fragments.SubDashboardFragment;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.DepartmentReq;
import com.appster.growthcard.network.response.DepartmentResponse;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 31/03/16.
 */
public class FilterActivity extends AppCompatActivity implements View.OnClickListener {

    public static ArrayList<DepartmentResponse.Result> filterData = new ArrayList<>();
    private Button btnToolDone;
    private TextView btnAllUser;
    private TextView btnManagers;
    private TextView btnPeers;
    private TextView btnSubordinate;
    private RelativeLayout llSubordinate;
    private RelativeLayout llAllUser;
    private RelativeLayout llPeers;
    private RelativeLayout llManagers;
    private ImageView ivSubordinate;
    private ImageView ivPeers;
    private ImageView ivManagers;
    private ImageView ivAllUser;
    private GoogleApiClient client;
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private GridView gridview;
    private FilterGridAdapter adapter;
    private int selected = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_filter);
        appPref = new AppPrefrences(this);
        initialize();
        selected = SubDashboardFragment.flag1;
        selectStatic(SubDashboardFragment.flag1);
        if (filterData.size() == 0)
            getDepartment();
        else {
            for (DepartmentResponse.Result result : filterData) {
                if (result.getDepartmentId() == SubDashboardFragment.flag2) {
                    result.setSelected(true);
                } else {
                    result.setSelected(false);
                }
            }

            adapter = new FilterGridAdapter(FilterActivity.this, filterData);
            gridview.setAdapter(adapter);
        }
    }

    private void getDepartment() {

        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(FilterActivity.this))
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<DepartmentResponse> ip = service.getDepartment(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<DepartmentResponse>() {
            @Override
            public void success(final Response<DepartmentResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        filterData.add(getAllDepartmentObj());
                        filterData.addAll(response.body().getResult());
                        adapter = new FilterGridAdapter(FilterActivity.this, filterData);
                        gridview.setAdapter(adapter);
                    }
                });
            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }
        }, this, true);

    }

    private DepartmentResponse.Result getAllDepartmentObj() {
        DepartmentResponse.Result result = new DepartmentResponse.Result();
        result.setDepartmentId(0);
        result.setDepartmentName("All Departments");
        result.setSelected(true);
        return result;
    }

    private DepartmentReq getPayload() {
        DepartmentReq req = new DepartmentReq();
        req.setDeviceId(Utils.getDeviceId(FilterActivity.this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setCompanyId(appPref.getCompanyId());
        return req;
    }

    private void initialize() {
        btnToolDone = (Button) findViewById(R.id.toolBtnDone);
        btnAllUser = (TextView) findViewById(R.id.btnAllUser);
        btnManagers = (TextView) findViewById(R.id.btnManagers);
        btnPeers = (TextView) findViewById(R.id.btnPeers);
        btnSubordinate = (TextView) findViewById(R.id.btnSubordinate);

        llAllUser = (RelativeLayout) findViewById(R.id.llAllUser);
        llManagers = (RelativeLayout) findViewById(R.id.llManagers);
        llPeers = (RelativeLayout) findViewById(R.id.llPeers);
        llSubordinate = (RelativeLayout) findViewById(R.id.llSubordinate);

        Utils.setTextStyle(btnAllUser, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(btnManagers, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(btnPeers, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(btnPeers, getApplication(), Utils.MEDIUM);


        ivAllUser = (ImageView) findViewById(R.id.imgAllUser);
        ivManagers = (ImageView) findViewById(R.id.imgManagers);
        ivPeers = (ImageView) findViewById(R.id.imgPeers);
        ivSubordinate = (ImageView) findViewById(R.id.imgSubordinate);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        Utils.setTextStyle(btnToolDone, getApplication(), Utils.MEDIUM);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.profileBg));
        setSupportActionBar(toolbar);
        btnToolDone.setOnClickListener(this);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        btnToolDone.setText(R.string.apply);
        btnToolDone.setTextColor(ContextCompat.getColor(this, R.color.button_green));
        btnToolDone.setOnClickListener(this);
        gridview = (GridView) findViewById(R.id.gvDynanicSelect);

        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                for (int i = 0; i < filterData.size(); i++) {

                    if (i == position) {
                        filterData.get(i).setSelected(true);
                    } else {
                        filterData.get(i).setSelected(false);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        llAllUser.setOnClickListener(this);
        llManagers.setOnClickListener(this);
        llPeers.setOnClickListener(this);
        llSubordinate.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llAllUser:
                selected = Integer.parseInt(v.getTag().toString());
                selectStatic(selected);
                break;
            case R.id.llManagers:
                selected = Integer.parseInt(v.getTag().toString());
                selectStatic(selected);
                break;
            case R.id.llPeers:
                selected = Integer.parseInt(v.getTag().toString());
                selectStatic(selected);
                break;
            case R.id.llSubordinate:
                selected = Integer.parseInt(v.getTag().toString());
                selectStatic(selected);
                break;
            case R.id.toolBtnDone:
                int deptId = 0;
                for (DepartmentResponse.Result result : filterData) {
                    if (result.getSelected()) {
                        deptId = result.getDepartmentId();
                    }
                }
                SubDashboardFragment.isUpdated = SubDashboardFragment.flag1 != selected || SubDashboardFragment.flag2 != deptId;
                SubDashboardFragment.flag1 = selected;
                SubDashboardFragment.flag2 = deptId;

                finish();
                break;
        }
    }

    private void selectStatic(int selected) {
        switch (selected) {
            case 1:

                llAllUser.setBackgroundColor(ContextCompat.getColor(FilterActivity.this, R.color.button_green));
                btnAllUser.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.pure_white));
                ivAllUser.setVisibility(View.VISIBLE);
                ivManagers.setVisibility(View.GONE);
                ivPeers.setVisibility(View.GONE);
                ivSubordinate.setVisibility(View.GONE);
                llManagers.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnManagers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llPeers.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnPeers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llSubordinate.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnSubordinate.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));


                break;
            case 2:
                llManagers.setBackgroundColor(ContextCompat.getColor(FilterActivity.this, R.color.button_green));
                btnManagers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.pure_white));
                llAllUser.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnAllUser.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llPeers.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnPeers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llSubordinate.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnSubordinate.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                ivAllUser.setVisibility(View.GONE);
                ivManagers.setVisibility(View.VISIBLE);
                ivPeers.setVisibility(View.GONE);
                ivSubordinate.setVisibility(View.GONE);

                break;
            case 3:
                llPeers.setBackgroundColor(ContextCompat.getColor(FilterActivity.this, R.color.button_green));
                btnPeers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.pure_white));
                llAllUser.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnAllUser.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llManagers.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnManagers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llSubordinate.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnSubordinate.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                ivAllUser.setVisibility(View.GONE);
                ivManagers.setVisibility(View.GONE);
                ivPeers.setVisibility(View.VISIBLE);
                ivSubordinate.setVisibility(View.GONE);
                break;
            case 4:

                llSubordinate.setBackgroundColor(ContextCompat.getColor(FilterActivity.this, R.color.button_green));
                btnSubordinate.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.pure_white));
                llAllUser.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnAllUser.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llManagers.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnManagers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                llPeers.setBackground(ContextCompat.getDrawable(FilterActivity.this, R.drawable.button_white_stroke));
                btnPeers.setTextColor(ContextCompat.getColor(FilterActivity.this, R.color.text_workdays_alpha_3));
                ivAllUser.setVisibility(View.GONE);
                ivManagers.setVisibility(View.GONE);
                ivPeers.setVisibility(View.GONE);
                ivSubordinate.setVisibility(View.VISIBLE);
                break;
        }

    }

}
