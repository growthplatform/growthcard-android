package com.appster.growthcard.ui.fragments;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.appster.growthcard.ui.activities.ManagerMainActivity;
import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.adapter.NotificationListAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.PostListReq;
import com.appster.growthcard.network.response.NotificationResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NotificationListFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private LinearLayoutManager mLayoutManager;
    private RecyclerView recNotification;
    private int currentPage;
    private int pageCount;
    private List<String> dummy = new ArrayList<String>();
    private NotificationListAdapter mAdapter;
    private AppPrefrences appPref;
    private ArrayList notificationData;
    private Button btntoolDone;

    public static NotificationListFragment newInstance(String param1, String param2) {
        NotificationListFragment fragment = new NotificationListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public NotificationListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_notification_list, container, false);
        appPref = new AppPrefrences(getActivity());
        initilize(view);
        currentPage = 1;
        pageCount = 1;

        if (appPref.getTeamId() != 0)
            getNotificationList();

        if (getActivity() instanceof SubMainActivity) {
            appPref.setSubNotification(false);
            ((SubMainActivity) getActivity()).updateNavigationMenu();
        } else {
            appPref.setManNotification(false);
            ((ManagerMainActivity) getActivity()).updateNavigationMenu();
        }

        return view;


    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void initilize(View view) {
        recNotification = (RecyclerView) view.findViewById(R.id.recNotification);

        recNotification.setNestedScrollingEnabled(false);

        mLayoutManager = new LinearLayoutManager(getActivity());
        recNotification.setLayoutManager(mLayoutManager);
        btntoolDone = (Button) view.findViewById(R.id.toolBtnDone);
        btntoolDone.setVisibility(View.GONE);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar_actionbar);
        toolbar.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.profileBg));
        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.notifications);

    }

    private void getNotificationList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);

        ErrorCallback.MyCall<NotificationResponse> ip = service.getNotificationList(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<NotificationResponse>() {
            @Override
            public void success(final Response<NotificationResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getNotifications().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                notificationData = (ArrayList) response.body().getResult().getNotifications();
                            } else if (currentPage > 1) {

                                notificationData.remove(notificationData.size() - 1);
                                mAdapter.notifyItemRemoved(notificationData.size());
                                notificationData.addAll(response.body().getResult().getNotifications());
                            }

                            fillData();
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);
            }
        }, getActivity(), currentPage == 1);
    }

    private void fillData() {

        if (currentPage == 1) {
            mAdapter = new NotificationListAdapter(notificationData, recNotification, getActivity());
            recNotification.setAdapter(mAdapter);
            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    if (currentPage != pageCount) {
                        notificationData.add(null);
                        mAdapter.notifyItemInserted(notificationData.size() - 1);
                        currentPage++;
                        getNotificationList();

                        System.out.println("load");
                    }
                }
            });

        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();
        }
    }

    public PostListReq getPayload() {
        PostListReq req = new PostListReq();
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setPageNo(currentPage);
        req.setTeamId(appPref.getTeamId());


        return req;
    }


    @Override
    public void onResume() {
        super.onResume();

        if (Utils.isNotification) {
            Utils.isNotification = false;
            currentPage = 1;
            getNotificationList();
        }
    }
}
