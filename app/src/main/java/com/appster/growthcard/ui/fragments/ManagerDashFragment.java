package com.appster.growthcard.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.ui.activities.SubOrdinateListActivity;
import com.appster.growthcard.adapter.DashboardAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.interfaces.OnFragmentInteractionListener;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.response.DashboardListResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class ManagerDashFragment extends Fragment implements View.OnClickListener {

    private OnFragmentInteractionListener mListener;
    private View view;
    private FloatingActionButton fab;
    private RecyclerView recPastEA;
    private LinearLayoutManager mLayoutManager;
    private DashboardAdapter mAdapter;
    private AppPrefrences appPref;
    private int currentPage = 1, totalPages = 1;
    private ArrayList<DashboardListResponse.Member> dashList = new ArrayList<DashboardListResponse.Member>();
    private TextView tvEmptyView;
    public static boolean isUpdated;
    private SwipeRefreshLayout swipeContainer;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        appPref = new AppPrefrences(getActivity());
        view = inflater.inflate(R.layout.fragment_dashboard, null);

        findViews();
        getDashBoardData();//Sprint 4
        // Inflate the layout for this fragment
        return view;
    }

    private void findViews() {
        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        recPastEA = (RecyclerView) view.findViewById(R.id.recPastEA);
        tvEmptyView = (TextView) view.findViewById(R.id.empty_view);
        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_dashboard);

        swipeContainer = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefreshLayout);
        swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);


        swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                currentPage = 1;
                totalPages = 1;


                dashList.clear();



                if (mAdapter != null) {
                    mAdapter = null;
                }
                getDashBoardData();
            }
        });



        ((TextView) view.findViewById(R.id.empty_view)).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.empty_view)).setText(R.string.no_data_available);
        fab.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        if (isUpdated) {
            isUpdated = false;
            currentPage = 1;
            getDashBoardData();
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            mListener = (OnFragmentInteractionListener) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:

                Intent intent = new Intent(getActivity(), SubOrdinateListActivity.class);
                startActivity(intent);

                break;
        }
    }


    private void getDashBoardData() {
        swipeContainer.setRefreshing(false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<DashboardListResponse> ip = service.dashboardList(getCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<DashboardListResponse>() {
            @Override
            public void success(final Response<DashboardListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getMembers().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    totalPages = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                dashList = response.body().getResult().getMembers();
                            } else if (currentPage > 1) {

                                dashList.remove(dashList.size() - 1);
                                mAdapter.notifyItemRemoved(dashList.size());

                                dashList.addAll(response.body().getResult().getMembers());
                            }


                            fillData(false);
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), true);


    }

    private SendCommentsReq getCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setTeamId(appPref.getTeamId());
        req.setPageNo(currentPage);

        return req;
    }


    private void fillData(boolean isUpdated) {


        if (currentPage == 1) {

            mLayoutManager = new LinearLayoutManager(getActivity());
            recPastEA.setLayoutManager(mLayoutManager);

            mAdapter = new DashboardAdapter(getActivity(), recPastEA, dashList);
            recPastEA.setAdapter(mAdapter);

            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != totalPages) {

                        dashList.add(null);
                        mAdapter.notifyItemInserted(dashList.size() - 1);

                        currentPage++;
                        getDashBoardData();

                        System.out.println("load");
                    }
                }
            });


        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();


        }

        if (dashList.size() > 0) {
            ((TextView) view.findViewById(R.id.empty_view)).setVisibility(View.GONE);

        }


    }
}
