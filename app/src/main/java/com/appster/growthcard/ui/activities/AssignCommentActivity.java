package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.appster.growthcard.model.Users;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.AssignEAReq;
import com.appster.growthcard.network.response.AssignEAResponse;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshu on 02/03/16.
 */
public class AssignCommentActivity extends RootActivity {

    private ImageView ivSubordinate;
    private TextView tvSubordinateName;
    private TextView tvSubordinateDesignation;
    private TextView tvEffectiveActionName;
    private EditText etEffectiveActionComment;
    private Button btnToolDone;

    private Button btnChangeEA;
    private Button btnDeleteEA;
    private String ea_name;
    private boolean isEdit;
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private boolean isChange;
    private Users user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_comment);
        user = getIntent().getParcelableExtra("USER");
        isEdit = getIntent().getBooleanExtra(getString(R.string.is_editting), false);

        appPref = new AppPrefrences(AssignCommentActivity.this);
        initialize();


        btnToolDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assignEA();
            }
        });

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btnChangeEA:
                isChange = true;
                deleteEA();

                break;

            case R.id.btnDeleteEA:
                isChange = false;

                deleteEA();

                break;

        }
    }

    private void initialize() {
        ivSubordinate = (ImageView) findViewById(R.id.imgSubordinateImage);
        tvSubordinateName = (TextView) findViewById(R.id.txtSubordinateName);
        tvSubordinateDesignation = (TextView) findViewById(R.id.txtSubordinateDesignation);
        tvEffectiveActionName = (TextView) findViewById(R.id.txtEffectiveActionName);
        etEffectiveActionComment = (EditText) findViewById(R.id.edtEffectiveActionComment);
        btnToolDone = (Button) findViewById(R.id.toolBtnDone);
        btnChangeEA = (Button) findViewById(R.id.btnChangeEA);
        btnDeleteEA = (Button) findViewById(R.id.btnDeleteEA);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        Utils.setTextStyle(btnToolDone, getApplication(), Utils.MEDIUM);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        if (toolbar != null) {
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        Utils.applyFontForToolbarTitle(AssignCommentActivity.this, toolbar);
        Utils.setTextStyle(tvSubordinateName, AssignCommentActivity.this, Utils.MEDIUM);

        Utils.setTextStyle(tvSubordinateDesignation, AssignCommentActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvEffectiveActionName, AssignCommentActivity.this, Utils.REGULAR);
        Utils.setTextStyle(etEffectiveActionComment, AssignCommentActivity.this, Utils.REGULAR);
        Utils.setTextStyle(btnDeleteEA, AssignCommentActivity.this, Utils.REGULAR);
        Utils.setTextStyle(btnChangeEA, AssignCommentActivity.this, Utils.REGULAR);

        btnToolDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(AssignCommentActivity.this, ManToSubProfileActivity.class));
            }
        });

        if (user != null) {


            Utils.loadUrlImage(user.getUser().getProfileImage(), ivSubordinate);
            tvSubordinateName.setText(user.getUser().getFirstName() + " " + user.getUser().getLastName());

            if (user.getDesignation() != null) {
                tvSubordinateDesignation.setText(user.getDesignation().getDesignationTitle());
            }

        }

        if (Utils.selectedEA != null) {
            tvEffectiveActionName.setText(Utils.selectedEA.getEffectiveActionTitle());

        }

        if (isEdit) {

            btnToolDone.setText(getString(R.string.done));
            etEffectiveActionComment.setText(Utils.selectedEADescription);


            btnDeleteEA.setVisibility(View.VISIBLE);
            btnChangeEA.setVisibility(View.VISIBLE);
        } else {

            btnToolDone.setText(getString(R.string.assign));

            btnDeleteEA.setVisibility(View.GONE);
            btnChangeEA.setVisibility(View.GONE);

        }


        btnChangeEA.setOnClickListener(this);
        btnDeleteEA.setOnClickListener(this);


    }


    private void assignEA() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(AssignCommentActivity.this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<AssignEAResponse> ip = service.assignEA(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<AssignEAResponse>() {
            @Override
            public void success(final Response<AssignEAResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Utils.selectedEADescription = etEffectiveActionComment.getText().toString();

                    }
                });

                LocalBroadcastManager.getInstance(AssignCommentActivity.this).sendBroadcast(new Intent(Utils.FINISH_SUB_LIST));
                LocalBroadcastManager.getInstance(AssignCommentActivity.this).sendBroadcast(new Intent(Utils.FINISH_EA_LIST));

                Intent assignIntent = new Intent(AssignCommentActivity.this, ManToSubProfileActivity.class);
                assignIntent.putExtra("USER", user.getUser().getUserId());
                assignIntent.putExtra("TEAM", user.getTeamId());
                assignIntent.putExtra("FROM_ASSIGN", true);

                startActivity(assignIntent);
                finish();


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, AssignCommentActivity.this, true);

    }

    private AssignEAReq getPayload() {

        AssignEAReq req = new AssignEAReq();
        req.setDeviceId(Utils.getDeviceId(AssignCommentActivity.this));
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());

        req.setEffectiveActionDescription(etEffectiveActionComment.getText().toString());
        req.setEffectiveActionId(Utils.selectedEA.getEffectiveActionId());
        req.setSubordinateId(user.getUser().getUserId());
        req.setTeamId(user.getTeamId());


        return req;

    }

    private void deleteEA() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(AssignCommentActivity.this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<AssignEAResponse> ip = service.changeEA(getDeletePayload());
        ip.enqueue(new ErrorCallback.MyCallback<AssignEAResponse>() {
            @Override
            public void success(final Response<AssignEAResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());

//
                LocalBroadcastManager.getInstance(AssignCommentActivity.this).sendBroadcast(new Intent(Utils.FINISH_SUB_PROFILE));


                if (isChange) {

                    Intent assignIntent = new Intent(AssignCommentActivity.this, EffectiveActionListActivity.class);
                    assignIntent.putExtra(getString(R.string.is_editting), false);
                    assignIntent.putExtra("USER", user);
                    startActivity(assignIntent);
                    finish();
                } else {

                    finish();
                }


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, AssignCommentActivity.this, true);

    }

    private AssignEAReq getDeletePayload() {

        AssignEAReq req = new AssignEAReq();
        req.setDeviceId(Utils.getDeviceId(AssignCommentActivity.this));
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setEffectiveActionDescription("");
        req.setEffectiveActionId(Utils.selectedEA.getEffectiveActionId());
        req.setSubordinateId(user.getUser().getUserId());
        req.setTeamId(user.getTeamId());


        return req;

    }
}
