package com.appster.growthcard.ui.activities;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.adapter.PastEAlistAdapter;
import com.appster.growthcard.model.Designation;
import com.appster.growthcard.model.UserDetail;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.ui.fragments.TeamMemberFragment;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.interfaces.OnFragmentInteractionListener;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.SubProfileReq;
import com.appster.growthcard.network.response.SubProfileResponse;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by navdeep on 15/03/16.
 */
public class SubToSubProfileActivity extends RootActivity implements OnFragmentInteractionListener, AppBarLayout.OnOffsetChangedListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_SHOW_COMMENT_BOX = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.9f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private AppBarLayout appBar;

    private boolean mIsTheTitleVisible = false;
    private boolean mIsCommentVisible = false;
    private boolean mIsTheTitleContainerVisible = true;
    private TextView tvTitle;
    private RelativeLayout mTitleContainer;
    private LinearLayoutManager mLayoutManager;
    private PastEAlistAdapter mPastAdapter;
    private boolean isCurrent = true;
    private CircleImageView civProfile;
    private TextView tvProfileName;
    private TextView tvCompanyName;
    private TextView tvDepartmentName;
    private TextView tvDesignation;
    private TextView tvEmailId;
    private View sep1;
    private Toolbar toolbar;
    private int mActionBarSize;
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private TeamMemberFragment teamMemberFragment;
    private Users user;
    private int userId;
    private int teamId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_to_sub);
        userId = getIntent().getIntExtra("USER", 0);
        teamId = getIntent().getIntExtra("TEAM", 0);

        appPref = new AppPrefrences(SubToSubProfileActivity.this);
        init();
        getProfileBasic();
    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Utils.applyFontForToolbarTitle(SubToSubProfileActivity.this, toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        civProfile = (CircleImageView) findViewById(R.id.profile_image);
        tvProfileName = (TextView) findViewById(R.id.profile_name);
        tvCompanyName = (TextView) findViewById(R.id.company_name);
        tvDepartmentName = (TextView) findViewById(R.id.department_name);
        tvDesignation = (TextView) findViewById(R.id.designation);
        tvEmailId = (TextView) findViewById(R.id.email_id);
        sep1 = (View) findViewById(R.id.sep1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        tvTitle = (TextView) findViewById(R.id.main_textview_title);
        mTitleContainer = (RelativeLayout) findViewById(R.id.container);


        Utils.setTextStyle(tvProfileName, SubToSubProfileActivity.this, Utils.LIGHT);
        Utils.setTextStyle(tvCompanyName, SubToSubProfileActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvDepartmentName, SubToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvDesignation, SubToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvEmailId, SubToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvTitle, SubToSubProfileActivity.this, Utils.MEDIUM);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_profile));
        tabLayout.addTab(tabLayout.newTab().setIcon(R.drawable.tab_stats));


        appBar = (AppBarLayout) findViewById(R.id.appBar);
        appBar.addOnOffsetChangedListener(this);

        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(false);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final TabLayout.Tab tab) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (teamMemberFragment != null)
                            teamMemberFragment.toggleList(tab.getPosition());
                    }
                }, 300);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    /**
     * toolbar offset changed
     */
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);


    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(tvTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(tvTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }


    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private void addFragmentStack(final String title, final Fragment fragment) {
        List<Fragment> list;
        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        boolean flag = false;
        try {
            list = getSupportFragmentManager().getFragments();
            String tag = list.get(list.size() - 1).getTag();

            if (tag != null)
                flag = tag.equals(title);

        } catch (Exception e) {
            e.printStackTrace();

        }
        if (!flag) {
            new Handler().post(new Runnable() {
                @Override
                public void run() {

                    ft.replace(R.id.content_frame_team, fragment, title);
                    // ft.addToBackStack(title);
                    ft.commit();
                }
            });

        }
    }

    private void setData(SubProfileResponse subProfileResponse) {

        setUserVO(subProfileResponse.getResult());

        tvProfileName.setText(subProfileResponse.getResult().getFirstName() + " " + subProfileResponse.getResult().getLastName());

        if (subProfileResponse.getResult().getMember() != null) {
            tvCompanyName.setText(subProfileResponse.getResult().getMember().getCompany().getCompanyName());
            tvDesignation.setText(subProfileResponse.getResult().getMember().getDesignation().getDesignationTitle());
        }
        tvDepartmentName.setText(subProfileResponse.getResult().getUserdepartment().getDepartment().getDepartmentName());
        tvEmailId.setText(subProfileResponse.getResult().getEmail());
        tvTitle.setText(subProfileResponse.getResult().getFirstName() + " " + subProfileResponse.getResult().getLastName());

        Utils.loadUrlImage(user.getUser().getProfileImage(), civProfile);

        boolean[] workDays = {false, false, false, false, false, false, false};
        String workDayString = "";
        for (SubProfileResponse.Day day : subProfileResponse.getResult().getDays()) {
            if (day.getIs_selected() == 1) {
                workDays[day.getWorkDay() - 1] = true;
            }
        }
        for (int i = 0; i < workDays.length; i++) {
            workDayString = workDayString + (workDays[i] ? "1" : "0");
        }

        teamMemberFragment = TeamMemberFragment.newInstance(user, (ArrayList) subProfileResponse.getResult().getGraph(), workDayString);
        addFragmentStack("Team", teamMemberFragment);


    }

    private void setUserVO(SubProfileResponse.Result subProfileResponse) {

        try {
            user = new Users();
            UserDetail userDetail = new UserDetail();
            userDetail.setUserId(subProfileResponse.getUserId());
            userDetail.setEmail(subProfileResponse.getEmail());
            userDetail.setFirstName(subProfileResponse.getFirstName());
            userDetail.setLastName(subProfileResponse.getLastName());
            userDetail.setProfileImage(subProfileResponse.getProfileImage());

            user.setUser(userDetail);
            Designation designation = new Designation();
            if (subProfileResponse.getMember() != null) {
                designation.setDesignationId(subProfileResponse.getMember().getDesignationId());
                designation.setDesignationTitle(subProfileResponse.getMember().getDesignation().getDesignationTitle());
                designation.setCompanyId(subProfileResponse.getMember().getDesignation().getCompanyId());
                user.setCompanyId(subProfileResponse.getMember().getDesignation().getCompanyId());
                user.setTeamId(subProfileResponse.getMember().getTeamId());

                if (subProfileResponse.getMember().getEffectiveActionId() != null) {
                    user.setEffectiveActionDescription(subProfileResponse.getMember().getEffectiveActionDescription());
                    user.setEffectiveActionId(subProfileResponse.getMember().getEffectiveActionId());
                }
            }
            user.setDesignationId(designation.getDesignationId());

            user.setDesignation(designation);


        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void getProfileBasic() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SubToSubProfileActivity.this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SubProfileResponse> ip = service.getSubProfileBasic(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<SubProfileResponse>() {
            @Override
            public void success(final Response<SubProfileResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("Tag", "SUCCESS! " + response.body());

                        setData(response.body());
                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SubToSubProfileActivity.this, true);

    }

    private SubProfileReq getPayload() {

        SubProfileReq req = new SubProfileReq();
        req.setDeviceId(Utils.getDeviceId(SubToSubProfileActivity.this));
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());

        req.setCompanyId(appPref.getCompanyId());
        req.setSubordinateId(userId);
        req.setTeamId(teamId);


        return req;

    }

    private offsetChangedListener offsetChangedListener;

    public interface offsetChangedListener {

        void onOffsetChanged(float percent);

    }


}
