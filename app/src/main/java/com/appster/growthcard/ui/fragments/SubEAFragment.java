package com.appster.growthcard.ui.fragments;

import android.content.Intent;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.EADetailsActivity;
import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.adapter.CommentAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.adapter.SubPastEAAdapter;
import com.appster.growthcard.model.Comment;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.PastEAListReq;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.response.CommentListResponse;
import com.appster.growthcard.network.response.EADetailResponse;
import com.appster.growthcard.network.response.PastEAListResponse;
import com.appster.growthcard.network.response.SendCommentsResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class SubEAFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private TextView txtEA;
    private TextView txtTimeStamp;
    private ExpandableTextView txtEADesc;
    private LinearLayout eaChart;
    private AppBarLayout appBar;
    private LinearLayoutManager layoutManager;
    private TextView toolBarName;
    private int commentCurrentPage = 1;
    private int commentPageCount = 1;
    private AppPrefrences appPref;
    private ArrayList<Comment> commentList;
    private RecyclerView commentRecylclerView;
    private CommentAdapter mCommentAdapter;
    private int mActionBarSize;
    private RecyclerView pastRecyclerView;
    private boolean mIsCommentVisible = true;
    private TextView profileName;
    private TextView userDesignation;
    private ArrayList<PastEAListResponse.PastEffectiveAction> eaList;
    private SubPastEAAdapter mPastAdapter;
    private EditText commentEditText;
    private Button sendComment;
    private RelativeLayout commentBox;
    private ImageView userGraphImage;
    private TabLayout tabLayout;


    public static SubEAFragment newInstance(String param1, String param2) {
        SubEAFragment fragment = new SubEAFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public SubEAFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_sub_ea, container, false);
        appPref = new AppPrefrences(getActivity());
        init(rootView);

        if (appPref.getEAId() != 0) {
            getEADetails();
            getCommentList();

        }
        return rootView;
    }

    private void init(View rootView) {
        txtEA = (TextView) rootView.findViewById(R.id.txtEA);
        txtTimeStamp = (TextView) rootView.findViewById(R.id.txtTime_Stamp);
        txtEADesc = (ExpandableTextView) rootView.findViewById(R.id.txtEAdescription);
        eaChart = (LinearLayout) rootView.findViewById(R.id.current_ea_parent);
        toolBarName = (TextView) rootView.findViewById(R.id.main_textview_title);
        profileName = (TextView) rootView.findViewById(R.id.name);
        userDesignation = (TextView) rootView.findViewById(R.id.designation);
        commentEditText = (EditText) rootView.findViewById(R.id.comment_edit);
        sendComment = (Button) rootView.findViewById(R.id.send_comment);
        commentBox = (RelativeLayout) rootView.findViewById(R.id.comment_box);
        userGraphImage = (ImageView) rootView.findViewById(R.id.user_graph_image);

        Utils.setTextStyle(profileName, getActivity(), Utils.MEDIUM);
        Utils.setTextStyle(toolBarName, getActivity(), Utils.MEDIUM);
        Utils.setTextStyle(txtEA, getActivity(), Utils.MEDIUM);
        Utils.setTextStyle(txtTimeStamp, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(txtEADesc, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(commentEditText, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(sendComment, getActivity(), Utils.MEDIUM);
        Utils.loadUrlImage(appPref.getProfileImage(), userGraphImage);

        commentRecylclerView = (RecyclerView) rootView.findViewById(R.id.commentList);
        pastRecyclerView = (RecyclerView) rootView.findViewById(R.id.past_ea_list);

        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((RootActivity) getActivity()).getSupportActionBar().setTitle("");

        if (appPref.getEAId() == 0) {
            commentBox.setVisibility(View.GONE);
            txtEA.setText(R.string.title_effective_action_current);
            mIsCommentVisible = false;
        }

        eaChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (appPref.getEAId() != 0) {
                    Intent i = new Intent(getActivity(), EADetailsActivity.class);
                    i.putExtra("isCurrent", true);
                    i.putExtra("EAid", appPref.getEAId());
                    i.putExtra("USER", appPref.getUserId());
                    i.putExtra("TEAM", appPref.getTeamId());

                    startActivity(i);
                }
            }
        });

        commentEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if (commentEditText.getText().toString().trim().length() > 0) {
                    sendComment.setTextColor(ContextCompat.getColor(getActivity(), R.color.button_green));
                    sendComment.setAlpha(1);


                } else {
                    sendComment.setTextColor(ContextCompat.getColor(getActivity(), R.color.text_workdays));
                    sendComment.setAlpha(0.3f);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        profileName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        toolBarName.setText(getString(R.string.title_effective_action_current));
        userDesignation.setText(appPref.getDesignation());

        appBar = (AppBarLayout) rootView.findViewById(R.id.appBar);
        appBar.addOnOffsetChangedListener(this);

        tabLayout = (TabLayout) rootView.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.current)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.past)));
        commentRecylclerView.setVisibility(View.VISIBLE);
        pastRecyclerView.setVisibility(View.GONE);

        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) rootView.findViewById(
                R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(false);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final TabLayout.Tab tab) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (tab.getPosition() == 0 && appPref.getEAId() != 0) {
                            if (!mIsCommentVisible) {
                                startAlphaAnimation(commentBox, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                                mIsCommentVisible = true;
                            }
                            commentRecylclerView.setVisibility(View.VISIBLE);
                            pastRecyclerView.setVisibility(View.GONE);
                            toolBarName.setText(getString(R.string.title_effective_action_current));
                            if (appPref.getEAId() == 0) {
                                txtEA.setText(R.string.title_effective_action_current);
                            }

                        } else {
                            if (mIsCommentVisible) {
                                startAlphaAnimation(commentBox, ALPHA_ANIMATIONS_DURATION, View.GONE);
                                mIsCommentVisible = false;
                            }
                            toolBarName.setText(getString(R.string.title_effective_action_past));
                            commentRecylclerView.setVisibility(View.GONE);
                            pastRecyclerView.setVisibility(View.VISIBLE);
                            if (appPref.getEAId() == 0) {
                                txtEA.setText(R.string.title_effective_action_past);
                            }
                            if (eaList != null) {

                            } else {
                                if (appPref.getTeamId() != 0)
                                    getPastEAList();
                            }

                        }
                    }
                }, 300);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        sendComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!TextUtils.isEmpty(commentEditText.getText().toString().trim())) {
                    sendComment();
                }
            }
        });


    }

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.9f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(toolBarName, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(toolBarName, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation((RelativeLayout) txtEA.getParent(), ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation((RelativeLayout) txtEA.getParent(), ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public void startAlphaAnimation(final View v, long duration, final int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.setVisibility(visibility);

        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

                v.setVisibility(visibility);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private void getCommentList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<CommentListResponse> ip = service.getCommentsList(getCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<CommentListResponse>() {
            @Override
            public void success(final Response<CommentListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getComment().size() > 0) {

                    commentCurrentPage = response.body().getResult().getPageNo();
                    commentPageCount = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (commentCurrentPage == 1) {
                                commentList = response.body().getResult().getComment();
                            } else if (commentCurrentPage > 1) {

                                commentList.remove(commentList.size() - 1);
                                mCommentAdapter.notifyItemRemoved(commentList.size());

                                commentList.addAll(response.body().getResult().getComment());
                            }


                            fillCommentData(false);
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), false);


    }

    private SendCommentsReq getCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setSubordinateId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(appPref.getEAId());
        req.setPageNo(commentCurrentPage);

        return req;
    }


    private void fillCommentData(boolean isUpdated) {

        if (commentCurrentPage == 1) {

            layoutManager = new LinearLayoutManager(getActivity());
            commentRecylclerView.setLayoutManager(layoutManager);

            mCommentAdapter = new CommentAdapter(getActivity(), commentList, commentRecylclerView, false);
            commentRecylclerView.setAdapter(mCommentAdapter);

            final TypedArray styledAttributes = getActivity().getTheme().obtainStyledAttributes(
                    new int[]{android.R.attr.actionBarSize});
            mActionBarSize = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();

            commentRecylclerView.setPadding(0, 0, 0, 2 * mActionBarSize + (int) getResources().getDimension(R.dimen.button_height));


            mCommentAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (commentCurrentPage != commentPageCount) {

                        commentList.add(null);
                        mCommentAdapter.notifyItemInserted(commentList.size() - 1);

                        commentCurrentPage++;
                        getCommentList();

                    }
                }
            });


        } else if (mCommentAdapter != null) {

            mCommentAdapter.notifyDataSetChanged();
            mCommentAdapter.setLoaded();


        }


        if (isUpdated) {
            layoutManager.scrollToPositionWithOffset(0, 20);

        }


    }

    public void refreshData() {
        getEADetails();

    }


    private void getEADetails() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<EADetailResponse> ip = service.getEADetails(getEADetailPayload());

        ip.enqueue(new ErrorCallback.MyCallback<EADetailResponse>() {
            @Override
            public void success(final Response<EADetailResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (response.body().getResult().getEffectiveActionTitle() != null) {
                            appPref.setEATitle(response.body().getResult().getEffectiveActionTitle());
                            txtEA.setText(response.body().getResult().getEffectiveActionTitle());
                        }
                        if (response.body().getResult().getEffectiveActionDescription() != null) {
                            txtEADesc.setText(response.body().getResult().getEffectiveActionDescription());
                            txtEADesc.collapseText();
                        }

                        Utils.setChart(getActivity(), eaChart, response.body().getResult().getGraph(), appPref.getRole() == 2);
                        try {

                            SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));

                            String startDate = Utils.getDate(outputFmt.parse(response.body().getResult().getStartDate()), "dd MMMM, yyyy");
                            txtTimeStamp.setText(startDate);

                            if (response.body().getResult().getEndDate() != null) {
                                String endDate = Utils.getDate(outputFmt.parse(response.body().getResult().getEndDate()), "dd MMMM, yyyy");
                                txtTimeStamp.setText(startDate + "-" + endDate);

                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                    }


                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), true);


    }

    private SendCommentsReq getEADetailPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(appPref.getEAId());
        req.setFlag(1);
        req.setSubordinateId(appPref.getUserId());

        return req;
    }


    private int eaCurrentPage = 1, eaPageCount = 1;

    private void getPastEAList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<PastEAListResponse> ip = service.getPastEAList(getPastEAListPayload());

        ip.enqueue(new ErrorCallback.MyCallback<PastEAListResponse>() {
            @Override
            public void success(final Response<PastEAListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getPastEffectiveAction().size() > 0) {

                    eaCurrentPage = response.body().getResult().getPageNo();
                    eaPageCount = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (eaCurrentPage == 1) {
                                eaList = response.body().getResult().getPastEffectiveAction();
                            } else if (eaCurrentPage > 1) {

                                eaList.remove(eaList.size() - 1);
                                mPastAdapter.notifyItemRemoved(eaList.size());

                                eaList.addAll(response.body().getResult().getPastEffectiveAction());
                            }


                            fillPastEAData();
                        }
                    });

                }


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), eaCurrentPage == 1);


    }

    private PastEAListReq getPastEAListPayload() {
        PastEAListReq req = new PastEAListReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setSubordinateId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setPageNo(eaCurrentPage);
        req.setFlag(2);
        return req;
    }


    private void fillPastEAData() {


        if (eaCurrentPage == 1) {
            final TypedArray styledAttributes = getActivity().getTheme().obtainStyledAttributes(
                    new int[]{android.R.attr.actionBarSize});
            mActionBarSize = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();
            mPastAdapter = new SubPastEAAdapter(getActivity(), pastRecyclerView, eaList);
            pastRecyclerView.setPadding(0, 0, 0, 2 * mActionBarSize);
            pastRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            pastRecyclerView.setAdapter(mPastAdapter);

            mPastAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (eaCurrentPage != eaPageCount) {

                        eaList.add(null);
                        mPastAdapter.notifyItemInserted(eaList.size() - 1);

                        eaCurrentPage++;
                        getPastEAList();

                        System.out.println("load");
                    }
                }
            });


        } else if (mPastAdapter != null) {

            mPastAdapter.notifyDataSetChanged();
            mPastAdapter.setLoaded();


        }

    }


    private SendCommentsReq getSendCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setSubordinateId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(appPref.getEAId());
        req.setContent(commentEditText.getText().toString());
        req.setCompnayId(appPref.getCompanyId());

        return req;
    }

    private void sendComment() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SendCommentsResponse> ip = service.sendComment(getSendCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SendCommentsResponse>() {
            @Override
            public void success(final Response<SendCommentsResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                if (response.body().getResult() != null) {


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (commentList != null && commentList.size() != 0) {
                                commentList.add(0, response.body().getResult());

                            } else {
                                commentCurrentPage = 1;
                                commentPageCount = 1;
                                commentList = new ArrayList<Comment>();
                                commentList.add(0, response.body().getResult());

                            }

                            commentEditText.setText("");
                            fillCommentData(true);
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), true);


    }

    @Override
    public void onResume() {
        super.onResume();


        if (Utils.isEACleared) {

            if (mCommentAdapter != null && commentList != null) {
                commentCurrentPage = 1;
                commentList.clear();
                mCommentAdapter.notifyDataSetChanged();
            }

        }

        if (Utils.isNotification)
            Utils.isNotification = false;


        if (Utils.isEAUpdated && appPref.getEAId() != 0) {

            if (tabLayout.getSelectedTabPosition() == 0) {
                commentCurrentPage = 1;
                getEADetails();
                getCommentList();

            } else {

                eaCurrentPage = 1;
                getEADetails();
                getPastEAList();
            }

        }


    }

    private void updateViews() {
        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        ((RootActivity) getActivity()).getSupportActionBar().setTitle("");

        if (appPref.getEAId() == 0) {
            commentBox.setVisibility(View.GONE);
            txtEA.setText(R.string.title_effective_action_current);

            mIsCommentVisible = false;
        }


    }
}
