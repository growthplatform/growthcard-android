package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.appster.growthcard.adapter.SelectedSubordinatesAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.interfaces.OnCreateClickListener;
import com.appster.growthcard.network.requests.CreateAnnouncementReq;
import com.appster.growthcard.network.response.CreateAnnouncementResponse;
import com.appster.growthcard.network.response.SearchSubordinateResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshu on 14/03/16.
 */
public class NewAnnoucementActivity extends AppCompatActivity implements View.OnClickListener, OnCreateClickListener {

    private RecyclerView selectedSubordinates;
    private Button btntoolDone;
    private LinearLayoutManager mLayoutManager;
    private SelectedSubordinatesAdapter mAdapter;
    public String subject;
    public String message;
    private AppPrefrences appPref;
    public static ArrayList<SearchSubordinateResponse.User> selecteddata = new ArrayList<SearchSubordinateResponse.User>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_annoucements);
        appPref = new AppPrefrences(this);
        initiaze();


        btntoolDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAdapter.onCreateClick();


            }
        });
    }

    private void initiaze() {
        selectedSubordinates = (RecyclerView) findViewById(R.id.selectedSubordinates);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btntoolDone = (Button) findViewById(R.id.toolBtnDone);


        Utils.setTextStyleButton(btntoolDone, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(btntoolDone, getApplication(), Utils.MEDIUM);


        setSupportActionBar(toolbar);

        btntoolDone.setText(getResources().getString(R.string.create));

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        mLayoutManager = new LinearLayoutManager(this);
        selectedSubordinates.setLayoutManager(mLayoutManager);

        mAdapter = new SelectedSubordinatesAdapter(NewAnnoucementActivity.this);
        selectedSubordinates.setAdapter(mAdapter);

    }


    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCreateClick(String subject, String message, int flag) {
        Log.d("subject", subject);
        Log.d("message", message);
        Log.d("flag", String.valueOf(flag));
        createAnnoucement(subject, message, flag);


    }

    private void createAnnoucement(String subject, String message, int flag) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<CreateAnnouncementResponse> ip = service.createAnnoucement(getPayload(subject, message, flag));

        ip.enqueue(new ErrorCallback.MyCallback<CreateAnnouncementResponse>() {
            @Override
            public void success(Response<CreateAnnouncementResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                NewAnnoucementActivity.this.finish();


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, this, true);


    }


    public CreateAnnouncementReq getPayload(String subject, String message, int flag) {
        ArrayList<Integer> your_array_list = new ArrayList<>();

        for (SearchSubordinateResponse.User user : selecteddata) {
            your_array_list.add(user.getUserId());
        }

        CreateAnnouncementReq req = new CreateAnnouncementReq();
        req.setDeviceId(Utils.getDeviceId(this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());
        req.setContent(message);
        req.setSubject(subject);
        req.setFlag(flag);
        req.setSenders(your_array_list);


        return req;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mAdapter.onActivityResult(requestCode, resultCode, data);
    }
}
