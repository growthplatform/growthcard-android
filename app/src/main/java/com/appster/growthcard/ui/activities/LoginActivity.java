package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.SignInReq;
import com.appster.growthcard.network.response.SignInResponse;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout mEmailLoginForm;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnEmailSignIn;
    private String email;
    private String password;
    private AppPrefrences appPref;
    private TextView tvTermsNconditions;
    private TextView tvAcceptance;
    private TextView tvForget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        appPref = new AppPrefrences(LoginActivity.this);
        findViews();

    }

    @Override
    public void onBackPressed() {

        finish();
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }

    private void findViews() {
        mEmailLoginForm = (LinearLayout) findViewById(R.id.email_login_form);
        etEmail = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);
        btnEmailSignIn = (Button) findViewById(R.id.email_sign_in_button);
        tvTermsNconditions = (TextView) findViewById(R.id.btn_termsNconditions);
        tvAcceptance = (TextView) findViewById(R.id.btn_acceptance);
        tvForget = (TextView) findViewById(R.id.txtForget);

        Utils.setTextStyleButton(btnEmailSignIn, LoginActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(etEmail, LoginActivity.this, Utils.REGULAR);
        Utils.setTextStyle(etPassword, LoginActivity.this, Utils.REGULAR);

        btnEmailSignIn.setOnClickListener(this);
        tvTermsNconditions.setOnClickListener(this);
        tvForget.setOnClickListener(this);

        etPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                attemptLogin();
                return false;
            }
        });

        if (!appPref.getEmail().isEmpty()) {
            etEmail.setText(appPref.getEmail());
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_termsNconditions:

                startActivity(new Intent(LoginActivity.this, TermsActivity.class));
                break;

            case R.id.email_sign_in_button:
                attemptLogin();
                break;

            case R.id.txtForget:
                startActivity(new Intent(this, ForgetPasswordActivity.class));
                break;

        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Store values at the time of the login attempt.
        email = etEmail.getText().toString().trim();
        password = etPassword.getText().toString();

        // Check for a valid email address.
        if (TextUtils.isEmpty(email) && TextUtils.isEmpty(password)) {
            Utils.showResMsg(btnEmailSignIn, getString(R.string.all_field_mandatory));
            return;
        } else {

            if (TextUtils.isEmpty(email)) {
                Utils.showResMsg(btnEmailSignIn, getString(R.string.empty_mail));
                return;
            }

            if (!isEmailValid(email)) {
                Utils.showResMsg(btnEmailSignIn, getString(R.string.error_invalid_email));
                return;
            }

            if (TextUtils.isEmpty(password)) {
                Utils.showResMsg(btnEmailSignIn, getString(R.string.empty_password));
                return;
            }


            if (!isPasswordValid(password)) {
                Utils.showResMsg(btnEmailSignIn, getString(R.string.password_atleast_6));
                return;
            }

        }

        login();
    }

    private void login() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addLog(LoginActivity.this)) ///to add header
                .build();

        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SignInResponse> ip = service.signIn(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<SignInResponse>() {
            @Override
            public void success(Response<SignInResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message() + " " + response.body().getResult());

                saveUserDetails(response.body());

                if (appPref.isWorkDaysSelected()) {
                    if (appPref.getSwitch() == 1) {
                        startActivity(new Intent(LoginActivity.this, SubMainActivity.class));
                        finish();
                    } else {
                        if (appPref.getRole() == 1) {
                            startActivity(new Intent(LoginActivity.this, ManagerMainActivity.class));
                            finish();
                        } else if (appPref.getRole() == 2) {
                            startActivity(new Intent(LoginActivity.this, SubMainActivity.class));
                            finish();

                        }
                    }
                } else
                    startActivity(new Intent(LoginActivity.this, WorkDaysActivity.class));
                finish();
            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, LoginActivity.this, true);

    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private boolean isPasswordValid(String password) {

// ^                 # start-of-string
// (?=.*[0-9])       # a digit must occur at least once
// (?=.*[a-z])       # a lower case letter must occur at least once
// (?=.*[A-Z])       # an upper case letter must occur at least once
// (?=.*[@#$%^&+=])  # a special character must occur at least once
// (?=\S+$)          # no whitespace allowed in the entire string
// .{8,}             # anything, at least eight places though
// $                 # end-of-string
//
        //TODO: Replace this with your own logic
        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=\\S+$).{6,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    private SignInReq getPayload() {
        SignInReq signInReq = new SignInReq();
        signInReq.setDeviceType(Utils.DEVICE_TYPE);
        signInReq.setDeviceId(Utils.getDeviceId(LoginActivity.this));
        signInReq.setDeviceToken(appPref.getGCMDeviceToken());
        signInReq.setEmail(email);
        signInReq.setPassword(password);

        return signInReq;

    }


    private void saveUserDetails(SignInResponse response) {
        appPref.setUserId(response.getResult().getUserId());
        appPref.setUserToken(response.getResult().getUserToken());
        appPref.setRole(response.getResult().getRole());
        appPref.setEmail(response.getResult().getEmail());
        appPref.setFirstName(response.getResult().getFirstName());
        appPref.setLastName(response.getResult().getLastName());
        appPref.setTermsAccepted(response.getResult().getTermsAccepted() == 1);
        appPref.setSwitch(response.getResult().getSwitch());


        if (response.getResult().getMember() != null) {
            if (response.getResult().getMember().getCompany() != null) {
                appPref.setCompanyName(response.getResult().getMember().getCompany().getCompanyName());
            }
            appPref.setCompanyId(response.getResult().getMember().getCompanyId());
            if (response.getResult().getMember().getEffective() != null) {
                appPref.setEATitle(response.getResult().getMember().getEffective().getEffectiveActionTitle());
            }
            if (response.getResult().getMember().getEffectiveActionId() != null)
                appPref.setEAId(response.getResult().getMember().getEffectiveActionId());
            appPref.setDesignation(response.getResult().getMember().getDesignation().getDesignationTitle());
            appPref.setDesignationId(response.getResult().getMember().getDesignation().getDesignationId());

            if (response.getResult().getMember().getTeamId() != null) {
                appPref.setTeamId(response.getResult().getMember().getTeamId());
            }
        }

        if (response.getResult().getUserdepartment() != null) {
            appPref.setDepartmentName(response.getResult().getUserdepartment().getDepartment().getDepartmentName());
            appPref.setDepartmentId(response.getResult().getUserdepartment().getDepartment().getDepartmentId());
        }

        appPref.setProfileImage(response.getResult().getProfileImage());
        appPref.setWorkDaysSelected(response.getResult().getDays().size() != 0);
        appPref.setLoginPath(getResources().getInteger(R.integer.TermsPath));

        boolean[] workDays = {false, false, false, false, false, false, false};
        String workDayString = "";
        for (SignInResponse.Day day : response.getResult().getDays()) {
            if (day.getIs_selected() == 1) {
                workDays[day.getWorkDay() - 1] = true;
            }
        }
        for (int i = 0; i < workDays.length; i++) {
            workDayString = workDayString + (workDays[i] ? "1" : "0");
        }
        appPref.setWorkDays(workDayString);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                etPassword.setText("");
            }
        });
    }


}
