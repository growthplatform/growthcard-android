package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.appster.growthcard.adapter.ChooseSubordinateAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.SearchSubordinateReq;
import com.appster.growthcard.network.response.SearchSubordinateResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 15/03/16.
 */
public class SearchSubordinateActivity extends RootActivity {
    private RecyclerView selectedSubordinates;
    private Button btntoolDone;
    private LinearLayoutManager mLayoutManager;
    private ChooseSubordinateAdapter mAdapter;
    private EditText etSearch;
    private AppPrefrences appPref;
    private ArrayList<SearchSubordinateResponse.User> SubordinateData = new ArrayList<SearchSubordinateResponse.User>();
    private int currentPage = 0;
    private int pageCount = 0;
    private boolean SelectedListClear = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchsubordinate);
        appPref = new AppPrefrences(this);
        initiate();
    }

    private void initiate() {
        selectedSubordinates = (RecyclerView) findViewById(R.id.selectedSubordinates);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        etSearch = (EditText) findViewById(R.id.edtSearch);
        btntoolDone = (Button) findViewById(R.id.toolBtnDone);


        Utils.setTextStyleButton(btntoolDone, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(btntoolDone, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(etSearch, getApplication(), Utils.LIGHT);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_close));
        }


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        btntoolDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mAdapter != null) {
                    if (mAdapter.getData().size() == 0) {
                        Utils.showResMsg(btntoolDone, "Select atleast 1 subordinate");
                    } else {
                        if (NewAnnoucementActivity.selecteddata.size() > 0) {
                            ArrayList<SearchSubordinateResponse.User> addedUser = new ArrayList<SearchSubordinateResponse.User>();
                            for (int j = 0; j < mAdapter.getData().size(); j++) {
                                for (SearchSubordinateResponse.User user : NewAnnoucementActivity.selecteddata) {
                                    if (user.getUserId() == (mAdapter.getData().get(j).getUserId())) {
                                        addedUser.add(mAdapter.getData().get(j));
                                    }
                                }
                            }
                            NewAnnoucementActivity.selecteddata.removeAll(addedUser);
                            NewAnnoucementActivity.selecteddata.addAll(mAdapter.getData());

                        } else {
                            NewAnnoucementActivity.selecteddata.addAll(mAdapter.getData());
                        }
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            }
        });
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }


        mLayoutManager = new LinearLayoutManager(this);
        selectedSubordinates.setLayoutManager(mLayoutManager);


        etSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (etSearch.getText().toString().isEmpty()) {
                    Utils.showResMsg(etSearch, "Enter SubordinateName");

                } else {

                    currentPage = 1;
                    pageCount = 1;
                    if (!SelectedListClear) {
                        SelectedListClear = true;
                    }
                    getList();


                }

                return false;
            }
        });


    }


    public SearchSubordinateReq getPayload() {
        SearchSubordinateReq req = new SearchSubordinateReq();
        req.setDeviceId(Utils.getDeviceId(this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());
        req.setSearchText(etSearch.getText().toString().trim());
        req.setPageNo(currentPage);


        return req;
    }

    private void fillData() {

        if (currentPage == 1) {

            mAdapter = new ChooseSubordinateAdapter(SearchSubordinateActivity.this, SubordinateData, selectedSubordinates);
            selectedSubordinates.setAdapter(mAdapter);
            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != pageCount) {

                        SubordinateData.add(null);
                        mAdapter.notifyItemInserted(SubordinateData.size() - 1);

                        currentPage++;
                        getList();

                        System.out.println("load");
                    }
                }
            });


            //}

        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();


        }


    }


    public void getList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SearchSubordinateActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SearchSubordinateResponse> ip = service.searchSubordinate(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SearchSubordinateResponse>() {
            @Override
            public void success(final Response<SearchSubordinateResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getUsers().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                SubordinateData = (ArrayList) response.body().getResult().getUsers();
                            } else if (currentPage > 1) {

                                SubordinateData.remove(SubordinateData.size() - 1);
                                mAdapter.notifyItemRemoved(SubordinateData.size());


                                SubordinateData.addAll(response.body().getResult().getUsers());
                            }


                            fillData();
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SearchSubordinateActivity.this, currentPage == 1);

    }


}
