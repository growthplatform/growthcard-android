package com.appster.growthcard.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.adapter.SubOrdinateListAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.SubListReq;
import com.appster.growthcard.network.response.SubListResponse;
import com.appster.growthcard.model.Users;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by himanshu on 01/03/16.
 */
public class SubOrdinateListActivity extends RootActivity {


    private RecyclerView rvSubordinateList;
    private Button btnToolDone;
    private LinearLayoutManager mLayoutManager;
    private SubOrdinateListAdapter mAdapter;
    private boolean loading = true;
    private AppPrefrences appPref;
    private int currentPage = 1, pageCount = 0;
    private ArrayList<Users> subordinateList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subordinate_list);

        appPref = new AppPrefrences(SubOrdinateListActivity.this);
        initiate();


    }


    private void fillData() {

        if (currentPage == 1) {
            mAdapter = new SubOrdinateListAdapter(SubOrdinateListActivity.this, subordinateList, rvSubordinateList);

            rvSubordinateList.setAdapter(mAdapter);

            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != pageCount) {

                        subordinateList.add(null);
                        mAdapter.notifyItemInserted(subordinateList.size() - 1);

                        currentPage++;
                        getList();

                        System.out.println("load");
                    }
                }
            });

        } else if (mAdapter != null) {


            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();

        }

    }

    private void initiate() {
        rvSubordinateList = (RecyclerView) findViewById(R.id.rvSubordinateList);

        btnToolDone = (Button) findViewById(R.id.toolBtnDone);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btnToolDone.setVisibility(View.GONE);

        Utils.setTextStyle(btnToolDone, getApplication(), Utils.MEDIUM);
        Utils.applyFontForToolbarTitle(SubOrdinateListActivity.this, toolbar);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_close));
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mLayoutManager = new LinearLayoutManager(this);
        rvSubordinateList.setLayoutManager(mLayoutManager);

        getList();

    }


    private void getList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SubOrdinateListActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SubListResponse> ip = service.getSubList(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SubListResponse>() {
            @Override
            public void success(final Response<SubListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                if (response.body().getResult() != null && response.body().getResult().getSubordinates()
                        .size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                subordinateList = response.body().getResult().getSubordinates();
                            } else if (currentPage > 1) {
                                subordinateList.remove(subordinateList.size() - 1);
                                mAdapter.notifyItemRemoved(subordinateList.size());

                                subordinateList.addAll(response.body().getResult().getSubordinates());
                            }


                            fillData();
                        }
                    });

                }


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SubOrdinateListActivity.this, currentPage == 1);


    }


    private SubListReq getPayload() {
        SubListReq req = new SubListReq();
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setPageNo(currentPage);
        req.setDeviceId(Utils.getDeviceId(SubOrdinateListActivity.this));


        return req;
    }


    private BroadcastReceiver finishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                SubOrdinateListActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(finishActivity, new IntentFilter(Utils.FINISH_SUB_LIST));
    }

    @Override
    protected void onPause() {

        super.onPause();
    }

}
