package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;

/**
 * Created by himanshu on 02/03/16.
 */
public class FeedbackActivity extends RootActivity {

    private EditText etFeedback;
    private Button btnSubmit;
    private Button btnToolDone;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        initialte();
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etFeedback.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "FeebBack is empty", Toast.LENGTH_SHORT).show();
                } else {
                    finish();

                }
            }
        });

    }

    private void initialte() {
        etFeedback = (EditText) findViewById(R.id.edtFeedback);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnToolDone = (Button) findViewById(R.id.toolBtnDone);
        toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        btnToolDone.setVisibility(View.GONE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Utils.setTextStyle(etFeedback, getApplication(), Utils.LIGHT);
        Utils.setTextStyle(btnSubmit, getApplication(), Utils.REGULAR);

    }
}
