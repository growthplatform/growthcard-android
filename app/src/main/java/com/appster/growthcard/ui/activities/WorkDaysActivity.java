package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appster.growthcard.adapter.WorkdaysAdapter;
import com.appster.growthcard.model.WorkdayModel;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.WorkDaysReq;
import com.appster.growthcard.network.response.WorkDaysResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by himanshu on 23/02/16.
 */
public class WorkDaysActivity extends RootActivity {

    private RecyclerView mWorkdays;
    private LinearLayoutManager mLayoutManager;
    private WorkdaysAdapter mAdapter;
    private ArrayList<WorkdayModel> dayList = new ArrayList<>();
    private Button btnToolDone;
    private boolean mFromSetting;
    private AppPrefrences appPref;
    private String workDays = "";
    private boolean isWorkdaySelected = false;
    private static final String FROM_SPLASH = "from_splash";
    private String workday = "";
    private TextView txt_workdays;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workdays);
        mWorkdays = (RecyclerView) findViewById(R.id.workdays);
        btnToolDone = (Button) findViewById(R.id.toolBtnDone);
        txt_workdays = (TextView) findViewById(R.id.txt_workdays);

        appPref = new AppPrefrences(WorkDaysActivity.this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        Utils.setTextStyle(btnToolDone, getApplication(), Utils.MEDIUM);
        Utils.setTextStyle(txt_workdays, getApplication(), Utils.REGULAR);
        setSupportActionBar(toolbar);


        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setTitle("");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mFromSetting = getIntent().getBooleanExtra("FROM_SETTING", false);

        if (mFromSetting) {
            workday = getIntent().getStringExtra("WORKDAYS");
            txt_workdays.setVisibility(View.GONE);
        } else
            workday = appPref.getWorkDays();

        mLayoutManager = new LinearLayoutManager(this);
        mWorkdays.setLayoutManager(mLayoutManager);
        btnToolDone.setTextColor(ContextCompat.getColor(WorkDaysActivity.this, R.color.light_gray));

        setData();

        btnToolDone.setOnClickListener(this);


    }

    @Override
    public void onBackPressed() {

        if (getIntent().getBooleanExtra(FROM_SPLASH, false)) {
            Intent i = new Intent(WorkDaysActivity.this, LoginActivity.class);
            startActivity(i);
        }
        finish();
    }

    private void setData() {

        if (!appPref.getWorkDays().isEmpty()) {
            for (int i = 0; i < Utils.days.length; i++) {
                WorkdayModel rnd = new WorkdayModel(Utils.days[i]);
                rnd.setSelected(appPref.getWorkDays().charAt(i) == '1');
                dayList.add(rnd);
            }
            mAdapter = new WorkdaysAdapter(dayList, WorkDaysActivity.this);

            mWorkdays.setAdapter(mAdapter);
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        if (!mFromSetting) {
            for (int i = 0; i < dayList.size(); i++) {
                workDays = workDays + (dayList.get(i).isSelected() ? "1" : "0");

            }
            updateWorkDays();
        } else {
            SettingActivity.updatedWorkDays = "";
            for (int i = 0; i < dayList.size(); i++) {
                SettingActivity.updatedWorkDays = SettingActivity.updatedWorkDays + (dayList.get(i).isSelected() ? "1" : "0");

            }
            finish();
        }

    }

    private void updateWorkDays() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(WorkDaysActivity.this)) ///to add header
                .build();

        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<WorkDaysResponse> ip = service.updateWorkDays(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<WorkDaysResponse>() {
            @Override
            public void success(Response<WorkDaysResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                workday = workDays;
                appPref.setWorkDays(workDays);
                appPref.setWorkDaysSelected(true);

                if (!mFromSetting) {
                    if (appPref.getRole() == 1)
                        startActivity(new Intent(WorkDaysActivity.this, ManagerMainActivity.class));
                    else if (appPref.getRole() == 2)
                        startActivity(new Intent(WorkDaysActivity.this, SubMainActivity.class));

                }

                finish();

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);
                workDays = "";
            }


        }, WorkDaysActivity.this, true);
    }

    private WorkDaysReq getPayload() {
        WorkDaysReq req = new WorkDaysReq();
        req.setDeviceId(Utils.getDeviceId(WorkDaysActivity.this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setWorkDay(workDays);


        return req;

    }

    public void isWorkdysSelected() {
        isWorkdaySelected = false;

        for (WorkdayModel workdayModel : dayList) {

            if (workdayModel.isSelected()) {
                isWorkdaySelected = true;
                btnToolDone.setEnabled(true);
                btnToolDone.setTextColor(ContextCompat.getColor(WorkDaysActivity.this, R.color.toolbar_btn));
                break;
            }


            Log.d("Days", workdayModel.getDay() + ":" + workdayModel.isSelected());
        }
        if (!isWorkdaySelected) {

            isWorkdaySelected = true;
            btnToolDone.setEnabled(false);
            btnToolDone.setTextColor(ContextCompat.getColor(WorkDaysActivity.this, R.color.light_gray));

        }

    }


}
