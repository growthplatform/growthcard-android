package com.appster.growthcard.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.appster.growthcard.ui.activities.NewPostActivity;
import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.adapter.PostListAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.PostListReq;
import com.appster.growthcard.network.response.PostListResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PostListFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private RecyclerView recPost;
    private FloatingActionButton fab;
    private LinearLayoutManager mLayoutManager;
    private PostListAdapter mAdapter;
    private int flag = 1;
    private AppPrefrences appPref;
    private int currentPage = 1;
    private int pageCount = 1;
    private ArrayList<PostListResponse.Post> postData = new ArrayList<PostListResponse.Post>();


    public static PostListFragment newInstance(String param1, String param2) {
        PostListFragment fragment = new PostListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public PostListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        appPref = new AppPrefrences(getActivity());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_list, container, false);
        initilize(view);
        return view;


    }


    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        pageCount = 1;
        hitApi(flag);
    }

    private void initilize(View view) {

        recPost = (RecyclerView) view.findViewById(R.id.recPost);
        fab = (FloatingActionButton) view.findViewById(R.id.fab);

        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);

        ((RootActivity) getActivity()).getSupportActionBar().setTitle(R.string.posts);

        recPost.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.line_seperate));
        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(R.string.all_posts));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.my_posts));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final TabLayout.Tab tab) {
                currentPage = 1;
                pageCount = 1;
                postData.clear();
                if (mAdapter != null)
                    mAdapter.notifyDataSetChanged();

                if (tab.getPosition() == 0) {
                    flag = 1;
                } else {
                    flag = 2;

                }

                hitApi(flag);
                Log.d("position", String.valueOf(tab.getPosition()));


            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


        mLayoutManager = new LinearLayoutManager(getActivity());
        recPost.setLayoutManager(mLayoutManager);


        fab.setOnClickListener(this);
    }

    private void hitApi(int flag) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(getActivity())) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);

        ErrorCallback.MyCall<PostListResponse> ip = service.getpost(getPayload(flag));

        ip.enqueue(new ErrorCallback.MyCallback<PostListResponse>() {
            @Override
            public void success(final Response<PostListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getPost().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                postData = (ArrayList) response.body().getResult().getPost();
                            } else if (currentPage > 1) {

                                postData.remove(postData.size() - 1);
                                mAdapter.notifyItemRemoved(postData.size());

                                postData.addAll(response.body().getResult().getPost());
                            }


                            fillData();
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, getActivity(), currentPage == 1);


    }

    private void fillData() {

        if (currentPage == 1) {

            mAdapter = new PostListAdapter(postData, recPost, getActivity(), flag);
            recPost.setAdapter(mAdapter);


            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (currentPage != pageCount) {

                        postData.add(null);
                        mAdapter.notifyItemInserted(postData.size() - 1);

                        currentPage++;
                        hitApi(flag);

                        System.out.println("load");
                    }
                }
            });


            //}

        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();
        }
    }

    public PostListReq getPayload(int flag) {
        PostListReq req = new PostListReq();
        req.setDeviceId(Utils.getDeviceId(getActivity()));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setCompanyId(appPref.getCompanyId());
        req.setPageNo(currentPage);
        req.setFlag(flag);
        req.setSubordinateId(appPref.getUserId());

        return req;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fab:

                Intent intent = new Intent(getActivity(), NewPostActivity.class);
                startActivity(intent);

                break;
        }
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
