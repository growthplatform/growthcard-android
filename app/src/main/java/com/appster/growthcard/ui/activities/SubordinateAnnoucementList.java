package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.appster.growthcard.adapter.AnnoucementListAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.AnnoucementListReq;
import com.appster.growthcard.network.response.AnnoucementListResponse;

import java.util.ArrayList;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 04/04/16.
 */
public class SubordinateAnnoucementList extends AppCompatActivity {

    private RecyclerView recAnnoucement;
    private FloatingActionButton floatingActionButton;
    private LinearLayoutManager mLayoutManager;
    private int currentPage;
    private int pageCount;
    private Button btnToolDone;
    private ArrayList<AnnoucementListResponse.Announcement> annoucementData = new ArrayList<AnnoucementListResponse.Announcement>();
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private AnnoucementListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_annoucement_list);


        initilize();
        appPref = new AppPrefrences(this);
        currentPage = 1;
        pageCount = 1;
        getList();

    }

    private void getList() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(this))
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<AnnoucementListResponse> ip = service.annoucementList(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<AnnoucementListResponse>() {
            @Override
            public void success(final Response<AnnoucementListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                if (response.body().getResult() != null && response.body().getResult().getAnnouncements().size() > 0) {

                    currentPage = response.body().getResult().getPageNo();
                    pageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPage == 1) {
                                annoucementData = (ArrayList) response.body().getResult().getAnnouncements();
                            } else if (currentPage > 1) {

                                annoucementData.remove(annoucementData.size() - 1);
                                mAdapter.notifyItemRemoved(annoucementData.size());

                                annoucementData.addAll(response.body().getResult().getAnnouncements());
                            }


                            fillData();
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, this, currentPage == 1);


    }

    private void fillData() {

        if (currentPage == 1) {


            mAdapter = new AnnoucementListAdapter(this, recAnnoucement, annoucementData);
            recAnnoucement.setAdapter(mAdapter);


            mAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {

                    if (currentPage != pageCount) {

                        annoucementData.add(null);
                        mAdapter.notifyItemInserted(annoucementData.size() - 1);

                        currentPage++;
                        getList();

                        System.out.println("load");
                    }
                }
            });

        } else if (mAdapter != null) {

            mAdapter.notifyDataSetChanged();
            mAdapter.setLoaded();


        }


    }

    private void initilize() {
        recAnnoucement = (RecyclerView) findViewById(R.id.recAnnoucement);
        floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        btnToolDone = (Button) findViewById(R.id.toolBtnDone);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        btnToolDone.setVisibility(View.GONE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        floatingActionButton.setVisibility(View.GONE);

        mLayoutManager = new LinearLayoutManager(this);
        recAnnoucement.setLayoutManager(mLayoutManager);


    }

    public AnnoucementListReq getPayload() {
        AnnoucementListReq req = new AnnoucementListReq();
        req.setDeviceId(Utils.getDeviceId(this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());
        req.setRole(appPref.getRole());
        req.setPageNo(currentPage);
        return req;
    }


}
