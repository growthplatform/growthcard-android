package com.appster.growthcard.ui.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.adapter.NavigationDrawerAdapter;
import com.appster.growthcard.model.NavItems;
import com.appster.growthcard.ui.fragments.AnnoucementListFragment;
import com.appster.growthcard.ui.fragments.ManagerDashFragment;
import com.appster.growthcard.ui.fragments.NotificationListFragment;
import com.appster.growthcard.ui.fragments.ProfileFragment;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.interfaces.OnFragmentInteractionListener;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.SwitchReq;
import com.appster.growthcard.network.response.SignInResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ManagerMainActivity extends RootActivity implements OnFragmentInteractionListener {

    private ArrayList<NavItems> mNavItems = new ArrayList<>();
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout mDrawerPane;
    private LinearLayoutManager mLayoutManager;
    private NavigationDrawerAdapter adapter;
    private String[] nav_array;
    private ActionBarDrawerToggle mDrawerToggle;
    public Toolbar mToolbar;
    private FragmentManager mFragmentManager;
    private TextView tvUserName;
    private TextView tvUserDesig;
    private AppPrefrences appPref;
    private CircleImageView userImage;
    private ImageButton ivClose;
    private RelativeLayout menu;
    private Button btnSwitch;
    private int flag = 2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_main);
        appPref = new AppPrefrences(ManagerMainActivity.this);
        initUI();
        if (getIntent().getStringExtra("NOTIFICATION") != null) {

            if (!getIntent().getBooleanExtra("SWITCH", false)) {
                String notify = getIntent().getStringExtra("NOTIFICATION");
                handleNotification(notify);
            } else {
                switchUser(true);
                return;
            }

        }
        setupData();
    }

    private void switchUser(final boolean isNotify) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(ManagerMainActivity.this)) ///to add header
                .build();

        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SignInResponse> ip = service.fSwitch(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SignInResponse>() {
            @Override
            public void success(final Response<SignInResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message() + " " + response.body().getResult().getEmail());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Utils.saveUserDetails(response.body(), ManagerMainActivity.this);
                        Intent intent = new Intent(ManagerMainActivity.this, SubMainActivity.class);
                        NotificationManager nMgr = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        nMgr.cancelAll();
                        String i = ManagerMainActivity.this.getIntent().getStringExtra("NOTIFICATION");
                        if (isNotify)
                            intent.putExtra("NOTIFICATION", i);

                        intent.putExtra("OVERLAY", true);
                        startActivity(intent);
                        finish();

                    }
                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, ManagerMainActivity.this, true);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_profile:
            case R.id.profile_image:
            case R.id.tv_nav_title:
            case R.id.tv_nav_sub_title:
                selectItemFromDrawer(0);
                break;

            case R.id.btnSwitch:
                switchUser(false);
                break;

            case R.id.btnClose:
                mDrawerLayout.closeDrawers();
        }
    }


    protected void initUI() {

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout1);
        ivClose = (ImageButton) findViewById(R.id.btnClose);
        mDrawerLayout.setScrimColor(Color.TRANSPARENT);
        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.slider);
        mDrawerList = (ListView) findViewById(R.id.navList);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        menu = (RelativeLayout) findViewById(R.id.menu);

        setSupportActionBar(mToolbar);

        mFragmentManager = getSupportFragmentManager();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        ivClose.setOnClickListener(this);
    }


    private void addFragmentStack(String title, Fragment fragment) {
        List<Fragment> list = null;
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        boolean flag = false;
        try {
            list = mFragmentManager.getFragments();
            String tag = list.get(list.size() - 1).getTag();

            if (tag != null)
                flag = tag.equals(title);

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!flag) {

            ft.replace(R.id.content_frame, fragment, title);
            ft.addToBackStack(title);
            ft.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (userImage!=null) {
            Utils.loadUrlImage(appPref.getProfileImage(), userImage);
            adapter.notifyDataSetChanged();
        }
        if (appPref.getUserId() == 0 && appPref.getUserToken().equals("")) {
            startActivity(new Intent(ManagerMainActivity.this, LoginActivity.class));

            finish();
        }

        if (appPref.isManNotification()) {
            updateNavigationMenu();
        }
    }

    public void updateNavigationMenu() {
        for (int i = 0; i < mNavItems.size(); i++) {
            if (mNavItems.get(i).getmTitle().equalsIgnoreCase(nav_array[2]) && appPref.isManNotification()) {
                mNavItems.get(i).setIsNotifications(true);
            } else {
                mNavItems.get(i).setIsNotifications(false);

            }
        }

        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    protected void setupData() {

        nav_array = getResources().getStringArray(R.array.manager_navigation_menu);
        for (int i = 0; i < nav_array.length; i++) {
            mNavItems.add(new NavItems(nav_array[i]));
            if (nav_array[i].equalsIgnoreCase(nav_array[2]) && appPref.isManNotification()) {
                mNavItems.get(i).setIsNotifications(true);
            } else {
                mNavItems.get(i).setIsNotifications(false);
            }
        }
        mNavItems.get(0).setIsSelected(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        adapter = new NavigationDrawerAdapter(mNavItems, ManagerMainActivity.this);
        mDrawerList.setAdapter(adapter);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.menu_profile_header, mDrawerList, false);
        btnSwitch = (Button) header.findViewById(R.id.btnSwitch);
        header.setClickable(true);
        header.setOnClickListener(this);
        btnSwitch.setOnClickListener(this);

        mDrawerList.addHeaderView(header, null, false);
        if (appPref.getSwitch() == 1) {
            btnSwitch.setVisibility(View.VISIBLE);
        }
        tvUserName = (TextView) header.findViewById(R.id.tv_nav_title);
        userImage = (CircleImageView) header.findViewById(R.id.profile_image);
        tvUserDesig = (TextView) header.findViewById(R.id.tv_nav_sub_title);

        tvUserName.setOnClickListener(this);
        userImage.setOnClickListener(this);
        tvUserDesig.setOnClickListener(this);

        Utils.setTextStyle(tvUserName, ManagerMainActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvUserDesig, ManagerMainActivity.this, Utils.REGULAR);
        Utils.setTextStyleButton(btnSwitch, ManagerMainActivity.this, Utils.MEDIUM);

        if (!appPref.getProfileImage().isEmpty())
            Utils.loadUrlImage(appPref.getProfileImage(), userImage);

        tvUserName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        tvUserDesig.setText(appPref.getDesignation());

        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.blue_button, mDrawerList, false);
        footer.setPadding(0, Utils.dpToPx(ManagerMainActivity.this, 60), 0, 0);
        Button blue_btn = (Button) footer.findViewById(R.id.blue_btn);
        blue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mDrawerLayout.closeDrawers();
                Intent intent = new Intent(ManagerMainActivity.this, NewAnnoucementActivity.class);
                startActivity(intent);
            }
        });
        mDrawerList.addFooterView(footer, null, false);

        ((Button) footer.findViewById(R.id.blue_btn)).setText(getString(R.string.new_announcement));

        Utils.setTextStyle((TextView) header.findViewById(R.id.tv_nav_title), ManagerMainActivity.this, Utils.MEDIUM);
        Utils.setTextStyle((Button) footer.findViewById(R.id.blue_btn), ManagerMainActivity.this, Utils.MEDIUM);
        Utils.setTextStyle((TextView) header.findViewById(R.id.tv_nav_title), ManagerMainActivity.this, Utils.MEDIUM);

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectItemFromDrawer(position);
            }


        });

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        mDrawerToggle = new ActionBarDrawerToggle(ManagerMainActivity.this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_hamburger); //set your own
        mDrawerToggle.syncState();

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        selectItemFromDrawer(1);


    }


    public void selectItemFromDrawer(final int position) {
        updateMenu(position);
        mDrawerLayout.closeDrawer(GravityCompat.START);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                switch (position) {
                    case 0:
                        addFragmentStack(getString(R.string.profile_fragment), new ProfileFragment());
                        break;
                    case 1:
                        addFragmentStack(getString(R.string.dashboard_fragment), new ManagerDashFragment());
                        break;

                    case 2:
                        addFragmentStack(getString(R.string.announcement_fragment), new AnnoucementListFragment());
                        break;
                    case 3:
                        addFragmentStack(getString(R.string.notification_fragment), new NotificationListFragment());
                        break;

                }

            }
        }, 300);
    }

    private void updateMenu(int position) {
        if (position > 0) {
            position--;
            for (int i = 0; i < mNavItems.size(); i++) {

                mNavItems.get(i).setIsSelected(false);
            }

            mNavItems.get(position).setIsSelected(true);
            adapter.notifyDataSetInvalidated();
            tvUserName.setAlpha(0.5f);

        } else {
            for (int i = 0; i < mNavItems.size(); i++) {

                mNavItems.get(i).setIsSelected(false);
            }

            adapter.notifyDataSetInvalidated();
            tvUserName.setAlpha(1);

        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(menu);

                break;

        }
        // Handle your other action bar items...
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finish();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
    }


    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private SwitchReq getPayload() {
        SwitchReq switchReq = new SwitchReq();
        switchReq.setDeviceId(Utils.getDeviceId(ManagerMainActivity.this));
        switchReq.setFlag(flag);
        switchReq.setUserId(appPref.getUserId());
        switchReq.setUserToken(appPref.getUserToken());
        return switchReq;

    }

    private void handleNotification(String notify) {
        try {
            JSONObject bodyJson = new JSONObject(notify);
            appPref.setManNotification(false);

            String type = bodyJson.getString("type");

            if (type.equals(getString(R.string.notification_type_8))) {/// Avg Adherence Magr


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
