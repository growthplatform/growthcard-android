package com.appster.growthcard.ui.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.ManagerMainActivity;
import com.appster.growthcard.ui.activities.RootActivity;
import com.appster.growthcard.ui.activities.SettingActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.interfaces.OnFragmentInteractionListener;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.model.Users;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Retrofit;

public class ProfileFragment extends Fragment implements AppBarLayout.OnOffsetChangedListener {

    private CircleImageView civProfileImage;
    private TextView tvProfileName;
    private TextView companyName;
    private TextView departmentName;
    private TextView designation;
    private TextView emailId;
    private TextView workdays;
    private TextView toolBarName;
    private List<Users> teamMembers = new ArrayList<>();
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    private OnFragmentInteractionListener mListener;
    private AppPrefrences appPref;
    private AppBarLayout appBar;
    private Retrofit retrofit;
    private LinearLayoutManager layoutManager;
    private FrameLayout frameLayout;
    private TeamMemberFragment teamMemberFragment;

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile_new, null);
        setHasOptionsMenu(true);

        appPref = new AppPrefrences(getActivity());
        findViews(view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        setData();
    }

    private void findViews(View rootView) {

        civProfileImage = (CircleImageView) rootView.findViewById(R.id.profile_image);
        tvProfileName = (TextView) rootView.findViewById(R.id.profile_name);
        companyName = (TextView) rootView.findViewById(R.id.company_name);
        departmentName = (TextView) rootView.findViewById(R.id.department_name);
        designation = (TextView) rootView.findViewById(R.id.designation);
        emailId = (TextView) rootView.findViewById(R.id.email_id);
        workdays = (TextView) rootView.findViewById(R.id.workdays);
        toolBarName = (TextView) rootView.findViewById(R.id.main_textview_title);
        frameLayout = (FrameLayout) rootView.findViewById(R.id.content_frame);

        ((RootActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(true);
        ((RootActivity) getActivity()).getSupportActionBar().setTitle("");

        appBar = (AppBarLayout) rootView.findViewById(R.id.appBar);
        appBar.addOnOffsetChangedListener(this);
        layoutManager
                = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);


        Utils.setTextStyle(tvProfileName, getActivity(), Utils.LIGHT);
        Utils.setTextStyle(toolBarName, getActivity(), Utils.LIGHT);
        Utils.setTextStyle(companyName, getActivity(), Utils.MEDIUM);
        Utils.setTextStyle(departmentName, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(designation, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(emailId, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(workdays, getActivity(), Utils.REGULAR);

        if (getActivity() instanceof SubMainActivity)
            ((SubMainActivity) getActivity()).mToolbar.getBackground().setAlpha(0);
        else if (getActivity() instanceof ManagerMainActivity)
            ((ManagerMainActivity) getActivity()).mToolbar.getBackground().setAlpha(0);

        setData();
        teamMemberFragment = new TeamMemberFragment();
        offsetChangedListener = (ProfileFragment.offsetChangedListener) teamMemberFragment;
        addFragmentStack("Team", teamMemberFragment);

    }

    private void addFragmentStack(String title, Fragment fragment) {
        List<Fragment> list = null;
        FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
        boolean flag = false;
        try {
            list = getActivity().getSupportFragmentManager().getFragments();
            String tag = list.get(list.size() - 1).getTag();

            if (tag != null)
                flag = tag.equals(title);

        } catch (Exception e) {
            e.printStackTrace();

        }
        if (!flag) {

            ft.add(R.id.content_frame_team, fragment, title);
            ft.commit();
        }
    }

    private void setData() {
        tvProfileName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        toolBarName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        companyName.setText(appPref.getCompanyName());
        departmentName.setText(appPref.getDepartmentName());
        designation.setText(appPref.getDesignation());
        emailId.setText(appPref.getEmail());

        String workDays = "";
        for (int i = 0; i < appPref.getWorkDays().length(); i++) {
            if (appPref.getWorkDays().charAt(i) == '1') {

                if (workDays.equals(""))
                    workDays = Utils.days[i].substring(0, 3);
                else
                    workDays = workDays + "  " + Utils.days[i].substring(0, 3);
            }

        }

        workdays.setText(workDays);

        Utils.loadUrlImage(appPref.getProfileImage(), civProfileImage);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.profile_frag_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:

                startActivity(new Intent(getActivity(), SettingActivity.class));

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.9f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    @Override
    /**
     * toolbar offset changed
     */
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);

        if (teamMemberFragment != null)
            teamMemberFragment.onOffsetChanged(percentage);

    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(toolBarName, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(toolBarName, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }


    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation((RelativeLayout) tvProfileName.getParent(), ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation((RelativeLayout) tvProfileName.getParent(), ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    private offsetChangedListener offsetChangedListener;

    public interface offsetChangedListener {

        void onOffsetChanged(float percent);

    }


}
