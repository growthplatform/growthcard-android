package com.appster.growthcard.ui.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.ui.customviews.CircularProgressBar;
import com.appster.growthcard.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class RandomImageActivity extends RootActivity implements OnClickListener {

    private ImageButton ivClose;
    private ImageView ivRandom;
    private CircularProgressBar progressBar;
    private CountDownTimer randomTimer;
    private static final int TEN_SEC = 10000;
    private int seconds = 10;
    private static final int ONE_SEC = 1000;
    private TextView tvMessage;
    private TextView tvCaption;
    private Toolbar mToolbar;
    private File shareFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random_image);

        ivClose = (ImageButton) findViewById(R.id.btnClose);
        tvCaption = (TextView) findViewById(R.id.caption);
        tvMessage = (TextView) findViewById(R.id.message);
        ivRandom = (ImageView) findViewById(R.id.randomImage);
        progressBar = (CircularProgressBar) findViewById(R.id.circularprogressbar);
        progressBar.setTitle(seconds + "");
        progressBar.setProgress(100);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(ContextCompat.getDrawable(this, android.R.drawable.ic_menu_share));

        }
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setShareIntent();
            }
        });

        Utils.setTextStyle(tvCaption, RandomImageActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvMessage, RandomImageActivity.this, Utils.REGULAR);

        tvCaption.setText(Utils.imageCaption);
        tvMessage.setText(Utils.imageMessage);

        if (Utils.imageUrl != null)
            Utils.loadUrlImage(Utils.imageUrl, ivRandom, false);

        ivClose.setOnClickListener(this);
        randomTimer = new CountDownTimer(TEN_SEC, ONE_SEC) {
            public void onTick(long millisUntilFinished) {
                progressBar.setTitle(--seconds + "");
                progressBar.setProgress(seconds * 10);
            }
            public void onFinish() {
                finish();
            }
        }.start();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.btnClose:
                if (randomTimer != null)
                    randomTimer.cancel();
                finish();
                break;


        }

    }

    private ShareActionProvider mShareActionProvider;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.menu_random_image, menu);
        MenuItem item = menu.findItem(R.id.close);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.close:
                finish();


                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setShareIntent() {

        Uri bmpUri = getLocalBitmapUri(ivRandom);
        if (bmpUri != null) {
            // Construct a ShareIntent with link to image
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, Utils.imageCaption + "\n" + Utils.imageMessage);
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, "GrowthCard");
            shareIntent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            shareIntent.setType("image/*");
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(Intent.createChooser(shareIntent, getString(R.string.share_images)));
        } else {
            Utils.showResMsg(ivRandom, getString(R.string.image_not_loaded));
        }

    }

    // Call to update the share intent

    public Uri getLocalBitmapUri(ImageView imageView) {
        // Extract Bitmap from ImageView drawable
        Drawable drawable = imageView.getDrawable();
        Bitmap bmp = null;
        if (drawable instanceof BitmapDrawable) {
            bmp = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        } else {
            return null;
        }
        // Store image to default external storage directory
        Uri bmpUri = null;
        try {
            // Use methods on Context to access package-specific directories on external storage.
            // This way, you don't need to request external read/write permission.
            // See https://youtu.be/5xVh-7ywKpE?t=25m25s
            File file = new File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "share_image_" + System.currentTimeMillis() + ".png");
            FileOutputStream out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.close();
            bmpUri = Uri.fromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bmpUri;
    }


}
