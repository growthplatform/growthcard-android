package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.ForgetPasswordReq;
import com.appster.growthcard.network.response.ForgetPasswordResponse;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.util.regex.Pattern;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 04/04/16.
 */
public class ForgetPasswordActivity extends AppCompatActivity {

    private TextView tvPassword;
    private EditText etEmail;
    private Button btnEmailSignIn;
    private Retrofit retrofit;
    private Button btnToolBtnDone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        initialize();
    }

    private void hitApi() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<ForgetPasswordResponse> ip = service.getForgetPassword(getForgetPassword());


        ip.enqueue(new ErrorCallback.MyCallback<ForgetPasswordResponse>() {
            @Override
            public void success(final Response<ForgetPasswordResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        new SweetAlertDialog(ForgetPasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Done")
                                .setContentText(response.body().getMessage())
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();

                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, this, true);


    }

    private ForgetPasswordReq getForgetPassword() {

        ForgetPasswordReq req = new ForgetPasswordReq();
        req.setEmail(etEmail.getText().toString().trim());

        return req;
    }

    private void initialize() {
        tvPassword = (TextView) findViewById(R.id.txtPasswordText);
        etEmail = (EditText) findViewById(R.id.email);
        btnEmailSignIn = (Button) findViewById(R.id.email_sign_in_button);
        btnToolBtnDone = (Button) findViewById(R.id.toolBtnDone);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        btnToolBtnDone.setVisibility(View.GONE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Utils.setTextStyle(etEmail, this, Utils.REGULAR);
        Utils.setTextStyleButton(btnEmailSignIn, this, Utils.MEDIUM);

        btnEmailSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    Utils.showResMsg(btnEmailSignIn, getString(R.string.empty_mail));
                    return;
                } else if (!isEmailValid(etEmail.getText().toString().trim())) {
                    Utils.showResMsg(btnEmailSignIn, getString(R.string.error_invalid_email));
                    return;
                }

                hitApi();


            }
        });

    }

    private boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }
}
