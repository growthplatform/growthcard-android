package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.appster.growthcard.ApplicationClass;
import com.appster.growthcard.R;

public abstract class RootActivity extends AppCompatActivity implements View.OnClickListener {
    private int mOnStartCount = 0;
    public boolean isAnimation = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mOnStartCount = 1;
        if (savedInstanceState == null) // 1st time
        {
            if (isAnimation) {
                this.overridePendingTransition(R.anim.anim_slide_in_left,
                        R.anim.anim_slide_out_left);
            }
        } else // already created so reverse animation
        {
            mOnStartCount = 2;
        }
    }
    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        if (mOnStartCount > 1) {
            if (isAnimation) {
                this.overridePendingTransition(R.anim.anim_slide_in_right,
                        R.anim.anim_slide_out_right);
            }
        } else if (mOnStartCount == 1) {
            mOnStartCount++;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        ApplicationClass.activityResumed();
    }
    @Override
    protected void onPause() {
        super.onPause();
        ApplicationClass.activityPaused();
    }
    @Override
    public void onClick(View v) {

    }
}
