package com.appster.growthcard.ui.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.ChangePasswordReq;
import com.appster.growthcard.network.response.GenericResponse;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 05/04/16.
 */
public class ChangePasswordActivity extends AppCompatActivity {

    private Button btntoolDone;
    private EditText etOldPassword;
    private TextView tvOldPassword;
    private EditText etNewPassword;
    private TextView tvNewPassword;
    private TextView tvConfirmPassword;
    private EditText etConfirmPassword;
    private Button btnEmailSignIn;
    private Retrofit retrofit;
    private AppPrefrences appPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        appPref = new AppPrefrences(this);
        initiate();
    }

    private void initiate() {
        btntoolDone = (Button) findViewById(R.id.toolBtnDone);
        tvOldPassword = (TextView) findViewById(R.id.txtOldPasswordWord);
        etOldPassword = (EditText) findViewById(R.id.txtOldPassword);

        tvNewPassword = (TextView) findViewById(R.id.txtNewPasswordWord);
        etNewPassword = (EditText) findViewById(R.id.txtNewPassword);

        tvConfirmPassword = (TextView) findViewById(R.id.txtConfirmPasswordWord);
        etConfirmPassword = (EditText) findViewById(R.id.txtConfirmPassword);
        btnEmailSignIn = (Button) findViewById(R.id.email_sign_in_button);

        btntoolDone.setVisibility(View.GONE);

        Utils.setTextStyle(tvOldPassword, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(etOldPassword, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvNewPassword, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(etNewPassword, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(etConfirmPassword, getApplication(), Utils.REGULAR);
        Utils.setTextStyle(tvConfirmPassword, getApplication(), Utils.REGULAR);
        Utils.setTextStyleButton(btnEmailSignIn, getApplication(), Utils.MEDIUM);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        btnEmailSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validate();

            }
        });

    }

    private void validate() {

        if (TextUtils.isEmpty(etOldPassword.getText().toString().trim())) {
            Utils.showResMsg(etOldPassword, getString(R.string.empty_old_password));
            return;
        }else if (TextUtils.isEmpty(etNewPassword.getText().toString().trim())) {
            Utils.showResMsg(etNewPassword, getString(R.string.empty_new_password));
            return;
        } else if (!isPasswordValid(etNewPassword.getText().toString().trim())) {
            Utils.showResMsg(etNewPassword, getString(R.string.new_password_atleast_6));
            return;
        } else if (TextUtils.isEmpty(etConfirmPassword.getText().toString().trim())) {
            Utils.showResMsg(etConfirmPassword, getString(R.string.empty_confirm_password));
            return;
        } else if (!isPasswordValid(etConfirmPassword.getText().toString().trim())) {
            Utils.showResMsg(etConfirmPassword, getString(R.string.confirm_password_atleast_6));
            return;
        } else if (!etConfirmPassword.getText().toString().trim().equals(etNewPassword.getText().toString().trim())) {
            Utils.showResMsg(etConfirmPassword, getString(R.string.password_not_match));
            return;

        } else if (etOldPassword.getText().toString().trim().equals(etNewPassword.getText().toString().trim())) {
            Utils.showResMsg(etConfirmPassword, getString(R.string.password_match));
            return;
        }

        hitapi();

    }

    private void hitapi() {


        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<GenericResponse> ip = service.getChangePassword(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<GenericResponse>() {
            @Override
            public void success(Response<GenericResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        new SweetAlertDialog(ChangePasswordActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                .setTitleText("Done")
                                .setContentText("Password changed!")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        finish();
                                    }
                                })
                                .show();

                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, this, true);


    }

    private ChangePasswordReq getPayload() {
        ChangePasswordReq req = new ChangePasswordReq();
        req.setDeviceId(Utils.getDeviceId(this));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setOldPassword(etOldPassword.getText().toString().trim());
        req.setNewPassword(etNewPassword.getText().toString().trim());
        return req;

    }

    private boolean isPasswordValid(String password) {

// ^                 # start-of-string
// (?=.*[0-9])       # a digit must occur at least once
// (?=.*[a-z])       # a lower case letter must occur at least once
// (?=.*[A-Z])       # an upper case letter must occur at least once
// (?=.*[@#$%^&+=])  # a special character must occur at least once
// (?=\S+$)          # no whitespace allowed in the entire string
// .{8,}             # anything, at least eight places though
// $                 # end-of-string
//
        //TODO: Replace this with your own logic
        Pattern pattern;
        Matcher matcher;

//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
        final String PASSWORD_PATTERN = "^(?=\\S+$).{6,}$";

        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();
    }


}
