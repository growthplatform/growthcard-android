package com.appster.growthcard.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;


public class TutorialFragment extends Fragment {
    private ImageView iv_tour;
    private TextView textTourTitle;
    private TextView textTourDetail;
    private TextView textTourNum;

    public static TutorialFragment instance(String data) {
        TutorialFragment layout = new TutorialFragment();
        Bundle bundle = new Bundle();
        bundle.putString("KEY", data);
        layout.setArguments(bundle);
        return layout;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tour, null);
        iv_tour = (ImageView) view.findViewById(R.id.iv_tour);
        textTourNum = (TextView) view.findViewById(R.id.txt_tour_num);
        textTourTitle = (TextView) view.findViewById(R.id.txt_tour_title);
        textTourDetail = (TextView) view.findViewById(R.id.txt_tour_detail);

        Utils.setTextStyle(textTourDetail, getActivity(), Utils.REGULAR);
        Utils.setTextStyle(textTourTitle, getActivity(), Utils.MEDIUM);

        String str = getArguments().getString("KEY");
        if (str != null && str.equalsIgnoreCase("a")) {
            textTourNum.setText(R.string.tutorial_welcome);
            textTourTitle.setVisibility(View.GONE);
            textTourDetail.setVisibility(View.GONE);
        } else if (str != null && str.equalsIgnoreCase("b")) {
            textTourNum.setText(R.string.tutorial_swipe);
            textTourTitle.setVisibility(View.GONE);
            textTourDetail.setVisibility(View.GONE);
        } else if (str != null && str.equalsIgnoreCase("c")) {
            textTourNum.setText(R.string.tutorial_comment);
            textTourTitle.setVisibility(View.GONE);
            textTourDetail.setVisibility(View.GONE);
        } else if (str != null && str.equalsIgnoreCase("d")) {
            textTourNum.setText(R.string.tutorial_sharepatterns);
            textTourTitle.setVisibility(View.VISIBLE);
            textTourDetail.setVisibility(View.GONE);
            textTourTitle.setText(R.string.tutorial_sharepatterns_detail);
        } else if (str != null && str.equalsIgnoreCase("e")) {
            textTourNum.setText(R.string.tutorial_sharepatterns);
            textTourTitle.setVisibility(View.VISIBLE);
            textTourDetail.setVisibility(View.GONE);
            textTourTitle.setText(R.string.tutorial_sharepatterns_detail2);
        } else if (str != null && str.equalsIgnoreCase("f")) {
            textTourNum.setText(R.string.tutorial_bigpicture);
            textTourTitle.setVisibility(View.GONE);
            textTourDetail.setVisibility(View.GONE);
        }

        return view;
    }
}
