package com.appster.growthcard.ui.activities;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.adapter.TutorialAdapter;
import com.appster.growthcard.ui.customviews.CirclePageIndicator;
import com.appster.growthcard.ui.customviews.PageIndicator;
import com.appster.growthcard.utils.Utils;

/**
 * Created by navdeep on 16/10/15.
 */
public class TutorialActivity extends RootActivity {


    private TutorialAdapter mAdapter;
    private ViewPager mPager;
    private PageIndicator mIndicator;
    public static boolean mIsActive;
    private static Context sContext;
    private Button btnLetMeInBtn;
    private TextView btnSkip;
    private boolean mFromSetting = false;


    protected void initUI() {
        mPager = (ViewPager) findViewById(R.id.pager);
        btnLetMeInBtn = (Button) findViewById(R.id.letmein_id);
        btnSkip = (TextView) findViewById(R.id.skip_id);

        Utils.setTextStyleButton(btnLetMeInBtn, TutorialActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(btnSkip, TutorialActivity.this, Utils.MEDIUM);

        mFromSetting = getIntent().getBooleanExtra("FROM_SETTING", false);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("");

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        if (mFromSetting) {

            btnSkip.setVisibility(View.INVISIBLE);
            btnLetMeInBtn.setVisibility(View.INVISIBLE);
            toolbar.setVisibility(View.VISIBLE);
        } else
            toolbar.setVisibility(View.INVISIBLE);


    }


    protected void setupData() {
        final float density = getResources().getDisplayMetrics().density;

        mAdapter = new TutorialAdapter(this.getSupportFragmentManager());
        mPager.setAdapter(mAdapter);
        sContext = TutorialActivity.this;

        CirclePageIndicator indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        mIndicator = indicator;
        indicator.setViewPager(mPager);

        indicator.setBackgroundColor(Color.TRANSPARENT);
        indicator.setRadius(5 * density);
        indicator.setPageColor(ContextCompat.getColor(this, R.color.indicator_grey));
        indicator.setFillColor(ContextCompat.getColor(this, R.color.tutorial_blue));
//        indicator.setStrokeColor(0xFFFFFFFF);

        indicator.setStrokeWidth(0);
        indicator.setInnerStrokeWidth(3 * density);

        mIndicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 5) {
                    btnLetMeInBtn.setText("Done");
                    btnSkip.setVisibility(View.INVISIBLE);
                } else {
                    btnLetMeInBtn.setText("Next");
                    if (!mFromSetting) {
                        btnSkip.setVisibility(View.VISIBLE);
                    }

                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        btnLetMeInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btnLetMeInBtn.getText().toString().equalsIgnoreCase("NEXT")) {
                    if (mPager.getCurrentItem() < 5)
                        mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                } else {
                    finishActivity();
                }


            }
        });
        btnSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();

            }
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour);

        initUI();
        setupData();

    }


    @Override
    protected void onResume() {
        super.onResume();
        mIsActive = true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mIsActive = false;
    }

    // finish this activity
    public void finishActivity() {
        if (mFromSetting) {
            finish();
        } else {
            startActivity(new Intent(TutorialActivity.this, LoginActivity.class));
            finish();
        }
    }


}
