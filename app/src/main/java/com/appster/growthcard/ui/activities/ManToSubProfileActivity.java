package com.appster.growthcard.ui.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.adapter.CommentAdapter;
import com.appster.growthcard.adapter.EndlessAdapter;
import com.appster.growthcard.adapter.PastEAlistAdapter;
import com.appster.growthcard.model.Comment;
import com.appster.growthcard.model.Designation;
import com.appster.growthcard.model.UserDetail;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.ui.fragments.ManagerDashFragment;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.PastEAListReq;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.requests.SubProfileReq;
import com.appster.growthcard.network.response.CommentListResponse;
import com.appster.growthcard.network.response.PastEAListResponse;
import com.appster.growthcard.network.response.SendCommentsResponse;
import com.appster.growthcard.network.response.SubProfileResponse;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ManToSubProfileActivity extends RootActivity implements AppBarLayout.OnOffsetChangedListener {


    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_SHOW_COMMENT_BOX = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.9f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    public int pastEAFlag = 1;
    private AppBarLayout appBar;

    private boolean mIsTheTitleVisible = false;
    private boolean mIsCommentVisible = false;
    private boolean mIsTheTitleContainerVisible = true;
    private TextView tvTitle;
    private RelativeLayout mTitleContainer;
    private RecyclerView commentRecylclerView;
    private RecyclerView pastEARecylclerView;
    private LinearLayoutManager mLayoutManager;
    private RelativeLayout commentBox;
    private CommentAdapter mCommentAdapter;
    private PastEAlistAdapter mPastAdapter;
    private boolean isCurrent = true;
    private CircleImageView civProfile;
    private TextView tvProfileName;
    private TextView tvCompanyName;
    private TextView tvDepartmentName;
    private TextView tvDesignation;
    private TextView tvEmailId;
    private View sep1;
    private Toolbar toolbar;
    private EditText etComment;
    private Button btnSendComment;
    private int mActionBarSize;
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private Users user;
    private int eaCurrentPage = 1, eaPageCount = 1;
    private int commentCurrentPage = 1, commentPageCount = 1;
    private ArrayList<PastEAListResponse.PastEffectiveAction> eaList = new ArrayList<PastEAListResponse.PastEffectiveAction>();
    private ArrayList<Comment> commentList;
    private SubProfileResponse subProfileResponse;
    private int userId;
    private int teamId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_profile);
        userId = getIntent().getIntExtra("USER", 0);
        teamId = getIntent().getIntExtra("TEAM", 0);

        appPref = new AppPrefrences(ManToSubProfileActivity.this);
        init();
        getProfileBasic();

    }

    private void init() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        commentBox = (RelativeLayout) findViewById(R.id.comment_box);
        btnSendComment = (Button) findViewById(R.id.send_comment);
        civProfile = (CircleImageView) findViewById(R.id.profile_image);
        tvProfileName = (TextView) findViewById(R.id.profile_name);
        tvCompanyName = (TextView) findViewById(R.id.company_name);
        tvDepartmentName = (TextView) findViewById(R.id.department_name);
        tvDesignation = (TextView) findViewById(R.id.designation);
        tvEmailId = (TextView) findViewById(R.id.email_id);
        sep1 = (View) findViewById(R.id.sep1);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        etComment = (EditText) findViewById(R.id.comment_edit);
        tvTitle = (TextView) findViewById(R.id.main_textview_title);
        mTitleContainer = (RelativeLayout) findViewById(R.id.container);

        Utils.setTextStyle(tvProfileName, ManToSubProfileActivity.this, Utils.LIGHT);
        Utils.setTextStyle(btnSendComment, ManToSubProfileActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvCompanyName, ManToSubProfileActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvDepartmentName, ManToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvDesignation, ManToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvEmailId, ManToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvTitle, ManToSubProfileActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(etComment, ManToSubProfileActivity.this, Utils.REGULAR);
        Utils.setTextStyle(btnSendComment, ManToSubProfileActivity.this, Utils.REGULAR);


        etComment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (etComment.getText().toString().trim().length() > 0) {
                    btnSendComment.setTextColor(ContextCompat.getColor(ManToSubProfileActivity.this, R.color.button_green));
                    btnSendComment.setAlpha(1);

                } else {
                    btnSendComment.setTextColor(ContextCompat.getColor(ManToSubProfileActivity.this, R.color.text_workdays));
                    btnSendComment.setAlpha(0.3f);


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.current)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.past)));

        commentRecylclerView = (RecyclerView) findViewById(R.id.commentRecyclerView);
        pastEARecylclerView = (RecyclerView) findViewById(R.id.pasteaRecyclerView);


        appBar = (AppBarLayout) findViewById(R.id.appBar);
        appBar.addOnOffsetChangedListener(this);

        commentRecylclerView.setPadding(0, 0, 0, 2 * mActionBarSize + (int) getResources().getDimension(R.dimen.button_height));
        commentRecylclerView.setAdapter(mCommentAdapter);
        btnSendComment.setOnClickListener(this);


        final CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(
                R.id.collapse_toolbar);

        collapsingToolbar.setTitleEnabled(false);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(final TabLayout.Tab tab) {

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (tab.getPosition() == 0) {
                            isCurrent = true;
                            commentRecylclerView.setVisibility(View.VISIBLE);
                            pastEARecylclerView.setVisibility(View.GONE);

                        } else {
                            isCurrent = false;
                            if (mIsCommentVisible) {
                                startAlphaAnimation(commentBox, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                                mIsCommentVisible = false;
                            }
                            commentRecylclerView.setVisibility(View.GONE);
                            pastEARecylclerView.setVisibility(View.VISIBLE);

                            if (eaList.size() != 0) {


                            } else {
                                getPastEAList();
                            }

                        }
                    }
                }, 300);

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getIntent().getBooleanExtra("FROM_ASSIGN", false))
            ManagerDashFragment.isUpdated = true;
    }

    private void setData(SubProfileResponse subProfileResponse) {


        setUserVO(subProfileResponse.getResult());
        tvProfileName.setText(subProfileResponse.getResult().getFirstName() + " " + subProfileResponse.getResult().getLastName());
        if (subProfileResponse.getResult().getMember() != null) {
            tvCompanyName.setText(subProfileResponse.getResult().getMember().getCompany().getCompanyName());
            tvDesignation.setText(subProfileResponse.getResult().getMember().getDesignation().getDesignationTitle());
            if (subProfileResponse.getResult().getMember().getDesignation() != null) {
                Designation designationVO = new Designation();
                designationVO.setDesignationId(subProfileResponse.getResult().getMember().getDesignation().getDesignationId());
                designationVO.setDesignationTitle(subProfileResponse.getResult().getMember().getDesignation().getDesignationTitle());
                designationVO.setCompanyId(subProfileResponse.getResult().getMember().getDesignation().getCompanyId());
                user.setDesignation(designationVO);

            }
        }
        tvDepartmentName.setText(subProfileResponse.getResult().getUserdepartment().getDepartment().getDepartmentName());
        tvEmailId.setText(subProfileResponse.getResult().getEmail());
        tvTitle.setText(subProfileResponse.getResult().getFirstName() + " " + subProfileResponse.getResult().getLastName());
        this.subProfileResponse = subProfileResponse;
        commentList = new ArrayList<>();


        Utils.loadUrlImage(user.getUser().getProfileImage(), civProfile);
        tvProfileName.setText(user.getUser().getFirstName() + " " + user.getUser().getLastName());
        tvTitle.setText(user.getUser().getFirstName() + " " + user.getUser().getLastName());


        fillCommentData(false);


        if (user.getEffectiveActionId() != null)
            getCommentList();


    }

    private void setUserVO(SubProfileResponse.Result subProfileResponse) {

        try {
            user = new Users();
            UserDetail userDetail = new UserDetail();
            userDetail.setUserId(subProfileResponse.getUserId());
            userDetail.setEmail(subProfileResponse.getEmail());
            userDetail.setFirstName(subProfileResponse.getFirstName());
            userDetail.setLastName(subProfileResponse.getLastName());
            userDetail.setProfileImage(subProfileResponse.getProfileImage());

            user.setUser(userDetail);
            if (subProfileResponse.getMember() != null) {
                Designation designation = new Designation();
                designation.setDesignationId(subProfileResponse.getMember().getDesignationId());
                designation.setDesignationTitle(subProfileResponse.getMember().getDesignation().getDesignationTitle());
                designation.setCompanyId(subProfileResponse.getMember().getDesignation().getCompanyId());
                user.setDesignation(designation);
                user.setDesignationId(designation.getDesignationId());
                user.setCompanyId(subProfileResponse.getMember().getDesignation().getCompanyId());
                user.setTeamId(subProfileResponse.getMember().getTeamId());

                if (subProfileResponse.getMember().getEffectiveActionId() != null) {
                    user.setEffectiveActionDescription(subProfileResponse.getMember().getEffectiveActionDescription());
                    user.setEffectiveActionId(subProfileResponse.getMember().getEffectiveActionId());
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void getProfileBasic() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(ManToSubProfileActivity.this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SubProfileResponse> ip = service.getSubProfileBasic(getPayload());
        ip.enqueue(new ErrorCallback.MyCallback<SubProfileResponse>() {
            @Override
            public void success(final Response<SubProfileResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("Tag", "SUCCESS! " + response.body());

                        setData(response.body());
                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, ManToSubProfileActivity.this, true);

    }

    private SubProfileReq getPayload() {

        SubProfileReq req = new SubProfileReq();
        req.setDeviceId(Utils.getDeviceId(ManToSubProfileActivity.this));
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());

        req.setCompanyId(appPref.getCompanyId());
        req.setSubordinateId(userId);
        req.setTeamId(teamId);


        return req;

    }


    @Override
    /**
     * toolbar offset changed
     */
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);

        if (isCurrent)
            handleComment(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(tvTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(tvTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleComment(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_COMMENT_BOX) {

            if (!mIsCommentVisible) {
                startAlphaAnimation(commentBox, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsCommentVisible = true;
            }

        } else {

            if (mIsCommentVisible) {
                startAlphaAnimation(commentBox, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsCommentVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    public void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);

        switch (v.getId()) {
            case R.id.send_comment:

                if (etComment.getText().toString().trim().length() > 0) {

                    if (subProfileResponse.getResult().getMember().getEffectiveActionId() != null)
                        sendComment();
                }
                break;
        }
    }

    private BroadcastReceiver finishActivity = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                ManToSubProfileActivity.this.finish();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(finishActivity, new IntentFilter(Utils.FINISH_SUB_PROFILE));
    }

    public void getPastEAList() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(ManToSubProfileActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<PastEAListResponse> ip = service.getPastEAList(getPastEAListPayload());

        ip.enqueue(new ErrorCallback.MyCallback<PastEAListResponse>() {
            @Override
            public void success(final Response<PastEAListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getPastEffectiveAction().size() > 0) {

                    eaCurrentPage = response.body().getResult().getPageNo();
                    eaPageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (eaCurrentPage == 1) {
                                eaList = response.body().getResult().getPastEffectiveAction();
                            } else if (eaCurrentPage > 1) {

                                eaList.remove(eaList.size() - 1);
                                mPastAdapter.notifyItemRemoved(eaList.size());

                                eaList.addAll(response.body().getResult().getPastEffectiveAction());
                            }


                            if (user.getUser() != null)
                                fillPastEAData();
                        }
                    });

                } else {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {


                            mPastAdapter = new PastEAlistAdapter(ManToSubProfileActivity.this, pastEARecylclerView, eaList, user);
                            pastEARecylclerView.setPadding(0, 0, 0, 2 * mActionBarSize);
                            pastEARecylclerView.setLayoutManager(new LinearLayoutManager(ManToSubProfileActivity.this));
                            pastEARecylclerView.setAdapter(mPastAdapter);
                        }
                    });


                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, ManToSubProfileActivity.this, eaCurrentPage == 1);


    }

    private PastEAListReq getPastEAListPayload() {
        PastEAListReq req = new PastEAListReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(ManToSubProfileActivity.this));
        req.setSubordinateId(user.getUser().getUserId());
        req.setTeamId(user.getTeamId());
        req.setPageNo(eaCurrentPage);
        req.setFlag(pastEAFlag);
        return req;
    }


    private void fillPastEAData() {


        if (eaCurrentPage == 1) {

            mPastAdapter = new PastEAlistAdapter(ManToSubProfileActivity.this, pastEARecylclerView, eaList, user);
            pastEARecylclerView.setPadding(0, 0, 0, 2 * mActionBarSize);
            pastEARecylclerView.setLayoutManager(new LinearLayoutManager(ManToSubProfileActivity.this));
            pastEARecylclerView.setAdapter(mPastAdapter);

            mPastAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (eaCurrentPage != eaPageCount) {

                        eaList.add(null);
                        mPastAdapter.notifyItemInserted(eaList.size() - 1);

                        eaCurrentPage++;
                        getPastEAList();

                        System.out.println("load");
                    }
                }
            });


        } else if (mPastAdapter != null) {

            mPastAdapter.notifyDataSetChanged();
            mPastAdapter.setLoaded();


        }

    }

    private SendCommentsReq getSendCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(ManToSubProfileActivity.this));
        req.setSubordinateId(user.getUser().getUserId());
        req.setTeamId(teamId);
        req.setEffectiveActionId(subProfileResponse.getResult().getMember().getEffectiveActionId());
        req.setContent(etComment.getText().toString());
        req.setCompnayId(appPref.getCompanyId());

        return req;
    }

    private void sendComment() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(ManToSubProfileActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SendCommentsResponse> ip = service.sendComment(getSendCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SendCommentsResponse>() {
            @Override
            public void success(final Response<SendCommentsResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());

                if (response.body().getResult() != null) {


                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (commentList != null && commentList.size() != 0) {
                                commentList.add(0, response.body().getResult());

                            } else {
                                commentCurrentPage = 1;
                                commentPageCount = 1;
                                commentList = new ArrayList<Comment>();
                                commentList.add(0, response.body().getResult());

                            }

                            etComment.setText("");
                            fillCommentData(true);
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, ManToSubProfileActivity.this, true);


    }

    private void getCommentList() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(ManToSubProfileActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<CommentListResponse> ip = service.getCommentsList(getCommentPayload());

        ip.enqueue(new ErrorCallback.MyCallback<CommentListResponse>() {
            @Override
            public void success(final Response<CommentListResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                if (response.body().getResult() != null && response.body().getResult().getComment().size() > 0) {

                    commentCurrentPage = response.body().getResult().getPageNo();
                    commentPageCount = response.body().getResult().getTotalPages();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (commentCurrentPage == 1) {
                                commentList = response.body().getResult().getComment();
                            } else if (commentCurrentPage > 1) {

                                commentList.remove(commentList.size() - 1);
                                mCommentAdapter.notifyItemRemoved(commentList.size());

                                commentList.addAll(response.body().getResult().getComment());
                            }


                            fillCommentData(false);
                        }
                    });

                }

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, ManToSubProfileActivity.this, false);


    }

    private SendCommentsReq getCommentPayload() {
        SendCommentsReq req = new SendCommentsReq();
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(ManToSubProfileActivity.this));
        req.setSubordinateId(user.getUser().getUserId());
        req.setTeamId(teamId);
        req.setEffectiveActionId(user.getEffectiveActionId());
        req.setPageNo(commentCurrentPage);

        return req;
    }


    private void fillCommentData(boolean isUpdated) {


        if (commentCurrentPage == 1) {

            mLayoutManager = new LinearLayoutManager(ManToSubProfileActivity.this);
            commentRecylclerView.setLayoutManager(mLayoutManager);

            mCommentAdapter = new CommentAdapter(ManToSubProfileActivity.this, commentList, commentRecylclerView, subProfileResponse, user, true);
            commentRecylclerView.setAdapter(mCommentAdapter);

            final TypedArray styledAttributes = getTheme().obtainStyledAttributes(
                    new int[]{android.R.attr.actionBarSize});
            mActionBarSize = (int) styledAttributes.getDimension(0, 0);
            styledAttributes.recycle();

            commentRecylclerView.setPadding(0, 0, 0, 2 * mActionBarSize + (int) getResources().getDimension(R.dimen.button_height));


            mCommentAdapter.setOnLoadMoreListener(new EndlessAdapter.OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    //add progress item'

                    if (commentCurrentPage != commentPageCount) {

                        commentList.add(null);
                        mCommentAdapter.notifyItemInserted(commentList.size() - 1);

                        commentCurrentPage++;
                        getCommentList();

                        System.out.println("load");
                    }
                }
            });


        } else if (mCommentAdapter != null) {

            mCommentAdapter.notifyDataSetChanged();
            mCommentAdapter.setLoaded();


        }


        if (isUpdated) {
            mLayoutManager.scrollToPositionWithOffset(1, 20);

        }


    }


}
