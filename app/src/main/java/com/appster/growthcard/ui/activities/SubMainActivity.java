package com.appster.growthcard.ui.activities;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.adapter.NavigationDrawerAdapter;
import com.appster.growthcard.interfaces.OnFragmentInteractionListener;
import com.appster.growthcard.model.NavItems;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.MarkAdherenceReq;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.requests.SwitchReq;
import com.appster.growthcard.network.response.EADetailResponse;
import com.appster.growthcard.network.response.FeedsResponse;
import com.appster.growthcard.network.response.MarkAdherenceResponse;
import com.appster.growthcard.network.response.SignInResponse;
import com.appster.growthcard.ui.fragments.NotificationListFragment;
import com.appster.growthcard.ui.fragments.ProfileFragment;
import com.appster.growthcard.ui.fragments.SubDashboardFragment;
import com.appster.growthcard.ui.fragments.SubEAFragment;
import com.appster.growthcard.utils.AlarmReceiver;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SlidingLayer;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SubMainActivity extends RootActivity implements OnFragmentInteractionListener {

    private ArrayList<NavItems> mNavItems = new ArrayList<>();
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private RelativeLayout mDrawerPane;
    private LinearLayoutManager mLayoutManager;
    private NavigationDrawerAdapter adapter;
    private String[] nav_array;
    private ActionBarDrawerToggle mDrawerToggle;
    public Toolbar mToolbar;
    private FragmentManager mFragmentManager;
    private TextView tvUserName;
    private TextView tvUserDesig;
    private AppPrefrences appPref;
    private CircleImageView civUser;
    private ImageButton btnClose;
    private final ThreadLocal<SlidingLayer> sliderEAdone = new ThreadLocal<>();
    private SlidingLayer sliderEAComment;
    private ImageButton iBtnClosed;
    private Button btnCreateAnnouncemant;
    private Button btnMakeComment;
    private Button btnCreatePost;
    private Button btnCancel;
    private RelativeLayout relSlidingComment;
    private RelativeLayout relCommentCurve;
    private Animation mSlideIn;
    private Animation mSlideOut;
    private ImageButton iBtnClosedRight;
    private RelativeLayout relDoneCurve;
    private TextView tvEAName;
    private RelativeLayout newOverlaymain;
    private RelativeLayout dailyOverlay;
    private ImageButton btnClosedRightmain;
    private TextView tvEAtag;
    private TextView tvEAtag2;
    private TextView tvcheckDetails;
    private TextView tvEATitle;
    private TextView tvSubordinateName;
    private CircleImageView civSubordinateImage;
    private Retrofit retrofit;
    private RelativeLayout menu;
    private Button btnSwitch;
    private int flag = 1;
    private boolean isOnFeed;
    private FloatingActionButton fab;

    private TextView tvLatestAnnoucement;
    private Button btnShowAll;
    private CircleImageView civManagerImage;
    private TextView tvManagerName;
    private TextView tvManagerDesignation;
    private TextView tvSubjectAnnoucement;
    private TextView tvTime_Stamp;
    private RelativeLayout parentRecent;
    private TextView tvDailyOverlayEAName;
    private ImageButton btnClosedDaily;
    private CountDownTimer sliderTimer;
//    private TextView txtSwipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_main);

        appPref = new AppPrefrences(SubMainActivity.this);

        if (appPref.getEAId() != 0) {
            setDailyAlarm();

            if (appPref.getAdherenceRandomNum() != 0) {
                generateRandomNum();
            }
        }


        initUI();
        if (getIntent().getStringExtra("NOTIFICATION") != null) {

            if (!getIntent().getBooleanExtra("SWITCH", false)) {
                String notify = getIntent().getStringExtra("NOTIFICATION");
                handleNotification(notify);
            } else {
                switchUser(true);
                return;
            }

        }
        setupData();


    }

    private void switchUser(final boolean isNotify) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SubMainActivity.this)) ///to add header
                .build();

        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<SignInResponse> ip = service.fSwitch(getPayload());

        ip.enqueue(new ErrorCallback.MyCallback<SignInResponse>() {
            @Override
            public void success(final Response<SignInResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message() + " " + response.body().getResult().getEmail());


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        Utils.saveUserDetails(response.body(), SubMainActivity.this);
                        Intent intent = new Intent(SubMainActivity.this, ManagerMainActivity.class);
                        NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        nMgr.cancelAll();
                        String i = SubMainActivity.this.getIntent().getStringExtra("NOTIFICATION");
                        if (isNotify)
                            intent.putExtra("NOTIFICATION", i);

                        startActivity(intent);
                        finish();
                    }
                });
            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);
            }
        }, SubMainActivity.this, true);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header_profile:
            case R.id.profile_image:
            case R.id.tv_nav_title:
            case R.id.tv_nav_sub_title:
                selectItemFromDrawer(0);
                break;

            case R.id.btnShowAll:
                mDrawerLayout.closeDrawers();
                startActivity(new Intent(this, SubordinateAnnoucementList.class));
                break;

            case R.id.fab:
                if (appPref.getTeamId() != 0) {
                    Intent postIntent = new Intent(SubMainActivity.this, NewPostActivity.class);
                    startActivity(postIntent);
                } else {
                    new SweetAlertDialog(SubMainActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(getString(R.string.no_team_assigned))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }
                break;

            case R.id.btnSwitch:
                switchUser(false);
                break;

            case R.id.btnClose:
                mDrawerLayout.closeDrawers();
                break;

            case R.id.btnClosed:
                sliderEAComment.closeLayer(true);
                break;

            case R.id.btnCancel:
                sliderEAComment.closeLayer(true);
                break;

            case R.id.btnMakeComment:
                if (appPref.getEAId() != 0) {
                    Intent i = new Intent(SubMainActivity.this, EADetailsActivity.class);
                    i.putExtra("isCurrent", true);
                    i.putExtra("EAid", appPref.getEAId());
                    i.putExtra("USER", appPref.getUserId());
                    i.putExtra("TEAM", appPref.getTeamId());
                    startActivity(i);
                } else {
                    new SweetAlertDialog(SubMainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.alert))
                            .setContentText(getString(R.string.no_ea_assigned))
                            .show();
                }
                break;

            case R.id.btnClosedRight:
                sliderEAdone.get().closeLayer(true);
                break;

            case R.id.btnClosedRightmain:
                newOverlaymain.setVisibility(View.GONE);
                mToolbar.setVisibility(View.VISIBLE);
                break;

            case R.id.btnCloseDaily:
                dailyOverlay.setVisibility(View.GONE);
                mToolbar.setVisibility(View.VISIBLE);
                break;

            case R.id.btnCreatePost:
                sliderEAComment.closeLayer(true);
                if (appPref.getTeamId() != 0) {
                    Intent intent = new Intent(SubMainActivity.this, NewPostActivity.class);
                    startActivity(intent);
                } else {
                    new SweetAlertDialog(SubMainActivity.this, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("")
                            .setContentText(getString(R.string.no_team_assigned))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .show();
                }
                break;
        }
    }


    protected void initUI() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout1);
        btnClose = (ImageButton) findViewById(R.id.btnClose);
        iBtnClosedRight = (ImageButton) findViewById(R.id.btnClosedRight);
        sliderEAdone.set((SlidingLayer) findViewById(R.id.EAdone));
        sliderEAComment = (SlidingLayer) findViewById(R.id.EAComment);
        iBtnClosed = (ImageButton) findViewById(R.id.btnClosed);
        btnCreateAnnouncemant = (Button) findViewById(R.id.btnCreateAnnouncemant);
        btnMakeComment = (Button) findViewById(R.id.btnMakeComment);
        btnCreatePost = (Button) findViewById(R.id.btnCreatePost);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        relSlidingComment = (RelativeLayout) findViewById(R.id.relSlidingComment);
        relCommentCurve = (RelativeLayout) findViewById(R.id.relCommentCurve);
        relDoneCurve = (RelativeLayout) findViewById(R.id.relDoneCurve);
        tvDailyOverlayEAName = (TextView) findViewById(R.id.txtEANameOverlay);
        newOverlaymain = (RelativeLayout) findViewById(R.id.relOverlaymain);
        dailyOverlay = (RelativeLayout) findViewById(R.id.relOverlay);
        tvEAName = (TextView) findViewById(R.id.txtEAName);
        tvEAtag = (TextView) findViewById(R.id.txtEAtag);
        tvEAtag2 = (TextView) findViewById(R.id.txtEAtag2);
        tvcheckDetails = (TextView) findViewById(R.id.txtcheckDetails);
        tvEATitle = (TextView) findViewById(R.id.txtEATitle);
        tvSubordinateName = (TextView) findViewById(R.id.txtSubordinateName);
        btnClosedRightmain = (ImageButton) findViewById(R.id.btnClosedRightmain);
        btnClosedDaily = (ImageButton) findViewById(R.id.btnCloseDaily);
        civSubordinateImage = (CircleImageView) findViewById(R.id.imgSubordinateImage);
        menu = (RelativeLayout) findViewById(R.id.menu);
//        txtSwipe = (TextView) findViewById(R.id.txtSwipe);
        fab = (FloatingActionButton) findViewById(R.id.fab);


        mDrawerLayout.setScrimColor(Color.TRANSPARENT);
        // Populate the Navigtion Drawer with options
        mDrawerPane = (RelativeLayout) findViewById(R.id.slider);
        mDrawerList = (ListView) findViewById(R.id.navList);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mFragmentManager = getSupportFragmentManager();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_hamburger);
        }


     /*   String text = "<font color=#6f7480>Swipe E</font><font color=#fbfbfb>ffective A</font> <font color=#6f7480>ction</font>";


        txtSwipe.setText(Html.fromHtml(text));*/
        mToolbar.getBackground().setAlpha(0);
        btnClose.setOnClickListener(this);
        iBtnClosed.setOnClickListener(this);
        iBtnClosedRight.setOnClickListener(this);
        btnClosedRightmain.setOnClickListener(this);
        btnClosedDaily.setOnClickListener(this);
        btnCreatePost.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        btnMakeComment.setOnClickListener(this);
        fab.setOnClickListener(this);

        Utils.setTextStyleButton(btnCreateAnnouncemant, this, Utils.MEDIUM);
        Utils.setTextStyleButton(btnMakeComment, this, Utils.MEDIUM);
        Utils.setTextStyleButton(btnCreatePost, this, Utils.MEDIUM);
        Utils.setTextStyleButton(btnCancel, this, Utils.MEDIUM);
        Utils.setTextStyle(tvEAName, this, Utils.REGULAR);
        Utils.setTextStyle(tvEAtag, this, Utils.REGULAR);
        Utils.setTextStyle(tvEAtag2, this, Utils.REGULAR);
        Utils.setTextStyle(tvcheckDetails, this, Utils.REGULAR);
        Utils.setTextStyle(tvEATitle, this, Utils.REGULAR);
        Utils.setTextStyle(tvSubordinateName, this, Utils.REGULAR);

        if (appPref.getEAId() == 0) {
            newOverlaymain.setVisibility(View.GONE);
            dailyOverlay.setVisibility(View.GONE);

            mToolbar.setVisibility(View.VISIBLE);
        } else {
            mToolbar.setVisibility(View.GONE);
        }


        tvSubordinateName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        tvEAName.setText(appPref.geteaTitle());
        tvDailyOverlayEAName.setText(appPref.geteaTitle());
        tvEATitle.setText(appPref.geteaTitle());


        Utils.loadUrlImage(appPref.getProfileImage(), civSubordinateImage);

        sliderEAdone.get().setChangeStateOnTap(false);
        sliderEAdone.get().setStickTo(SlidingLayer.STICK_TO_LEFT);
        setupLayerOffset(true);
        sliderEAdone.get().setOnInteractListener(new SlidingLayer.OnInteractListener() {
            @Override
            public void onOpen() {
                iBtnClosedRight.setVisibility(View.VISIBLE);

                Log.d("mSlidingLayer ", "onopen");
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setElevation(0);
                }

                mToolbar.setVisibility(View.GONE);
                if (appPref.getEAId() != 0) {
                    sliderTimer = new CountDownTimer(1000, 250) {
                        public void onTick(long millisUntilFinished) {
                        }

                        public void onFinish() {
                            if (sliderEAdone.get().isOpened())
                                sliderEAdone.get().closeLayer(true);
                        }
                    }.start();

                    if (isTodayWorkingDay()) {
                        markDone();
                    } else {
                        new SweetAlertDialog(SubMainActivity.this, SweetAlertDialog.NORMAL_TYPE)
                                .setTitleText(getString(R.string.alert))
                                .showCancelButton(true)
                                .setCancelText(getString(R.string.cancel))
                                .setContentText(getString(R.string.mark_adherence_on_other_day))
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                    }
                                })
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.dismiss();
                                        markDone();
                                    }
                                })
                                .show();
                    }
                } else {

                    new SweetAlertDialog(SubMainActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getString(R.string.alert))
                            .setContentText(getString(R.string.no_ea_assigned))
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    sliderEAdone.get().closeLayer(true);
                                }
                            })
                            .show();

                }

            }

            @Override
            public void onShowPreview() {
                Log.d("mSlidingLayer ", "show preview");
            }

            @Override
            public void onClose() {
                Log.d("mSlidingLayer ", "onclose");
                iBtnClosedRight.setVisibility(View.GONE);


            }

            public void onOpened() {
                Log.d("mSlidingLayer ", "onopened");
            }

            @Override
            public void onPreviewShowed() {
                Log.d("mSlidingLayer ", "onpreviewshowed");
            }

            @Override
            public void onClosed() {
                Log.d("mSlidingLayer ", "onclosed");
                sliderEAComment.setSlidingEnabled(true);
                sliderEAdone.get().setSlidingEnabled(true);

                if (getSupportActionBar() != null) {
                    getSupportActionBar().setElevation(6);
                }

                if (dailyOverlay.getVisibility() != View.VISIBLE)
                    mToolbar.setVisibility(View.VISIBLE);
/*
                if (sliderTimer != null)
                    sliderTimer.cancel();*/

            }

            @Override
            public void onMoved(float percentMoved) {
                Log.d("mSlidingLayer ", "Percent Moved" + percentMoved);

                if (percentMoved >= 13)

                {
                    sliderEAComment.setSlidingEnabled(false);
                    sliderEAdone.get().setSlidingEnabled(true);
                }
                if (percentMoved >= 50)

                {
                    sliderEAComment.setVisibility(View.GONE);

                } else {
                    sliderEAComment.setVisibility(View.VISIBLE);
                }


            }
        });

        sliderEAComment.setChangeStateOnTap(false);
        sliderEAComment.setStickTo(SlidingLayer.STICK_TO_RIGHT);

        relDoneCurve.post(new Runnable() {
            @Override
            public void run() {

                DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) sliderEAdone.get().getLayoutParams();
                layoutParams.setMargins(0, 0, -relDoneCurve.getWidth(), 0);

            }
        });


        relCommentCurve.post(new Runnable() {
            @Override
            public void run() {

                DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) sliderEAComment.getLayoutParams();
                layoutParams.setMargins(-relCommentCurve.getWidth(), 0, 0, 0);

            }
        });

        sliderEAComment.setOnInteractListener(new SlidingLayer.OnInteractListener() {
            @Override
            public void onOpen() {
                Log.d("mSlidingLayer ", "onopen");
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setElevation(0);
                }
                mToolbar.setVisibility(View.GONE);
                btnClosedDaily.setVisibility(View.GONE);

            }

            @Override
            public void onShowPreview() {
                Log.d("mSlidingLayer ", "show preview");
            }

            @Override
            public void onClose() {
                Log.d("mSlidingLayer ", "onclose");
                sliderEAdone.get().setVisibility(View.VISIBLE);
                sliderEAComment.setVisibility(View.VISIBLE);
                btnClosedDaily.setVisibility(View.VISIBLE);

            }

            public void onOpened() {
                Log.d("mSlidingLayer ", "onopened");

            }

            @Override
            public void onPreviewShowed() {
                Log.d("mSlidingLayer ", "onpreviewshowed");
            }

            @Override
            public void onClosed() {
                Log.d("mSlidingLayer ", "onclosed");
                sliderEAComment.setSlidingEnabled(true);
                sliderEAdone.get().setSlidingEnabled(true);
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setElevation(6);
                }
                if (dailyOverlay.getVisibility() != View.VISIBLE)
                    mToolbar.setVisibility(View.VISIBLE);


            }

            @Override
            public void onMoved(float percentMoved) {
                Log.d("mSlidingLayer ", "Percent Moved" + percentMoved);

                if (percentMoved >= 13) {
                    sliderEAdone.get().setSlidingEnabled(false);
                    sliderEAComment.setSlidingEnabled(true);
                }
                if (percentMoved >= 50) {
                    sliderEAdone.get().setVisibility(View.GONE);

                } else {
                    sliderEAdone.get().setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private boolean isTodayWorkingDay() {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Log.d("selected days", appPref.getWorkDays());

        if (appPref.getWorkDays() != null) {
            switch (day) {
                case Calendar.SUNDAY:
                    // Current day is Sunday
                    if ((appPref.getWorkDays().charAt(6) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;

                case Calendar.MONDAY:
                    // Current day is Monday
                    if ((appPref.getWorkDays().charAt(0) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;


                case Calendar.TUESDAY:

                    if ((appPref.getWorkDays().charAt(1) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;


                case Calendar.WEDNESDAY:

                    if ((appPref.getWorkDays().charAt(2) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;

                case Calendar.THURSDAY:

                    if ((appPref.getWorkDays().charAt(3) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;

                case Calendar.FRIDAY:

                    if ((appPref.getWorkDays().charAt(4) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;


                case Calendar.SATURDAY:

                    if ((appPref.getWorkDays().charAt(5) + "").equalsIgnoreCase("1")) {
                        return true;
                    }
                    break;


            }
        }
        return false;
    }


    private void setupLayerOffset(boolean enabled) {
        int offsetDistance = enabled ? getResources().getDimensionPixelOffset(R.dimen.offset_distance) : 0;
        sliderEAdone.get().setOffsetDistance(offsetDistance);
        sliderEAComment.setOffsetDistance(offsetDistance);
    }

    private void addFragmentStack(String title, Fragment fragment) {
        List<Fragment> list;
        FragmentTransaction ft = mFragmentManager.beginTransaction();
        boolean flag = false;
        try {
            list = mFragmentManager.getFragments();
            String tag = list.get(list.size() - 1).getTag();

            if (tag != null)
                flag = tag.equals(title);

        } catch (Exception e) {
            e.printStackTrace();

        }
        if (!flag) {

            if (title.equals(getString(R.string.post_fragment))) {
                fab.setVisibility(View.VISIBLE);
            } else {
                fab.setVisibility(View.INVISIBLE);

            }

            ft.replace(R.id.content_frame, fragment, title);
            ft.addToBackStack(title);
            ft.commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.loadUrlImage(appPref.getProfileImage(), civUser);
        adapter.notifyDataSetChanged();


        if (appPref.getUserId() == 0 && appPref.getUserToken().equals("")) {
            startActivity(new Intent(SubMainActivity.this, LoginActivity.class));

            finish();
        }

        if (appPref.isSubNotification()) {
            updateNavigationMenu();
        }


    }

    public void updateOverlay() {
        tvEATitle.setText(appPref.geteaTitle());
        tvEAName.setText(appPref.geteaTitle());
        newOverlaymain.setVisibility(View.VISIBLE);
        dailyOverlay.setVisibility(View.GONE);
        mToolbar.setVisibility(View.GONE);

    }

    public void updateNavigationMenu() {

        for (int i = 0; i < mNavItems.size(); i++) {
            if (mNavItems.get(i).getmTitle().equalsIgnoreCase(nav_array[2]) && appPref.isSubNotification()) {
                mNavItems.get(i).setIsNotifications(true);
            } else {
                mNavItems.get(i).setIsNotifications(false);

            }


        }

        if (adapter != null)
            adapter.notifyDataSetChanged();
    }

    protected void setupData() {

        nav_array = getResources().getStringArray(R.array.subordinate_navigation_menu);
        for (int i = 0; i < nav_array.length; i++) {

            mNavItems.add(new NavItems(nav_array[i]));
            if (nav_array[i].equalsIgnoreCase(nav_array[2]) && appPref.isSubNotification()) {
                mNavItems.get(i).setIsNotifications(true);
            } else {
                mNavItems.get(i).setIsNotifications(false);
            }
        }

        mNavItems.get(0).setIsSelected(true);
        adapter = new NavigationDrawerAdapter(mNavItems, SubMainActivity.this);
        mDrawerList.setAdapter(adapter);

        LayoutInflater inflater = getLayoutInflater();
        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.menu_profile_header, mDrawerList, false);
        ViewGroup footer = (ViewGroup) inflater.inflate(R.layout.menu_profile_footer, mDrawerList, false);


        btnSwitch = (Button) header.findViewById(R.id.btnSwitch);
        btnShowAll = (Button) footer.findViewById(R.id.btnShowAll);
        civManagerImage = (CircleImageView) footer.findViewById(R.id.imgManagerImage);
        parentRecent = (RelativeLayout) footer.findViewById(R.id.parentRecent);
        tvLatestAnnoucement = (TextView) footer.findViewById(R.id.txtLatestAnnoucement);
        tvManagerName = (TextView) footer.findViewById(R.id.txtManagerName);
        tvManagerDesignation = (TextView) footer.findViewById(R.id.txtManagerDesignation);
        tvSubjectAnnoucement = (TextView) footer.findViewById(R.id.txtSubjectAnnoucement);
        tvTime_Stamp = (TextView) footer.findViewById(R.id.txtTime_Stamp);
        parentRecent.setVisibility(View.INVISIBLE);


        Utils.setTextStyleButton(btnShowAll, SubMainActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvLatestAnnoucement, SubMainActivity.this, Utils.LIGHT);
        Utils.setTextStyle(tvManagerName, SubMainActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvManagerDesignation, SubMainActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvSubjectAnnoucement, SubMainActivity.this, Utils.REGULAR);
        Utils.setTextStyle(tvTime_Stamp, SubMainActivity.this, Utils.REGULAR);


        header.setClickable(true);
        header.setOnClickListener(this);

        if (appPref.getSwitch() == 1) {
            btnSwitch.setVisibility(View.VISIBLE);
        }
        btnSwitch.setOnClickListener(this);


        if (getIntent().getBooleanExtra("OVERLAY", false)) {
            dailyOverlay.setVisibility(View.GONE);
            newOverlaymain.setVisibility(View.GONE);
            mToolbar.setVisibility(View.VISIBLE);
        }


        mDrawerList.addHeaderView(header, null, false);
        mDrawerList.addFooterView(footer);
        tvUserName = (TextView) header.findViewById(R.id.tv_nav_title);
        civUser = (CircleImageView) header.findViewById(R.id.profile_image);
        tvUserDesig = (TextView) header.findViewById(R.id.tv_nav_sub_title);

        tvUserName.setOnClickListener(this);
        tvUserDesig.setOnClickListener(this);
        btnSwitch.setOnClickListener(this);
        btnShowAll.setOnClickListener(this);

        Utils.setTextStyle(tvUserName, SubMainActivity.this, Utils.MEDIUM);
        Utils.setTextStyle(tvUserDesig, SubMainActivity.this, Utils.REGULAR);
        Utils.setTextStyleButton(btnSwitch, SubMainActivity.this, Utils.MEDIUM);

        if (!appPref.getProfileImage().isEmpty())
            Utils.loadUrlImage(appPref.getProfileImage(), civUser);

        tvUserName.setText(appPref.getFirstName() + " " + appPref.getLastName());
        tvUserDesig.setText(appPref.getDesignation());


        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                selectItemFromDrawer(position);
            }


        });

        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        mDrawerToggle = new ActionBarDrawerToggle(SubMainActivity.this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);


                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(false); //disable "hamburger to arrow" drawable
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.ic_hamburger); //set your own
        mDrawerToggle.syncState();


        mDrawerLayout.setDrawerListener(mDrawerToggle);
        selectItemFromDrawer(1);
    }

    public void updateRecentAnnouncement(FeedsResponse.RecentAnnouncement recentAnnouncement) {

        if (recentAnnouncement != null) {
            parentRecent.setVisibility(View.VISIBLE);

            tvManagerName.setText(recentAnnouncement.getUser().getFirstName() + " " + recentAnnouncement.getUser().getLastName());
            tvManagerDesignation.setText(recentAnnouncement.getMember().getDesignation().getDesignationTitle());
            tvSubjectAnnoucement.setText(recentAnnouncement.getContent());

            Utils.loadUrlImage(recentAnnouncement.getUser().getProfileImage(), civManagerImage);

            try {
                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                long l = outputFmt.parse(recentAnnouncement.getCreatedAt().getDate()).getTime();
                String str = DateUtils.getRelativeDateTimeString(

                        SubMainActivity.this, // Suppose you are in an activity or other Context subclass

                        l, // The time to display

                        DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                        // minutes (no "3 seconds ago")


                        DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                        // to default date instead of spans. This will not
                        // display "3 weeks ago" but a full date instead

                        0).toString();
                if (str.contains(",")) {
                    tvTime_Stamp.setText(str.split(",")[0]);
                } else {
                    tvTime_Stamp.setText(str);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else
            parentRecent.setVisibility(View.INVISIBLE);
    }


    public void selectItemFromDrawer(final int position) {


        updateMenu(position);
        mDrawerLayout.closeDrawer(GravityCompat.START);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                switch (position) {

                    case 0:
                        sliderEAdone.get().setVisibility(View.GONE);
                        sliderEAComment.setVisibility(View.GONE);
                        isOnFeed = false;
                        fab.setVisibility(View.INVISIBLE);

                        addFragmentStack(getString(R.string.profile_fragment), new ProfileFragment());

                        break;
                    case 1:
                        sliderEAdone.get().setVisibility(View.VISIBLE);
                        sliderEAComment.setVisibility(View.VISIBLE);
                        isOnFeed = true;
                        fab.setVisibility(View.INVISIBLE);

                        addFragmentStack(getString(R.string.feed_fragment), new SubDashboardFragment());

                        break;

                    case 2:
                        sliderEAdone.get().setVisibility(View.VISIBLE);
                        sliderEAComment.setVisibility(View.VISIBLE);
                        isOnFeed = false;
                        fab.setVisibility(View.INVISIBLE);

                        addFragmentStack(getString(R.string.ea_fragment), new SubEAFragment());
                        break;

//                    case 3:
//                        sliderEAdone.setVisibility(View.VISIBLE);
//                        sliderEAComment.setVisibility(View.VISIBLE);
//                        isOnFeed = false;
//
//                        addFragmentStack(getString(R.string.post_fragment), new PostListFragment());
//                        fab.setVisibility(View.VISIBLE);
//                        break;

                    case 3:
                        sliderEAdone.get().setVisibility(View.VISIBLE);
                        sliderEAComment.setVisibility(View.VISIBLE);
                        isOnFeed = false;
                        fab.setVisibility(View.INVISIBLE);

                        addFragmentStack("Notification", new NotificationListFragment());
                        break;
                }

            }
        }, 300);

    }

    private void updateMenu(int position) {
        if (position > 0) {
            position--;
            for (int i = 0; i < mNavItems.size(); i++) {

                mNavItems.get(i).setIsSelected(false);
            }

            mNavItems.get(position).setIsSelected(true);
            adapter.notifyDataSetInvalidated();
            tvUserName.setAlpha(0.5f);

        } else {
            for (int i = 0; i < mNavItems.size(); i++) {

                mNavItems.get(i).setIsSelected(false);
            }
            adapter.notifyDataSetInvalidated();
            tvUserName.setAlpha(1);

        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        if (mDrawerToggle != null)
            mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null)
            mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        switch (itemId) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(menu);

                break;

        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        if (sliderEAdone.get().isOpened() || sliderEAComment.isOpened()) {
            if (sliderEAComment.isOpened())
                sliderEAComment.closeLayer(true);

            if (sliderEAdone.get().isOpened())
                sliderEAdone.get().closeLayer(true);

            return;
        }

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            finish();
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }


    private void markDone() {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SubMainActivity.this))
                .build();


        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<MarkAdherenceResponse> ip = service.markAdherence(getMarkDonePayload());
        ip.enqueue(new ErrorCallback.MyCallback<MarkAdherenceResponse>() {
            @Override
            public void success(final Response<MarkAdherenceResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.body());


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        sliderEAdone.get().closeLayer(true);

                        if (response.body().getResult() != null && response.body().getResult().size() > 0) {
                            if (response.body().getResult().get(0).getName() != null) {
                                Utils.imageUrl = response.body().getResult().get(0).getName();
                                Utils.imageCaption = response.body().getResult().get(0).getCaption();
                                Utils.imageMessage = response.body().getResult().get(0).getMessage();
                            }
                        }

                        try {
                            String tag = mFragmentManager.getFragments().get(mFragmentManager.getFragments().size() - 1).getTag();

                            if (tag != null && tag.equalsIgnoreCase(getString(R.string.feed_fragment))) {
                                ((SubDashboardFragment) mFragmentManager.getFragments().get(mFragmentManager.getFragments().size() - 1)).refreshData();
                            }
                            if (tag != null && tag.equalsIgnoreCase(getString(R.string.ea_fragment))) {
                                ((SubEAFragment) mFragmentManager.getFragments().get(mFragmentManager.getFragments().size() - 1)).refreshData();
                            }


                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                        if (appPref.getAdherenceRandomNum() == appPref.getAdherenceRandomCount()) {
                            sliderEAdone.get().closeLayer(true);
                            appPref.setAdherenceRandomCount(0);
                            generateRandomNum();
                            startActivity(new Intent(SubMainActivity.this, RandomImageActivity.class));
                        } else {
                            appPref.setAdherenceRandomCount(appPref.getAdherenceRandomCount() + 1);
                        }


                    }
                });
            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SubMainActivity.this, false);

    }

    private MarkAdherenceReq getMarkDonePayload() {

        MarkAdherenceReq req = new MarkAdherenceReq();
        req.setDeviceId(Utils.getDeviceId(SubMainActivity.this));
        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setEffectiveActionId(appPref.getEAId());
        req.setSubordinateId(0);
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());


        return req;

    }


    private SwitchReq getPayload() {
        SwitchReq switchReq = new SwitchReq();
        switchReq.setDeviceId(Utils.getDeviceId(SubMainActivity.this));
        switchReq.setFlag(flag);
        switchReq.setUserId(appPref.getUserId());
        switchReq.setUserToken(appPref.getUserToken());
        return switchReq;

    }

    /**
     *
     */
    public void setDailyAlarm() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        alarmIntent.putExtra("ID", 123);
        Log.d("setRepeatedNotification", "ID:" + 123);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(SubMainActivity.this, 123, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 15);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        //check whether the time is earlier than current time. If so, set it to tomorrow. Otherwise, all alarms for earlier time will fire

        if (calendar.before(now)) {
            calendar.add(Calendar.DATE, 1);
        }

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);

    }

    private void handleNotification(String notify) {
        try {
            JSONObject bodyJson = new JSONObject(notify);

            appPref.setSubNotification(false);

            String type = bodyJson.getString("type");

            if (type.equals(getString(R.string.notification_type_1))) {//3pm notification local
                dailyOverlay.setVisibility(View.VISIBLE);
                newOverlaymain.setVisibility(View.GONE);

            } else if (type.equals(getString(R.string.notification_type_2))) {//EA assigned
                getCurrentEADetails();
                //     mToolbar.setVisibility(View.VISIBLE);
/*
                if (appPref.getEAId() != 0) {
                    Intent i = new Intent(SubMainActivity.this, EADetailsActivity.class);
                    i.putExtra("isCurrent", Integer.parseInt(bodyJson.getString("effectiveActionId")) == appPref.getEAId());
                    i.putExtra("EAid", Integer.parseInt(bodyJson.getString("effectiveActionId")));
                    i.putExtra("USER", appPref.getUserId());
                    i.putExtra("TEAM", appPref.getTeamId());

                    i.putExtra("NEW_EA", true);
                    startActivity(i);
                }*/

            } else if (type.equals(getString(R.string.notification_type_3))) {// Comment on EA
                dailyOverlay.setVisibility(View.GONE);
                newOverlaymain.setVisibility(View.GONE);
                mToolbar.setVisibility(View.VISIBLE);


                if (appPref.getEAId() != 0) {
                    Intent i = new Intent(SubMainActivity.this, EADetailsActivity.class);
                    i.putExtra("isCurrent", Integer.parseInt(bodyJson.getString("effectiveActionId")) == appPref.getEAId());
                    i.putExtra("EAid", Integer.parseInt(bodyJson.getString("effectiveActionId")));
                    i.putExtra("USER", appPref.getUserId());
                    i.putExtra("TEAM", appPref.getTeamId());

                    startActivity(i);
                }

            } else if (type.equals(getString(R.string.notification_type_4))) {/// Likes

                dailyOverlay.setVisibility(View.GONE);
                newOverlaymain.setVisibility(View.GONE);
                mToolbar.setVisibility(View.VISIBLE);


            } else if (type.equals(getString(R.string.notification_type_5))) {/// Announcement

                dailyOverlay.setVisibility(View.GONE);
                newOverlaymain.setVisibility(View.GONE);
                mToolbar.setVisibility(View.VISIBLE);


                Intent intent = new Intent(SubMainActivity.this, SubordinateAnnoucementList.class);
                startActivity(intent);
            } else if (type.equals(getString(R.string.notification_type_6))) {/// Team Assigned

            } else if (type.equals(getString(R.string.notification_type_7))) {/// Ea Delete

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void generateRandomNum() {
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((50 - 1) + 1) + 1;
        appPref.setAdherenceRandomNum(randomNum);


    }

    private void getCurrentEADetails() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(SubMainActivity.this)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<EADetailResponse> ip = service.getEADetails(getEADetailPayload());

        ip.enqueue(new ErrorCallback.MyCallback<EADetailResponse>() {
            @Override
            public void success(final Response<EADetailResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());
                appPref.setEATitle(response.body().getResult().getEffectiveActionTitle());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        appPref.setEATitle(response.body().getResult().getEffectiveActionTitle());
                        appPref.setEADescription(response.body().getResult().getEffectiveActionDescription());

                        tvEATitle.setText(response.body().getResult().getEffectiveActionTitle());
                        newOverlaymain.setVisibility(View.VISIBLE);
                        dailyOverlay.setVisibility(View.GONE);
                        mToolbar.setVisibility(View.GONE);
                    }
                });

            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, SubMainActivity.this, true);

    }


    private SendCommentsReq getEADetailPayload() {
        SendCommentsReq req = new SendCommentsReq();
        int getFlag = 1;


        req.setUserId(appPref.getUserId());
        req.setUserToken(appPref.getUserToken());
        req.setDeviceId(Utils.getDeviceId(SubMainActivity.this));
        req.setTeamId(appPref.getTeamId());
        req.setEffectiveActionId(appPref.getEAId());
        req.setFlag(getFlag);
        req.setSubordinateId(appPref.getUserId());

        return req;
    }


}

