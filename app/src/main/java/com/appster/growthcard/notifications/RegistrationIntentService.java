package com.appster.growthcard.notifications;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.appster.growthcard.ui.activities.SplashScreen;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import java.io.IOException;

/**
 * Created by navdeep on 22/02/16.
 */
public class RegistrationIntentService extends IntentService {

    // abbreviated tag name
    private static final String TAG = "RegIntentService";
//    private static final String[] TOPICS = {"msgtopic"};
    private AppPrefrences appPrefrences;

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        // Make a call to Instance API
        appPrefrences = new AppPrefrences(getApplication());
        InstanceID instanceID = InstanceID.getInstance(this);
        String senderId = GoogleConstant.SENDER_ID;//getResources().getString(R.string.gcm_SenderId);
        try {
            // request token that will be used by the server to send push notifications
            String token = instanceID.getToken(senderId, GoogleCloudMessaging.INSTANCE_ID_SCOPE);
            Log.d(TAG, "GCM Registration Token: " + token);

            // pass along this data

            appPrefrences.setGCMDeviceToken(token);
            appPrefrences.setGCMRegistrationStatus(true);

            if (!(intent != null && intent.getStringExtra("FROMSERVER") != null && intent.getStringExtra("FROMSERVER").equalsIgnoreCase("fromserver")))
                sendRegistrationToServer(token);
            else {
                Intent dialogIntent = new Intent(this, SplashScreen.class);
                dialogIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(dialogIntent);
            }
//            subscribeTopics(token);
        } catch (IOException e) {
            e.printStackTrace();
            sendRegistrationToServer("");
            appPrefrences.setGCMRegistrationStatus(false);
        }


    }

    private void sendRegistrationToServer(String token) {

        Intent registrationComplete = new Intent(Utils.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
//    private void subscribeTopics(String token) throws IOException {
//        GcmPubSub pubSub = GcmPubSub.getInstance(this);
//        for (String topic : TOPICS) {
//            pubSub.subscribe(token, "/topics/" + topic, null);
//        }
//    }
}