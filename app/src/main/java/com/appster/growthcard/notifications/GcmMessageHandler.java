package com.appster.growthcard.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.response.EADetailResponse;
import com.appster.growthcard.ui.activities.ManagerMainActivity;
import com.appster.growthcard.ui.activities.NotifyDialogActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.ApplicationClass;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.google.android.gms.gcm.GcmListenerService;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by navdeep on 22/02/16.
 */
public class GcmMessageHandler extends GcmListenerService {
    public static final int MESSAGE_NOTIFICATION_ID = 435345;
    private static final String TAG = "PUSH NOTIFICATION";
    public static final String NOTIFICATION_MSG = "NOTIFICATION_MSG";
    public static final String NOTIFICATION_TYPE = "NOTIFICATION_TYPE";
    private AppPrefrences appPref;
    private Intent resultIntent;
    private JSONObject bodyJson;

    @Override
    public void onMessageReceived(String from, Bundle data) {
//        String message = data.getString("message");
//
        try {
            Log.d(TAG, "Notification Received from-" + from + " " + data.getString("payload"));
            String body = data.getString("payload");
            Context context = getBaseContext();
            appPref = new AppPrefrences(context);
            bodyJson = new JSONObject(body);

            if (appPref.getUserId() != 0) {//&& appPref.getUserId() == Integer.parseInt(bodyJson.getString("userId"))) {
                createNotification(body);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void createNotification(String body) {
        Context context = getBaseContext();

        Log.d("ON receive", "Notification");
        try {


            if (ApplicationClass.isActivityVisible()) {


                if (bodyJson.getString("type").equals(context.getString(R.string.notification_type_8))) {
                    resultIntent = new Intent(context, ManagerMainActivity.class);
                    appPref.setManNotification(true);
                } else {
                    resultIntent = new Intent(context, SubMainActivity.class);
                    appPref.setSubNotification(true);

                }

                if ((bodyJson.getString("type").equals(context.getString(R.string.notification_type_2)))) {
                    appPref.setEAId(Integer.parseInt(bodyJson.getString("effectiveActionId")));

                }

                if ((bodyJson.getString("type").equals(context.getString(R.string.notification_type_6)))) {
                    appPref.setTeamId(Integer.parseInt(bodyJson.getString("teamId")));
                }

                if ((bodyJson.getString("type").equals(context.getString(R.string.notification_type_7)))) {
                    appPref.setEATitle("");
                    appPref.setEAId(0);
                }

                Intent noti_intent = new Intent(context, NotifyDialogActivity.class);
                noti_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                noti_intent.putExtra(NOTIFICATION_MSG, bodyJson.getString("message"));
                noti_intent.putExtra(NOTIFICATION_TYPE, bodyJson.getString("type"));
                context.startActivity(noti_intent);


            } else {

                if (bodyJson.getString("type").equals(context.getString(R.string.notification_type_8))) {
                    resultIntent = new Intent(context, ManagerMainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                } else {
                    resultIntent = new Intent(context, SubMainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                }
//
//                resultIntent.putExtra("SWITCH", false);

                if (bodyJson.getString("type").equals(context.getString(R.string.notification_type_8))) {
                    resultIntent = new Intent(context, ManagerMainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    appPref.setManNotification(true);
                } else {
                    resultIntent = new Intent(context, SubMainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    appPref.setSubNotification(true);

                }

                if (appPref.getRole() == 1) {//Manager

                    resultIntent = new Intent(context, ManagerMainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    resultIntent.putExtra("SWITCH", !bodyJson.getString("type").equals(context.getString(R.string.notification_type_8)));

                } else if (appPref.getRole() == 2) {//Subordinate

                    resultIntent = new Intent(context, SubMainActivity.class);
                    resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
                            Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    resultIntent.putExtra("SWITCH", bodyJson.getString("type").equals(context.getString(R.string.notification_type_8)));


                }


                if ((bodyJson.getString("type").equals(context.getString(R.string.notification_type_2)))) {
                    appPref.setEAId(Integer.parseInt(bodyJson.getString("effectiveActionId")));

                }

                if ((bodyJson.getString("type").equals(context.getString(R.string.notification_type_6)))) {
                    appPref.setTeamId(Integer.parseInt(bodyJson.getString("teamId")));
                }

                if ((bodyJson.getString("type").equals(context.getString(R.string.notification_type_7)))) {
                    appPref.setEATitle("");
                    appPref.setEAId(0);
                }


                resultIntent.putExtra("NOTIFICATION", body);

                int requestID = (int) System.currentTimeMillis();
                PendingIntent contentIntent = PendingIntent.getActivity(context, requestID, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);


                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setContentIntent(contentIntent)
                        .setSmallIcon(R.mipmap.ic_launcher).setContentTitle("GrowthCard")
                        .setPriority(Notification.PRIORITY_DEFAULT)
                        .setContentText(bodyJson.getString("message"))
                        .setAutoCancel(true)
                        .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

                NotificationCompat.BigTextStyle bigTextStyle =
                        new NotificationCompat.BigTextStyle();
                String[] events = new String[6];
// Sets a title for the Inbox in expanded layout
                bigTextStyle.setBigContentTitle("GrowthCard");
                bigTextStyle.bigText(bodyJson.getString("message"));
//
// Moves the expanded layout object into the notification object.
                mBuilder.setStyle(bigTextStyle);

                NotificationManager mNotificationManager = (NotificationManager) context
                        .getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(requestID, mBuilder.build());


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }






}