
package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AnnoucementListResponse {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private int statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public static class Announcement {

        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("announcementId")
        @Expose
        private int announcementId;
        @SerializedName("createdAt")
        @Expose
        private CreatedAt createdAt;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("member")
        @Expose
        private Member member;

        /**
         * @return The subject
         */
        public String getSubject() {
            return subject;
        }

        /**
         * @param subject The subject
         */
        public void setSubject(String subject) {
            this.subject = subject;
        }

        /**
         * @return The content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content The content
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return The announcementId
         */
        public int getAnnouncementId() {
            return announcementId;
        }

        /**
         * @param announcementId The announcementId
         */
        public void setAnnouncementId(int announcementId) {
            this.announcementId = announcementId;
        }

        /**
         * @return The createdAt
         */
        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The createdAt
         */
        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The user
         */
        public User getUser() {
            return user;
        }

        /**
         * @param user The user
         */
        public void setUser(User user) {
            this.user = user;
        }

        /**
         * @return The member
         */
        public Member getMember() {
            return member;
        }

        /**
         * @param member The member
         */
        public void setMember(Member member) {
            this.member = member;
        }

    }

    public static class CreatedAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private int timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The timezoneType
         */
        public int getTimezoneType() {
            return timezoneType;
        }

        /**
         * @param timezoneType The timezone_type
         */
        public void setTimezoneType(int timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         * @return The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         * @param timezone The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }

    public static class Designation {

        @SerializedName("designationId")
        @Expose
        private int designationId;
        @SerializedName("companyId")
        @Expose
        private int companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public int getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(int designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public int getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }

    public static class Member {

        @SerializedName("designationId")
        @Expose
        private int designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Object effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private int companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("teamId")
        @Expose
        private int teamId;
        @SerializedName("designation")
        @Expose
        private Designation designation;

        /**
         * @return The designationId
         */
        public int getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(int designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Object getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Object effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public int getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The teamId
         */
        public int getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(int teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The designation
         */
        public Designation getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation designation) {
            this.designation = designation;
        }

    }

    public static class Result {

        @SerializedName("pageNo")
        @Expose
        private int pageNo;
        @SerializedName("totalPages")
        @Expose
        private int totalPages;
        @SerializedName("announcements")
        @Expose
        private List<Announcement> announcements = new ArrayList<Announcement>();

        /**
         * @return The pageNo
         */
        public int getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The pageNo
         */
        public void setPageNo(int pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The totalPages
         */
        public int getTotalPages() {
            return totalPages;
        }

        /**
         * @param totalPages The totalPages
         */
        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        /**
         * @return The announcements
         */
        public List<Announcement> getAnnouncements() {
            return announcements;
        }

        /**
         * @param announcements The announcements
         */
        public void setAnnouncements(List<Announcement> announcements) {
            this.announcements = announcements;
        }

    }

    public static class User {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private int userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private int termsAccepted;

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public int getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(int userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public int getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(int termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

    }
}
