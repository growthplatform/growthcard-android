
package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class DepartmentResponse {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private int statusCode;
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();

    /**
     * @return The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }

    public static class Result {

        @SerializedName("departmentId")
        @Expose
        private int departmentId;
        @SerializedName("departmentName")
        @Expose
        private String departmentName;
        private boolean selected = false;

        /**
         * @return The departmentId
         */
        public int getDepartmentId() {
            return departmentId;
        }

        /**
         * @param departmentId The departmentId
         */
        public void setDepartmentId(int departmentId) {
            this.departmentId = departmentId;
        }

        /**
         * @return The departmentName
         */
        public String getDepartmentName() {
            return departmentName;
        }

        /**
         * @param departmentName The departmentName
         */
        public void setDepartmentName(String departmentName) {
            this.departmentName = departmentName;
        }


        public void setSelected(boolean selected) {
            this.selected = selected;
        }


        public boolean getSelected() {
            return selected;
        }

    }
}
