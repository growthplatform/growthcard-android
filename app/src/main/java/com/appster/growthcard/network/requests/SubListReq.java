package com.appster.growthcard.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubListReq {

    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("teamId")
    @Expose
    private Integer teamId;

    @SerializedName("pageNo")
    @Expose
    private Integer pageNo;

    @SerializedName("deviceId")
    @Expose
    private String deviceId;


    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The userToken
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The userToken
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The teamId
     */
    public Integer getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The pageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

}