
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostListReq {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("flag")
    @Expose
    private int flag;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    private String deviceId;
    private int subordinateId;
    private int pageNo;

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public int getSubordinateId() {
        return subordinateId;
    }

    public void setSubordinateId(int subordinateId) {
        this.subordinateId = subordinateId;
    }

    private int teamId;

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return The companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public int getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The pageNo
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

}
