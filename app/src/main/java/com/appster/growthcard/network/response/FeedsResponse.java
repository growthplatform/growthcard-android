
package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class FeedsResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }


    public static class Result_ {

        @SerializedName("feed")
        @Expose
        private List<Feed> feed = new ArrayList<Feed>();
        @SerializedName("recentAnnouncement")
        @Expose
        private RecentAnnouncement recentAnnouncement;

        /**
         * @return The feed
         */
        public List<Feed> getFeed() {
            return feed;
        }

        /**
         * @param feed The feed
         */
        public void setFeed(List<Feed> feed) {
            this.feed = feed;
        }

        /**
         * @return The recentAnnouncement
         */
        public RecentAnnouncement getRecentAnnouncement() {
            return recentAnnouncement;
        }

        /**
         * @param recentAnnouncement The recentAnnouncement
         */
        public void setRecentAnnouncement(RecentAnnouncement recentAnnouncement) {
            this.recentAnnouncement = recentAnnouncement;
        }

    }


    public static class Designation {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }


    public static class Designation_ {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }


    public static class Result {

        @SerializedName("pageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("result")
        @Expose
        private Result_ result;

        /**
         * @return The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The pageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The totalPages
         */
        public Integer getTotalPages() {
            return totalPages;
        }

        /**
         * @param totalPages The totalPages
         */
        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        /**
         * @return The result
         */
        public Result_ getResult() {
            return result;
        }

        /**
         * @param result The result
         */
        public void setResult(Result_ result) {
            this.result = result;
        }

    }


    public static class Member {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("teamId")
        @Expose
        private Integer teamId;
        @SerializedName("designation")
        @Expose
        private Designation designation;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The teamId
         */
        public Integer getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The designation
         */
        public Designation getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation designation) {
            this.designation = designation;
        }

    }


    public static class Graph {

        @SerializedName("adhrenceCount")
        @Expose
        private Integer adhrenceCount;
        @SerializedName("days")
        @Expose
        private String days;

        /**
         * @return The adhrenceCount
         */
        public Integer getAdhrenceCount() {
            return adhrenceCount;
        }

        /**
         * @param adhrenceCount The adhrenceCount
         */
        public void setAdhrenceCount(Integer adhrenceCount) {
            this.adhrenceCount = adhrenceCount;
        }

        /**
         * @return The days
         */
        public String getDays() {
            return days;
        }

        /**
         * @param days The days
         */
        public void setDays(String days) {
            this.days = days;
        }

    }

    public class Announcement {

        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("announcementId")
        @Expose
        private Integer announcementId;
        @SerializedName("createdAt")
        @Expose
        private CreatedAt createdAt;

        /**
         * @return The subject
         */
        public String getSubject() {
            return subject;
        }

        /**
         * @param subject The subject
         */
        public void setSubject(String subject) {
            this.subject = subject;
        }

        /**
         * @return The content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content The content
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return The announcementId
         */
        public Integer getAnnouncementId() {
            return announcementId;
        }

        /**
         * @param announcementId The announcementId
         */
        public void setAnnouncementId(Integer announcementId) {
            this.announcementId = announcementId;
        }

        /**
         * @return The createdAt
         */
        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The createdAt
         */
        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

    }


    public class RecentAnnouncement {

        @SerializedName("subject")
        @Expose
        private String subject;
        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("announcementId")
        @Expose
        private Integer announcementId;
        @SerializedName("createdAt")
        @Expose
        private CreatedAt createdAt;
        @SerializedName("user")
        @Expose
        private User_ user;
        @SerializedName("member")
        @Expose
        private Member_ member;

        /**
         * @return The subject
         */
        public String getSubject() {
            return subject;
        }

        /**
         * @param subject The subject
         */
        public void setSubject(String subject) {
            this.subject = subject;
        }

        /**
         * @return The content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content The content
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return The announcementId
         */
        public Integer getAnnouncementId() {
            return announcementId;
        }

        /**
         * @param announcementId The announcementId
         */
        public void setAnnouncementId(Integer announcementId) {
            this.announcementId = announcementId;
        }

        /**
         * @return The createdAt
         */
        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The createdAt
         */
        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The user
         */
        public User_ getUser() {
            return user;
        }

        /**
         * @param user The user
         */
        public void setUser(User_ user) {
            this.user = user;
        }

        /**
         * @return The member
         */
        public Member_ getMember() {
            return member;
        }

        /**
         * @param member The member
         */
        public void setMember(Member_ member) {
            this.member = member;
        }

    }


    public static class CreatedAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The timezoneType
         */
        public Integer getTimezoneType() {
            return timezoneType;
        }

        /**
         * @param timezoneType The timezone_type
         */
        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         * @return The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         * @param timezone The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }

    public class Comment {

        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("role")
        @Expose
        private Integer role;
        @SerializedName("commentId")
        @Expose
        private Integer commentId;
        @SerializedName("createdAt")
        @Expose
        private CreatedAt createdAt;
        @SerializedName("user")
        @Expose
        private User user;

        /**
         * @return The content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content The content
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return The role
         */
        public Integer getRole() {
            return role;
        }

        /**
         * @param role The role
         */
        public void setRole(Integer role) {
            this.role = role;
        }

        /**
         * @return The commentId
         */
        public Integer getCommentId() {
            return commentId;
        }

        /**
         * @param commentId The commentId
         */
        public void setCommentId(Integer commentId) {
            this.commentId = commentId;
        }

        /**
         * @return The createdAt
         */
        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The createdAt
         */
        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The user
         */
        public User getUser() {
            return user;
        }

        /**
         * @param user The user
         */
        public void setUser(User user) {
            this.user = user;
        }

    }

    public class Effectiveaction {


        @SerializedName("graph")
        @Expose
        private List<com.appster.growthcard.model.Graph> graph = new ArrayList<com.appster.growthcard.model.Graph>();
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionTitle")
        @Expose
        private String effectiveActionTitle;
        @SerializedName("assignEffectiveActionDate")
        @Expose
        private AssignEffectiveActionDate assignEffectiveActionDate;


        /**
         * @return The graph
         */
        public List<com.appster.growthcard.model.Graph> getGraph() {
            return graph;
        }

        /**
         * @param graph The graph
         */
        public void setGraph(List<com.appster.growthcard.model.Graph> graph) {
            this.graph = graph;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionTitle
         */
        public String getEffectiveActionTitle() {
            return effectiveActionTitle;
        }

        /**
         * @param effectiveActionTitle The effectiveActionTitle
         */
        public void setEffectiveActionTitle(String effectiveActionTitle) {
            this.effectiveActionTitle = effectiveActionTitle;
        }

        /**
         * @return The assignEffectiveActionDate
         */
        public AssignEffectiveActionDate getAssignEffectiveActionDate() {
            return assignEffectiveActionDate;
        }

        /**
         * @param assignEffectiveActionDate The assignEffectiveActionDate
         */
        public void setAssignEffectiveActionDate(AssignEffectiveActionDate assignEffectiveActionDate) {
            this.assignEffectiveActionDate = assignEffectiveActionDate;
        }

    }

    public static class AssignEffectiveActionDate {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The timezoneType
         */
        public Integer getTimezoneType() {
            return timezoneType;
        }

        /**
         * @param timezoneType The timezone_type
         */
        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         * @return The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         * @param timezone The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }


    public static class User {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private Integer termsAccepted;
        @SerializedName("member")
        @Expose
        private Member member;

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public Integer getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(Integer termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

        /**
         * @return The member
         */
        public Member getMember() {
            return member;
        }

        /**
         * @param member The member
         */
        public void setMember(Member member) {
            this.member = member;
        }

    }


    public class Post {

        @SerializedName("content")
        @Expose
        private String content;
        @SerializedName("postId")
        @Expose
        private Integer postId;
        @SerializedName("createdAt")
        @Expose
        private CreatedAt createdAt;

        /**
         * @return The content
         */
        public String getContent() {
            return content;
        }

        /**
         * @param content The content
         */
        public void setContent(String content) {
            this.content = content;
        }

        /**
         * @return The postId
         */
        public Integer getPostId() {
            return postId;
        }

        /**
         * @param postId The postId
         */
        public void setPostId(Integer postId) {
            this.postId = postId;
        }

        /**
         * @return The createdAt
         */
        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        /**
         * @param createdAt The createdAt
         */
        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

    }

    public static class Feed {

        @SerializedName("type")
        @Expose
        private Integer type;
        @SerializedName("announcementId")
        @Expose
        private Object announcementId;
        @SerializedName("feedId")
        @Expose
        private Integer feedId;
        @SerializedName("postId")
        @Expose
        private Object postId;
        @SerializedName("likeCount")
        @Expose
        private Integer likeCount;

        @SerializedName("pastEffectiveActionId")
        @Expose
        private Integer pastEffectiveActionId;

        @SerializedName("pasteffectiveaction")
        @Expose
        private Pasteffectiveaction pasteffectiveaction;

        public Pasteffectiveaction getPasteffectiveaction() {
            return pasteffectiveaction;
        }

        public void setPasteffectiveaction(Pasteffectiveaction pasteffectiveaction) {
            this.pasteffectiveaction = pasteffectiveaction;
        }

        public Integer getPastEffectiveActionId() {
            return pastEffectiveActionId;
        }

        public void setPastEffectiveActionId(Integer pastEffectiveActionId) {
            this.pastEffectiveActionId = pastEffectiveActionId;
        }

        public Integer getLocalLikeCount() {
            return localLikeCount;
        }

        public void setLocalLikeCount(Integer localLikeCount) {
            this.localLikeCount = localLikeCount;
        }

        private Integer localLikeCount;

        @SerializedName("comment")
        @Expose
        private Comment comment;

        @SerializedName("isLiked")
        @Expose
        private Integer isLike;
        private Integer isLikeLocal;

        @SerializedName("commentCount")
        @Expose
        private Integer commentCount;
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("announcement")
        @Expose
        private Announcement announcement;
        @SerializedName("post")
        @Expose
        private Post post;
        @SerializedName("effectiveaction")
        @Expose
        private Effectiveaction effectiveaction;

        public Integer getIsLikeLocal() {
            return isLikeLocal;
        }

        public void setIsLikeLocal(Integer isLikeLocal) {
            this.isLikeLocal = isLikeLocal;
        }

        public Integer getIsLike() {
            return isLike;
        }

        public void setIsLike(Integer isLike) {
            this.isLike = isLike;
        }

        /**
         * @return The comment
         */
        public Comment getComment() {
            return comment;
        }

        /**
         * @param comment The comment
         */
        public void setComment(Comment comment) {
            this.comment = comment;
        }


        /**
         * @return The type
         */
        public Integer getType() {
            return type;
        }

        /**
         * @param type The type
         */
        public void setType(Integer type) {
            this.type = type;
        }

        /**
         * @return The announcementId
         */
        public Object getAnnouncementId() {
            return announcementId;
        }

        /**
         * @param announcementId The announcementId
         */
        public void setAnnouncementId(Object announcementId) {
            this.announcementId = announcementId;
        }

        /**
         * @return The feedId
         */
        public Integer getFeedId() {
            return feedId;
        }

        /**
         * @param feedId The feedId
         */
        public void setFeedId(Integer feedId) {
            this.feedId = feedId;
        }

        /**
         * @return The postId
         */
        public Object getPostId() {
            return postId;
        }

        /**
         * @param postId The postId
         */
        public void setPostId(Object postId) {
            this.postId = postId;
        }

        /**
         * @return The likeCount
         */
        public Integer getLikeCount() {
            return likeCount;
        }

        /**
         * @param likeCount The likeCount
         */
        public void setLikeCount(Integer likeCount) {
            this.likeCount = likeCount;
        }

        /**
         * @return The commentCount
         */
        public Integer getCommentCount() {
            return commentCount;
        }

        /**
         * @param commentCount The commentCount
         */
        public void setCommentCount(Integer commentCount) {
            this.commentCount = commentCount;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The user
         */
        public User getUser() {
            return user;
        }

        /**
         * @param user The user
         */
        public void setUser(User user) {
            this.user = user;
        }

        /**
         * @return The announcement
         */
        public Announcement getAnnouncement() {
            return announcement;
        }

        /**
         * @param announcement The announcement
         */
        public void setAnnouncement(Announcement announcement) {
            this.announcement = announcement;
        }

        /**
         * @return The post
         */
        public Post getPost() {
            return post;
        }

        /**
         * @param post The post
         */
        public void setPost(Post post) {
            this.post = post;
        }

        /**
         * @return The effectiveaction
         */
        public Effectiveaction getEffectiveaction() {
            return effectiveaction;
        }

        /**
         * @param effectiveaction The effectiveaction
         */
        public void setEffectiveaction(Effectiveaction effectiveaction) {
            this.effectiveaction = effectiveaction;
        }

    }


    public static class Member_ {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Object effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private Object effectiveActionDescription;
        @SerializedName("teamId")
        @Expose
        private Integer teamId;
        @SerializedName("designation")
        @Expose
        private Designation_ designation;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Object getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Object effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public Object getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(Object effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The teamId
         */
        public Integer getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The designation
         */
        public Designation_ getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation_ designation) {
            this.designation = designation;
        }

    }


    public static class User_ {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private Integer termsAccepted;

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public Integer getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(Integer termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

    }

    public static class Pasteffectiveaction {

        @SerializedName("effectiveActionId")
        @Expose
        private int effectiveActionId;
        @SerializedName("startDate")
        @Expose
        private String startDate;
        @SerializedName("endDate")
        @Expose
        private EndDate endDate;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("pastEffectiveActionId")
        @Expose
        private int pastEffectiveActionId;

        /**
         *
         * @return
         *     The effectiveActionId
         */
        public int getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         *
         * @param effectiveActionId
         *     The effectiveActionId
         */
        public void setEffectiveActionId(int effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         *
         * @return
         *     The startDate
         */
        public String getStartDate() {
            return startDate;
        }

        /**
         *
         * @param startDate
         *     The startDate
         */
        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        /**
         *
         * @return
         *     The endDate
         */
        public EndDate getEndDate() {
            return endDate;
        }

        /**
         *
         * @param endDate
         *     The endDate
         */
        public void setEndDate(EndDate endDate) {
            this.endDate = endDate;
        }

        /**
         *
         * @return
         *     The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         *
         * @param effectiveActionDescription
         *     The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         *
         * @return
         *     The pastEffectiveActionId
         */
        public int getPastEffectiveActionId() {
            return pastEffectiveActionId;
        }

        /**
         *
         * @param pastEffectiveActionId
         *     The pastEffectiveActionId
         */
        public void setPastEffectiveActionId(int pastEffectiveActionId) {
            this.pastEffectiveActionId = pastEffectiveActionId;
        }

    }

    public static class EndDate {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private int timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         *
         * @return
         *     The date
         */
        public String getDate() {
            return date;
        }

        /**
         *
         * @param date
         *     The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         *
         * @return
         *     The timezoneType
         */
        public int getTimezoneType() {
            return timezoneType;
        }

        /**
         *
         * @param timezoneType
         *     The timezone_type
         */
        public void setTimezoneType(int timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         *
         * @return
         *     The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         *
         * @param timezone
         *     The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }
}
