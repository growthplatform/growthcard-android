package com.appster.growthcard.network;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.appster.growthcard.R;
import com.appster.growthcard.ui.activities.FilterActivity;
import com.appster.growthcard.ui.activities.LoginActivity;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by navdeep on 19/02/16.
 */
public class ErrorCallback {
    /**
     * A callback which offers granular callbacks for various conditions.
     */
    public interface MyCallback<T> {
        /**
         * Called for [200, 300) responses.
         */
        void success(Response<T> response);

        /**
         * @param errorMessage Message for error
         */
        void error(String errorMessage);

    }

    public interface MyCall<T> {
        void cancel();

        void enqueue(MyCallback<T> callback, Context context, boolean isProgress);

        MyCall<T> clone();

    }

    public static class ErrorHandlingCallAdapterFactory extends CallAdapter.Factory {
        @Override
        public CallAdapter<MyCall<?>> get(Type returnType, Annotation[] annotations,
                                          Retrofit retrofit) {
            if (getRawType(returnType) != MyCall.class) {
                return null;
            }
            if (!(returnType instanceof ParameterizedType)) {
                throw new IllegalStateException(
                        "MyCall must have generic type (e.g., MyCall<ResponseBody>)");
            }
            final Type responseType = getParameterUpperBound(0, (ParameterizedType) returnType);
            return new CallAdapter<MyCall<?>>() {
                @Override
                public Type responseType() {
                    return responseType;
                }

                @Override
                public <T> MyCall<T> adapt(Call<T> call) {
                    return new MyCallAdapter<>(call);
                }
            };
        }
    }


    /**
     * Adapts a {@link Call} to {@link MyCall}.
     */
    static class MyCallAdapter<T> implements MyCall<T> {
        private final Call<T> call;
        private Context context;
        private SweetAlertDialog dialog;

        MyCallAdapter(Call<T> call) {
            this.call = call;
//                this.context=context;
        }

        @Override
        public void cancel() {
            call.cancel();
        }

        @Override
        public void enqueue(final MyCallback<T> callback, final Context context, boolean isProgress) {
            if (isProgress)
                dialog = Utils.loadingDialog(context);

            this.context = context;
            call.enqueue(new Callback<T>() {
                @Override
                public void onResponse(Call<T> call, final Response<T> response) {


                    try {
                        if (dialog != null && dialog.isShowing())
                            dialog.dismiss();


                        int code = response.code();


                        ((Activity) context).runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                Log.e("Error", response.code() + "");

                            }
                        });
                        Log.e("Error", response.code() + "");


                        if (code >= 200 && code < 300) {
                            callback.success(response);
                        } else if (code == 401 || code == 604 ) {
                            serverError(response);
                            callback.error(response.message());
                        } else if (code >= 400 && code < 500) {
                            serverError(response);
                            callback.error(response.message());

                        } else if (code >= 500 && code < 600) {
                            serverError(response);
                            callback.error(response.message());

                        }

                        else if (code== 606){
                            serverError(response);
                            callback.error(response.message());
                        }

                        else {
                            unexpectedError(new RuntimeException("Unexpected response " + response));
                            callback.error("Unexpected response " + response);

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<T> call, Throwable t) {

                    if (dialog != null && dialog.isShowing())
                        dialog.dismiss();

                    if (t instanceof IOException) {
                        networkError((IOException) t);
                        callback.error("Network Error");

                    } else {
                        unexpectedError(t);
                        callback.error("Network Error");

                    }
                }
            });
        }

        private void unexpectedError(Throwable t) {
//            Log.e("Error",t.getMessage());
            t.printStackTrace();
            displayError(context.getString(R.string.unexpected_error));//+ t.getMessage());

        }

        private void networkError(IOException t) {
            //  Log.e("Error",t.getMessage());
            t.printStackTrace();

            displayError(context.getString(R.string.network_error));//+ t.getMessage());

        }

        private void unexpectedError(RuntimeException e) {
//            Log.e("Error", e.getMessage());
//            e.printStackTrace();

            displayError(context.getString(R.string.unexpected_error));

        }

        private void serverError(Response<T> response) {

            //     Log.e("Error", response.message());
            try {

                String errorMessage = response.errorBody().string();
                JSONObject jsonObject = new JSONObject(errorMessage);

                if (response.code() == 604)
                    displayExitError(context.getString(R.string.cms_change_msg));

                else if (response.code() == 401 && jsonObject.getString("message").equals(context.getString(R.string.unauthorised)))
                    displayExitError(context.getString(R.string.cms_change_msg));
                else
                    displayError(jsonObject.getString("message"));


            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        @Override
        public MyCall<T> clone() {
            return new MyCallAdapter<>(call.clone());

        }


        private void displayError(final String message) {


            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {

                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText(message)
                            .show();

                }
            });
        }


        private void displayExitError(final String message) {


            ((Activity) context).runOnUiThread(new Runnable() {
                public void run() {

                    new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText("Oops...")
                            .setContentText(message)
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    try {
                                        if (FilterActivity.filterData != null)
                                            FilterActivity.filterData.clear();

                                        NotificationManager nMgr = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
                                        nMgr.cancelAll();

                                        AppPrefrences appPref = new AppPrefrences(context);
                                        appPref.logOut();
                                        ((Activity) context).startActivity(new Intent(context, LoginActivity.class));

                                        ((Activity) context).finish();

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            })
                            .show();

                }
            });
        }


    }


}
