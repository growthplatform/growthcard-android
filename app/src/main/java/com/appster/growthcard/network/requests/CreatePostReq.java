
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreatePostReq {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("flag")
    @Expose
    private int flag;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("departmentId")
    @Expose
    private int departmentId;
    private String deviceId;

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The teamId
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return The companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return The departmentId
     */
    public int getDepartmentId() {
        return departmentId;
    }

    /**
     * @param departmentId The departmentId
     */
    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
