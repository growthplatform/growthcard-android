package com.appster.growthcard.network.response;

import com.appster.growthcard.model.Graph;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DashboardListResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }


    public class Designation {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }

    public class Effective {

        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionTitle")
        @Expose
        private String effectiveActionTitle;
        @SerializedName("assignEffectiveActionDate")
        @Expose
        private AssignEffectiveActionDate assignEffectiveActionDate;

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionTitle
         */
        public String getEffectiveActionTitle() {
            return effectiveActionTitle;
        }

        /**
         * @param effectiveActionTitle The effectiveActionTitle
         */
        public void setEffectiveActionTitle(String effectiveActionTitle) {
            this.effectiveActionTitle = effectiveActionTitle;
        }

        /**
         * @return The assignEffectiveActionDate
         */
        public AssignEffectiveActionDate getAssignEffectiveActionDate() {
            return assignEffectiveActionDate;
        }

        /**
         * @param assignEffectiveActionDate The assignEffectiveActionDate
         */
        public void setAssignEffectiveActionDate(AssignEffectiveActionDate assignEffectiveActionDate) {
            this.assignEffectiveActionDate = assignEffectiveActionDate;
        }

    }

    public class EffectiveActionSummary {

        @SerializedName("MIN")
        @Expose
        private Integer MIN;
        @SerializedName("MAX")
        @Expose
        private Integer MAX;
        @SerializedName("AVG")
        @Expose
        private Integer AVG;
        @SerializedName("TODAY")
        @Expose
        private Integer TODAY;

        /**
         * @return The MIN
         */
        public Integer getMIN() {
            return MIN;
        }

        /**
         * @param MIN The MIN
         */
        public void setMIN(Integer MIN) {
            this.MIN = MIN;
        }

        /**
         * @return The MAX
         */
        public Integer getMAX() {
            return MAX;
        }

        /**
         * @param MAX The MAX
         */
        public void setMAX(Integer MAX) {
            this.MAX = MAX;
        }

        /**
         * @return The AVG
         */
        public Integer getAVG() {
            return AVG;
        }

        /**
         * @param AVG The AVG
         */
        public void setAVG(Integer AVG) {
            this.AVG = AVG;
        }

        /**
         * @return The TODAY
         */
        public Integer getTODAY() {
            return TODAY;
        }

        /**
         * @param TODAY The TODAY
         */
        public void setTODAY(Integer TODAY) {
            this.TODAY = TODAY;
        }

    }

    public class Member {

        @SerializedName("graph")
        @Expose
        private ArrayList<Graph> graph = new ArrayList<Graph>();
        @SerializedName("effectiveActionSummary")
        @Expose
        private EffectiveActionSummary effectiveActionSummary;
        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("teamId")
        @Expose
        private Integer teamId;
        @SerializedName("user")
        @Expose
        private User user;
        @SerializedName("designation")
        @Expose
        private Designation designation;
        @SerializedName("effective")
        @Expose
        private Effective effective;

        /**
         * @return The graph
         */
        public ArrayList<Graph> getGraph() {
            return graph;
        }

        /**
         * @param graph The graph
         */
        public void setGraph(ArrayList<Graph> graph) {
            this.graph = graph;
        }

        /**
         * @return The effectiveActionSummary
         */
        public EffectiveActionSummary getEffectiveActionSummary() {
            return effectiveActionSummary;
        }

        /**
         * @param effectiveActionSummary The effectiveActionSummary
         */
        public void setEffectiveActionSummary(EffectiveActionSummary effectiveActionSummary) {
            this.effectiveActionSummary = effectiveActionSummary;
        }

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The teamId
         */
        public Integer getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The user
         */
        public User getUser() {
            return user;
        }

        /**
         * @param user The user
         */
        public void setUser(User user) {
            this.user = user;
        }

        /**
         * @return The designation
         */
        public Designation getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation designation) {
            this.designation = designation;
        }

        /**
         * @return The effective
         */
        public Effective getEffective() {
            return effective;
        }

        /**
         * @param effective The effective
         */
        public void setEffective(Effective effective) {
            this.effective = effective;
        }

    }

    public class Result {

        @SerializedName("pageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("members")
        @Expose
        private ArrayList<Member> members = new ArrayList<Member>();

        /**
         * @return The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The pageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The totalPages
         */
        public Integer getTotalPages() {
            return totalPages;
        }

        /**
         * @param totalPages The totalPages
         */
        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        /**
         * @return The members
         */
        public ArrayList<Member> getMembers() {
            return members;
        }

        /**
         * @param members The members
         */
        public void setMembers(ArrayList<Member> members) {
            this.members = members;
        }

    }

    public class User {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private Integer termsAccepted;

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public Integer getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(Integer termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

    }


    public class AssignEffectiveActionDate {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The timezoneType
         */
        public Integer getTimezoneType() {
            return timezoneType;
        }

        /**
         * @param timezoneType The timezone_type
         */
        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         * @return The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         * @param timezone The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }

}