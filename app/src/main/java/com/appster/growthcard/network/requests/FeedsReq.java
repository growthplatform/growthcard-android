
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FeedsReq {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("pageNo")
    @Expose
    private int pageNo;

    @SerializedName("flag1")
    @Expose
    private int flag1;
    @SerializedName("flag2")
    @Expose
    private int flag2;

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }

    @SerializedName("departmentId")
    @Expose
    private int departmentId;


    public int getFlag2() {
        return flag2;
    }

    public void setFlag2(int flag2) {
        this.flag2 = flag2;
    }

    public int getFlag1() {

        return flag1;
    }

    public void setFlag1(int flag1) {
        this.flag1 = flag1;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The teamId
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The pageNo
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The pageNo
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

}
