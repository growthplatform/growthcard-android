package com.appster.growthcard.network;

import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.network.requests.AnnoucementListReq;
import com.appster.growthcard.network.requests.AssignEAReq;
import com.appster.growthcard.network.requests.ChangePasswordReq;
import com.appster.growthcard.network.requests.CreateAnnouncementReq;
import com.appster.growthcard.network.requests.CreatePostReq;
import com.appster.growthcard.network.requests.CustomEAReq;
import com.appster.growthcard.network.requests.DeleteAnnoucementReq;
import com.appster.growthcard.network.requests.DeletePastEAReq;
import com.appster.growthcard.network.requests.DeletePostReq;
import com.appster.growthcard.network.requests.DepartmentReq;
import com.appster.growthcard.network.requests.EAListReq;
import com.appster.growthcard.network.requests.EditProfileReq;
import com.appster.growthcard.network.requests.FeedsReq;
import com.appster.growthcard.network.requests.ForgetPasswordReq;
import com.appster.growthcard.network.requests.LikeReq;
import com.appster.growthcard.network.requests.LogOutReq;
import com.appster.growthcard.network.requests.MarkAdherenceReq;
import com.appster.growthcard.network.requests.PastEAListReq;
import com.appster.growthcard.network.requests.PostListReq;
import com.appster.growthcard.network.requests.SearchSubordinateReq;
import com.appster.growthcard.network.requests.SendCommentsReq;
import com.appster.growthcard.network.requests.SignInReq;
import com.appster.growthcard.network.requests.SubListReq;
import com.appster.growthcard.network.requests.SubProfileReq;
import com.appster.growthcard.network.requests.SwitchReq;
import com.appster.growthcard.network.requests.TermsReq;
import com.appster.growthcard.network.requests.WorkDaysReq;
import com.appster.growthcard.network.response.AnnoucementListResponse;
import com.appster.growthcard.network.response.AssignEAResponse;
import com.appster.growthcard.network.response.CommentListResponse;
import com.appster.growthcard.network.response.CreateAnnouncementResponse;
import com.appster.growthcard.network.response.DashboardListResponse;
import com.appster.growthcard.network.response.DepartmentResponse;
import com.appster.growthcard.network.response.EADetailResponse;
import com.appster.growthcard.network.response.EAListResponse;
import com.appster.growthcard.network.response.EditProfileResponse;
import com.appster.growthcard.network.response.FeedsResponse;
import com.appster.growthcard.network.response.ForgetPasswordResponse;
import com.appster.growthcard.network.response.GenericResponse;
import com.appster.growthcard.network.response.LikeResponse;
import com.appster.growthcard.network.response.MarkAdherenceResponse;
import com.appster.growthcard.network.response.NotificationResponse;
import com.appster.growthcard.network.response.PastEAListResponse;
import com.appster.growthcard.network.response.PostListResponse;
import com.appster.growthcard.network.response.SearchSubordinateResponse;
import com.appster.growthcard.network.response.SendCommentsResponse;
import com.appster.growthcard.network.response.SignInResponse;
import com.appster.growthcard.network.response.SubListResponse;
import com.appster.growthcard.network.response.SubProfileResponse;
import com.appster.growthcard.network.response.TeamMemberResponse;
import com.appster.growthcard.network.response.TermsResponse;
import com.appster.growthcard.network.response.WorkDaysResponse;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;


/**
 * Created by himanshu on 17/02/16.
 */
public interface GCApi {


    @POST
    ErrorCallback.MyCall<Object> testApi(@Body Object reqObject);


    @POST(ApiConstants.LOGIN_URL)
    ErrorCallback.MyCall<SignInResponse> signIn(@Body SignInReq user);


    @GET(ApiConstants.TERMS_POST)
    ErrorCallback.MyCall<ResponseBody> getTerms();

    @PUT(ApiConstants.TERMS_PUT)
    ErrorCallback.MyCall<TermsResponse> updateTerms(@Body TermsReq termsReq);

    @POST(ApiConstants.WORKDAYS_POST)
    ErrorCallback.MyCall<WorkDaysResponse> updateWorkDays(@Body WorkDaysReq workDaysReq);

    @PUT(ApiConstants.USER_PROFILE_PUT)
    ErrorCallback.MyCall<EditProfileResponse> updateUserProfile(@Body EditProfileReq editProfileReq);

    @Multipart
    @POST(ApiConstants.UER_PROFILE_IMAGE)
    ErrorCallback.MyCall<SignInResponse> uploadProfilePhoto(@Part("image\"; filename=\"profileImage.png\" ") RequestBody file,
                                                            @Part("deviceId") RequestBody deviceId,
                                                            @Part("userToken") RequestBody userToken,
                                                            @Part("userId") RequestBody userId);


    @POST(ApiConstants.SUBORDINATE_LIST)
    ErrorCallback.MyCall<SubListResponse> getSubList(@Body SubListReq subListReq);

    @POST(ApiConstants.EA_LIST)
    ErrorCallback.MyCall<EAListResponse> getEAList(@Body EAListReq eaListReq);

    @POST(ApiConstants.CUSTOM_EA)
    ErrorCallback.MyCall<EAListResponse> addCustomEA(@Body CustomEAReq eaListReq);


    @POST(ApiConstants.ASSIGN_EA)
    ErrorCallback.MyCall<AssignEAResponse> assignEA(@Body AssignEAReq eaListReq);

    @POST(ApiConstants.CHANGE_ASSIGNED_EA)
    ErrorCallback.MyCall<AssignEAResponse> changeEA(@Body AssignEAReq eaListReq);


    @POST(ApiConstants.SUB_MARK_ADHERENCE)
    ErrorCallback.MyCall<MarkAdherenceResponse> markAdherence(@Body MarkAdherenceReq markAdherenceReq);

    @POST(ApiConstants.SUB_PROFILE_BASIC)
    ErrorCallback.MyCall<SubProfileResponse> getSubProfileBasic(@Body SubProfileReq subProfileReq);

    @POST(ApiConstants.ANNOUCEMENT)
    ErrorCallback.MyCall<CreateAnnouncementResponse> createAnnoucement(@Body CreateAnnouncementReq createAnnouncementReq);


    @POST(ApiConstants.SEARCH_USER)
    ErrorCallback.MyCall<SearchSubordinateResponse> searchSubordinate(@Body SearchSubordinateReq searchSubordinateReq);

    @POST(ApiConstants.GET_ANNOUCEMENT)
    ErrorCallback.MyCall<AnnoucementListResponse> annoucementList(@Body AnnoucementListReq annoucementListReq);

    @POST(ApiConstants.DELETE_ANNOUCEMENT)
    ErrorCallback.MyCall<GenericResponse> deleteAnnoucement(@Body DeleteAnnoucementReq deleteAnnoucementReq);

    @POST(ApiConstants.GET_TEAM_MEMBERS)
    ErrorCallback.MyCall<TeamMemberResponse> getTeamMembers(@Body SubProfileReq subProfileReq);


    @POST(ApiConstants.GET_PAST_EA_LIST)
    ErrorCallback.MyCall<PastEAListResponse> getPastEAList(@Body PastEAListReq subProfileReq);

    @POST(ApiConstants.SEND_COMMENT)
    ErrorCallback.MyCall<SendCommentsResponse> sendComment(@Body SendCommentsReq req);

    @POST(ApiConstants.COMMENT_LIST)
    ErrorCallback.MyCall<CommentListResponse> getCommentsList(@Body SendCommentsReq req);

    @POST(ApiConstants.GET_EA_DETAILS)
    ErrorCallback.MyCall<EADetailResponse> getEADetails(@Body SendCommentsReq req);


    @POST(ApiConstants.POST)
    ErrorCallback.MyCall<GenericResponse> post(@Body CreatePostReq req);

    @POST(ApiConstants.GET_POST)
    ErrorCallback.MyCall<PostListResponse> getpost(@Body PostListReq req);

    @POST(ApiConstants.DELETE_POST)
    ErrorCallback.MyCall<GenericResponse> deletepost(@Body DeletePostReq deletePostReq);

    @POST(ApiConstants.DASHBOARD_LIST)
    ErrorCallback.MyCall<DashboardListResponse> dashboardList(@Body SendCommentsReq req);

    @POST(ApiConstants.SWITCH_PROFILE)
    ErrorCallback.MyCall<SignInResponse> fSwitch(@Body SwitchReq switchReq);

    @POST(ApiConstants.GENERATE_FEED)
    ErrorCallback.MyCall<FeedsResponse> getFeeds(@Body FeedsReq req);

    @POST(ApiConstants.GET_DEPARTMENT)
    ErrorCallback.MyCall<DepartmentResponse> getDepartment(@Body DepartmentReq req);

    @POST(ApiConstants.FORGET_PASSSWORD)
    ErrorCallback.MyCall<ForgetPasswordResponse> getForgetPassword(@Body ForgetPasswordReq req);

    @POST(ApiConstants.CHANGE_PASSSWORD)
    ErrorCallback.MyCall<GenericResponse> getChangePassword(@Body ChangePasswordReq req);

    @POST(ApiConstants.LIKE_FEED)
    ErrorCallback.MyCall<LikeResponse> likeFeed(@Body LikeReq req);

    @POST(ApiConstants.LOG_OUT)
    ErrorCallback.MyCall<GenericResponse> logOut(@Body LogOutReq req);

    @POST(ApiConstants.DELETE_PAST_EA)
    ErrorCallback.MyCall<GenericResponse> DeletePastEA(@Body DeletePastEAReq req);

    @POST(ApiConstants.GET_NOTIFICATION_LIST)
    ErrorCallback.MyCall<NotificationResponse> getNotificationList(@Body PostListReq req);
}
