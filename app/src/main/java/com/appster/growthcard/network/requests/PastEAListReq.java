package com.appster.growthcard.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PastEAListReq {

    @SerializedName("teamId")
    @Expose
    private Integer teamId;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("pageNo")
    @Expose
    private Integer pageNo;
    @SerializedName("subordinateId")
    @Expose
    private Integer subordinateId;

    @SerializedName("flag")
    @Expose
    private Integer flag;

    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The teamId
     */
    public Integer getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The pageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * @return The subordinateId
     */
    public Integer getSubordinateId() {
        return subordinateId;
    }

    /**
     * @param subordinateId The subordinateId
     */
    public void setSubordinateId(Integer subordinateId) {
        this.subordinateId = subordinateId;
    }


    public Integer getFlag() {
        return flag;
    }

    /**
     * @param flag The subordinateId
     */
    public void setFlag(Integer flag) {
        this.flag = flag;
    }


}