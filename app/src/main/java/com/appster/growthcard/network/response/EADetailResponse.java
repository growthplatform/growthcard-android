package com.appster.growthcard.network.response;

import com.appster.growthcard.model.Graph;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class EADetailResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }


    public class EffectiveActionSummary {

        @SerializedName("MIN")
        @Expose
        private Integer MIN;
        @SerializedName("MAX")
        @Expose
        private Integer MAX;
        @SerializedName("AVG")
        @Expose
        private Integer AVG;
        @SerializedName("TODAY")
        @Expose
        private Integer TODAY;

        /**
         * @return The MIN
         */
        public Integer getMIN() {
            return MIN;
        }

        /**
         * @param MIN The MIN
         */
        public void setMIN(Integer MIN) {
            this.MIN = MIN;
        }

        /**
         * @return The MAX
         */
        public Integer getMAX() {
            return MAX;
        }

        /**
         * @param MAX The MAX
         */
        public void setMAX(Integer MAX) {
            this.MAX = MAX;
        }

        /**
         * @return The AVG
         */
        public Integer getAVG() {
            return AVG;
        }

        /**
         * @param AVG The AVG
         */
        public void setAVG(Integer AVG) {
            this.AVG = AVG;
        }

        /**
         * @return The TODAY
         */
        public Integer getTODAY() {
            return TODAY;
        }

        /**
         * @param TODAY The TODAY
         */
        public void setTODAY(Integer TODAY) {
            this.TODAY = TODAY;
        }

    }

    public class EndDate {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The timezoneType
         */
        public Integer getTimezoneType() {
            return timezoneType;
        }

        /**
         * @param timezoneType The timezone_type
         */
        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         * @return The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         * @param timezone The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }

    public class Result {

        @SerializedName("graph")
        @Expose
        private List<Graph> graph = new ArrayList<Graph>();
        @SerializedName("effectiveActionSummary")
        @Expose
        private EffectiveActionSummary effectiveActionSummary;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("startDate")
        @Expose
        private String startDate;
        @SerializedName("endDate")
        @Expose
        private String endDate;
        @SerializedName("effectiveActionTitle")
        @Expose
        private String effectiveActionTitle;

        /**
         * @return The graph
         */
        public List<Graph> getGraph() {
            return graph;
        }

        /**
         * @param graph The graph
         */
        public void setGraph(List<Graph> graph) {
            this.graph = graph;
        }

        /**
         * @return The effectiveActionSummary
         */
        public EffectiveActionSummary getEffectiveActionSummary() {
            return effectiveActionSummary;
        }

        /**
         * @param effectiveActionSummary The effectiveActionSummary
         */
        public void setEffectiveActionSummary(EffectiveActionSummary effectiveActionSummary) {
            this.effectiveActionSummary = effectiveActionSummary;
        }

        /**
         * @return The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The startDate
         */
        public String getStartDate() {
            return startDate;
        }

        /**
         * @param startDate The startDate
         */
        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        /**
         * @return The endDate
         */
        public String getEndDate() {
            return endDate;
        }

        /**
         * @param endDate The endDate
         */
        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        /**
         * @return The effectiveActionTitle
         */
        public String getEffectiveActionTitle() {
            return effectiveActionTitle;
        }

        /**
         * @param effectiveActionTitle The effectiveActionTitle
         */
        public void setEffectiveActionTitle(String effectiveActionTitle) {
            this.effectiveActionTitle = effectiveActionTitle;
        }

    }

}