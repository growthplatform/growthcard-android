
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CreateAnnouncementReq {

    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("flag")
    @Expose
    private int flag;
    @SerializedName("senders")
    @Expose
    private ArrayList<Integer> senders = new ArrayList<Integer>();

    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    /**
     * @return The teamId
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return The subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return The flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return The senders
     */
    public ArrayList<Integer> getSenders() {
        return senders;
    }

    /**
     * @param senders The senders
     */
    public void setSenders(ArrayList<Integer> senders) {
        this.senders = senders;
    }

    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
