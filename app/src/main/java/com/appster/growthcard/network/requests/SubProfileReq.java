package com.appster.growthcard.network.requests;

/**
 * Created by navdeep on 11/03/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SubProfileReq {

    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("subordinateId")
    @Expose
    private Integer subordinateId;

    @SerializedName("teamId")
    @Expose
    private int teamId;

    @SerializedName("pageNo")
    @Expose
    private int pageNo;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    /**
     * @return The subordinateId
     */
    public Integer getSubordinateId() {
        return subordinateId;
    }

    /**
     * @param subordinateId The subordinateId
     */
    public void setSubordinateId(Integer subordinateId) {
        this.subordinateId = subordinateId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }
}