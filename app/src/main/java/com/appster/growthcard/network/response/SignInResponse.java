package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SignInResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }


    public static class Company {

        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("companyName")
        @Expose
        private String companyName;
        @SerializedName("companyLogo")
        @Expose
        private String companyLogo;


        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The companyName
         */
        public String getCompanyName() {
            return companyName;
        }

        /**
         * @param companyName The companyName
         */
        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        /**
         * @return The companyLogo
         */
        public String getCompanyLogo() {
            return companyLogo;
        }

        /**
         * @param companyLogo The companyLogo
         */
        public void setCompanyLogo(String companyLogo) {
            this.companyLogo = companyLogo;
        }

    }

    public static class Day {

        @SerializedName("workDay")
        @Expose
        private Integer workDay;


        @SerializedName("is_selected")
        @Expose
        private Integer is_selected;

        public Integer getIs_selected() {
            return is_selected;
        }

        public void setIs_selected(Integer is_selected) {
            this.is_selected = is_selected;
        }

        /**
         * @return The workDay
         */
        public Integer getWorkDay() {
            return workDay;
        }

        /**
         * @param workDay The workDay
         */
        public void setWorkDay(Integer workDay) {
            this.workDay = workDay;
        }

    }

    public static class Designation {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }

    public static class Effective {

        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionTitle")
        @Expose
        private String effectiveActionTitle;

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionTitle
         */
        public String getEffectiveActionTitle() {
            return effectiveActionTitle;
        }

        /**
         * @param effectiveActionTitle The effectiveActionTitle
         */
        public void setEffectiveActionTitle(String effectiveActionTitle) {
            this.effectiveActionTitle = effectiveActionTitle;
        }

    }

    public static class Member {

        @SerializedName("teamId")
        @Expose
        private Integer teamId;
        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("company")
        @Expose
        private Company company;
        @SerializedName("designation")
        @Expose
        private Designation designation;
        @SerializedName("effective")
        @Expose
        private Effective effective;

        /**
         * @return The teamId
         */
        public Integer getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The company
         */
        public Company getCompany() {
            return company;
        }

        /**
         * @param company The company
         */
        public void setCompany(Company company) {
            this.company = company;
        }


        /**
         * @return The designation
         */
        public Designation getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation designation) {
            this.designation = designation;
        }

        /**
         * @return The effective
         */
        public Effective getEffective() {
            return effective;
        }

        /**
         * @param effective The effective
         */
        public void setEffective(Effective effective) {
            this.effective = effective;
        }

    }

    public static class Result {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userToken")
        @Expose
        private String userToken;
        @SerializedName("role")
        @Expose
        private Integer role;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private Integer termsAccepted;
        @SerializedName("member")
        @Expose
        private Member member;
        @SerializedName("days")
        @Expose
        private List<Day> days = new ArrayList<Day>();

        @SerializedName("userdepartment")
        @Expose
        private Userdepartment userdepartment;

        @SerializedName("switch")
        @Expose
        private Integer mSwitch;

        public Userdepartment getUserdepartment() {
            return userdepartment;
        }

        public void setUserdepartment(Userdepartment userdepartment) {
            this.userdepartment = userdepartment;
        }

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userToken
         */
        public String getUserToken() {
            return userToken;
        }

        /**
         * @param userToken The userToken
         */
        public void setUserToken(String userToken) {
            this.userToken = userToken;
        }

        /**
         * @return The role
         */
        public Integer getRole() {
            return role;
        }

        /**
         * @param role The role
         */
        public void setRole(Integer role) {
            this.role = role;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public Integer getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(Integer termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

        /**
         * @return The member
         */
        public Member getMember() {
            return member;
        }

        /**
         * @param member The member
         */
        public void setMember(Member member) {
            this.member = member;
        }

        /**
         * @return The days
         */
        public List<Day> getDays() {
            return days;
        }

        /**
         * @param days The days
         */
        public void setDays(List<Day> days) {
            this.days = days;
        }


        public Integer getSwitch() {
            return mSwitch;
        }

        /**
         * @param mSwitch The deviceToken
         */
        public void setSwitch(Integer mSwitch) {
            this.mSwitch = mSwitch;
        }

    }

    public class Userdepartment {

        @SerializedName("department")
        @Expose
        private Department department;

        /**
         * @return The department
         */
        public Department getDepartment() {
            return department;
        }

        /**
         * @param department The department
         */
        public void setDepartment(Department department) {
            this.department = department;
        }

    }

    public class Department {

        @SerializedName("departmentId")
        @Expose
        private Integer departmentId;
        @SerializedName("departmentName")
        @Expose
        private String departmentName;

        /**
         * @return The departmentId
         */
        public Integer getDepartmentId() {
            return departmentId;
        }

        /**
         * @param departmentId The departmentId
         */
        public void setDepartmentId(Integer departmentId) {
            this.departmentId = departmentId;
        }

        /**
         * @return The departmentName
         */
        public String getDepartmentName() {
            return departmentName;
        }

        /**
         * @param departmentName The departmentName
         */
        public void setDepartmentName(String departmentName) {
            this.departmentName = departmentName;
        }


    }
}