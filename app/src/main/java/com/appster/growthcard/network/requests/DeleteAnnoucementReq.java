
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteAnnoucementReq {

    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("announcementId")
    @Expose
    private int announcementId;
    private String deviceId;

    /**
     * @return The teamId
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The announcementId
     */
    public int getAnnouncementId() {
        return announcementId;
    }

    /**
     * @param announcementId The announcementId
     */
    public void setAnnouncementId(int announcementId) {
        this.announcementId = announcementId;
    }


    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
