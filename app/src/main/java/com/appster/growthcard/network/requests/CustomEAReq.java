package com.appster.growthcard.network.requests;

/**
 * Created by navdeep on 11/03/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomEAReq {

    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("designationId")
    @Expose
    private Integer designationId;
    @SerializedName("effectiveActionName")
    @Expose
    private String effectiveActionName;

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The designationId
     */
    public Integer getDesignationId() {
        return designationId;
    }

    /**
     * @param designationId The designationId
     */
    public void setDesignationId(Integer designationId) {
        this.designationId = designationId;
    }

    /**
     * @return The effectiveActionName
     */
    public String getEffectiveActionName() {
        return effectiveActionName;
    }

    /**
     * @param effectiveActionName The effectiveActionName
     */
    public void setEffectiveActionName(String effectiveActionName) {
        this.effectiveActionName = effectiveActionName;
    }

}