package com.appster.growthcard.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkDaysReq {

    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("workDay")
    @Expose
    private String workDay;

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The workDay
     */
    public String getWorkDay() {
        return workDay;
    }

    /**
     * @param workDay The workDay
     */
    public void setWorkDay(String workDay) {
        this.workDay = workDay;
    }

}