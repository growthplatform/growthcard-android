
package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CreateAnnouncementResponse {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private int statusCode;
    @SerializedName("result")
    @Expose
    private List<Object> result = new ArrayList<Object>();

    /**
     * @return The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public List<Object> getResult() {
        return result;
    }

    /**
     * The result
     */
    public void setResult(List<Object> result) {
        this.result = result;
    }

}
