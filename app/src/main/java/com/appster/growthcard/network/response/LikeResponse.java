package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by navdeep on 05/04/16.
 */
public class LikeResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public class Result {

        @SerializedName("LikeCount")
        @Expose
        private Integer LikeCount;
        @SerializedName("isLiked")
        @Expose
        private Integer isLiked;

        /**
         * @return The LikeCount
         */
        public Integer getLikeCount() {
            return LikeCount;
        }

        /**
         * @param LikeCount The LikeCount
         */
        public void setLikeCount(Integer LikeCount) {
            this.LikeCount = LikeCount;
        }

        /**
         * @return The isLiked
         */
        public Integer getIsLiked() {
            return isLiked;
        }

        /**
         * @param isLiked The isLiked
         */
        public void setIsLiked(Integer isLiked) {
            this.isLiked = isLiked;
        }

    }

}