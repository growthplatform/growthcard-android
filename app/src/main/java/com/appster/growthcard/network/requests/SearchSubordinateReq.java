
package com.appster.growthcard.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchSubordinateReq {

    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("companyId")
    @Expose
    private int companyId;
    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("pageNo")
    @Expose
    private int pageNo;
    @SerializedName("searchText")
    @Expose
    private String searchText;
    private String deviceId;

    /**
     * @return The teamId
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The companyId
     */
    public int getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The pageNo
     */
    public int getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The pageNo
     */
    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    /**
     * @return The searchText
     */
    public String getSearchText() {
        return searchText;
    }

    /**
     * @param searchText The searchText
     */
    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
