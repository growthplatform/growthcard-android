package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by navdeep on 05/04/16.
 */
public class LikeReq {

    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("feedId")
    @Expose
    private Integer feedId;

    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The feedId
     */
    public Integer getFeedId() {
        return feedId;
    }

    /**
     * @param feedId The feedId
     */
    public void setFeedId(Integer feedId) {
        this.feedId = feedId;
    }

}