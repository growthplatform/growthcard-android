package com.appster.growthcard.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EAListReq {

    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("userId")
    @Expose
    private String userId;
    @SerializedName("designationId")
    @Expose
    private Integer designationId;
    @SerializedName("pageNo")
    @Expose
    private Integer pageNo;

    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The designationId
     */
    public Integer getDesignationId() {
        return designationId;
    }

    /**
     * @param designationId The designationId
     */
    public void setDesignationId(Integer designationId) {
        this.designationId = designationId;
    }

    /**
     * @return The pageNo
     */
    public Integer getPageNo() {
        return pageNo;
    }

    /**
     * @param pageNo The pageNo
     */
    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }


    /**
     * @return The userToken
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The userToken
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}