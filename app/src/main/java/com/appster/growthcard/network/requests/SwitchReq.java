
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SwitchReq {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("flag")
    @Expose
    private int flag;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The flag
     */
    public int getFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    public void setFlag(int flag) {
        this.flag = flag;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


}
