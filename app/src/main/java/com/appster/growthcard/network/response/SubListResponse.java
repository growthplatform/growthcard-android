
package com.appster.growthcard.network.response;

import com.appster.growthcard.model.Users;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SubListResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result = new Result();

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public class Designation {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }

    public class Result {

        @SerializedName("pageNo")
        @Expose
        private Integer pageNo;

        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("subordinates")
        @Expose
        private ArrayList<Users> subordinates;

        public Integer getPageNo() {
            return pageNo;
        }

        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        public Integer getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        public ArrayList<Users> getSubordinates() {
            return subordinates;
        }

        public void setSubordinates(ArrayList<Users> subordinates) {
            this.subordinates = subordinates;
        }
    }

//    public  class Subordinate {
//
//        @SerializedName("designationId")
//        @Expose
//        private Integer designationId;
//        @SerializedName("effectiveActionId")
//        @Expose
//        private Object effectiveActionId;
//        @SerializedName("companyId")
//        @Expose
//        private Integer companyId;
//        @SerializedName("effectiveActionDescription")
//        @Expose
//        private String effectiveActionDescription;
//        @SerializedName("teamId")
//        @Expose
//        private Integer teamId;
//        @SerializedName("designation")
//        @Expose
//        private Designation designation;
//        @SerializedName("user")
//        @Expose
//        private User user;
//
//        /**
//         *
//         * @return
//         *     The designationId
//         */
//        public Integer getDesignationId() {
//            return designationId;
//        }
//
//        /**
//         *
//         * @param designationId
//         *     The designationId
//         */
//        public void setDesignationId(Integer designationId) {
//            this.designationId = designationId;
//        }
//
//        /**
//         *
//         * @return
//         *     The effectiveActionId
//         */
//        public Object getEffectiveActionId() {
//            return effectiveActionId;
//        }
//
//        /**
//         *
//         * @param effectiveActionId
//         *     The effectiveActionId
//         */
//        public void setEffectiveActionId(Object effectiveActionId) {
//            this.effectiveActionId = effectiveActionId;
//        }
//
//        /**
//         *
//         * @return
//         *     The companyId
//         */
//        public Integer getCompanyId() {
//            return companyId;
//        }
//
//        /**
//         *
//         * @param companyId
//         *     The companyId
//         */
//        public void setCompanyId(Integer companyId) {
//            this.companyId = companyId;
//        }
//
//        /**
//         *
//         * @return
//         *     The effectiveActionDescription
//         */
//        public String getEffectiveActionDescription() {
//            return effectiveActionDescription;
//        }
//
//        /**
//         *
//         * @param effectiveActionDescription
//         *     The effectiveActionDescription
//         */
//        public void setEffectiveActionDescription(String effectiveActionDescription) {
//            this.effectiveActionDescription = effectiveActionDescription;
//        }
//
//        /**
//         *
//         * @return
//         *     The teamId
//         */
//        public Integer getTeamId() {
//            return teamId;
//        }
//
//        /**
//         *
//         * @param teamId
//         *     The teamId
//         */
//        public void setTeamId(Integer teamId) {
//            this.teamId = teamId;
//        }
//
//        /**
//         *
//         * @return
//         *     The designation
//         */
//        public Designation getDesignation() {
//            return designation;
//        }
//
//        /**
//         *
//         * @param designation
//         *     The designation
//         */
//        public void setDesignation(Designation designation) {
//            this.designation = designation;
//        }
//
//        /**
//         *
//         * @return
//         *     The user
//         */
//        public User getUser() {
//            return user;
//        }
//
//        /**
//         *
//         * @param user
//         *     The user
//         */
//        public void setUser(User user) {
//            this.user = user;
//        }
//
//    }
//
//    public  class User {
//
//        @SerializedName("email")
//        @Expose
//        private String email;
//        @SerializedName("userId")
//        @Expose
//        private Integer userId;
//        @SerializedName("firstName")
//        @Expose
//        private String firstName;
//        @SerializedName("lastName")
//        @Expose
//        private String lastName;
//        @SerializedName("profileImage")
//        @Expose
//        private String profileImage;
//        @SerializedName("termsAccepted")
//        @Expose
//        private Integer termsAccepted;
//
//        /**
//         *
//         * @return
//         *     The email
//         */
//        public String getEmail() {
//            return email;
//        }
//
//        /**
//         *
//         * @param email
//         *     The email
//         */
//        public void setEmail(String email) {
//            this.email = email;
//        }
//
//        /**
//         *
//         * @return
//         *     The userId
//         */
//        public Integer getUserId() {
//            return userId;
//        }
//
//        /**
//         *
//         * @param userId
//         *     The userId
//         */
//        public void setUserId(Integer userId) {
//            this.userId = userId;
//        }
//
//        /**
//         *
//         * @return
//         *     The firstName
//         */
//        public String getFirstName() {
//            return firstName;
//        }
//
//        /**
//         *
//         * @param firstName
//         *     The firstName
//         */
//        public void setFirstName(String firstName) {
//            this.firstName = firstName;
//        }
//
//        /**
//         *
//         * @return
//         *     The lastName
//         */
//        public String getLastName() {
//            return lastName;
//        }
//
//        /**
//         *
//         * @param lastName
//         *     The lastName
//         */
//        public void setLastName(String lastName) {
//            this.lastName = lastName;
//        }
//
//        /**
//         *
//         * @return
//         *     The profileImage
//         */
//        public String getProfileImage() {
//            return profileImage;
//        }
//
//        /**
//         *
//         * @param profileImage
//         *     The profileImage
//         */
//        public void setProfileImage(String profileImage) {
//            this.profileImage = profileImage;
//        }
//
//        /**
//         *
//         * @return
//         *     The termsAccepted
//         */
//        public Integer getTermsAccepted() {
//            return termsAccepted;
//        }
//
//        /**
//         *
//         * @param termsAccepted
//         *     The termsAccepted
//         */
//        public void setTermsAccepted(Integer termsAccepted) {
//            this.termsAccepted = termsAccepted;
//        }
//
//    }
}
