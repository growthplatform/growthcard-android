package com.appster.growthcard.network.requests;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendCommentsReq {

    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("effectiveActionId")
    @Expose
    private Integer effectiveActionId;
    @SerializedName("subordinateId")
    @Expose
    private Integer subordinateId;
    @SerializedName("teamId")
    @Expose
    private Integer teamId;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("content")
    @Expose
    private String content;

    //    private String startDate;
//    private String endDate;
    @SerializedName("companyId")
    @Expose
    private Integer compnayId;

    public Integer getCompnayId() {
        return compnayId;
    }

    public void setCompnayId(Integer compnayId) {
        this.compnayId = compnayId;
    }

    @SerializedName("flag")
    @Expose
    private int flag;

    @SerializedName("pastEffectiveActionId")
    @Expose
    private Integer pastEffectiveActionId = 0;



    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    @SerializedName("pageNo")
    @Expose
    private int pageNo;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The effectiveActionId
     */
    public Integer getEffectiveActionId() {
        return effectiveActionId;
    }

    /**
     * @param effectiveActionId The effectiveActionId
     */
    public void setEffectiveActionId(Integer effectiveActionId) {
        this.effectiveActionId = effectiveActionId;
    }

    /**
     * @return The subordinateId
     */
    public Integer getSubordinateId() {
        return subordinateId;
    }

    /**
     * @param subordinateId The subordinateId
     */
    public void setSubordinateId(Integer subordinateId) {
        this.subordinateId = subordinateId;
    }

    /**
     * @return The teamId
     */
    public Integer getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getPastEffectiveActionId() {
        return pastEffectiveActionId;
    }

    public void setPastEffectiveActionId(Integer pastEffectiveActionId) {
        this.pastEffectiveActionId = pastEffectiveActionId;
    }

    //    public String getStartDate() {
//        return startDate;
//    }
//
//    public void setStartDate(String startDate) {
//        this.startDate = startDate;
//    }
//
//    public String getEndDate() {
//        return endDate;
//    }
//
//    public void setEndDate(String endDate) {
//        this.endDate = endDate;
//    }
}