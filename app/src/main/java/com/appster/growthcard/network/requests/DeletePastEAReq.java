
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeletePastEAReq {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("deviceId")
    @Expose
    private String deviceId;
    @SerializedName("teamId")
    @Expose
    private int teamId;
    @SerializedName("subordinateId")
    @Expose
    private int subordinateId;
    @SerializedName("effectiveActionId")
    @Expose
    private int effectiveActionId;

    /**
     * @return The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * @return The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * @param userToken The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * @return The deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The teamId
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The subordinateId
     */
    public int getSubordinateId() {
        return subordinateId;
    }

    /**
     * @param subordinateId The subordinateId
     */
    public void setSubordinateId(int subordinateId) {
        this.subordinateId = subordinateId;
    }

    /**
     * @return The effectiveActionId
     */
    public int getEffectiveActionId() {
        return effectiveActionId;
    }

    /**
     * @param effectiveActionId The effectiveActionId
     */
    public void setEffectiveActionId(int effectiveActionId) {
        this.effectiveActionId = effectiveActionId;
    }

}
