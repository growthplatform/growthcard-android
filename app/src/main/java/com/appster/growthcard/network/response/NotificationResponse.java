package com.appster.growthcard.network.response;

/**
 * Created by navdeep on 08/04/16.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class NotificationResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }

    public class Notification {

        @SerializedName("message1")
        @Expose
        private String message1;
        @SerializedName("message2")
        @Expose
        private String message2;
        @SerializedName("message3")
        @Expose
        private String message3;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("effectiveActionId")
        @Expose
        private Integer effectiveActionId;
        @SerializedName("teamId")
        @Expose
        private Integer teamId;
        @SerializedName("userId")
        @Expose
        private Integer userId;

        @SerializedName("createdAt")
        @Expose
        private String createdAt;
        @SerializedName("read")
        @Expose
        private int read;

        @SerializedName("type")
        @Expose
        private int type;

        @SerializedName("pastEffectiveActionId")
        @Expose
        private Integer pastEffectiveActionId;

        public Integer getPastEffectiveActionId() {
            return pastEffectiveActionId;
        }

        public void setPastEffectiveActionId(Integer pastEffectiveActionId) {
            this.pastEffectiveActionId = pastEffectiveActionId;
        }

        public int getRead() {
            return read;
        }

        public void setRead(int read) {
            this.read = read;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getCreatedAt() {

            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        /**
         * @return The message1
         */
        public String getMessage1() {
            return message1;
        }

        /**
         * @param message1 The message1
         */
        public void setMessage1(String message1) {
            this.message1 = message1;
        }

        /**
         * @return The message2
         */
        public String getMessage2() {
            return message2;
        }

        /**
         * @param message2 The message2
         */
        public void setMessage2(String message2) {
            this.message2 = message2;
        }

        /**
         * @return The message3
         */
        public String getMessage3() {
            return message3;
        }

        /**
         * @param message3 The message3
         */
        public void setMessage3(String message3) {
            this.message3 = message3;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The effectiveActionId
         */
        public Integer getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Integer effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The teamId
         */
        public Integer getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

    }

    public class Result {

        @SerializedName("pageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("notifications")
        @Expose
        private List<Notification> notifications = new ArrayList<Notification>();

        /**
         * @return The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The pageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The totalPages
         */
        public Integer getTotalPages() {
            return totalPages;
        }

        /**
         * @param totalPages The totalPages
         */
        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        /**
         * @return The notifications
         */
        public List<Notification> getNotifications() {
            return notifications;
        }

        /**
         * @param notifications The notifications
         */
        public void setNotifications(List<Notification> notifications) {
            this.notifications = notifications;
        }

    }
}
