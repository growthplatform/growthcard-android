package com.appster.growthcard.network.response;

/**
 * Created by navdeep on 15/03/16.
 */

import com.appster.growthcard.model.Users;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TeamMemberResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }


//    public class Member {
//
//        @SerializedName("designationId")
//        @Expose
//        private Integer designationId;
//        @SerializedName("effectiveActionId")
//        @Expose
//        private Integer effectiveActionId;
//        @SerializedName("companyId")
//        @Expose
//        private Integer companyId;
//        @SerializedName("effectiveActionDescription")
//        @Expose
//        private String effectiveActionDescription;
//        @SerializedName("teamId")
//        @Expose
//        private Integer teamId;
//        @SerializedName("user")
//        @Expose
//        private User user;
//
//        /**
//         * @return The designationId
//         */
//        public Integer getDesignationId() {
//            return designationId;
//        }
//
//        /**
//         * @param designationId The designationId
//         */
//        public void setDesignationId(Integer designationId) {
//            this.designationId = designationId;
//        }
//
//        /**
//         * @return The effectiveActionId
//         */
//        public Integer getEffectiveActionId() {
//            return effectiveActionId;
//        }
//
//        /**
//         * @param effectiveActionId The effectiveActionId
//         */
//        public void setEffectiveActionId(Integer effectiveActionId) {
//            this.effectiveActionId = effectiveActionId;
//        }
//
//        /**
//         * @return The companyId
//         */
//        public Integer getCompanyId() {
//            return companyId;
//        }
//
//        /**
//         * @param companyId The companyId
//         */
//        public void setCompanyId(Integer companyId) {
//            this.companyId = companyId;
//        }
//
//        /**
//         * @return The effectiveActionDescription
//         */
//        public String getEffectiveActionDescription() {
//            return effectiveActionDescription;
//        }
//
//        /**
//         * @param effectiveActionDescription The effectiveActionDescription
//         */
//        public void setEffectiveActionDescription(String effectiveActionDescription) {
//            this.effectiveActionDescription = effectiveActionDescription;
//        }
//
//        /**
//         * @return The teamId
//         */
//        public Integer getTeamId() {
//            return teamId;
//        }
//
//        /**
//         * @param teamId The teamId
//         */
//        public void setTeamId(Integer teamId) {
//            this.teamId = teamId;
//        }
//
//        /**
//         * @return The user
//         */
//        public User getUser() {
//            return user;
//        }
//
//        /**
//         * @param user The user
//         */
//        public void setUser(User user) {
//            this.user = user;
//        }
//
//    }

    public class Result {

        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("result")
        @Expose
        private Result_ result;

        /**
         * @return The status
         */
        public String getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return The message
         */
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The result
         */
        public Result_ getResult() {
            return result;
        }

        /**
         * @param result The result
         */
        public void setResult(Result_ result) {
            this.result = result;
        }

    }

    public class Result_ {

        @SerializedName("pageNo")
        @Expose
        private Integer pageNo;
        @SerializedName("totalPages")
        @Expose
        private Integer totalPages;
        @SerializedName("members")
        @Expose
        private List<Users> members = new ArrayList<Users>();

        /**
         * @return The pageNo
         */
        public Integer getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The pageNo
         */
        public void setPageNo(Integer pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The totalPages
         */
        public Integer getTotalPages() {
            return totalPages;
        }

        /**
         * @param totalPages The totalPages
         */
        public void setTotalPages(Integer totalPages) {
            this.totalPages = totalPages;
        }

        /**
         * @return The members
         */
        public List<Users> getMembers() {
            return members;
        }

        /**
         * @param members The members
         */
        public void setMembers(List<Users> members) {
            this.members = members;
        }

    }

    public class User {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private Integer termsAccepted;

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public Integer getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(Integer termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

    }
}