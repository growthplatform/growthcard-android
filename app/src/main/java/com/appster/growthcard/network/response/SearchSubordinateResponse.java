
package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchSubordinateResponse {

    @SerializedName("success")
    @Expose
    private int success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private int statusCode;
    @SerializedName("result")
    @Expose
    private Result result;

    /**
     * @return The success
     */
    public int getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(int success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public int getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Result getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Result result) {
        this.result = result;
    }


    public static class Designation {

        @SerializedName("designationId")
        @Expose
        private int designationId;
        @SerializedName("companyId")
        @Expose
        private int companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public int getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(int designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public int getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }

    public static class Member {

        @SerializedName("designationId")
        @Expose
        private int designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Object effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private int companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private Object effectiveActionDescription;
        @SerializedName("teamId")
        @Expose
        private Object teamId;
        @SerializedName("designation")
        @Expose
        private Designation designation;

        /**
         * @return The designationId
         */
        public int getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(int designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Object getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Object effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public int getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(int companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public Object getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(Object effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The teamId
         */
        public Object getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Object teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The designation
         */
        public Designation getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation designation) {
            this.designation = designation;
        }

    }

    public static class Result {

        @SerializedName("pageNo")
        @Expose
        private int pageNo;
        @SerializedName("totalPages")
        @Expose
        private int totalPages;
        @SerializedName("users")
        @Expose
        private List<User> users = new ArrayList<User>();

        /**
         * @return The pageNo
         */
        public int getPageNo() {
            return pageNo;
        }

        /**
         * @param pageNo The pageNo
         */
        public void setPageNo(int pageNo) {
            this.pageNo = pageNo;
        }

        /**
         * @return The totalPages
         */
        public int getTotalPages() {
            return totalPages;
        }

        /**
         * @param totalPages The totalPages
         */
        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        /**
         * @return The users
         */
        public List<User> getUsers() {
            return users;
        }

        /**
         * @param users The users
         */
        public void setUsers(List<User> users) {
            this.users = users;
        }

    }

    public static class User {


        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private int userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private int termsAccepted;
        @SerializedName("member")
        @Expose
        private Member member;
        private Boolean subordinateSelected = false;

        @Override
        public boolean equals(Object object) {
            boolean sameSame = false;

            if (object != null && object instanceof User) {
                sameSame = this.userId == ((User) object).getUserId();
            }

            return sameSame;
        }

        @Override
        public int hashCode() {
            return this.getUserId();
        }


        /**
         * @return The email
         */
        public Boolean getSelected() {
            return subordinateSelected;
        }

        /**
         * @param subordinateSelected The email
         */
        public void setSelected(Boolean subordinateSelected) {
            this.subordinateSelected = subordinateSelected;
        }


        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public int getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(int userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public int getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(int termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

        /**
         * @return The member
         */
        public Member getMember() {
            return member;
        }

        /**
         * @param member The member
         */
        public void setMember(Member member) {
            this.member = member;
        }

    }
}
