
package com.appster.growthcard.network.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeletePostReq {

    @SerializedName("userId")
    @Expose
    private int userId;
    @SerializedName("userToken")
    @Expose
    private String userToken;
    @SerializedName("postId")
    @Expose
    private int postId;
    private String deviceId;

    /**
     * 
     * @return
     *     The userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * 
     * @return
     *     The userToken
     */
    public String getUserToken() {
        return userToken;
    }

    /**
     * 
     * @param userToken
     *     The userToken
     */
    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    /**
     * 
     * @return
     *     The postId
     */
    public int getPostId() {
        return postId;
    }

    /**
     * 
     * @param postId
     *     The postId
     */
    public void setPostId(int postId) {
        this.postId = postId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    /**
     * @param deviceId The deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

}
