
package com.appster.growthcard.network.response;


import com.appster.growthcard.model.Comment;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class SendCommentsResponse {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("statusCode")
    @Expose
    private Integer statusCode;
    @SerializedName("result")
    @Expose
    private Comment result;

    /**
     * @return The success
     */
    public Integer getSuccess() {
        return success;
    }

    /**
     * @param success The success
     */
    public void setSuccess(Integer success) {
        this.success = success;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * @param statusCode The statusCode
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * @return The result
     */
    public Comment getResult() {
        return result;
    }

    /**
     * @param result The result
     */
    public void setResult(Comment result) {
        this.result = result;
    }

//    public class Result {
//
//        @SerializedName("commentId")
//        @Expose
//        private Integer commentId;
//
//        /**
//         * @return The commentId
//         */
//        public Integer getCommentId() {
//            return commentId;
//        }
//
//        /**
//         * @param commentId The commentId
//         */
//        public void setCommentId(Integer commentId) {
//            this.commentId = commentId;
//        }
//
//    }

}