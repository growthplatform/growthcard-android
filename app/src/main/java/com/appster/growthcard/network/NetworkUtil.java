package com.appster.growthcard.network;

import android.content.Context;

import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by navdeep on 25/02/16.
 */
public class NetworkUtil {

    public static final String USER_ID = "userId";
    public static final String USER_TOKEN = "userToken";
    public static final String DEVICE_ID = "deviceId";
    private static AppPrefrences appPref;


    public static OkHttpClient addHeader(final Context context) {

        appPref = new AppPrefrences(context);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(USER_ID, appPref.getUserId() + "")
                        .header(USER_TOKEN, appPref.getUserToken())
                        .header(DEVICE_ID, Utils.getDeviceId(context))
                        .method(original.method(), original.body())
                        .build();


                return chain.proceed(request);
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(logging);

        return httpClient.build();
    }


    public static OkHttpClient addLog(final Context context) {

        appPref = new AppPrefrences(context);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header(USER_ID, appPref.getUserId() + "")
                        .header(USER_TOKEN, appPref.getUserToken())
                        .header(DEVICE_ID, Utils.getDeviceId(context))
                        .method(original.method(), original.body())
                        .build();


                return chain.proceed(request);
            }
        });

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
// set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.interceptors().add(logging);

        return httpClient.build();
    }
}
