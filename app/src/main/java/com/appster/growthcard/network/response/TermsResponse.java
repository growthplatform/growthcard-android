package com.appster.growthcard.network.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class TermsResponse {

    @SerializedName("Success")
    @Expose
    private Integer Success;
    @SerializedName("Message")
    @Expose
    private String Message;
    @SerializedName("StatusCode")
    @Expose
    private Integer StatusCode;
    @SerializedName("Result")
    @Expose
    private List<Object> Result = new ArrayList<Object>();

    /**
     * @return The Success
     */
    public Integer getSuccess() {
        return Success;
    }

    /**
     * @param Success The Success
     */
    public void setSuccess(Integer Success) {
        this.Success = Success;
    }

    /**
     * @return The Message
     */
    public String getMessage() {
        return Message;
    }

    /**
     * @param Message The Message
     */
    public void setMessage(String Message) {
        this.Message = Message;
    }

    /**
     * @return The StatusCode
     */
    public Integer getStatusCode() {
        return StatusCode;
    }

    /**
     * @param StatusCode The StatusCode
     */
    public void setStatusCode(Integer StatusCode) {
        this.StatusCode = StatusCode;
    }

    /**
     * @return The Result
     */
    public List<Object> getResult() {
        return Result;
    }

    /**
     * @param Result The Result
     */
    public void setResult(List<Object> Result) {
        this.Result = Result;
    }

}