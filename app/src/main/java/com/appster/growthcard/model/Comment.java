package com.appster.growthcard.model;

/**
 * Created by navdeep on 21/03/16.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Comment {

    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("role")
    @Expose
    private int role;
    @SerializedName("commentId")
    @Expose
    private Integer commentId;
    @SerializedName("createdAt")
    @Expose
    private CreatedAt createdAt;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("member")
    @Expose
    private Member member;

    /**
     * @return The content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * @return The commentId
     */
    public Integer getCommentId() {
        return commentId;
    }

    /**
     * @param commentId The commentId
     */
    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    /**
     * @return The createdAt
     */
    public CreatedAt getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The createdAt
     */
    public void setCreatedAt(CreatedAt createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The user
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * @return The member
     */
    public Member getMember() {
        return member;
    }

    /**
     * @param member The member
     */
    public void setMember(Member member) {
        this.member = member;
    }


    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public class CreatedAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        /**
         * @return The date
         */
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The timezoneType
         */
        public Integer getTimezoneType() {
            return timezoneType;
        }

        /**
         * @param timezoneType The timezone_type
         */
        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        /**
         * @return The timezone
         */
        public String getTimezone() {
            return timezone;
        }

        /**
         * @param timezone The timezone
         */
        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }

    public class Designation {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("designationTitle")
        @Expose
        private String designationTitle;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The designationTitle
         */
        public String getDesignationTitle() {
            return designationTitle;
        }

        /**
         * @param designationTitle The designationTitle
         */
        public void setDesignationTitle(String designationTitle) {
            this.designationTitle = designationTitle;
        }

    }

    public class Member {

        @SerializedName("designationId")
        @Expose
        private Integer designationId;
        @SerializedName("effectiveActionId")
        @Expose
        private Object effectiveActionId;
        @SerializedName("companyId")
        @Expose
        private Integer companyId;
        @SerializedName("effectiveActionDescription")
        @Expose
        private String effectiveActionDescription;
        @SerializedName("teamId")
        @Expose
        private Integer teamId;
        @SerializedName("designation")
        @Expose
        private Designation designation;

        /**
         * @return The designationId
         */
        public Integer getDesignationId() {
            return designationId;
        }

        /**
         * @param designationId The designationId
         */
        public void setDesignationId(Integer designationId) {
            this.designationId = designationId;
        }

        /**
         * @return The effectiveActionId
         */
        public Object getEffectiveActionId() {
            return effectiveActionId;
        }

        /**
         * @param effectiveActionId The effectiveActionId
         */
        public void setEffectiveActionId(Object effectiveActionId) {
            this.effectiveActionId = effectiveActionId;
        }

        /**
         * @return The companyId
         */
        public Integer getCompanyId() {
            return companyId;
        }

        /**
         * @param companyId The companyId
         */
        public void setCompanyId(Integer companyId) {
            this.companyId = companyId;
        }

        /**
         * @return The effectiveActionDescription
         */
        public String getEffectiveActionDescription() {
            return effectiveActionDescription;
        }

        /**
         * @param effectiveActionDescription The effectiveActionDescription
         */
        public void setEffectiveActionDescription(String effectiveActionDescription) {
            this.effectiveActionDescription = effectiveActionDescription;
        }

        /**
         * @return The teamId
         */
        public Integer getTeamId() {
            return teamId;
        }

        /**
         * @param teamId The teamId
         */
        public void setTeamId(Integer teamId) {
            this.teamId = teamId;
        }

        /**
         * @return The designation
         */
        public Designation getDesignation() {
            return designation;
        }

        /**
         * @param designation The designation
         */
        public void setDesignation(Designation designation) {
            this.designation = designation;
        }

    }

    public class User {

        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("userId")
        @Expose
        private Integer userId;
        @SerializedName("firstName")
        @Expose
        private String firstName;
        @SerializedName("lastName")
        @Expose
        private String lastName;
        @SerializedName("profileImage")
        @Expose
        private String profileImage;
        @SerializedName("termsAccepted")
        @Expose
        private Integer termsAccepted;

        /**
         * @return The email
         */
        public String getEmail() {
            return email;
        }

        /**
         * @param email The email
         */
        public void setEmail(String email) {
            this.email = email;
        }

        /**
         * @return The userId
         */
        public Integer getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        /**
         * @return The firstName
         */
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        /**
         * @return The lastName
         */
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The profileImage
         */
        public String getProfileImage() {
            return profileImage;
        }

        /**
         * @param profileImage The profileImage
         */
        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        /**
         * @return The termsAccepted
         */
        public Integer getTermsAccepted() {
            return termsAccepted;
        }

        /**
         * @param termsAccepted The termsAccepted
         */
        public void setTermsAccepted(Integer termsAccepted) {
            this.termsAccepted = termsAccepted;
        }

    }

}