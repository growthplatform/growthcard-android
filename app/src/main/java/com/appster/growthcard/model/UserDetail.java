package com.appster.growthcard.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetail implements Parcelable {

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("userId")
    @Expose
    private Integer userId;
    @SerializedName("firstName")
    @Expose
    private String firstName;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("profileImage")
    @Expose
    private String profileImage;
    @SerializedName("termsAccepted")
    @Expose
    private Integer termsAccepted;

    /**
     * @return The email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return The firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName The firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     * @param profileImage The profileImage
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     * @return The termsAccepted
     */
    public Integer getTermsAccepted() {
        return termsAccepted;
    }

    /**
     * @param termsAccepted The termsAccepted
     */
    public void setTermsAccepted(Integer termsAccepted) {
        this.termsAccepted = termsAccepted;
    }

    protected UserDetail(Parcel in) {
        email = in.readString();
        userId = in.readByte() == 0x00 ? null : in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        profileImage = in.readString();
        termsAccepted = in.readByte() == 0x00 ? null : in.readInt();
    }

    public UserDetail() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
        if (userId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(userId);
        }
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(profileImage);
        if (termsAccepted == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(termsAccepted);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<UserDetail> CREATOR = new Parcelable.Creator<UserDetail>() {
        @Override
        public UserDetail createFromParcel(Parcel in) {
            return new UserDetail(in);
        }

        @Override
        public UserDetail[] newArray(int size) {
            return new UserDetail[size];
        }
    };
}