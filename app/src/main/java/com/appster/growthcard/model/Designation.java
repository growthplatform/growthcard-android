package com.appster.growthcard.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by navdeep on 17/03/16.
 */
public class Designation implements Parcelable {

    @SerializedName("designationId")
    @Expose
    private Integer designationId;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("designationTitle")
    @Expose
    private String designationTitle;

    /**
     * @return The designationId
     */
    public Integer getDesignationId() {
        return designationId;
    }

    /**
     * @param designationId The designationId
     */
    public void setDesignationId(Integer designationId) {
        this.designationId = designationId;
    }

    /**
     * @return The companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The designationTitle
     */
    public String getDesignationTitle() {
        return designationTitle;
    }

    /**
     * @param designationTitle The designationTitle
     */
    public void setDesignationTitle(String designationTitle) {
        this.designationTitle = designationTitle;
    }

    public Designation() {

    }


    private Designation(Parcel in) {
        designationId = in.readByte() == 0x00 ? null : in.readInt();
        companyId = in.readByte() == 0x00 ? null : in.readInt();
        designationTitle = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (designationId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(designationId);
        }
        if (companyId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(companyId);
        }
        dest.writeString(designationTitle);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Designation> CREATOR = new Parcelable.Creator<Designation>() {
        @Override
        public Designation createFromParcel(Parcel in) {
            return new Designation(in);
        }

        @Override
        public Designation[] newArray(int size) {
            return new Designation[size];
        }
    };
}