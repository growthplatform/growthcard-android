package com.appster.growthcard.model;

/**
 * Created by manish on 12/10/15.
 */
public class NavItems {
    public String mTitle;
    public boolean isSelected = false;
    public boolean isNotifications = false;

    public NavItems(String title) {
        mTitle = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public boolean isNotifications() {
        return isNotifications;
    }

    public void setIsNotifications(boolean isNotifications) {
        this.isNotifications = isNotifications;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setIsSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }
}
