package com.appster.growthcard.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by navdeep on 16/03/16.
 */
public class Graph implements Parcelable {

    @SerializedName("adhrenceCount")
    @Expose
    private Integer adhrenceCount;
    @SerializedName("days")
    @Expose
    private String days;

    /**
     * @return The adhrenceCount
     */
    public Integer getAdhrenceCount() {
        return adhrenceCount;
    }

    /**
     * @param adhrenceCount The adhrenceCount
     */
    public void setAdhrenceCount(Integer adhrenceCount) {
        this.adhrenceCount = adhrenceCount;
    }

    /**
     * @return The days
     */
    public String getDays() {
        return days;
    }

    /**
     * @param days The days
     */
    public void setDays(String days) {
        this.days = days;
    }


    protected Graph(Parcel in) {
        adhrenceCount = in.readByte() == 0x00 ? null : in.readInt();
        days = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (adhrenceCount == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(adhrenceCount);
        }
        dest.writeString(days);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Graph> CREATOR = new Parcelable.Creator<Graph>() {
        @Override
        public Graph createFromParcel(Parcel in) {
            return new Graph(in);
        }

        @Override
        public Graph[] newArray(int size) {
            return new Graph[size];
        }
    };
}