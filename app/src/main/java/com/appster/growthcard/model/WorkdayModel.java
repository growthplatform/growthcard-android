package com.appster.growthcard.model;

/**
 * Created by himanshu on 23/02/16.
 */
public class WorkdayModel {

    private final String day;
    private boolean selected;

    public WorkdayModel(String day) {
        this.day = day;

    }

    public String getDay() {
        return day;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }


}
