package com.appster.growthcard.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by navdeep on 15/03/16.
 */
public class Users implements Parcelable {

    @SerializedName("designationId")
    @Expose
    private Integer designationId;
    @SerializedName("effectiveActionId")
    @Expose
    private Integer effectiveActionId;
    @SerializedName("companyId")
    @Expose
    private Integer companyId;
    @SerializedName("effectiveActionDescription")
    @Expose
    private String effectiveActionDescription;
    @SerializedName("teamId")
    @Expose
    private Integer teamId;
    @SerializedName("designation")
    @Expose
    private Designation designation;
    @SerializedName("user")
    @Expose
    private UserDetail user;

    /**
     * @return The designationId
     */
    public Integer getDesignationId() {
        return designationId;
    }

    /**
     * @param designationId The designationId
     */
    public void setDesignationId(Integer designationId) {
        this.designationId = designationId;
    }

    /**
     * @return The effectiveActionId
     */
    public Integer getEffectiveActionId() {
        return effectiveActionId;
    }

    /**
     * @param effectiveActionId The effectiveActionId
     */
    public void setEffectiveActionId(int effectiveActionId) {
        this.effectiveActionId = effectiveActionId;
    }

    /**
     * @return The companyId
     */
    public Integer getCompanyId() {
        return companyId;
    }

    /**
     * @param companyId The companyId
     */
    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    /**
     * @return The effectiveActionDescription
     */
    public String getEffectiveActionDescription() {
        return effectiveActionDescription;
    }

    /**
     * @param effectiveActionDescription The effectiveActionDescription
     */
    public void setEffectiveActionDescription(String effectiveActionDescription) {
        this.effectiveActionDescription = effectiveActionDescription;
    }

    /**
     * @return The teamId
     */
    public Integer getTeamId() {
        return teamId;
    }

    /**
     * @param teamId The teamId
     */
    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    /**
     * @return The designation
     */
    public Designation getDesignation() {
        return designation;
    }

    /**
     * @param designation The designation
     */
    public void setDesignation(Designation designation) {
        this.designation = designation;
    }

    /**
     * @return The user
     */
    public UserDetail getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    public void setUser(UserDetail user) {
        this.user = user;
    }


    public Users() {

    }


    protected Users(Parcel in) {
        designationId = in.readByte() == 0x00 ? null : in.readInt();
        effectiveActionId = in.readByte() == 0x00 ? null : in.readInt();
        companyId = in.readByte() == 0x00 ? null : in.readInt();
        effectiveActionDescription = in.readString();
        teamId = in.readByte() == 0x00 ? null : in.readInt();
        designation = (Designation) in.readValue(Designation.class.getClassLoader());
        user = (UserDetail) in.readValue(UserDetail.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (designationId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(designationId);
        }
        if (effectiveActionId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(effectiveActionId);
        }
        if (companyId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(companyId);
        }
        dest.writeString(effectiveActionDescription);
        if (teamId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(teamId);
        }
        dest.writeValue(designation);
        dest.writeValue(user);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Users> CREATOR = new Parcelable.Creator<Users>() {
        @Override
        public Users createFromParcel(Parcel in) {
            return new Users(in);
        }

        @Override
        public Users[] newArray(int size) {
            return new Users[size];
        }
    };
}