package com.appster.growthcard.interfaces;

import android.net.Uri;

/**
 * Created by santosh on 22/10/15.
 */
public interface OnFragmentInteractionListener {

    void onFragmentInteraction(Uri uri);
}
