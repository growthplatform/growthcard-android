package com.appster.growthcard.interfaces;

import com.appster.growthcard.ui.customviews.ScrollViewList;

/**
 * Created by navdeep on 12/04/16.
 */
public interface ScrollViewListener {
    void onScrollChanged(ScrollViewList scrollView,
                         int x, int y, int oldx, int oldy);
}