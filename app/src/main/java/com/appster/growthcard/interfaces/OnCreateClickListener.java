package com.appster.growthcard.interfaces;

/**
 * Created by himanshukathuria on 16/03/16.
 */
public interface OnCreateClickListener {
    void onCreateClick(String subject, String message, int flag);
}
