package com.appster.growthcard.utils;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.model.Graph;
import com.appster.growthcard.network.response.EAListResponse;
import com.appster.growthcard.network.response.FeedsResponse;
import com.appster.growthcard.network.response.SignInResponse;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;



/**
 * Created by navdeep on 13/10/15.
 */
public class Utils {

    public static final String[] days = new String[]{"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"};

    public static final String FINISH_SUB_LIST = "finish_sub";
    public static final String ALARM_CALL = "alarm_call";
    public static final String FINISH_SUB_PROFILE = "finish_sub";
    public static final String FINISH_EA_LIST = "finish_ea";

    public static final String DEVICE_TYPE = "android";

    public static final String REGISTRATION_COMPLETE = "complete";
    public static final String BOLD = "b";
    public static final String REGULAR = "r";
    public static final String MEDIUM = "m";
    public static final String LIGHT = "l";
    public static final String ITALIC = "i";

    public static final int EA_CHAR_LIMIT = 140;

    public static boolean isEACleared = false;
    public static boolean isEAUpdated = false;
    public static boolean isNotification = false;


    public static final int[] subMenuDrawableRes = {R.drawable.ico_feed, R.drawable.ico_effective, R.drawable.ico_post, R.drawable.ico_notifications};
    public static final int[] subMenuActiveDrawableRes = {R.drawable.ico_feed_active, R.drawable.ico_effective_active, R.drawable.ico_post_active, R.drawable.ico_notifications_active};

    public static final int[] manMenuDrawableRes = {R.drawable.ico_effective, R.drawable.ico_post, R.drawable.ico_notifications};
    public static final int[] manMenuActiveDrawableRes = {R.drawable.ico_effective_active, R.drawable.ico_post_active, R.drawable.ico_notifications_active};

    public static FeedsResponse.RecentAnnouncement recentAnnouncement;

    public static EAListResponse.EffectiveAction selectedEA;
    public static String selectedEADescription = "";
    public static String imageUrl;
    public static String imageCaption;
    public static String imageMessage;


    public static void showResMsg(View view, String msg) {

        final View.OnClickListener clickListener = new View.OnClickListener() {
            public void onClick(View v) {

            }
        };

        Snackbar.make(view, msg, Snackbar.LENGTH_SHORT).setAction("CANCEL", clickListener).show();


    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static SweetAlertDialog loadingDialog(final Context context) {
        final SweetAlertDialog pDialog = new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(ContextCompat.getColor(context, R.color.colorAccent));
        pDialog.setTitleText("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
        return pDialog;
    }


    public static void setTextStyleButton(Button v, Context context, String fontType) {
        if (fontType.equalsIgnoreCase(BOLD)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Bold.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(REGULAR)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Regular.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(ITALIC)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Italic.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(MEDIUM)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Medium.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(LIGHT)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Light.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        }

    }

    public static void setTextStyle(TextView v, Context context, String fontType) {//
        if (fontType.equalsIgnoreCase(BOLD)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Bold.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(REGULAR)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Regular.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(ITALIC)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Italic.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(MEDIUM)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Medium.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        } else if (fontType.equalsIgnoreCase(LIGHT)) {
            Typeface tf = Typeface.createFromAsset(context.getAssets(), "Rubik-Light.ttf");
            v.setTypeface(tf);
            v.getPaint().setSubpixelText(true);
        }

    }

    public static void applyFontForToolbarTitle(Activity context, Toolbar toolbar) {
        try {
            for (int i = 0; i < toolbar.getChildCount(); i++) {
                View view = toolbar.getChildAt(i);
                if (view instanceof TextView) {
                    TextView tv = (TextView) view;

                    if (tv.getText().equals(context.getTitle())) {
                        setTextStyle(tv, context, Utils.REGULAR);
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadUrlImage(String url, ImageView imageView, boolean placeHolderImage) {
        DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoader.getInstance().displayImage(url, imageView, mOptions);

    }


    public static void loadUrlImage(String url, ImageView imageView) {
        DisplayImageOptions mOptions = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.profile_holder)
                .showImageForEmptyUri(R.drawable.profile_holder)
                .showImageOnFail(R.drawable.profile_holder)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoader.getInstance().displayImage(url, imageView, mOptions);

    }


    public static String getDateData() {

        Date dateObj = new Date();
        SimpleDateFormat postFormater = new SimpleDateFormat("dd MMMM");
        return postFormater.format(dateObj);

    }

    public static String getUTCDate(Date myDate) {


        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.setTime(myDate);
        Date time = calendar.getTime();
        //2015-09-25 12:30:32
        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateAsString = outputFmt.format(time);
        System.out.println(dateAsString);
        return dateAsString;
    }


    public static String getDateTime(Date myDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.setTime(myDate);
        Date time = calendar.getTime();
        //2015-09-25 12:30:32
        SimpleDateFormat outputFmt = new SimpleDateFormat("MMM dd, hh:mm");
        String dateAsString = outputFmt.format(time);
        System.out.println(dateAsString);
        return dateAsString;
    }

    public static String getDate(Date myDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.setTime(myDate);
        Date time = calendar.getTime();
        //2015-09-25 12:30:32
        SimpleDateFormat outputFmt = new SimpleDateFormat("dd MMM");
        String dateAsString = outputFmt.format(time);
        System.out.println(dateAsString);
        return dateAsString;
    }

    public static String getDate(Date myDate, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getDefault());
        calendar.setTime(myDate);
        Date time = calendar.getTime();
        SimpleDateFormat outputFmt = new SimpleDateFormat(format);
        String dateAsString = outputFmt.format(time);
        System.out.println(dateAsString);
        return dateAsString;
    }


    public static int dpToPx(Context context, int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
    }

    public static int pxToDp(Context context, int px) {

        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, px,
                context.getResources().getDisplayMetrics());
    }


    public static void makeTextViewResizable(final Context context, final TextView tv, final int maxLine, final String expandText, final boolean viewMore) {

        if (tv.getTag() == null) {
            tv.setTag(tv.getText());
        }
        ViewTreeObserver vto = tv.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @Override
            public void onGlobalLayout() {

                ViewTreeObserver obs = tv.getViewTreeObserver();
                obs.removeGlobalOnLayoutListener(this);
                if (maxLine == 0) {
                    int lineEndIndex = tv.getLayout().getLineEnd(0);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(context, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else if (maxLine > 0 && tv.getLineCount() >= maxLine) {
                    int lineEndIndex = tv.getLayout().getLineEnd(maxLine - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex - expandText.length() + 1) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(context, Html.fromHtml(tv.getText().toString()), tv, maxLine, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                } else {
                    int lineEndIndex = tv.getLayout().getLineEnd(tv.getLayout().getLineCount() - 1);
                    String text = tv.getText().subSequence(0, lineEndIndex) + " " + expandText;
                    tv.setText(text);
                    tv.setMovementMethod(LinkMovementMethod.getInstance());
                    tv.setText(
                            addClickablePartTextViewResizable(context, Html.fromHtml(tv.getText().toString()), tv, lineEndIndex, expandText,
                                    viewMore), TextView.BufferType.SPANNABLE);
                }
            }
        });

    }

    public static void expandTextView(TextView tv) {
        ObjectAnimator animation = ObjectAnimator.ofInt(tv, "maxLines", tv.getLineCount());
        animation.setDuration(200).start();
    }


    private static SpannableStringBuilder addClickablePartTextViewResizable(final Context context, final Spanned strSpanned, final TextView tv,
                                                                            final int maxLine, final String spanableText, final boolean viewMore) {
        String str = strSpanned.toString();
        SpannableStringBuilder ssb = new SpannableStringBuilder(strSpanned);

        if (str.contains(spanableText)) {
            ssb.setSpan(new MySpannable(false) {

                @Override
                public void onClick(View widget) {

                    if (viewMore) {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(context, tv, -1, "Read less", false);
                    } else {
                        tv.setLayoutParams(tv.getLayoutParams());
                        tv.setText(tv.getTag().toString(), TextView.BufferType.SPANNABLE);
                        tv.invalidate();
                        makeTextViewResizable(context, tv, 4, "Read More", true);
                    }

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), 0);

            ssb.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.button_green)), str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);


        }
        return ssb;


    }

    public static int getScreenHeight(Activity context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        return height;
    }

    public static int getScreenWidth(Activity context) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        context.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int width = displaymetrics.widthPixels;
        return width;
    }

    public static BarChart setChart(Context context, LinearLayout layoutParent, List<Graph> graphVO, boolean isSub) {


        BarChart mChart = new BarChart(context);
        int height = Utils.dpToPx(context, 150);
        int width = graphVO.size() * Utils.dpToPx(context, 50);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mChart.setLayoutParams(params);//new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        if (layoutParent.getId() == R.id.llGraphSmall) {
            if (graphVO.size() > 0) {
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view_small)).setVisibility(View.GONE);
            } else {
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view_small)).setVisibility(View.VISIBLE);
            }
        } else if (layoutParent.getChildCount() > 1) {
            if (graphVO.size() > 0) {
                ((TextView) layoutParent.findViewById(R.id.empty_graph_view)).setVisibility(View.GONE);
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view)).setVisibility(View.GONE);
            } else {
                ((TextView) layoutParent.findViewById(R.id.empty_graph_view)).setVisibility(View.VISIBLE);
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view)).setVisibility(View.VISIBLE);
            }
        }


        if (graphVO.size() < 6 && layoutParent.getChildCount() > 1) {
            mChart.setLayoutParams(new FrameLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
        }

        ///TO IMPLEMENT SCROLLABLE GRAPH Uncomment this
//        if(width>getScreenWidth((Activity)context)) {
//            mChart.setLayoutParams(new FrameLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
//        }
//        else {
//            mChart.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//        }
//

        if (layoutParent.getChildCount() > 2) {
            layoutParent.removeViewAt(2);


        }


        mChart.setClickable(false);
        layoutParent.addView(mChart);
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.getXAxis().setDrawAxisLine(false);
        mChart.setTouchEnabled(false);

        if (graphVO.size() > 15)
            mChart.getAxisLeft().setEnabled(true);
        else
            mChart.getAxisLeft().setEnabled(false);

        mChart.getAxisRight().setEnabled(false);
        mChart.setDescription("");
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getAxisRight().setDrawGridLines(false);
        mChart.getLegend().setEnabled(false);


        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setScaleEnabled(false);
        mChart.setHorizontalScrollBarEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setData(Utils.setData(graphVO, context, layoutParent, isSub));

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setLabelsToSkip(0);

        if (graphVO.size() > 8) {//            xAxis.label(0);
            mChart.getXAxis().setEnabled(false);
        } else {
            mChart.getXAxis().setEnabled(true);

        }

        mChart.getXAxis().setEnabled(true);
        if (layoutParent.getChildCount() == 2) {
            mChart.getXAxis().setEnabled(false);

        }


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(6, false);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)
        mChart.getXAxis().setTextColor(ContextCompat.getColor(context, R.color.graph_gray));
        mChart.notifyDataSetChanged();

        mChart.animateY(2000);
        return mChart;
    }

    public static void setGraphChart(Context context, LinearLayout layoutParent, List<Graph> graphVO, boolean isSub) {


        BarChart mChart = new BarChart(context);
        int height = Utils.dpToPx(context, 150);
        int width = graphVO.size() * Utils.dpToPx(context, 50);

        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mChart.setLayoutParams(params);//new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));


        if (layoutParent.getId() == R.id.llGraphSmall) {
            if (graphVO.size() > 0) {
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view_small)).setVisibility(View.GONE);
            } else {
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view_small)).setVisibility(View.VISIBLE);
            }
        } else if (layoutParent.getChildCount() > 1) {
            if (graphVO.size() > 0) {
                ((TextView) layoutParent.findViewById(R.id.empty_graph_view)).setVisibility(View.GONE);
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view)).setVisibility(View.GONE);
            } else {
                ((TextView) layoutParent.findViewById(R.id.empty_graph_view)).setVisibility(View.VISIBLE);
                ((ImageView) layoutParent.findViewById(R.id.empty_image_view)).setVisibility(View.VISIBLE);
            }
        }


        if (graphVO.size() < 6 && layoutParent.getChildCount() > 1) {
            mChart.setLayoutParams(new FrameLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
        }

        ///TO IMPLEMENT SCROLLABLE GRAPH Uncomment this
//        if(width>getScreenWidth((Activity)context)) {
//            mChart.setLayoutParams(new FrameLayout.LayoutParams(width, ViewGroup.LayoutParams.MATCH_PARENT));
//        }
//        else {
//            mChart.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
//
//        }
//

        if (layoutParent.getChildCount() > 2) {
            layoutParent.removeViewAt(2);


        }


        mChart.setClickable(false);
        layoutParent.addView(mChart);
        mChart.setDrawBarShadow(false);
        mChart.setDrawValueAboveBar(true);
        mChart.getXAxis().setDrawAxisLine(false);
        mChart.setTouchEnabled(false);

        if (graphVO.size() > 15)
            mChart.getAxisLeft().setEnabled(true);
        else
            mChart.getAxisLeft().setEnabled(false);

        mChart.getAxisRight().setEnabled(false);
        mChart.setDescription("");
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getAxisRight().setDrawGridLines(false);
        mChart.getLegend().setEnabled(false);


        // scaling can now only be done on x- and y-axis separately
        mChart.setPinchZoom(false);
        mChart.setScaleEnabled(false);
        mChart.setHorizontalScrollBarEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setData(Utils.setData(graphVO, context, layoutParent, isSub));

        XAxis xAxis = mChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setLabelsToSkip(0);
        if (graphVO.size() > 8) {//            xAxis.label(0);
            mChart.getXAxis().setEnabled(false);
        } else {
            mChart.getXAxis().setEnabled(true);

        }

        mChart.getXAxis().setEnabled(true);
        if (layoutParent.getChildCount() == 2) {
            mChart.getXAxis().setEnabled(false);

        }


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setLabelCount(6, false);
        leftAxis.setSpaceTop(15f);
        leftAxis.setAxisMinValue(0f); // this replaces setStartAtZero(true)

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setSpaceTop(15f);
        rightAxis.setAxisMinValue(0f); // this replaces setStargit tAtZero(true)
        mChart.getXAxis().setTextColor(ContextCompat.getColor(context, R.color.graph_gray));
        mChart.notifyDataSetChanged();
    }


    public static BarData setData(List<Graph> graphVOs, Context context, LinearLayout layout, boolean isSub) {

        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<BarEntry> yVals1 = new ArrayList<BarEntry>();

        int green = ContextCompat.getColor(context, R.color.button_green);
        int[] colors = new int[graphVOs.size()];// {ContextCompat.getColor(context, R.color.graph_yellow), green, green, green, green, green, ContextCompat.getColor(context, R.color.toolbar_btn)};


        for (int i = 0; i < graphVOs.size(); i++) {
            xVals.add(graphVOs.get(i).getDays().toString().trim().toUpperCase());

            float val = (float) (graphVOs.get(i).getAdhrenceCount());

            yVals1.add(new BarEntry(val, i));
            if (graphVOs.get(i).getDays().equalsIgnoreCase("avg"))
                colors[i] = ContextCompat.getColor(context, R.color.graph_yellow);
            else if (graphVOs.get(i).getDays().equalsIgnoreCase("tod") || graphVOs.get(i).getDays().equalsIgnoreCase("today"))
                colors[i] = ContextCompat.getColor(context, R.color.toolbar_btn);
            else
                colors[i] = green;


        }


        BarDataSet set1 = new BarDataSet(yVals1, "DataSet");
//        set1.setBarSpacePercent(35f);

        if (graphVOs.size() > 15)
            set1.setDrawValues(false);
        else
            set1.setDrawValues(true);

        if (layout.getChildCount() == 2)
            set1.setDrawValues(false);

        if (isSub)
            set1.setDrawValues(false);


        set1.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
                return (int) value + "";
            }
        });
        set1.setColors(colors);
        set1.setBarSpacePercent(5f);
        set1.setBarShadowColor(Color.TRANSPARENT);

        ArrayList<IBarDataSet> dataSets = new ArrayList<IBarDataSet>();
        dataSets.add(set1);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(10f);

        return data;
    }


    public static void saveUserDetails(SignInResponse response, Context context) {
        try {
            AppPrefrences appPref = new AppPrefrences(context);
            appPref.setUserId(response.getResult().getUserId());
            appPref.setUserToken(response.getResult().getUserToken());
            //TODO
            appPref.setRole(response.getResult().getRole());
            appPref.setEmail(response.getResult().getEmail());
            appPref.setFirstName(response.getResult().getFirstName());
            appPref.setLastName(response.getResult().getLastName());
            appPref.setTermsAccepted(response.getResult().getTermsAccepted() == 1);
            appPref.setSwitch(response.getResult().getSwitch());


            if (response.getResult().getMember() != null) {

                if (response.getResult().getMember().getCompany() != null) {
                    appPref.setCompanyName(response.getResult().getMember().getCompany().getCompanyName());
                }

                appPref.setCompanyId(response.getResult().getMember().getCompanyId());


                if (response.getResult().getMember().getEffective() != null) {
                    appPref.setEATitle(response.getResult().getMember().getEffective().getEffectiveActionTitle());
                }

                if (response.getResult().getMember().getEffectiveActionId() != null)
                    appPref.setEAId(response.getResult().getMember().getEffectiveActionId());

                appPref.setDesignation(response.getResult().getMember().getDesignation().getDesignationTitle());
                appPref.setDesignationId(response.getResult().getMember().getDesignation().getDesignationId());
                appPref.setTeamId(response.getResult().getMember().getTeamId());
            }

            if (response.getResult().getUserdepartment() != null) {
                appPref.setDepartmentName(response.getResult().getUserdepartment().getDepartment().getDepartmentName());
                appPref.setDepartmentId(response.getResult().getUserdepartment().getDepartment().getDepartmentId());
            }

            appPref.setProfileImage(response.getResult().getProfileImage());
            appPref.setWorkDaysSelected(response.getResult().getDays().size() != 0);
            appPref.setLoginPath(context.getResources().getInteger(R.integer.TermsPath));


            boolean[] workDays = {false, false, false, false, false, false, false};
            String workDayString = "";
            for (SignInResponse.Day day : response.getResult().getDays()) {

                workDays[day.getWorkDay() - 1] = true;

            }
            for (int i = 0; i < workDays.length; i++) {
                workDayString = workDayString + (workDays[i] ? "1" : "0");
            }

            appPref.setWorkDays(workDayString);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public static void setTranslucentStatusBar(Window window) {
        if (window == null) return;
        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt >= Build.VERSION_CODES.LOLLIPOP) {
            setTranslucentStatusBarLollipop(window);
        } else if (sdkInt >= Build.VERSION_CODES.KITKAT) {
            setTranslucentStatusBarKiKat(window);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void setTranslucentStatusBarLollipop(Window window) {
        window.setStatusBarColor(
                ContextCompat.getColor(window.getContext(), R.color.colorPrimary));
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private static void setTranslucentStatusBarKiKat(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

}
