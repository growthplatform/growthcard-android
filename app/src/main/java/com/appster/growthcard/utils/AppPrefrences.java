package com.appster.growthcard.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.appster.growthcard.ui.fragments.SubDashboardFragment;

/**
 * Created by navdeep on 19/02/16.
 */
public class AppPrefrences {

    /**
     * AppPrefrences class for saving data in device
     */

    private static final String APP_SHARED_PREFS = "growthcard";
    private SharedPreferences appSharedPreferences;
    private SharedPreferences.Editor prefEditor;
    private Context mContext;

    @SuppressLint("CommitPrefEdits")
    public AppPrefrences(Context context) {
        mContext = context;
        this.appSharedPreferences = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
        this.prefEditor = appSharedPreferences.edit();
    }


    public void logOut() {

        String email = getEmail();
        String deviceToken = getGCMDeviceToken();
        prefEditor.clear();
        prefEditor.commit();

        setEmail(email);
        SubDashboardFragment.flag1 = 1;
        SubDashboardFragment.flag2 = 0;
        setGCMDeviceToken(deviceToken);

    }



    public void setGCMDeviceToken(String GCMDeviceToken) {
        prefEditor.putString("setGCMDeviceToken", GCMDeviceToken);
        prefEditor.commit();
    }

    public String getGCMDeviceToken() {
        return appSharedPreferences.getString("setGCMDeviceToken", "");
    }

    public void setGCMRegistrationStatus(boolean GCMRegistrationStatus) {
        prefEditor.putBoolean("setGCMRegistrationStatus", GCMRegistrationStatus);
        prefEditor.commit();
    }

    public boolean isGCMRegistrationStatus() {
        return appSharedPreferences.getBoolean("setGCMRegistrationStatus", false);
    }


    public void setLoginPath(int pathTag) {
        prefEditor.putInt("loginPath", pathTag);
        prefEditor.commit();

    }

    public int getLoginPath() {
        return appSharedPreferences.getInt("loginPath", 0);
    }


    ////user details

    public void setUserId(int userId) {
        prefEditor.putInt("userId", userId);
        prefEditor.commit();

    }

    public int getUserId() {
        return appSharedPreferences.getInt("userId", 0);
    }

    public void setUserToken(String userToken) {
        prefEditor.putString("userToken", userToken);
        prefEditor.commit();

    }

    public String getUserToken() {
        return appSharedPreferences.getString("userToken", "");
    }


    public void setEmail(String email) {
        prefEditor.putString("email", email);
        prefEditor.commit();

    }

    public String getEmail() {
        return appSharedPreferences.getString("email", "");
    }

    public void setFirstName(String firstName) {
        prefEditor.putString("firstName", firstName);
        prefEditor.commit();

    }

    public String getFirstName() {
        return appSharedPreferences.getString("firstName", "");
    }

    public void setLastName(String lastName) {
        prefEditor.putString("lastName", lastName);
        prefEditor.commit();

    }

    public String getLastName() {
        return appSharedPreferences.getString("lastName", "");
    }

    public void setRole(int role) {
        prefEditor.putInt("role", role);
        prefEditor.commit();

    }

    public int getRole() {
        return appSharedPreferences.getInt("role", 0);
    }


    public void setTermsAccepted(boolean termsAccepted) {
        prefEditor.putBoolean("termsAccepted", termsAccepted);
        prefEditor.commit();

    }

    public boolean isTermsAcceted() {
        return appSharedPreferences.getBoolean("termsAccepted", false);
    }

    public void setWorkDaysSelected(boolean termsAccepted) {
        prefEditor.putBoolean("workDaysSelected", termsAccepted);
        prefEditor.commit();

    }

    public boolean isWorkDaysSelected() {
        return appSharedPreferences.getBoolean("workDaysSelected", false);
    }


    public void setCompanyName(String companyName) {
        prefEditor.putString("companyName", companyName);
        prefEditor.commit();

    }

    public String getCompanyName() {
        return appSharedPreferences.getString("companyName", "");
    }

    public void setProfileImage(String profileImage) {
        prefEditor.putString("profileImage", profileImage);
        prefEditor.commit();

    }

    public String getProfileImage() {
        return appSharedPreferences.getString("profileImage", "");
    }

    public void setDepartmentName(String departmentName) {
        prefEditor.putString("departmentName", departmentName);
        prefEditor.commit();

    }

    public String getDepartmentName() {
        return appSharedPreferences.getString("departmentName", "");
    }


    public void setEADescription(String eaDescription) {
        prefEditor.putString("eaDescription", eaDescription);
        prefEditor.commit();

    }

    public String getEADescription() {
        return appSharedPreferences.getString("eaDescription", "");

    }


    public void setDesignation(String teamName) {
        prefEditor.putString("teamName", teamName);
        prefEditor.commit();

    }

    public String getDesignation() {
        return appSharedPreferences.getString("teamName", "");
    }

    public void setWorkDays(String workDays) {

        prefEditor.putString("workDays", workDays);
        prefEditor.commit();

    }

    public String getWorkDays() {
        return appSharedPreferences.getString("workDays", "");
    }


    public void setTeamId(Integer teamId) {
        prefEditor.putInt("teamId", teamId);
        prefEditor.commit();
    }

    public int getTeamId() {
        return appSharedPreferences.getInt("teamId", 0);
    }

    public void setDesignationId(Integer designationId) {
        prefEditor.putInt("designationId", designationId);
        prefEditor.commit();
    }

    public int getDesignationId() {
        return appSharedPreferences.getInt("designationId", 0);
    }

    public void setEAId(Integer designationId) {
        prefEditor.putInt("eaId", designationId);
        prefEditor.commit();
    }

    public int getEAId() {
        return appSharedPreferences.getInt("eaId", 0);
    }

    public void setCompanyId(Integer teamId) {
        prefEditor.putInt("companyId", teamId);
        prefEditor.commit();
    }

    public int getCompanyId() {
        return appSharedPreferences.getInt("companyId", 0);
    }

    public void setEATitle(String eaTitle) {
        prefEditor.putString("eaTitle", eaTitle);
        prefEditor.commit();

    }

    public String geteaTitle() {
        return appSharedPreferences.getString("eaTitle", "");
    }


    public void setDepartmentId(Integer departmentId) {
        prefEditor.putInt("departmentId", departmentId);
        prefEditor.commit();
    }

    public int getDepartmentId() {
        return appSharedPreferences.getInt("departmentId", 0);
    }

    public void setSwitch(Integer mSwitch) {
        prefEditor.putInt("mSwitch", mSwitch);
        prefEditor.commit();
    }

    public int getSwitch() {
        return appSharedPreferences.getInt("mSwitch", 0);
    }

    public void setAdherenceRandomNum(int adherenceRandomNum) {
        prefEditor.putInt("adherenceRandomNum", adherenceRandomNum);
        prefEditor.commit();

    }

    public int getAdherenceRandomNum() {

        return appSharedPreferences.getInt("adherenceRandomNum", 0);

    }

    public void setAdherenceRandomCount(int adherenceRandomCount) {
        prefEditor.putInt("adherenceRandomCount", adherenceRandomCount);
        prefEditor.commit();

    }

    public int getAdherenceRandomCount() {

        return appSharedPreferences.getInt("adherenceRandomCount", 0);

    }


    public boolean isSubNotification() {
        return appSharedPreferences.getBoolean("isSubNotification", false);
    }

    public void setSubNotification(boolean notification) {
        prefEditor.putBoolean("isSubNotification", notification);
        prefEditor.commit();
    }

    public boolean isManNotification() {
        return appSharedPreferences.getBoolean("isSubNotification", false);
    }

    public void setManNotification(boolean notification) {
        prefEditor.putBoolean("isSubNotification", notification);
        prefEditor.commit();
    }

    public void setNotificationUpdate(boolean notificationUpdate) {
        prefEditor.putBoolean("setNotificationUpdate", notificationUpdate);
        prefEditor.commit();
    }

    public boolean getNotificationUpdate() {
        return appSharedPreferences.getBoolean("setNotificationUpdate", false);
    }


    public void setRecentAnnouncementId(int announcementId) {
        prefEditor.putInt("recentannouncementId", announcementId);
        prefEditor.commit();

    }

    public int getRecentAnnouncementId() {

        return appSharedPreferences.getInt("recentannouncementId", 0);

    }

    public void setStartDate(String startDate) {
        prefEditor.putString("startDate", startDate);
        prefEditor.commit();

    }

    public String getStartDate() {
        return appSharedPreferences.getString("startDate", "");
    }


}
