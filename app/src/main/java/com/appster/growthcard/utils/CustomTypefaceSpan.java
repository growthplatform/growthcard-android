package com.appster.growthcard.utils;

/**
 * Created by himanshukathuria on 31/03/16.
 */

import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

public class CustomTypefaceSpan extends TypefaceSpan {

    private final Typeface newType;
    private final Context context;
    private int textColor = 0;


    public CustomTypefaceSpan(Context context, Typeface type, int textColor) {
        super("");
        newType = type;
        this.textColor = textColor;
        this.context = context;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        applyCustomTypeFace(ds, newType);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        applyCustomTypeFace(paint, newType);
    }

    private void applyCustomTypeFace(Paint paint, Typeface tf) {
        int oldStyle;
        Typeface old = paint.getTypeface();
        if (old == null) {
            oldStyle = 0;
        } else {
            oldStyle = old.getStyle();
        }

        paint.setColor(textColor);

//        if (isBold) {
//            paint.setColor(ContextCompat.getColor(context, R.color.text_grey));
//
//        } else {
//            paint.setColor(ContextCompat.getColor(context, R.color.text_workdays));
//
//        }
//
//        if (isMediam == 1) {
//            paint.setColor(ContextCompat.getColor(context, R.color.text_workdays));
//
//        } else if (isMediam == 2) {
//
//            paint.setColor(ContextCompat.getColor(context, R.color.text_grey));
//
//        } else {
//            paint.setColor(ContextCompat.getColor(context, R.color.fab_color));


        int fake = oldStyle & ~tf.getStyle();
        if ((fake & Typeface.BOLD) != 0) {
            paint.setFakeBoldText(true);
        }

        if ((fake & Typeface.ITALIC) != 0) {
            paint.setTextSkewX(-0.25f);
        }

        paint.setTypeface(tf);
    }
}