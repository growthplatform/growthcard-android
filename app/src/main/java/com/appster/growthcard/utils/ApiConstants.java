package com.appster.growthcard.utils;

import com.appster.growthcard.AppConfig;

/**
 * Created by navdeep on 24/02/16.
 */
public interface ApiConstants {

    String BASE_URL = AppConfig.BASE_URL;

    String LOGIN_URL = "user/sign-in";
    String TERMS_POST = "user/terms";
    String TERMS_PUT = "user/terms-status";
    String WORKDAYS_POST = "user/work-days";
    String USER_PROFILE_PUT = "user/user-profile";
    String UER_PROFILE_IMAGE = "user/save-image";
    String SUBORDINATE_LIST = "user/get-subordinates";
    String EA_LIST = "user/get-effective-actions";
    String CUSTOM_EA = "user/custom-effective-action";
    String ASSIGN_EA = "user/assign-effective-action";
    String CHANGE_ASSIGNED_EA = "user/change-effective-action";

    String SUB_MARK_ADHERENCE = "user/mark-adhrence";//MARK DONE
    String SUB_PROFILE_BASIC = "user/get-subordinate-profile";
    String GET_TEAM_MEMBERS = "user/get-team-members";

    String GET_PAST_EA_LIST = "user/get-past-effective-actions";
    String SEND_COMMENT = "user/comment-on-effective-action";
    String COMMENT_LIST = "user/get-comment-of-effective-action";

    String ANNOUCEMENT = "user/announcement";
    String SEARCH_USER = "user/search-user";
    String GET_ANNOUCEMENT = "user/get-announcement";
    String DELETE_ANNOUCEMENT = "user/delete-announcement";

    String GET_EA_DETAILS = "user/get-current-effective-action";
    String POST = "user/post";
    String GET_POST = "user/get-post";

    String DELETE_POST = "user/delete-post";

    String DASHBOARD_LIST = "user/get-manager-dashboard";
    String SWITCH_PROFILE = "user/switch-profile";
    String GENERATE_FEED = "user/generate-feed";
    String GET_DEPARTMENT = "user/get-department";
    String FORGET_PASSSWORD = "user/forgot-password";
    String CHANGE_PASSSWORD = "user/change-password";
    String LIKE_FEED = "user/like-feed";
    String LOG_OUT = "user/log-out";
    String DELETE_PAST_EA = "user/delete-past-effective-action";
    String GET_NOTIFICATION_LIST = "user/push-notification";

}
