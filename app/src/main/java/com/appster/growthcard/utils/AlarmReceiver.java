package com.appster.growthcard.utils;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.appster.growthcard.ui.activities.NotifyDialogActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.ApplicationClass;
import com.appster.growthcard.R;

/**
 * Created by navdeep on 05/04/16.
 */
public class AlarmReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        Log.d("ON receive", "alarm");
        AppPrefrences appPref = new AppPrefrences(context);

        if (ApplicationClass.isActivityVisible()) {
            Intent noti_intent = new Intent(context, NotifyDialogActivity.class);
            noti_intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(noti_intent);
        } else {
            Intent resultIntent = new Intent(context, SubMainActivity.class);
            resultIntent.putExtra("NOTIFICATION", context.getString(R.string.notification_type_1));

            int requestID = (int) System.currentTimeMillis();
            PendingIntent contentIntent = PendingIntent.getActivity(context, requestID, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);


            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher).setContentTitle("GrowthCard")
                    .setContentText(context.getString(R.string.daily_notification_message).replace("XX", appPref.getFirstName()))
                    .setAutoCancel(true)
                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

            NotificationCompat.BigTextStyle bigTextStyle =
                    new NotificationCompat.BigTextStyle();
// Sets a title for the Inbox in expanded layout
            bigTextStyle.setBigContentTitle("GrowthCard");
            bigTextStyle.bigText(context.getString(R.string.daily_notification_message).replace("XX", appPref.getFirstName()));
//
// Moves the expanded layout object into the notification object.
            mBuilder.setStyle(bigTextStyle);
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(requestID, mBuilder.build());
        }


    }
}
