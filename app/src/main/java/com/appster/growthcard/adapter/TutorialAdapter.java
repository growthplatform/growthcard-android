package com.appster.growthcard.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.appster.growthcard.ui.fragments.TutorialFragment;

/**
 * Created by navdeep on 19/02/16.
 */
public class TutorialAdapter extends FragmentPagerAdapter {

    private static String[] CONTENT = new String[]{"a", "b", "c", "d", "e", "f"};

    private int mCount = CONTENT.length;

    public TutorialAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return TutorialFragment.instance(CONTENT[position % CONTENT.length]);
    }

    @Override
    public int getCount() {
        return mCount;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return TutorialAdapter.CONTENT[position % CONTENT.length];
    }

}
