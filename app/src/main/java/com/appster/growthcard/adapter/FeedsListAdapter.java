package com.appster.growthcard.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.EADetailsActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.ui.activities.SubToSubProfileActivity;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.CustomTypefaceSpan;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.requests.LikeReq;
import com.appster.growthcard.network.response.FeedsResponse;
import com.appster.growthcard.network.response.LikeResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 30/03/16.
 */
public class FeedsListAdapter extends EndlessAdapter<FeedsResponse.Feed> {
    private final List<FeedsResponse.Feed> mDataSet;
    private static final int TYPE_HEADER = 0;
    private final Context context;
    private final AppPrefrences appPref;
    private FeedsResponse.RecentAnnouncement recentAnnouncement;

    public FeedsListAdapter(Context context, RecyclerView recyclerView, ArrayList<FeedsResponse.Feed> myDataSet, FeedsResponse.RecentAnnouncement recentAnnouncement) {
        super(myDataSet, recyclerView);
        this.mDataSet = myDataSet;
        this.context = context;
        appPref = new AppPrefrences(context);
        this.recentAnnouncement = recentAnnouncement;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_feeds_annoucement, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);

            Utils.setTextStyle(viewHolder.txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtEAdetail, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtEA, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.btnComment, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.btnLike, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtTime_Stamp, context, Utils.REGULAR);

            return viewHolder;
        } else if (viewType == TYPE_HEADER) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_annoucement_feed_header, parent, false);
            ViewHeaderHolder viewHolder = new ViewHeaderHolder(view);

            return viewHolder;

        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            ProgressViewHolder vh = new ProgressViewHolder(v);
            return vh;
        }


    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_ITEM) {
            ViewHolder viewHolder = (ViewHolder) holder;

            FeedsResponse.Feed feed = getItem(position);
            if (feed != null) {
                ((ViewHolder) holder).imgSubordinateImage.setTag(feed.getUser());
                ((ViewHolder) holder).imgSubordinateImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        FeedsResponse.User mem = (FeedsResponse.User) v.getTag();

                        if (mem.getUserId() != appPref.getUserId() || mem.getMember()!=null) {
                            Intent intent = new Intent(context, SubToSubProfileActivity.class);
                            intent.putExtra("USER", mem.getUserId());
                            if (mem.getMember()!=null) {
                                intent.putExtra("TEAM", mem.getMember().getTeamId());
                            }
                            context.startActivity(intent);
                        } else {

                            ((SubMainActivity) context).selectItemFromDrawer(0);
                        }

                    }
                });


                ((View) viewHolder.eaChartFull).setTag(feed);

                ((View) viewHolder.eaChartFull).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FeedsResponse.Feed pos = (FeedsResponse.Feed) v.getTag();
                        Intent i = new Intent(context, EADetailsActivity.class);
                        i.putExtra("isCurrent", (pos.getEffectiveActionId() == appPref.getEAId() && pos.getUser().getUserId() == appPref.getUserId()));
                        i.putExtra("EAid", pos.getEffectiveActionId());
                        i.putExtra("USER", pos.getUser().getUserId());
                        i.putExtra("TEAM", pos.getUser().getMember().getTeamId());
                        i.putExtra("pastEAid", pos.getPastEffectiveActionId());
                        context.startActivity(i);
                    }
                });


                ((View) viewHolder.eaChartSmall).setTag(feed);

                ((View) viewHolder.eaChartSmall).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        FeedsResponse.Feed pos = (FeedsResponse.Feed) v.getTag();
                        Intent i = new Intent(context, EADetailsActivity.class);
                        i.putExtra("isCurrent", (pos.getEffectiveActionId() == appPref.getEAId() && pos.getUser().getUserId() == appPref.getUserId()));
                        i.putExtra("EAid", pos.getEffectiveActionId());
                        i.putExtra("USER", pos.getUser().getUserId());
                        i.putExtra("TEAM", pos.getUser().getMember().getTeamId());
                        i.putExtra("pastEAid", pos.getPastEffectiveActionId());

                        context.startActivity(i);
                    }
                });

                viewHolder.txtSubordinateName.setText(feed.getUser().getFirstName() + " " + feed.getUser().getLastName());
              if(feed.getUser().getMember()!=null) {
                  viewHolder.txtSubordinateDesignation.setText(feed.getUser().getMember().getDesignation().getDesignationTitle());
              }  Utils.loadUrlImage(feed.getUser().getProfileImage(), ((ViewHolder) holder).imgSubordinateImage);

                if (feed.getCommentCount() != null)
                    viewHolder.btnComment.setText(feed.getCommentCount() + " " + (feed.getCommentCount() == 1 ? context.getString(R.string.comment) : context.getString(R.string.comments)));
                else
                    viewHolder.btnComment.setText("0 " + context.getString(R.string.comments));

                viewHolder.btnLike.setText(feed.getLikeCount() + " " + (feed.getLikeCount() == 1 ? context.getString(R.string.like) : context.getString(R.string.likes)));
                viewHolder.btnLike.setTag(getItem(position));
                viewHolder.btnLike.setOnClickListener(likeClickListener);

                viewHolder.txtEAdetail.setVisibility(View.VISIBLE);
                viewHolder.txtEA.setVisibility(View.VISIBLE);
                viewHolder.separator.setVisibility(View.VISIBLE);

                feed.setIsLikeLocal(feed.getIsLike());
                feed.setLocalLikeCount(feed.getLikeCount());
                if (feed.getIsLike() == 0) {
                    viewHolder.btnLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unlike, 0, 0, 0);
                    viewHolder.btnLike.setCompoundDrawablePadding(Utils.dpToPx(context, 5));
                    viewHolder.btnLike.setTextColor(ContextCompat.getColor(context, R.color.last_announcement));

                } else {
                    viewHolder.btnLike.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);
                    viewHolder.btnLike.setCompoundDrawablePadding(Utils.dpToPx(context, 5));
                    viewHolder.btnLike.setTextColor(ContextCompat.getColor(context, R.color.button_green));
                }

                if (getItem(position).getType() == 1) {// Announcements

                    viewHolder.eaChartFull.setVisibility(View.GONE);
                    viewHolder.eaChartSmall.setVisibility(View.GONE);
                    viewHolder.txtEA.setVisibility(View.GONE);
                    ((View) viewHolder.commentText.getParent()).setVisibility(View.GONE);
                    viewHolder.separator.setVisibility(View.GONE);
                    viewHolder.btnLike.setVisibility(View.INVISIBLE);
                    viewHolder.btnComment.setVisibility(View.INVISIBLE);
                    viewHolder.txtSubordinateName.setText(feed.getUser().getFirstName() + " " + feed.getUser().getLastName());
                    viewHolder.txtSubordinateDesignation.setText(feed.getUser().getMember().getDesignation().getDesignationTitle());

                    if (feed.getAnnouncement() != null) {
                        viewHolder.txtEAdetail.setText(feed.getAnnouncement().getContent());
                        viewHolder.txtTime_Stamp.setText(setTimeStamp(feed.getAnnouncement().getCreatedAt().getDate()));
                    }

                } else if (getItem(position).getType() == 2) {//Post
                    viewHolder.eaChartFull.setVisibility(View.GONE);
                    viewHolder.eaChartSmall.setVisibility(View.GONE);
                    viewHolder.txtEA.setVisibility(View.GONE);
                    viewHolder.btnComment.setVisibility(View.INVISIBLE);
                    viewHolder.separator.setVisibility(View.GONE);
                    ((View) viewHolder.commentText.getParent()).setVisibility(View.GONE);
                    viewHolder.btnLike.setVisibility(View.VISIBLE);
                    if (feed.getPost() != null) {
                        viewHolder.txtEAdetail.setText(feed.getPost().getContent());
                        viewHolder.txtTime_Stamp.setText(setTimeStamp(feed.getPost().getCreatedAt().getDate()));
                    }

                } else if (getItem(position).getType() == 3) {//EA with comment

                    viewHolder.eaChartFull.setVisibility(View.GONE);
                    viewHolder.eaChartSmall.setVisibility(View.VISIBLE);
                    ((View) viewHolder.commentText.getParent()).setVisibility(View.VISIBLE);
                    viewHolder.btnLike.setVisibility(View.VISIBLE);
                    viewHolder.btnComment.setVisibility(View.VISIBLE);
                    viewHolder.txtEA.setText(feed.getEffectiveaction().getEffectiveActionTitle());
                    viewHolder.txtEAdetail.setText(feed.getUser().getMember().getEffectiveActionDescription());
                    viewHolder.txtTime_Stamp.setText(setTimeStamp(feed.getEffectiveaction().getAssignEffectiveActionDate().getDate()));

                    if (feed.getEffectiveaction().getGraph() != null) {
                        if (viewHolder.eaChartSmall.getChildCount() > 1) {
                            viewHolder.eaChartSmall.removeViewAt(1);
                        }
                        if (viewHolder.eaChartSmall.getChildCount() == 1) {
                            Utils.setChart(context, viewHolder.eaChartSmall, feed.getEffectiveaction().getGraph(), appPref.getRole() == 2);
                        }
                    }

                    if (feed.getComment() != null) {
                        Utils.loadUrlImage(feed.getComment().getUser().getProfileImage(), ((ViewHolder) holder).commentImageView);
                        Typeface font = Typeface.createFromAsset(context.getAssets(), "Rubik-Bold.ttf");
                        Typeface font2 = Typeface.createFromAsset(context.getAssets(), "Rubik-Regular.ttf");
                        SpannableStringBuilder SS = new SpannableStringBuilder(feed.getComment().getUser().getFirstName() + " " + feed.getComment().getUser().getLastName() + " " + feed.getComment().getContent());//Html.fromHtml("<b><font color=\"#000000\">" + "Himanshu " + "</font></b>" + "<small><font color=\"#636771\">" + "Kathuria " + "</font></small>" +
                        int boldLength = (feed.getComment().getUser().getFirstName() + " " + feed.getComment().getUser().getLastName()).length();
                        SS.setSpan(new CustomTypefaceSpan(context, font, ContextCompat.getColor(context, R.color.text_grey)), 0, boldLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        SS.setSpan(new CustomTypefaceSpan(context, font2, ContextCompat.getColor(context, R.color.text_workdays)), boldLength, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        viewHolder.commentText.setText(SS);
                        ((View) viewHolder.commentText.getParent()).setVisibility(View.VISIBLE);

                    } else {
                        ((View) viewHolder.commentText.getParent()).setVisibility(View.GONE);

                    }


                } else if (getItem(position).getType() == 4) {//EA with graph
                    viewHolder.eaChartSmall.setVisibility(View.GONE);
                    ((View) viewHolder.commentText.getParent()).setVisibility(View.VISIBLE);
                    viewHolder.eaChartFull.setVisibility(View.VISIBLE);
                    viewHolder.btnLike.setVisibility(View.VISIBLE);
                    viewHolder.btnComment.setVisibility(View.VISIBLE);
                    viewHolder.txtEA.setText(feed.getEffectiveaction().getEffectiveActionTitle());
                    viewHolder.txtEAdetail.setText(feed.getUser().getMember().getEffectiveActionDescription());


                    if (feed.getEffectiveaction().getGraph() != null) {
                        if (viewHolder.eaChartFull.getChildCount() > 2) {
                            viewHolder.eaChartFull.removeViewAt(2);
                        }

                        if (viewHolder.eaChartFull.getChildCount() == 2) {
                            Utils.setChart(context, viewHolder.eaChartFull, feed.getEffectiveaction().getGraph(), appPref.getRole() == 2);

                        }
                    }

                    if (feed.getComment() != null) {

                        Utils.loadUrlImage(feed.getComment().getUser().getProfileImage(), ((ViewHolder) holder).commentImageView);

                        Typeface font = Typeface.createFromAsset(context.getAssets(), "Rubik-Bold.ttf");
                        Typeface font2 = Typeface.createFromAsset(context.getAssets(), "Rubik-Regular.ttf");
                        SpannableStringBuilder SS = new SpannableStringBuilder(feed.getComment().getUser().getFirstName() + " " + feed.getComment().getUser().getLastName() + " " + feed.getComment().getContent());//Html.fromHtml("<b><font color=\"#000000\">" + "Himanshu " + "</font></b>" + "<small><font color=\"#636771\">" + "Kathuria " + "</font></small>" +
//                    "<small>" + "Android " + "</small>"));
                        int boldLength = (feed.getComment().getUser().getFirstName() + " " + feed.getComment().getUser().getLastName()).length();
                        SS.setSpan(new CustomTypefaceSpan(context, font, ContextCompat.getColor(context, R.color.text_grey)), 0, boldLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        SS.setSpan(new CustomTypefaceSpan(context, font2, ContextCompat.getColor(context, R.color.text_workdays)), boldLength, SS.length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
                        viewHolder.commentText.setText(SS);
                        ((View) viewHolder.commentText.getParent()).setVisibility(View.VISIBLE);


                    } else {
                        ((View) viewHolder.commentText.getParent()).setVisibility(View.GONE);

                    }

                    viewHolder.txtTime_Stamp.setText(setTimeStamp(feed.getEffectiveaction().getAssignEffectiveActionDate().getDate()));

                }
            }


        } else if (getItemViewType(position) == TYPE_HEADER) {

            ViewHeaderHolder viewHolder = (ViewHeaderHolder) holder;

            Utils.setTextStyle(viewHolder.txtManagerName, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtManagerDesignation, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtSubjectAnnoucement, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtRecentAnnoucement, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtTime_Stamp, context, Utils.REGULAR);

            viewHolder.txtManagerName.setText(recentAnnouncement.getUser().getFirstName() + " " + recentAnnouncement.getUser().getLastName());
            viewHolder.txtManagerDesignation.setText(recentAnnouncement.getMember().getDesignation().getDesignationTitle());
            viewHolder.txtSubjectAnnoucement.setText(recentAnnouncement.getContent());

            viewHolder.txtTime_Stamp.setText(setTimeStamp(recentAnnouncement.getCreatedAt().getDate()));


        } else if (getItemViewType(position) == VIEW_PROG) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }


    }

    private View.OnClickListener likeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            FeedsResponse.Feed feed = (FeedsResponse.Feed) v.getTag();
            //TODO like update


            if (feed.getIsLikeLocal() == 0) {
                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_like, 0, 0, 0);
                ((TextView) v).setCompoundDrawablePadding(Utils.dpToPx(context, 5));
                ((TextView) v).setTextColor(ContextCompat.getColor(context, R.color.button_green));

                feed.setLocalLikeCount(feed.getLocalLikeCount() + 1);
                if ((feed.getLocalLikeCount()) > 0)
                    ((TextView) v).setText((feed.getLocalLikeCount()) + " " + ((feed.getLocalLikeCount()) == 1 ? context.getString(R.string.like) : context.getString(R.string.likes)));

                feed.setIsLikeLocal(1);
            } else {

                ((TextView) v).setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_unlike, 0, 0, 0);
                ((TextView) v).setCompoundDrawablePadding(Utils.dpToPx(context, 5));
                ((TextView) v).setTextColor(ContextCompat.getColor(context, R.color.last_announcement));

                feed.setLocalLikeCount(feed.getLocalLikeCount() - 1);
                if ((feed.getLocalLikeCount()) > -1)
                    ((TextView) v).setText((feed.getLocalLikeCount()) + " " + ((feed.getLocalLikeCount()) == 1 ? context.getString(R.string.like) : context.getString(R.string.likes)));

                feed.setIsLikeLocal(0);

            }

            likeApi(feed.getFeedId());

        }
    };


    private void likeApi(final int feedId) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(context)) ///to add header
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<LikeResponse> ip = service.likeFeed(getLikePayload(feedId));

        ip.enqueue(new ErrorCallback.MyCallback<LikeResponse>() {
            @Override
            public void success(final Response<LikeResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        try {
                            for (int i = 0; i < mDataSet.size(); i++) {

                                if (mDataSet.get(i) != null && mDataSet.get(i).getFeedId() == feedId) {
                                    if (response.body().getResult().getIsLiked() == 1) {
                                        mDataSet.get(i).setIsLikeLocal(1);
                                        mDataSet.get(i).setIsLike(1);
                                    } else {
                                        mDataSet.get(i).setIsLikeLocal(0);
                                        mDataSet.get(i).setIsLike(0);
                                    }

                                    mDataSet.get(i).setLikeCount(response.body().getResult().getLikeCount());
                                    mDataSet.get(i).setLocalLikeCount(response.body().getResult().getLikeCount());

                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

//                        notifyDataSetChanged();

                    }


                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, context, false);
    }

    private LikeReq getLikePayload(int feedId) {
        LikeReq likeReq = new LikeReq();
        likeReq.setUserId(appPref.getUserId());
        likeReq.setDeviceId(Utils.getDeviceId(context));
        likeReq.setUserToken(appPref.getUserToken());
        likeReq.setFeedId(feedId);
        return likeReq;
    }

    private String setTimeStamp(String timeStamp) {
        try {
            SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
            //  ((VHHeader) holder).eaDate.setText(UtileClass.getDate(outputFmt.parse(response.getResult().getMember().getEffective().getAssignEffectiveActionDate().getDate()), "dd MMMM, yyyy"));


            long l = outputFmt.parse(timeStamp).getTime();

            String str = DateUtils.getRelativeDateTimeString(

                    context, // Suppose you are in an activity or other Context subclass

                    l, // The time to display

                    DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                    // minutes (no "3 seconds ago")


                    DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                    // to default date instead of spans. This will not
                    // display "3 weeks ago" but a full date instead

                    0).toString();
            if (str.contains(",")) {
                return (str.split(",")[0]);
            } else {
                return (str);
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }

    }

    public void updateRecentAnnouncement(FeedsResponse.RecentAnnouncement recentAnnouncement) {
        this.recentAnnouncement = recentAnnouncement;
    }

    @Override
    public int getItemCount() {

        if (recentAnnouncement != null)
            return mDataSet.size() + 1;

        return mDataSet.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (recentAnnouncement != null) {
            if (position == 0)
                return TYPE_HEADER;

            return mDataSet.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG;
        } else {
            return mDataSet.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        }

    }


    public FeedsResponse.Feed getItem(int position) {


        if (recentAnnouncement != null)
            return mDataSet.get(position - 1);

        return mDataSet.get(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final CircleImageView imgSubordinateImage;
        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final TextView txtEA;
        private final TextView txtEAdetail;
        private final TextView btnLike;
        private final TextView btnComment;
        private final LinearLayout eaChartFull;
        private final LinearLayout eaChartSmall;
        private final TextView txtTime_Stamp;
        private final CircleImageView commentImageView;
        private final TextView commentText;
        private final View separator;

        public ViewHolder(View itemView) {
            super(itemView);


            separator = (View) itemView.findViewById(R.id.view);
            imgSubordinateImage = (CircleImageView) itemView.findViewById(R.id.imgSubordinateImage);
            commentImageView = (CircleImageView) itemView.findViewById(R.id.commentUserImage);
            eaChartFull = (LinearLayout) itemView.findViewById(R.id.llGraphFull);
            eaChartSmall = (LinearLayout) itemView.findViewById(R.id.llGraphSmall);


            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            txtEA = (TextView) itemView.findViewById(R.id.txtEA);
            txtEAdetail = (TextView) itemView.findViewById(R.id.txtEAdetail);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            btnLike = (TextView) itemView.findViewById(R.id.btnLike);
            btnComment = (TextView) itemView.findViewById(R.id.btnComment);
            commentText = (TextView) itemView.findViewById(R.id.commentText);
        }
    }


    public static class ViewHeaderHolder extends RecyclerView.ViewHolder {


        private final TextView txtManagerName;
        private final TextView txtManagerDesignation;
        private final CircleImageView imgManagerImage;
        private final TextView txtSubjectAnnoucement;
        private final TextView txtTime_Stamp;
        private final TextView txtRecentAnnoucement;


        public ViewHeaderHolder(View itemView) {
            super(itemView);
            txtRecentAnnoucement = (TextView) itemView.findViewById(R.id.txtRecentAnnoucement);

            txtManagerName = (TextView) itemView.findViewById(R.id.txtManagerName);
            txtManagerDesignation = (TextView) itemView.findViewById(R.id.txtManagerDesignation);
            txtSubjectAnnoucement = (TextView) itemView.findViewById(R.id.txtSubjectAnnoucement);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            imgManagerImage = (CircleImageView) itemView.findViewById(R.id.imgManagerImage);


        }
    }


}
