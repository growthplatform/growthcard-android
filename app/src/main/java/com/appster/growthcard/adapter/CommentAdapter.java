package com.appster.growthcard.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.AssignCommentActivity;
import com.appster.growthcard.ui.activities.EffectiveActionListActivity;
import com.appster.growthcard.model.Comment;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.SubProfileResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by navdeep on 08/03/16.
 */
public class CommentAdapter extends EndlessAdapter<Comment> {
    private static final int TYPE_HEADER = 0;
    private final Context context;
    private ArrayList<Comment> data;
    private SubProfileResponse response;
    private Users user;
    private boolean isFirst;
    private boolean isHeaderPresent;

    public CommentAdapter(Context context, ArrayList<Comment> data, RecyclerView recyclerView, SubProfileResponse response, Users user, boolean isHeaderPresent) {
        super(data, recyclerView);
        this.data = data;
        this.context = context;
        this.response = response;
        this.user = user;
        this.isHeaderPresent = isHeaderPresent;
    }

    public CommentAdapter(Context context, ArrayList<Comment> data, RecyclerView recyclerView, boolean isHeaderPresent) {
        super(data, recyclerView);
        this.data = data;
        this.context = context;
        this.isHeaderPresent = isHeaderPresent;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_list_row, parent, false);
            VHItem viewHolder = new VHItem(view);
            return viewHolder;
        } else if (viewType == TYPE_HEADER && isHeaderPresent) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_sub_current, parent, false);
            VHHeader viewHolder = new VHHeader(view);
            AppPrefrences appPrefrences = new AppPrefrences(context);

            if (!isFirst) {
                Utils.setChart(context, ((VHHeader) viewHolder).graphParent, response.getResult().getGraph(), appPrefrences.getRole() == 2);
                isFirst = true;
            }

            return viewHolder;
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            ProgressViewHolder vh = new ProgressViewHolder(v);
            return vh;
        }

    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            Comment dataItem = getItem(position);
            Utils.setTextStyle(((VHItem) holder).commentName, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHItem) holder).commentDesignation, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHItem) holder).commentDate, context, Utils.REGULAR);
            Utils.setTextStyle(((VHItem) holder).commentText, context, Utils.REGULAR);

            ((VHItem) holder).cardView.setBackgroundResource(R.drawable.white_box);
            ((VHItem) holder).blueTriangle.setVisibility(View.INVISIBLE);
            ((VHItem) holder).whiteTriangle.setVisibility(View.VISIBLE);
            ((VHItem) holder).imgManagerImage.setVisibility(View.VISIBLE);
            ((VHItem) holder).commentText.setTextColor(ContextCompat.getColor(context, R.color.text_grey));
            ((VHItem) holder).commentDesignation.setTextColor(ContextCompat.getColor(context, R.color.button_green));


            if (dataItem != null) {
                if (dataItem.getRole() == 2) {
                    ((VHItem) holder).cardView.setBackgroundResource(R.drawable.blue_box);
                    ((VHItem) holder).blueTriangle.setVisibility(View.VISIBLE);
                    ((VHItem) holder).whiteTriangle.setVisibility(View.INVISIBLE);
                    ((VHItem) holder).imgManagerImage.setVisibility(View.INVISIBLE);
                    ((VHItem) holder).commentText.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                    ((VHItem) holder).commentDesignation.setTextColor(ContextCompat.getColor(context, R.color.text_grey));
                }

                ((VHItem) holder).commentName.setText(dataItem.getUser().getFirstName() + " " + dataItem.getUser().getLastName());
                if (dataItem.getMember() != null) {
                    ((VHItem) holder).commentDesignation.setText(dataItem.getMember().getDesignation().getDesignationTitle());
                }
                ((VHItem) holder).commentText.setText(dataItem.getContent());

                Utils.loadUrlImage(dataItem.getUser().getProfileImage(), ((VHItem) holder).imgManagerImage);

                try {
                    SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                    long l = outputFmt.parse(dataItem.getCreatedAt().getDate()).getTime();
                    String ab = Utils.getDate(outputFmt.parse(dataItem.getCreatedAt().getDate()), "dd MMMM, yyyy");
                    Log.d("TAG", ab);
                    String str = DateUtils.getRelativeDateTimeString(

                            context, // Suppose you are in an activity or other Context subclass

                            l, // The time to display

                            DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                            // minutes (no "3 seconds ago")


                            DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                            // to default date instead of spans. This will not
                            // display "3 weeks ago" but a full date instead

                            0).toString();
                    if (str.contains(",")) {
                        ((VHItem) holder).commentDate.setText(str.split(",")[0]);
                    } else {
                        ((VHItem) holder).commentText.setText(str);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


        } else if (holder instanceof VHHeader) {

            Utils.setTextStyle(((VHHeader) holder).editAssignedEA, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).addEAButton, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHHeader) holder).eaTitle, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHHeader) holder).eaDate, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).eaDesc, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).txtMin, context, Utils.BOLD);
            Utils.setTextStyle(((VHHeader) holder).txtMinTime, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).txtMax, context, Utils.BOLD);
            Utils.setTextStyle(((VHHeader) holder).txtMaxTime, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).txtAvg, context, Utils.BOLD);
            Utils.setTextStyle(((VHHeader) holder).txtAvgTime, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).txtToday, context, Utils.BOLD);
            Utils.setTextStyle(((VHHeader) holder).txtTodayTime, context, Utils.REGULAR);

            if (data != null && data.size() > 0) {
                ((VHHeader) holder).emptyView.setText(R.string.no_comments);
                ((VHHeader) holder).emptyView.setVisibility(View.GONE);
            } else {
                ((VHHeader) holder).emptyView.setVisibility(View.VISIBLE);
            }

            if (response.getResult().getEffectioveActionSummary() != null) {
                ((VHHeader) holder).txtMaxTime.setText(response.getResult().getEffectioveActionSummary().getMax() + "");
                ((VHHeader) holder).txtMinTime.setText(response.getResult().getEffectioveActionSummary().getMin() + "");
                ((VHHeader) holder).txtAvgTime.setText(response.getResult().getEffectioveActionSummary().getAvg() + "");
                ((VHHeader) holder).txtTodayTime.setText(response.getResult().getEffectioveActionSummary().getToday() + "");
            }

            ((VHHeader) holder).addEAButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent effectiveActionIntent = new Intent(context, EffectiveActionListActivity.class);
                    effectiveActionIntent.putExtra(context.getString(R.string.is_editting), true);
                    effectiveActionIntent.putExtra("USER", user);
                    context.startActivity(effectiveActionIntent);

                }
            });

            ((VHHeader) holder).graphParent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            ((VHHeader) holder).editAssignedEA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (response.getResult().getMember() != null && response.getResult().getMember().getEffective() != null) {
                        Intent effectiveActionIntent = new Intent(context, AssignCommentActivity.class);
                        effectiveActionIntent.putExtra(context.getString(R.string.is_editting), true);
                        effectiveActionIntent.putExtra("USER", user);

                        context.startActivity(effectiveActionIntent);
                    }

                }
            });


            if (response.getResult().getMember() != null && response.getResult().getMember().getEffective() != null) {

                ((VHHeader) holder).editAssignedEA.setVisibility(View.INVISIBLE);
                ((VHHeader) holder).editAssignedEA.setClickable(true);
                ((VHHeader) holder).editAssignedEA.setAlpha(1);
                ((VHHeader) holder).eaTitle.setText(response.getResult().getMember().getEffective().getEffectiveActionTitle());
            } else {
                ((VHHeader) holder).editAssignedEA.setVisibility(View.INVISIBLE);
                ((VHHeader) holder).editAssignedEA.setClickable(false);
                ((VHHeader) holder).editAssignedEA.setAlpha(0.5f);

                ((VHHeader) holder).eaTitle.setText(R.string.no_ea_assigned);
                ((VHHeader) holder).eaDate.setVisibility(View.INVISIBLE);
            }

            ((VHHeader) holder).eaDesc.setVisibility(View.VISIBLE);


            if (response.getResult().getMember() != null && response.getResult().getMember().getEffectiveActionDescription() != null) {

                ((VHHeader) holder).eaDesc.collapseText();
                ((VHHeader) holder).eaDesc.setText(response.getResult().getMember().getEffectiveActionDescription());

                if (TextUtils.isEmpty(response.getResult().getMember().getEffectiveActionDescription())) {

                //    ((VHHeader) holder).eaDesc.setText(R.string.no_desc);
                }

                if (response.getResult().getMember().getEffective() != null) {
                    try {
                        SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                        outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                        ((VHHeader) holder).eaDate.setText(Utils.getDate(outputFmt.parse(response.getResult().getMember().getEffective().getAssignEffectiveActionDate().getDate()), "dd MMMM, yyyy"));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }
    }


    @Override
    public int getItemCount() {
        if (!isHeaderPresent)
            return data.size();

        return data.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (!isHeaderPresent)
            return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        else {
            if (isPositionHeader(position))
                return TYPE_HEADER;

            return data.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG;
        }
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private Comment getItem(int position) {
        if (!isHeaderPresent)
            return data.get(position);

        return data.get(position - 1);
    }

    class VHItem extends RecyclerView.ViewHolder {
        public final CircleImageView imgManagerImage;
        public final TextView commentName;
        public final TextView commentDesignation;
        public final TextView commentDate;
        public final ImageView whiteTriangle;
        public final ImageView blueTriangle;
        public final RelativeLayout cardView;
        public final TextView commentText;

        public VHItem(View rootView) {
            super(rootView);

            imgManagerImage = (CircleImageView) rootView.findViewById(R.id.imgManagerImage);
            commentName = (TextView) rootView.findViewById(R.id.comment_name);
            commentDesignation = (TextView) rootView.findViewById(R.id.comment_designation);
            commentDate = (TextView) rootView.findViewById(R.id.comment_date);
            whiteTriangle = (ImageView) rootView.findViewById(R.id.white_triangle);
            blueTriangle = (ImageView) rootView.findViewById(R.id.blue_triangle);
            cardView = (RelativeLayout) rootView.findViewById(R.id.card_view);
            commentText = (TextView) rootView.findViewById(R.id.comment_text);
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        public final TextView eaTitle;
        public final TextView eaDate;
        public final ExpandableTextView eaDesc;
        public final TextView txtMin;
        public final TextView txtMinTime;
        public final TextView txtMax;
        public final TextView txtMaxTime;
        public final TextView txtAvg;
        public final TextView txtAvgTime;
        public final TextView txtToday;
        public final TextView txtTodayTime;
        public final Button addEAButton;
        private final TextView editAssignedEA;
        private final TextView emptyView;
        private final LinearLayout graphParent;


        public VHHeader(View rootView) {
            super(rootView);

            graphParent = (LinearLayout) rootView.findViewById(R.id.chart_ea_parent);

            addEAButton = (Button) rootView.findViewById(R.id.btnAddEffectiveAction);
            editAssignedEA = (TextView) rootView.findViewById(R.id.edit_assigned_ea);
            eaTitle = (TextView) rootView.findViewById(R.id.ea_title);
            eaDate = (TextView) rootView.findViewById(R.id.ea_date);
            eaDesc = (ExpandableTextView) rootView.findViewById(R.id.ea_desc);
            //chartEa = new BarChart(context);//(BarChart) rootView.findViewById(R.id.chart_ea);
            txtMin = (TextView) rootView.findViewById(R.id.txtMin);
            txtMinTime = (TextView) rootView.findViewById(R.id.txtMinTime);
            txtMax = (TextView) rootView.findViewById(R.id.txtMax);
            txtMaxTime = (TextView) rootView.findViewById(R.id.txtMaxTime);
            txtAvg = (TextView) rootView.findViewById(R.id.txtAvg);
            txtAvgTime = (TextView) rootView.findViewById(R.id.txtAvgTime);
            txtToday = (TextView) rootView.findViewById(R.id.txtToday);
            emptyView = (TextView) rootView.findViewById(R.id.empty_view);
            txtTodayTime = (TextView) rootView.findViewById(R.id.txtTodayTime);
        }
    }


}
