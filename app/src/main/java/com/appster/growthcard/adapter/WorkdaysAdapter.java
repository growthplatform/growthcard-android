package com.appster.growthcard.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.appster.growthcard.ui.activities.WorkDaysActivity;
import com.appster.growthcard.model.WorkdayModel;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;

import java.util.ArrayList;


/**
 * Created by himanshu on 23/02/16.
 */
public class WorkdaysAdapter extends RecyclerView.Adapter<WorkdaysAdapter.ViewHolder> {

    private final ArrayList<WorkdayModel> mDataset;
    private final Context context;

    // private Boolean selected;

    public WorkdaysAdapter(ArrayList<WorkdayModel> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;

    }

    @Override
    public WorkdaysAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.lst_workdays, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final WorkdaysAdapter.ViewHolder holder, int position) {
        // holder.mDays.setText(mDataset[position]);
        holder.mSelectedDays.setText(mDataset.get(position).getDay());
        Utils.setTextStyle(holder.mSelectedDays, context, Utils.REGULAR);


        holder.mSelectedDays.setChecked(mDataset.get(position).isSelected());

        holder.mSelectedDays.setTag(position);
        holder.mSelectedDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos = (int) v.getTag();
                if (holder.mSelectedDays.isChecked()) {

                    mDataset.get(pos).setSelected(true);

                } else {
                    mDataset.get(pos).setSelected(false);

                }
                ((WorkDaysActivity) context).isWorkdysSelected();
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox mSelectedDays;


        public ViewHolder(View itemView) {
            super(itemView);

            mSelectedDays = (CheckBox) itemView.findViewById(R.id.selected_days);
            //    mDays = (TextView) itemView.findViewById(R.id.days);


        }


    }


}
