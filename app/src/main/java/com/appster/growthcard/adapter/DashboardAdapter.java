package com.appster.growthcard.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.ManToSubProfileActivity;
import com.appster.growthcard.model.Designation;
import com.appster.growthcard.model.UserDetail;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.DashboardListResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by himanshu on 08/03/16.
 */
public class DashboardAdapter extends EndlessAdapter<DashboardListResponse.Member> {

    private Context context;
    private static final int TYPE_HEADER = 0;
    private ArrayList<DashboardListResponse.Member> data;

    public DashboardAdapter(Context context, RecyclerView recyclerView, ArrayList<DashboardListResponse.Member> data) {
        super(data, recyclerView);
        this.context = context;
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.dashboard_row, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);

            Utils.setTextStyle(viewHolder.txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtEA, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtEAdescription, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtTime_Stamp, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtMin, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtMinTime, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtMax, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtMaxTime, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtAvg, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtAvgTime, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtToday, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtTodayTime, context, Utils.REGULAR);


            return viewHolder;
        } else if (viewType == TYPE_HEADER) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.past_ea_header, parent, false);
            ViewHeaderHolder viewHolder = new ViewHeaderHolder(view);
            Utils.setTextStyle(viewHolder.alphabeticalCheck, context, Utils.LIGHT);
            Utils.setTextStyle(viewHolder.latestCheck, context, Utils.LIGHT);

            return viewHolder;

        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            ProgressViewHolder vh = new ProgressViewHolder(v);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;

            DashboardListResponse.Member member = data.get(position);
            AppPrefrences appPrefrences = new AppPrefrences(context);
            if (viewHolder.chartEa.getChildCount() > 2) {
                viewHolder.chartEa.removeViewAt(2);
            }

            if (viewHolder.chartEa.getChildCount() == 2) {
                Utils.setChart(context, viewHolder.chartEa, member.getGraph(), appPrefrences.getRole() == 2);
            }

            if (member.getUser().getUserId() == appPrefrences.getUserId())
                viewHolder.txtSubordinateName.setText(context.getString(R.string.me));
            else
                viewHolder.txtSubordinateName.setText(member.getUser().getFirstName() + " " + member.getUser().getLastName());

            if (member.getDesignation() != null)
                viewHolder.txtSubordinateDesignation.setText(member.getDesignation().getDesignationTitle());

            if (member.getEffective() != null && member.getEffective().getEffectiveActionTitle() != null) {
                viewHolder.txtEA.setText(member.getEffective().getEffectiveActionTitle());
            }
            if (member.getEffectiveActionDescription() != null) {
                viewHolder.txtEAdescription.collapseText();
                viewHolder.txtEAdescription.setText(member.getEffectiveActionDescription());
            }

            if (member.getEffectiveActionSummary() != null) {
                ((ViewHolder) holder).txtMaxTime.setText(member.getEffectiveActionSummary().getMAX() + "");
                ((ViewHolder) holder).txtMinTime.setText(member.getEffectiveActionSummary().getMIN() + "");
                ((ViewHolder) holder).txtAvgTime.setText(member.getEffectiveActionSummary().getAVG() + "");
                ((ViewHolder) holder).txtTodayTime.setText(member.getEffectiveActionSummary().getTODAY() + "");
            }
            Utils.loadUrlImage(member.getUser().getProfileImage(), ((ViewHolder) holder).imgUserProfile);

            ((ViewHolder) holder).imgUserProfile.setTag(member);
            ((ViewHolder) holder).imgUserProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DashboardListResponse.Member mem = (DashboardListResponse.Member) v.getTag();
                    if (mem != null) {
                        Intent intent = new Intent(context, ManToSubProfileActivity.class);
                        intent.putExtra("USER", getUserVO(mem).getUser().getUserId());
                        intent.putExtra("TEAM", getUserVO(mem).getTeamId());

                        context.startActivity(intent);
                    }
                }
            });


            try {
                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));

                if (member.getEffective() != null) {
                    String startDate = Utils.getDate(outputFmt.parse(member.getEffective().getAssignEffectiveActionDate().getDate()), "dd MMMM, yyyy");
                    ((ViewHolder) holder).txtTime_Stamp.setText(startDate);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else if (holder instanceof ViewHeaderHolder) {

            if (data != null && data.size() > 0) {

                ((ViewHeaderHolder) holder).emptyView.setText(R.string.no_past_data);
                ((ViewHeaderHolder) holder).emptyView.setVisibility(View.GONE);
            } else {
                ((ViewHeaderHolder) holder).emptyView.setText(R.string.no_past_data);
                ((ViewHeaderHolder) holder).emptyView.setVisibility(View.VISIBLE);
            }


            ((ViewHeaderHolder) holder).alphabeticalCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((ViewHeaderHolder) holder).alphabeticalCheck.isChecked()) {

                        ((ViewHeaderHolder) holder).latestCheck.setChecked(false);
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));

                    } else {
                        ((ViewHeaderHolder) holder).latestCheck.setChecked(true);

                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));
                    }
                }
            });

            ((ViewHeaderHolder) holder).latestCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((ViewHeaderHolder) holder).latestCheck.isChecked()) {

                        ((ViewHeaderHolder) holder).alphabeticalCheck.setChecked(false);
                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));


                    } else {
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setChecked(true);
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));


                    }
                }
            });

        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final TextView txtEA;
        private final ExpandableTextView txtEAdescription;
        private final TextView txtTime_Stamp;
        private final TextView txtMin;
        private final TextView txtMinTime;
        private final TextView txtMaxTime;
        private final TextView txtMax;
        private final TextView txtAvg;
        private final TextView txtAvgTime;
        private final TextView txtToday;
        private final TextView txtTodayTime;
        private final LinearLayout chartEa;
        private final ImageView imgUserProfile;

        public ViewHolder(View itemView) {
            super(itemView);

            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            imgUserProfile = (ImageView) itemView.findViewById(R.id.imgManagerImage);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            txtEA = (TextView) itemView.findViewById(R.id.txtEA);
            txtEAdescription = (ExpandableTextView) itemView.findViewById(R.id.txtEAdescription);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            txtMin = (TextView) itemView.findViewById(R.id.txtMin);
            txtMinTime = (TextView) itemView.findViewById(R.id.txtMinTime);
            txtMax = (TextView) itemView.findViewById(R.id.txtMax);
            txtMaxTime = (TextView) itemView.findViewById(R.id.txtMaxTime);
            txtAvg = (TextView) itemView.findViewById(R.id.txtAvg);
            txtAvgTime = (TextView) itemView.findViewById(R.id.txtAvgTime);
            txtToday = (TextView) itemView.findViewById(R.id.txtToday);
            txtTodayTime = (TextView) itemView.findViewById(R.id.txtTodayTime);

            chartEa = (LinearLayout) itemView.findViewById(R.id.chart_ea);

        }
    }

    public static class ViewHeaderHolder extends RecyclerView.ViewHolder {


        private final CheckBox alphabeticalCheck;
        private final CheckBox latestCheck;
        private final TextView emptyView;


        public ViewHeaderHolder(View itemView) {
            super(itemView);

            alphabeticalCheck = (CheckBox) itemView.findViewById(R.id.alphabetical);
            latestCheck = (CheckBox) itemView.findViewById(R.id.lastAssigned);
            emptyView = (TextView) itemView.findViewById(R.id.empty_view);

        }
    }

    private Users getUserVO(DashboardListResponse.Member member) {

        Users userVO = new Users();
        UserDetail userDetail = new UserDetail();
        userDetail.setUserId(member.getUser().getUserId());
        userDetail.setEmail(member.getUser().getEmail());
        userDetail.setFirstName(member.getUser().getFirstName());
        userDetail.setLastName(member.getUser().getLastName());
        userDetail.setProfileImage(member.getUser().getProfileImage());

        userVO.setUser(userDetail);
        Designation designation = new Designation();
        designation.setDesignationId(member.getDesignationId());
        designation.setDesignationTitle(member.getDesignation().getDesignationTitle());
        userVO.setDesignationId(designation.getDesignationId());

        designation.setCompanyId(member.getDesignation().getCompanyId());
        userVO.setDesignation(designation);
        if (member.getEffectiveActionDescription() != null) {
            userVO.setEffectiveActionDescription(member.getEffectiveActionDescription());

        }
        if (member.getEffectiveActionId() != null) {
            userVO.setEffectiveActionId(member.getEffectiveActionId());
        }
        userVO.setTeamId(member.getTeamId());

        return userVO;

    }

}
