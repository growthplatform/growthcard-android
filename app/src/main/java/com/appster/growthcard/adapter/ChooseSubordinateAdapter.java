package com.appster.growthcard.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.SearchSubordinateActivity;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.SearchSubordinateResponse;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by himanshukathuria on 15/03/16.
 */
public class ChooseSubordinateAdapter extends EndlessAdapter<SearchSubordinateResponse.User> {
    private final ArrayList<SearchSubordinateResponse.User> data;
    private ArrayList<SearchSubordinateResponse.User> selecteddata = new ArrayList<SearchSubordinateResponse.User>();
    private Context context;
    private Button btnToolDone;

    public ChooseSubordinateAdapter(Context context, ArrayList<SearchSubordinateResponse.User> data, RecyclerView recyclerView) {
        super(data, recyclerView);
        this.context = context;
        this.data = data;
        btnToolDone = (Button) ((SearchSubordinateActivity) context).findViewById(R.id.toolBtnDone);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_choose_subordinate, parent, false);
            vh = new ViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);
            vh = new ProgressViewHolder(view);

        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_ITEM) {

            final ViewHolder vholder = (ViewHolder) holder;

            Utils.setTextStyle(vholder.txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(vholder.txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyleButton(vholder.btnSelected, context, Utils.REGULAR);


            vholder.txtSubordinateName.setText(data.get(position).getFirstName() + " " + data.get(position).getLastName());
            if (data.get(position).getMember() != null) {
                vholder.txtSubordinateDesignation.setText(data.get(position).getMember().getDesignation().getDesignationTitle());
            }
            Utils.loadUrlImage(data.get(position).getProfileImage(), vholder.imgSubordinateImage);
            if (!data.get(position).getSelected()) {
                data.get(position).setSelected(false);
                vholder.btnSelected.setTextColor(ContextCompat.getColor(context, R.color.button_green));
                vholder.btnSelected.setText(context.getString(R.string.select));
                Utils.setTextStyleButton(vholder.btnSelected, context, Utils.REGULAR);
            } else {
                data.get(position).setSelected(true);
                vholder.btnSelected.setTextColor(ContextCompat.getColor(context, R.color.button_green_alpha_5));
                vholder.btnSelected.setText(context.getString(R.string.selected));
                Utils.setTextStyleButton(vholder.btnSelected, context, Utils.MEDIUM);

            }

            vholder.btnSelected.setTag(position);
            vholder.btnSelected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();

                    if (data.get(pos).getSelected()) {
                        data.get(pos).setSelected(false);
                        vholder.btnSelected.setTextColor(ContextCompat.getColor(context, R.color.button_green));
                        vholder.btnSelected.setText(context.getString(R.string.select));
                        selecteddata.remove(data.get(pos));
                        Utils.setTextStyleButton(vholder.btnSelected, context, Utils.REGULAR);

                    } else {
                        data.get(pos).setSelected(true);
                        selecteddata.add(data.get(pos));
                        vholder.btnSelected.setTextColor(ContextCompat.getColor(context, R.color.button_green_alpha_5));
                        vholder.btnSelected.setText(context.getString(R.string.selected));
                        Utils.setTextStyleButton(vholder.btnSelected, context, Utils.MEDIUM);

                    }
                    Log.d("SelectedData", String.valueOf(selecteddata.size()));
                    if (selecteddata.size() == 0) {
                        btnToolDone.setText("Done");
                    } else {
                        btnToolDone.setVisibility(View.VISIBLE);
                        btnToolDone.setText("Done(" + selecteddata.size() + ")");
                    }
                }
            });
        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final Button btnSelected;
        private final CircleImageView imgSubordinateImage;

        public ViewHolder(View itemView) {
            super(itemView);

            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            btnSelected = (Button) itemView.findViewById(R.id.btnSelect);
            imgSubordinateImage = (CircleImageView) itemView.findViewById(R.id.imgSubordinateImage);

        }
    }


    public ArrayList<SearchSubordinateResponse.User> getData() {
        return selecteddata;
    }


}
