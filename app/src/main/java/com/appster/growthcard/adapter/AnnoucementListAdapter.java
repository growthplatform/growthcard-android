package com.appster.growthcard.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.DeleteAnnoucementReq;
import com.appster.growthcard.network.response.AnnoucementListResponse;
import com.appster.growthcard.network.response.GenericResponse;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 16/03/16.
 */
public class AnnoucementListAdapter extends EndlessAdapter<AnnoucementListResponse.Announcement> {

    private final ArrayList<AnnoucementListResponse.Announcement> announcement;
    private Context context;
    private AppPrefrences appPref;
    private Retrofit retrofit;
    private DeleteAnnoucementReq payload;

    public AnnoucementListAdapter(Context context, RecyclerView recyclerView, ArrayList<AnnoucementListResponse.Announcement> announcement) {
        super(announcement, recyclerView);
        this.context = context;
        this.announcement = announcement;
        appPref = new AppPrefrences(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_annoucement, parent, false);
            vh = new ViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);
            vh = new ProgressViewHolder(view);

        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == VIEW_ITEM) {
            final ViewHolder vholder = (ViewHolder) holder;

            Utils.setTextStyle(vholder.txtManagerName, context, Utils.MEDIUM);
            Utils.setTextStyle(vholder.txtManagerDesignation, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.txtSubjectAnnoucementWord, context, Utils.MEDIUM);
            Utils.setTextStyle(vholder.txtSubjectAnnoucement, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.txtTime_Stamp, context, Utils.REGULAR);
            if (announcement.get(position).getMember() != null) {
                vholder.txtManagerDesignation.setText(announcement.get(position).getMember().getDesignation().getDesignationTitle());
            }
            if (announcement.get(position).getUser().getFirstName().equals(appPref.getFirstName()) && announcement.get(position).getUser().getLastName().equals(appPref.getLastName())) {
                vholder.txtManagerName.setText(R.string.you);
            } else {
                vholder.txtManagerName.setText(announcement.get(position).getUser().getFirstName() + " " + announcement.get(position).getUser().getLastName());
            }
            vholder.txtSubjectAnnoucement.collapseText();
            vholder.txtSubjectAnnoucement.setText(announcement.get(position).getContent());
            vholder.txtSubjectAnnoucementWord.setText(announcement.get(position).getSubject());

            Utils.loadUrlImage(announcement.get(position).getUser().getProfileImage(), vholder.imgManagerImage);
            vholder.relAnnoucementDetail.setTag(position);
            vholder.relAnnoucementDetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {

                    final int pos = (int) v.getTag();
                    new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("Alert")
                            .showCancelButton(true)
                            .setCancelText("No")
                            .setConfirmText("Yes")
                            .setContentText("Are you sure you want to Delete?")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    deleteAnnoucement(pos);
                                }
                            })
                            .show();


                    return false;
                }
            });

            try {
                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));


                long l = outputFmt.parse(announcement.get(position).getCreatedAt().getDate()).getTime();

                String str = DateUtils.getRelativeDateTimeString(

                        context, // Suppose you are in an activity or other Context subclass

                        l, // The time to display

                        DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                        // minutes (no "3 seconds ago")


                        DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                        // to default date instead of spans. This will not

                        0).toString();
                if (str.contains(",")) {
                    vholder.txtTime_Stamp.setText(str.split(",")[0]);
                } else {
                    vholder.txtTime_Stamp.setText(str);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    private void deleteAnnoucement(final int position) {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(context))
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<GenericResponse> ip = service.deleteAnnoucement(getPayload(position));


        ip.enqueue(new ErrorCallback.MyCallback<GenericResponse>() {
            @Override
            public void success(Response<GenericResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        announcement.remove(announcement.get(position));
                        notifyDataSetChanged();
                    }
                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, context, true);

    }

    @Override
    public int getItemCount() {
        return announcement.size();
    }

    public DeleteAnnoucementReq getPayload(int position) {

        DeleteAnnoucementReq req = new DeleteAnnoucementReq();

        req.setDeviceId(Utils.getDeviceId(context));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(appPref.getTeamId());
        req.setCompanyId(appPref.getCompanyId());
        req.setAnnouncementId(announcement.get(position).getAnnouncementId());

        return req;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView txtManagerName;
        private final TextView txtManagerDesignation;
        private final CircleImageView imgManagerImage;
        private final TextView txtSubjectAnnoucementWord;
        private final ExpandableTextView txtSubjectAnnoucement;
        private final TextView txtTime_Stamp;
        private final RelativeLayout relAnnoucementDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            txtManagerName = (TextView) itemView.findViewById(R.id.txtManagerName);
            txtManagerDesignation = (TextView) itemView.findViewById(R.id.txtManagerDesignation);
            txtSubjectAnnoucementWord = (TextView) itemView.findViewById(R.id.txtSubjectAnnoucementWord);
            txtSubjectAnnoucement = (ExpandableTextView) itemView.findViewById(R.id.txtSubjectAnnoucement);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            imgManagerImage = (CircleImageView) itemView.findViewById(R.id.imgManagerImage);
            relAnnoucementDetail = (RelativeLayout) itemView.findViewById(R.id.relAnnoucementDetail);

        }
    }
}
