package com.appster.growthcard.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.EffectiveActionListActivity;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.model.Users;

import java.util.ArrayList;

/**
 * Created by himanshu on 01/03/16.
 */
public class SubOrdinateListAdapter extends EndlessAdapter<Users> {
    private final Context context;
    private ArrayList<Users> subordinateList;
    private AppPrefrences appPref;


    public SubOrdinateListAdapter(Context context, ArrayList<Users> subordinateList, RecyclerView recyclerView) {
        super(subordinateList, recyclerView);
        this.context = context;
        appPref = new AppPrefrences(context);
        this.subordinateList = subordinateList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_subourdinate_list, parent, false);
            vh = new ViewHolder(view);

            Utils.setTextStyle(((ViewHolder) vh).txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(((ViewHolder) vh).txtSubordinateDesignation, context, Utils.REGULAR);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            vh = new ProgressViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {

        if (getItemViewType(position) == VIEW_ITEM) {
            final Users result = subordinateList.get(position);

            ViewHolder vHolder = (ViewHolder) viewHolder;
            vHolder.txtSubordinateName.setText(result.getUser().getFirstName() + " " + result.getUser().getLastName());
            vHolder.txtSubordinateDesignation.setText(result.getDesignation().getDesignationTitle());
            Utils.loadUrlImage(result.getUser().getProfileImage(), vHolder.imgSubordinateImage);


            if (result.getUser().getUserId() == appPref.getUserId()) {
                vHolder.txtSubordinateName.setText(context.getString(R.string.me));
            }

            vHolder.rel_subordinateData.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent effectiveActionIntent = new Intent(context, EffectiveActionListActivity.class);
                    //effectiveActionIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    //   UtileClass.selectedSubordinate=result;

//                    effectiveActionIntent.putExtra(context.getString(R.string.userid), result.getUser().getUserId());
//                    effectiveActionIntent.putExtra(context.getString(R.string.username), result.getUser().getFirstName() + " " + result.getUser().getLastName());
//                    effectiveActionIntent.putExtra(context.getString(R.string.design), result.getDesignation().getDesignationTitle());
//                    effectiveActionIntent.putExtra(context.getString(R.string.userimage), result.getUser().getProfileImage());

                    effectiveActionIntent.putExtra(context.getString(R.string.is_editting), false);
                    effectiveActionIntent.putExtra("USER", result);
                    context.startActivity(effectiveActionIntent);
//                    ((Activity)context).finish();
                }
            });

        } else {
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);

        }
    }

    @Override
    public int getItemCount() {
        return subordinateList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final ImageView imgSubordinateImage;
        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final RelativeLayout rel_subordinateData;

        public ViewHolder(View itemView) {
            super(itemView);
            imgSubordinateImage = (ImageView) itemView.findViewById(R.id.imgSubordinateImage);
            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            rel_subordinateData = (RelativeLayout) itemView.findViewById(R.id.rel_subordinateData);


        }
    }
}
