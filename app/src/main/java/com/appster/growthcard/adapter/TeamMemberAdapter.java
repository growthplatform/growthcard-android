package com.appster.growthcard.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.ManToSubProfileActivity;
import com.appster.growthcard.ui.activities.SubToSubProfileActivity;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.model.Users;

import java.util.List;

/**
 * Created by navdeep on 26/02/16.
 */
public class TeamMemberAdapter extends EndlessAdapter<Users> implements View.OnClickListener {

    private final List<Users> mDataset;
    private final Context context;
    private AppPrefrences appPref;

    // private Boolean selected;

    public TeamMemberAdapter(List<Users> mDataset, Context context, RecyclerView recyclerView) {
        super(mDataset, recyclerView);
        this.mDataset = mDataset;
        this.context = context;
        appPref = new AppPrefrences(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.team_image, parent, false);
            vh = new ViewHolder(view);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_team, parent, false);

            vh = new ProgressViewHolder(v);
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (getItemViewType(position) == VIEW_ITEM) {
            Utils.setTextStyle(((ViewHolder) holder).mTeamName, context, Utils.REGULAR);
            ((ViewHolder) holder).mTeamName.setText(mDataset.get(position).getUser().getFirstName() + " " + mDataset.get(position).getUser().getLastName());
            Utils.loadUrlImage(mDataset.get(position).getUser().getProfileImage(), ((ViewHolder) holder).mTeamImage);

            ((ViewHolder) holder).mTeamName.setTag(mDataset.get(position));
            ((ViewHolder) holder).mTeamImage.setTag(mDataset.get(position));

            ((ViewHolder) holder).mTeamImage.setOnClickListener(this);
            ((ViewHolder) holder).mTeamName.setOnClickListener(this);


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.team_image:
            case R.id.name:
                Users user = (Users) v.getTag();


                if (appPref.getUserId() == user.getUser().getUserId()) {

                    if (context instanceof ManToSubProfileActivity || context instanceof SubToSubProfileActivity) {
                        ((Activity) context).finish();
                    }
                } else {
                    if (appPref.getRole() == 1) {
                        //UtileClass.selectedSubordinate = user;
                        Intent intent = new Intent(context, ManToSubProfileActivity.class);
//                        intent.putExtra("USER", user);
                        intent.putExtra("USER", user.getUser().getUserId());
                        intent.putExtra("TEAM", user.getTeamId());

                        context.startActivity(intent);

                    } else if (appPref.getRole() == 2) {
                        //UtileClass.selectedSubordinate = user;
                        Intent intent = new Intent(context, SubToSubProfileActivity.class);
                        intent.putExtra("USER", user.getUser().getUserId());
                        intent.putExtra("TEAM", user.getTeamId());

                        context.startActivity(intent);

                    }

                    if (context instanceof ManToSubProfileActivity || context instanceof SubToSubProfileActivity) {
                        ((Activity) context).finish();
                    }

                }
                break;
        }

    }


    public static class ViewHolder extends RecyclerView.ViewHolder
            implements View
            .OnClickListener {

        private TextView mTeamName;
        private ImageView mTeamImage;


        public ViewHolder(View itemView) {
            super(itemView);

            mTeamName = (TextView) itemView.findViewById(R.id.name);
            mTeamImage = (ImageView) itemView.findViewById(R.id.team_image);
            //    mDays = (TextView) itemView.findViewById(R.id.days);


        }

        @Override
        public void onClick(View v) {


        }
    }


}
