package com.appster.growthcard.adapter;

/**
 * Created by navdeep on 17/03/16.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.EADetailsActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.PastEAListResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

/**
 * Created by himanshu on 08/03/16.
 */
public class SubPastEAAdapter extends EndlessAdapter<PastEAListResponse.PastEffectiveAction> {

    private static final int TYPE_HEADER = 0;
    private Context context;
    private View behindView;
    private ArrayList<PastEAListResponse.PastEffectiveAction> data;
    private int userId;
    private AppPrefrences appPref;

    public SubPastEAAdapter(Context context, RecyclerView recyclerView, ArrayList<PastEAListResponse.PastEffectiveAction> data, View behindView, Users userVO) {
        super(data, recyclerView);
        this.context = context;
        this.data = data;
        appPref = new AppPrefrences(context);
        this.behindView = behindView;
        this.userId = userVO.getUser().getUserId();
    }

    public SubPastEAAdapter(Context context, RecyclerView recyclerView, ArrayList<PastEAListResponse.PastEffectiveAction> data) {
        super(data, recyclerView);
        this.context = context;
        this.data = data;
        appPref = new AppPrefrences(context);

        this.userId = (new AppPrefrences(context)).getUserId();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder vh;

        if (viewType == VIEW_ITEM) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_past_ea_sub, parent, false);
            vh = new ViewHolder(view);

            ViewHolder viewHolder = (ViewHolder) vh;


            return viewHolder;


        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.transparent_header, parent, false);
            VHHeader viewHolder = new VHHeader(view);
            return viewHolder;
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            vh = new ProgressViewHolder(v);
        }


        return vh;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;
            PastEAListResponse.PastEffectiveAction pastEffectiveAction = getItem(position);
            ((View) viewHolder.chartParent.getParent().getParent()).setTag(pastEffectiveAction);

            ((View) viewHolder.chartParent.getParent().getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    PastEAListResponse.PastEffectiveAction data = (PastEAListResponse.PastEffectiveAction) v.getTag();
                    if (data.getEffectiveActionId() != null) {
                        Intent i = new Intent(context, EADetailsActivity.class);
                        i.putExtra("isCurrent", false);
                        i.putExtra("pastEAid", data.getPastEffectiveActionId());
                        i.putExtra("EAid", data.getEffectiveActionId());
                        i.putExtra("USER", userId);
//                    i.putExtra("TEAM", data.ge);

                        context.startActivity(i);
                    }

                }
            });


            Utils.setTextStyle(viewHolder.txtEA, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtEAdescription, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.likesNum, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtTimeStamp, context, Utils.REGULAR);

            //data.get(position - 1);

            if (viewHolder.chartParent.getChildCount() == 2) {
                Utils.setChart(context, viewHolder.chartParent, pastEffectiveAction.getGraph(), appPref.getRole() == 2);
            }
            if (pastEffectiveAction.getEffectiveaction().getEffectiveActionTitle() != null) {
                viewHolder.txtEA.setText(pastEffectiveAction.getEffectiveaction().getEffectiveActionTitle());
            }

            if (pastEffectiveAction.getEffectiveActionDescription() != null) {

                viewHolder.txtEAdescription.setText(pastEffectiveAction.getEffectiveActionDescription());
                viewHolder.txtEAdescription.collapseText();
            }

            viewHolder.likesNum.setText(pastEffectiveAction.getLikeCount() + " " + (pastEffectiveAction.getLikeCount() == 1 ? context.getString(R.string.like) : context.getString(R.string.likes)));

//            if (pastEffectiveAction.getEffectiveActionDescription().length() > 200) {
//                UtileClass.makeTextViewResizable(context, viewHolder.txtEAdescription, 4, "Read More", true);
//            }


            try {
                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));

                String startDate = Utils.getDate(outputFmt.parse(pastEffectiveAction.getStartDate()), "dd MMMM, yyyy");
                String endDate = Utils.getDate(outputFmt.parse(pastEffectiveAction.getEndDate().getDate()), "dd MMMM, yyyy");

                ((ViewHolder) holder).txtTimeStamp.setText(startDate + " - " + endDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else if (holder instanceof VHHeader) {
            Utils.setTextStyle(((VHHeader) holder).header, context, Utils.MEDIUM);
            ((View) ((VHHeader) holder).header.getParent()).setVisibility(View.VISIBLE);


            ViewGroup.LayoutParams params = ((VHHeader) holder).transHead.getLayoutParams();
            params.height = Utils.dpToPx(context, 240);

            ((VHHeader) holder).transHead.setLayoutParams(params);


            if (getItemCount() > 0)
                ((VHHeader) holder).header.setText(context.getString(R.string.past_ea_heading) + "  (" + (getItemCount() - 1) + ")");
            else
                ((VHHeader) holder).header.setText(context.getString(R.string.past_ea_heading));


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (context instanceof SubMainActivity)
            return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;


        if (position == 0)
            return TYPE_HEADER;
        return data.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG;
    }

    @Override
    public int getItemCount() {
        if (context instanceof SubMainActivity)
            return data.size();

        return data.size() + 1;
    }

    private PastEAListResponse.PastEffectiveAction getItem(int position) {

        if (context instanceof SubMainActivity)
            return data.get(position);

        return data.get(position - 1);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView txtEA;
        private final ExpandableTextView txtEAdescription;
        private final TextView txtTimeStamp;
        private final LinearLayout chartParent;
        private final TextView likesNum;
        private final RelativeLayout rel_subordinateDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            txtEA = (TextView) itemView.findViewById(R.id.txtEA);
            txtEAdescription = (ExpandableTextView) itemView.findViewById(R.id.txtEAdescription);
            txtTimeStamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            likesNum = (TextView) itemView.findViewById(R.id.likes_num);
            chartParent = (LinearLayout) itemView.findViewById(R.id.chart_ea);
            rel_subordinateDetail = (RelativeLayout) itemView.findViewById(R.id.rel_subordinateDetail);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {

        public View transHead;
        public TextView header;


        public VHHeader(View rootView) {
            super(rootView);

            transHead = (View) rootView.findViewById(R.id.trans_head);

            transHead.setOnTouchListener(new View.OnTouchListener() {
                                             @Override
                                             public boolean onTouch(View v, MotionEvent event) {
//                                                  http://stackoverflow.com/questions/8121491/is-it-possible-to-add-a-scrollable-textview-to-a-listview
                                                 v.getParent().requestDisallowInterceptTouchEvent(true); // needed for complex gestures
                                                 // simple tap works without the above line as well
                                                 return behindView.dispatchTouchEvent(event); // onTouchEvent won't work


                                             }
                                         }

            );

            header = (TextView) rootView.findViewById(R.id.ea_header);

        }
    }


}
