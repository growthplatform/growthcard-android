package com.appster.growthcard.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.model.Comment;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.EADetailResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by navdeep on 08/03/16.
 */
public class DetailViewCommentAdapter extends EndlessAdapter<Comment> {
    private static final int TYPE_HEADER = 0;
    private final Context context;
    private final EADetailResponse.Result result;
    private ArrayList<Comment> data;
    private boolean isFirst;
    private boolean isHeaderPresent;


    public DetailViewCommentAdapter(Context context, ArrayList<Comment> data, RecyclerView recyclerView, boolean isHeaderPresent, EADetailResponse.Result result) {
        super(data, recyclerView);
        this.data = data;
        this.context = context;
        this.isHeaderPresent = isHeaderPresent;
        this.result = result;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_list_row, parent, false);
            VHItem viewHolder = new VHItem(view);
            return viewHolder;
        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.comment_header, parent, false);
            VHHeader viewHolder = new VHHeader(view);
            AppPrefrences appPrefrences = new AppPrefrences(context);

            if (!isFirst) {
                Utils.setChart(context, ((VHHeader) viewHolder).eaChart, result.getGraph(), appPrefrences.getRole() == 2);
                isFirst = true;
            }

            return viewHolder;
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            ProgressViewHolder vh = new ProgressViewHolder(v);
            return vh;
        }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            Comment dataItem = getItem(position);

            Utils.setTextStyle(((VHItem) holder).commentName, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHItem) holder).commentDesignation, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHItem) holder).commentDate, context, Utils.REGULAR);
            Utils.setTextStyle(((VHItem) holder).commentText, context, Utils.REGULAR);

            ((VHItem) holder).cardView.setBackgroundResource(R.drawable.white_box);
            ((VHItem) holder).blueTriangle.setVisibility(View.INVISIBLE);
            ((VHItem) holder).whiteTriangle.setVisibility(View.VISIBLE);
            ((VHItem) holder).imgManagerImage.setVisibility(View.VISIBLE);
            ((VHItem) holder).commentText.setTextColor(ContextCompat.getColor(context, R.color.text_grey));
            ((VHItem) holder).commentDesignation.setTextColor(ContextCompat.getColor(context, R.color.button_green));

            if (dataItem != null) {
                if (dataItem.getRole() == 2) {
                    ((VHItem) holder).cardView.setBackgroundResource(R.drawable.blue_box);
                    ((VHItem) holder).blueTriangle.setVisibility(View.VISIBLE);
                    ((VHItem) holder).whiteTriangle.setVisibility(View.INVISIBLE);
                    ((VHItem) holder).imgManagerImage.setVisibility(View.INVISIBLE);
                    ((VHItem) holder).commentText.setTextColor(ContextCompat.getColor(context, android.R.color.white));
                    ((VHItem) holder).commentDesignation.setTextColor(ContextCompat.getColor(context, R.color.text_grey));
                }

                ((VHItem) holder).commentName.setText(dataItem.getUser().getFirstName() + " " + dataItem.getUser().getLastName());

                if (dataItem.getMember() != null && dataItem.getMember().getDesignation() !=null && dataItem.getMember().getDesignation().getDesignationTitle()!=null) {
                    ((VHItem) holder).commentDesignation.setText(dataItem.getMember().getDesignation().getDesignationTitle());
                }
                ((VHItem) holder).commentText.setText(dataItem.getContent());

                Utils.loadUrlImage(dataItem.getUser().getProfileImage(), ((VHItem) holder).imgManagerImage);

                try {
                    SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                    long l = outputFmt.parse(dataItem.getCreatedAt().getDate()).getTime();
                    String ab = Utils.getDate(outputFmt.parse(dataItem.getCreatedAt().getDate()), "dd MMMM, yyyy");
                    Log.d("TAG", ab);
                    String str = DateUtils.getRelativeDateTimeString(

                            context, // Suppose you are in an activity or other Context subclass

                            l, // The time to display

                            DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                            // minutes (no "3 seconds ago")


                            DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                            // to default date instead of spans. This will not
                            // display "3 weeks ago" but a full date instead

                            0).toString();
                    if (str.contains(",")) {
                        ((VHItem) holder).commentDate.setText(str.split(",")[0]);
                    } else {
                        ((VHItem) holder).commentText.setText(str);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


        } else if (holder instanceof VHHeader) {

            AppPrefrences appPref = new AppPrefrences(context);
            Utils.setTextStyle(((VHHeader) holder).txtEA, context, Utils.MEDIUM);
            Utils.setTextStyle(((VHHeader) holder).txtTimeStamp, context, Utils.REGULAR);
            Utils.setTextStyle(((VHHeader) holder).txtTimeStamp, context, Utils.REGULAR);

            if(result.getEffectiveActionTitle()!= null) {
                ((VHHeader) holder).txtEA.setText(result.getEffectiveActionTitle());
            }
            if(result.getEffectiveActionDescription()!= null) {
                ((VHHeader) holder).txtEADesc.collapseText();
                ((VHHeader) holder).txtEADesc.setText(result.getEffectiveActionDescription());
            }
                try {

                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                String startDate = Utils.getDate(outputFmt.parse(result.getStartDate()), "dd MMMM, yyyy");
                ((VHHeader) holder).txtTimeStamp.setText(startDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(position!=0)
                holder.itemView.setVisibility(View.GONE);
            else
                holder.itemView.setVisibility(View.VISIBLE);


        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }
    }


    @Override
    public int getItemCount() {
        if (!isHeaderPresent)
            return data.size();

        return data.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (!isHeaderPresent)
            return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;

        if (isPositionHeader(position))
            return TYPE_HEADER;

        return data.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private Comment getItem(int position) {

        if (!isHeaderPresent)
            return data.get(position);

        return data.get(position - 1);
    }


    class VHItem extends RecyclerView.ViewHolder {

        public final CircleImageView imgManagerImage;
        public final TextView commentName;
        public final TextView commentDesignation;
        public final TextView commentDate;
        public final ImageView whiteTriangle;
        public final ImageView blueTriangle;
        public final RelativeLayout cardView;
        public final TextView commentText;

        public VHItem(View rootView) {
            super(rootView);

            imgManagerImage = (CircleImageView) rootView.findViewById(R.id.imgManagerImage);
            commentName = (TextView) rootView.findViewById(R.id.comment_name);
            commentDesignation = (TextView) rootView.findViewById(R.id.comment_designation);
            commentDate = (TextView) rootView.findViewById(R.id.comment_date);
            whiteTriangle = (ImageView) rootView.findViewById(R.id.white_triangle);
            blueTriangle = (ImageView) rootView.findViewById(R.id.blue_triangle);
            cardView = (RelativeLayout) rootView.findViewById(R.id.card_view);
            commentText = (TextView) rootView.findViewById(R.id.comment_text);
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        public final TextView txtEA;
        public final ExpandableTextView txtEADesc;

        private final LinearLayout eaChart;
        private final TextView txtTimeStamp;


        public VHHeader(View rootView) {
            super(rootView);

            txtEA = (TextView) rootView.findViewById(R.id.txtEA);
            txtTimeStamp = (TextView) rootView.findViewById(R.id.txtTime_Stamp);
            txtEADesc = (ExpandableTextView) rootView.findViewById(R.id.txtEAdescription);
            eaChart = (LinearLayout) rootView.findViewById(R.id.ea_chart);

        }
    }


}
