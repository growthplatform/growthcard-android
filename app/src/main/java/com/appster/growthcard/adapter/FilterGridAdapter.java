package com.appster.growthcard.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.DepartmentResponse;

import java.util.List;

/**
 * Created by himanshukathuria on 31/03/16.
 */
public class FilterGridAdapter extends BaseAdapter {


    private final Context context;
    private List<DepartmentResponse.Result> mDepartmentData;
    private TextView tvDepartment;
    private RelativeLayout llDepartment;
    private ImageView ivDepartment;

    public FilterGridAdapter(Context context, List<DepartmentResponse.Result> departmentData) {
        this.context = context;
        this.mDepartmentData = departmentData;

    }

    @Override
    public int getCount() {
        return mDepartmentData.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final View grid;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = inflater.inflate(R.layout.grid_button, null);
        } else {
            grid = convertView;
        }

        tvDepartment = (TextView) grid.findViewById(R.id.txtDepartment);
        llDepartment = (RelativeLayout) grid.findViewById(R.id.llDepartment);
        ivDepartment = (ImageView) grid.findViewById(R.id.imgDepartment);

        Utils.setTextStyle(tvDepartment, context, Utils.MEDIUM);

        tvDepartment.setText(mDepartmentData.get(position).getDepartmentName());
        tvDepartment.setTag(position);

        if (mDepartmentData.get(position).getSelected()) {
            llDepartment.setBackgroundColor(ContextCompat.getColor(context, R.color.button_green));
            tvDepartment.setTextColor(ContextCompat.getColor(context, R.color.pure_white));
            ivDepartment.setVisibility(View.VISIBLE);
        } else {
            llDepartment.setBackground(ContextCompat.getDrawable(context, R.drawable.button_white_stroke));
            tvDepartment.setTextColor(ContextCompat.getColor(context, R.color.text_workdays_alpha_3));
            ivDepartment.setVisibility(View.GONE);
        }

      /*  txtDynamicData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int pos=(int)v.getTag();
                for (int i = 0; i <= dummyData.length; i++) {

                    if (i == pos) {


                        txtDynamicData.setBackgroundColor(ContextCompat.getColor(context, R.color.button_green));
                        txtDynamicData.setTextColor(ContextCompat.getColor(context, R.color.pure_white));
                        break;
                    }
                    else {
                        txtDynamicData.setBackground(ContextCompat.getDrawable(context, R.drawable.button_white_stroke));
                        txtDynamicData.setTextColor(ContextCompat.getColor(context, R.color.text_workdays_alpha_3));


                    }


                }
                notifyDataSetChanged();
            }
        });*/
        return grid;


    }
}
