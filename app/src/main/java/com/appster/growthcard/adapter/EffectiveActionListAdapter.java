package com.appster.growthcard.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.EAListResponse;

import java.util.ArrayList;

/**
 * Created by himanshu on 01/03/16.
 */
public class EffectiveActionListAdapter extends EndlessAdapter<EAListResponse.EffectiveAction> {

    private final Context context;
    private EffectiveActionListener listener;
    private ArrayList<EAListResponse.EffectiveAction> eaListResponse;

    public EffectiveActionListAdapter(Context context, ArrayList<EAListResponse.EffectiveAction> eaListResponse, RecyclerView recyclerView) {
        super(eaListResponse, recyclerView);
        this.context = context;
        this.eaListResponse = eaListResponse;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_effective_list, parent, false);
            vh = new ViewHolder(view);
        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);
            vh = new ProgressViewHolder(v);
        }

        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (getItemViewType(position) == VIEW_ITEM) {

            final EAListResponse.EffectiveAction result = eaListResponse.get(position);
            Utils.setTextStyle(((ViewHolder) viewHolder).txtEffectiveAction, context, Utils.REGULAR);
            ((ViewHolder) viewHolder).txtEffectiveAction.setText(result.getEffectiveActionTitle());
            ((ViewHolder) viewHolder).ll_effective_action.setTag(position);
            ((ViewHolder) viewHolder).ll_effective_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int pos = (int) v.getTag();
                    if (listener != null) {
                        listener.onAction(pos);
                    }


                }
            });
        } else {
            ((ProgressViewHolder) viewHolder).progressBar.setIndeterminate(true);

        }

    }

    @Override
    public int getItemCount() {
        return eaListResponse.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView txtEffectiveAction;
        private final LinearLayout ll_effective_action;
        public ViewHolder(View itemView) {
            super(itemView);
            txtEffectiveAction = (TextView) itemView.findViewById(R.id.txtEffectiveAction);
            ll_effective_action = (LinearLayout) itemView.findViewById(R.id.ll_effective_action);
        }
    }

    public void setListener(EffectiveActionListener listener) {
        this.listener = listener;
    }

    public interface EffectiveActionListener {
        void onAction(int position);
    }

}
