package com.appster.growthcard.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.NewAnnoucementActivity;
import com.appster.growthcard.ui.activities.SearchSubordinateActivity;
import com.appster.growthcard.Constants;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.interfaces.OnCreateClickListener;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by himanshu on 14/03/16.
 */
public class SelectedSubordinatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnClickListener {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private final Context context;
    public int flag = 1;

    public SelectedSubordinatesAdapter(Context context) {
        this.context = context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_new_annoucement, parent, false);

            VHHeader viewHolder = new VHHeader(view);
            return viewHolder;

        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_selectedsubordinates, parent, false);
            VHItem viewHolder = new VHItem(view);
            return viewHolder;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof VHItem) {
            final VHItem vholder = (VHItem) holder;
            Utils.setTextStyle(vholder.txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(vholder.txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyleButton(vholder.btnCancel, context, Utils.REGULAR);


            vholder.txtSubordinateName.setText(NewAnnoucementActivity.selecteddata.get(position - 1).getFirstName() + " " + NewAnnoucementActivity.selecteddata.get(position - 1).getLastName());
            vholder.txtSubordinateDesignation.setText(NewAnnoucementActivity.selecteddata.get(position - 1).getMember().getDesignation().getDesignationTitle());

            Utils.loadUrlImage(NewAnnoucementActivity.selecteddata.get(position - 1).getProfileImage(), vholder.imgSubordinateImage);

            vholder.btnCancel.setTag(position);
            vholder.btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    int pos = (int) v.getTag();
                    NewAnnoucementActivity.selecteddata.remove(NewAnnoucementActivity.selecteddata.get(pos - 1));
                    notifyDataSetChanged();
                }
            });

        } else {


            final VHHeader vholder = (VHHeader) holder;
            Utils.setTextStyle(vholder.txtSubjectWord, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.edtSubject, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.txtMessageWord, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.edtMessage, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.txtRecipents, context, Utils.REGULAR);
            Utils.setTextStyleButton(vholder.btnSearchUser, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.chkAll, context, Utils.LIGHT);
            Utils.setTextStyle(vholder.chkManager, context, Utils.LIGHT);
            Utils.setTextStyle(vholder.chkSubordinate, context, Utils.LIGHT);


            if (NewAnnoucementActivity.selecteddata.size() == 0) {
                vholder.viewlast.setVisibility(View.GONE);


            } else {
                flag = 0;
                vholder.viewlast.setVisibility(View.VISIBLE);

                vholder.chkAll.setChecked(false);
                vholder.chkManager.setChecked(false);
                vholder.chkSubordinate.setChecked(false);
            }

            //   vholder.viewlast.setVisibility(View.GONE);
            vholder.btnSearchUser.setOnClickListener(this);
            vholder.chkAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flag = 1;
                    if (vholder.chkAll.isChecked()) {
                        vholder.chkManager.setChecked(false);
                        vholder.chkSubordinate.setChecked(false);

                    } else {
                        vholder.chkAll.setChecked(true);

                    }
                    NewAnnoucementActivity.selecteddata.clear();
                    notifyDataSetChanged();

                }
            });

            vholder.chkManager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flag = 3;
                    if (vholder.chkManager.isChecked()) {
                        vholder.chkAll.setChecked(false);
                        vholder.chkSubordinate.setChecked(false);

                    } else {
                        vholder.chkManager.setChecked(true);

                    }
                    NewAnnoucementActivity.selecteddata.clear();
                    notifyDataSetChanged();

                }
            });
            vholder.chkSubordinate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    flag = 2;
                    if (vholder.chkSubordinate.isChecked()) {
                        vholder.chkAll.setChecked(false);
                        vholder.chkManager.setChecked(false);

                    } else {
                        vholder.chkSubordinate.setChecked(true);

                    }
                    NewAnnoucementActivity.selecteddata.clear();
                    notifyDataSetChanged();
                }
            });


        }


    }

    @Override
    public int getItemCount() {
        return NewAnnoucementActivity.selecteddata.size() + 1;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSearchUser:
                Intent intent = new Intent(context, SearchSubordinateActivity.class);


                ((NewAnnoucementActivity) context).startActivityForResult(intent, Constants.NEWANNOUCEMNET_REQUESTCODE);
                break;


        }
    }

    public class VHItem extends RecyclerView.ViewHolder {
        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final Button btnCancel;
        private final CircleImageView imgSubordinateImage;
        private final View viewlast;

        public VHItem(View itemView) {
            super(itemView);

            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            btnCancel = (Button) itemView.findViewById(R.id.btnCancel);
            imgSubordinateImage = (CircleImageView) itemView.findViewById(R.id.imgSubordinateImage);
            viewlast = (View) itemView.findViewById(R.id.viewlast);

            Utils.setTextStyle(txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyleButton(btnCancel, context, Utils.REGULAR);


        }
    }

    private EditText edtSubject, edtMessage;

    class VHHeader extends RecyclerView.ViewHolder {

        private final TextView txtSubjectWord;
        private final EditText edtSubject;
        private final TextView txtMessageWord;
        private final EditText edtMessage;
        private final TextView txtRecipents;
        private final Button btnSearchUser;
        private final CheckBox chkAll;
        private final CheckBox chkSubordinate;
        private final CheckBox chkManager;
        private final View viewlast;

        public VHHeader(View itemView) {
            super(itemView);
            txtSubjectWord = (TextView) itemView.findViewById(R.id.txtSubjectWord);
            txtMessageWord = (TextView) itemView.findViewById(R.id.txtMessageWord);
            edtSubject = (EditText) itemView.findViewById(R.id.edtSubject);
            edtMessage = (EditText) itemView.findViewById(R.id.edtMessage);
            SelectedSubordinatesAdapter.this.edtMessage = edtMessage;
            SelectedSubordinatesAdapter.this.edtSubject = edtSubject;
            txtRecipents = (TextView) itemView.findViewById(R.id.txtRecipents);
            btnSearchUser = (Button) itemView.findViewById(R.id.btnSearchUser);
            chkAll = (CheckBox) itemView.findViewById(R.id.chkAll);
            chkSubordinate = (CheckBox) itemView.findViewById(R.id.chkSubordinate);
            chkManager = (CheckBox) itemView.findViewById(R.id.chkManager);
            viewlast = (View) itemView.findViewById(R.id.viewlast);


        }
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    public void onCreateClick() {
        if (edtSubject.getText().toString().trim().isEmpty()) {
            Utils.showResMsg(edtSubject, context.getString(R.string.enter_subject));
        } else if (edtMessage.getText().toString().trim().equals("")) {
            Utils.showResMsg(edtMessage, context.getString(R.string.enter_message));
        } else {
            ((OnCreateClickListener) context).onCreateClick(edtSubject.getText().toString().trim(), edtMessage.getText().toString().trim(), flag);
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("MyAdapter", "onActivityResult");

        if (requestCode == Constants.NEWANNOUCEMNET_REQUESTCODE && resultCode == ((NewAnnoucementActivity) context).RESULT_OK)
            notifyDataSetChanged();
    }
}
