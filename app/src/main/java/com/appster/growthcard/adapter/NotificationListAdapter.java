package com.appster.growthcard.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.EADetailsActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.ui.activities.SubordinateAnnoucementList;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.CustomTypefaceSpan;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.NotificationResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by himanshukathuria on 05/04/16.
 */
public class NotificationListAdapter extends EndlessAdapter<NotificationResponse.Notification> {
    private final Context context;
    private final ArrayList<NotificationResponse.Notification> myDataSet;
    private final AppPrefrences appPref;
    private Intent intent;

    public NotificationListAdapter(List<NotificationResponse.Notification> myDataSet, RecyclerView recyclerView, Context context) {
        super(myDataSet, recyclerView);
        appPref = new AppPrefrences(context);
        this.context = context;
        this.myDataSet = (ArrayList) myDataSet;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_notification, parent, false);
            vh = new ViewHolder(view);
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);
            vh = new ProgressViewHolder(view);

        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == VIEW_ITEM) {


            NotificationResponse.Notification notification = getItem(position);
            final ViewHolder vholder = (ViewHolder) holder;
            Utils.setTextStyle(vholder.txtTime_Stamp, context, Utils.REGULAR);


            Typeface fontMedium = Typeface.createFromAsset(context.getAssets(), "Rubik-Medium.ttf");
            Typeface fontRegular = Typeface.createFromAsset(context.getAssets(), "Rubik-Regular.ttf");
            SpannableStringBuilder SS = new SpannableStringBuilder(notification.getMessage1() + " " + notification.getMessage2() + " " + notification.getMessage3());
            // int boldLength = (feed.getEffectiveaction().getComment().getUser().getFirstName() + " " + feed.getEffectiveaction().getComment().getUser().getLastName()).length();
            SS.setSpan(new CustomTypefaceSpan(context, fontMedium, ContextCompat.getColor(context, R.color.text_grey)), 0, notification.getMessage1().length(), Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            int oneLength = notification.getMessage1().length();
            int secLength = oneLength + notification.getMessage2().length();
            int thirdLength = secLength + 2 + notification.getMessage3().length();

            SS.setSpan(new CustomTypefaceSpan(context, fontRegular, ContextCompat.getColor(context, R.color.text_workdays)), oneLength, secLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            SS.setSpan(new CustomTypefaceSpan(context, fontMedium, ContextCompat.getColor(context, R.color.fab_color)), secLength, thirdLength, Spanned.SPAN_EXCLUSIVE_INCLUSIVE);
            vholder.txtUserName.setText(SS);

            Utils.loadUrlImage(notification.getProfileImage(), vholder.imgUserImage);

            if (notification.getRead() == 0) {
                vholder.imgCircularDot.setVisibility(View.VISIBLE);
            } else {
                vholder.imgCircularDot.setVisibility(View.GONE);
            }

            ((View) vholder.imgUserImage.getParent()).setTag(notification);
            ((View) vholder.imgUserImage.getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    NotificationResponse.Notification notification = (NotificationResponse.Notification) v.getTag();

                    switch (notification.getType()) {

                        case 2://EA assign


                            if (notification.getEffectiveActionId() != null && notification.getEffectiveActionId() != 0  && notification.getTeamId() != null && notification.getTeamId() != 0 && notification.getUserId() != null && notification.getUserId() != 0) {
                                intent = new Intent(context, EADetailsActivity.class);
                                intent.putExtra("isCurrent", notification.getEffectiveActionId() == appPref.getEAId());
                                intent.putExtra("EAid", notification.getEffectiveActionId());
                                intent.putExtra("USER", notification.getUserId());
                                intent.putExtra("TEAM", notification.getTeamId());
                                intent.putExtra("pastEAid", notification.getPastEffectiveActionId());

                                intent.putExtra("NEW_EA", false);
                                context.startActivity(intent);
                            }

                            break;

                        case 3://EA comment

                            if (notification.getEffectiveActionId() != null && notification.getEffectiveActionId() != 0  && notification.getTeamId() != null && notification.getTeamId() != 0 && notification.getUserId() != null && notification.getUserId() != 0) {
                                intent = new Intent(context, EADetailsActivity.class);
                                intent.putExtra("isCurrent", notification.getEffectiveActionId() == appPref.getEAId());
                                intent.putExtra("EAid", notification.getEffectiveActionId());
                                intent.putExtra("USER", notification.getUserId());
                                intent.putExtra("TEAM", notification.getTeamId());
                                intent.putExtra("pastEAid", notification.getPastEffectiveActionId());
                                intent.putExtra("NEW_EA", false);
                                context.startActivity(intent);
                            }

                            break;
                        case 4:// Like

//                            intent = new Intent(context, SubToSubProfileActivity.class);
//                            intent.putExtra("USER", notification.getUserId());
//                            intent.putExtra("TEAM", notification.getTeamId());
//
//                            context.startActivity(intent);

                            if (context instanceof SubMainActivity) {
                                ((SubMainActivity) context).selectItemFromDrawer(1);
                            }


                            break;

                        case 5:// Announcement

                            intent = new Intent(context, SubordinateAnnoucementList.class);
                            context.startActivity(intent);


                            break;

                        case 8://Mgr push

                            break;

                    }
                }
            });


            try {
                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                //  ((VHHeader) holder).eaDate.setText(UtileClass.getDate(outputFmt.parse(response.getResult().getMember().getEffective().getAssignEffectiveActionDate().getDate()), "dd MMMM, yyyy"));


                long l = outputFmt.parse(notification.getCreatedAt()).getTime();

                String ab = Utils.getDate(outputFmt.parse(notification.getCreatedAt()), "dd MMMM, yyyy");
                Log.d("TAG", ab);

                String str = DateUtils.getRelativeDateTimeString(

                        context, // Suppose you are in an activity or other Context subclass

                        l, // The time to display

                        DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                        // minutes (no "3 seconds ago")


                        DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                        // to default date instead of spans. This will not
                        // display "3 weeks ago" but a full date instead

                        0).toString();
                if (str.contains(",")) {
                    vholder.txtTime_Stamp.setText(str.split(",")[0]);
                } else {
                    vholder.txtTime_Stamp.setText(str);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }


    }

    private NotificationResponse.Notification getItem(int pos) {
        return myDataSet.get(pos);
    }

    @Override
    public int getItemCount() {
        return myDataSet.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final CircleImageView imgUserImage;
        private final TextView txtUserName;
        private final TextView txtTime_Stamp;
        private final ImageView imgCircularDot;

        public ViewHolder(View itemView) {
            super(itemView);

            txtUserName = (TextView) itemView.findViewById(R.id.txtUserName);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            imgUserImage = (CircleImageView) itemView.findViewById(R.id.imgUserImage);
            imgCircularDot = (ImageView) itemView.findViewById(R.id.imgCircularDot);

        }
    }
}
