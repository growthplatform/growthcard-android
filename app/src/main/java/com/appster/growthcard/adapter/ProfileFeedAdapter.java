package com.appster.growthcard.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.SubToSubProfileActivity;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.Utils;
import com.appster.growthcard.network.response.SubProfileResponse;

import java.util.List;

import retrofit2.Retrofit;

/**
 * Created by navdeep on 15/03/16.
 */
public class ProfileFeedAdapter extends EndlessAdapter<String> {

    private static final int TYPE_HEADER = 0;
    private final Context context;
    private List<String> myDataSet;
    private List<Users> teamMembers;
    private SubProfileResponse response;
    private Retrofit retrofit;
    private AppPrefrences appPref;
    private int pageNo = 0, totalPage = 0;
    private TeamMemberAdapter mAdapter;
    private View behindView;

    public ProfileFeedAdapter(List<String> myDataSet, RecyclerView recyclerView, List<Users> teamMembers, Context context, View behindView) {
        super(myDataSet, recyclerView);
        this.myDataSet = myDataSet;
        this.context = context;
        this.response = response;
        this.teamMembers = teamMembers;
        appPref = new AppPrefrences(context);
        this.behindView = behindView;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_ITEM) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_past_ea_man, parent, false);
            VHItem viewHolder = new VHItem(view);
            return viewHolder;
        } else if (viewType == TYPE_HEADER) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.transparent_header, parent, false);
            VHHeader viewHolder = new VHHeader(view);
            return viewHolder;
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            //   String dataItem = getItem(position);
            //cast holder to VHItem and set data


        } else if (holder instanceof VHHeader) {

            ((View) ((VHHeader) holder).header.getParent()).setVisibility(View.INVISIBLE);

            if (context instanceof SubToSubProfileActivity) {
                ViewGroup.LayoutParams params = ((VHHeader) holder).transHead.getLayoutParams();
                params.height = Utils.dpToPx(context, 240);

                ((VHHeader) holder).transHead.setLayoutParams(params);
            }


        }
    }


    @Override
    public int getItemCount() {
        return myDataSet.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return myDataSet.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

//    private String getItem(int position) {
//        return myDataSet.get(position - 1);
//    }


    class VHItem extends RecyclerView.ViewHolder {


        public VHItem(View rootView) {
            super(rootView);

        }
    }

    class VHHeader extends RecyclerView.ViewHolder {

        public View transHead;
        public TextView header;


        public VHHeader(View rootView) {
            super(rootView);

            transHead = (View) rootView.findViewById(R.id.trans_head);
            transHead.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // http://stackoverflow.com/questions/8121491/is-it-possible-to-add-a-scrollable-textview-to-a-listview
                    v.getParent().requestDisallowInterceptTouchEvent(true); // needed for complex gestures
                    // simple tap works without the above line as well
                    return behindView.dispatchTouchEvent(event); // onTouchEvent won't work
                }
            });
            header = (TextView) rootView.findViewById(R.id.ea_header);


        }
    }


}
