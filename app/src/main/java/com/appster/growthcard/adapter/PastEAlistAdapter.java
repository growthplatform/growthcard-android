package com.appster.growthcard.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.model.Users;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.DeletePastEAReq;
import com.appster.growthcard.network.response.GenericResponse;
import com.appster.growthcard.network.response.PastEAListResponse;
import com.appster.growthcard.ui.activities.EADetailsActivity;
import com.appster.growthcard.ui.activities.ManToSubProfileActivity;
import com.appster.growthcard.ui.activities.ManagerMainActivity;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshu on 08/03/16.
 */
public class PastEAlistAdapter extends EndlessAdapter<PastEAListResponse.PastEffectiveAction> {

    private Context context;
    private static final int TYPE_HEADER = 0;
    private ArrayList<PastEAListResponse.PastEffectiveAction> data;
    private Users userVO;
    private AppPrefrences appPref;
    private Retrofit retrofit;


    public PastEAlistAdapter(Context context, RecyclerView recyclerView, ArrayList<PastEAListResponse.PastEffectiveAction> data, Users userVO) {
        super(data, recyclerView);
        this.context = context;
        this.data = data;
        appPref = new AppPrefrences(context);
        this.userVO = userVO;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_past_ea_man, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);

            Utils.setTextStyle(viewHolder.txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtEA, context, Utils.MEDIUM);
            Utils.setTextStyle(viewHolder.txtEAdescription, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtTime_Stamp, context, Utils.REGULAR);
            Utils.setTextStyle(viewHolder.txtMin, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtMinTime, context, Utils.REGULAR);

            Utils.setTextStyle(viewHolder.txtMax, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtMaxTime, context, Utils.REGULAR);

            Utils.setTextStyle(viewHolder.txtAvg, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtAvgTime, context, Utils.REGULAR);

            Utils.setTextStyle(viewHolder.txtToday, context, Utils.BOLD);
            Utils.setTextStyle(viewHolder.txtTodayTime, context, Utils.REGULAR);


            return viewHolder;
        } else if (viewType == TYPE_HEADER) {

            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.past_ea_header, parent, false);
            ViewHeaderHolder viewHolder = new ViewHeaderHolder(view);

            Utils.setTextStyle(viewHolder.alphabeticalCheck, context, Utils.LIGHT);
            Utils.setTextStyle(viewHolder.latestCheck, context, Utils.LIGHT);


            return viewHolder;

        } else {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);

            ProgressViewHolder vh = new ProgressViewHolder(v);
            return vh;
        }
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof ViewHolder) {
            ViewHolder viewHolder = (ViewHolder) holder;

            final PastEAListResponse.PastEffectiveAction pastEffectiveAction = data.get(position - 1);

            if (viewHolder.chartEa.getChildCount() == 2) {
                Utils.setChart(context, viewHolder.chartEa, pastEffectiveAction.getGraph(), appPref.getRole() == 2);

            }

            Utils.loadUrlImage(userVO.getUser().getProfileImage(), viewHolder.profileImage);
            viewHolder.txtSubordinateName.setText(userVO.getUser().getFirstName() + " " + userVO.getUser().getLastName());

            if (userVO.getDesignation() != null)
                viewHolder.txtSubordinateDesignation.setText(userVO.getDesignation().getDesignationTitle());


            if (pastEffectiveAction.getEffectiveaction() != null) {
                viewHolder.txtEA.setText(pastEffectiveAction.getEffectiveaction().getEffectiveActionTitle());
                viewHolder.txtEAdescription.setText(pastEffectiveAction.getEffectiveActionDescription());

            }
            viewHolder.txtEAdescription.collapseText();
//            if (pastEffectiveAction.getEffectiveActionDescription().length() > 200) {
//                UtileClass.makeTextViewResizable(context, viewHolder.txtEAdescription, 4, "Read More", true);
//            }

            if (pastEffectiveAction.getEffectiveActionSummary() != null) {
                ((ViewHolder) holder).txtMaxTime.setText(pastEffectiveAction.getEffectiveActionSummary().getMAX() + "");
                ((ViewHolder) holder).txtMinTime.setText(pastEffectiveAction.getEffectiveActionSummary().getMIN() + "");
                ((ViewHolder) holder).txtAvgTime.setText(pastEffectiveAction.getEffectiveActionSummary().getAVG() + "");
                ((ViewHolder) holder).txtTodayTime.setText(pastEffectiveAction.getEffectiveActionSummary().getTODAY() + "");
            }

            ((View) viewHolder.chartEa.getParent().getParent()).setTag(position - 1);

            ((View) viewHolder.chartEa.getParent().getParent()).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = (int) v.getTag();
                    Intent i = new Intent(context, EADetailsActivity.class);
                    i.putExtra("isCurrent", false);
                    i.putExtra("EAid", pastEffectiveAction.getEffectiveActionId());
                    i.putExtra("USER", userVO.getUser().getUserId());
                    i.putExtra("TEAM", userVO.getTeamId());
                    i.putExtra("pastEAid", data.get(position-1).getPastEffectiveActionId());
                    context.startActivity(i);
                }
            });

            try {
                SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));

                String startDate = Utils.getDate(outputFmt.parse(pastEffectiveAction.getStartDate()), "dd MMMM, yyyy");
                String endDate = Utils.getDate(outputFmt.parse(pastEffectiveAction.getEndDate().getDate()), "dd MMMM, yyyy");

                ((ViewHolder) holder).txtTime_Stamp.setText(startDate + "-" + endDate);

            } catch (ParseException e) {
                e.printStackTrace();
            }

            viewHolder.rel_subordinateDetail.setTag(position);
            viewHolder.rel_subordinateDetail.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    final int pos = (int) v.getTag();
                    new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                            .setTitleText("Alert")
                            .showCancelButton(true)
                            .setCancelText("No")
                            .setConfirmText("Yes")
                            .setContentText("Are you sure you want to Delete?")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                }
                            })
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismiss();
                                    deletePost(pos - 1);
                                }
                            })
                            .show();


                    return false;
                }
            });


            //((ViewHolder) holder).chartEa.animateY(2000);
        } else if (holder instanceof ViewHeaderHolder) {

            if (data != null && data.size() > 0) {

                ((ViewHeaderHolder) holder).emptyView.setText(R.string.no_past_data);
                ((ViewHeaderHolder) holder).emptyView.setVisibility(View.GONE);
            } else {
                ((ViewHeaderHolder) holder).emptyView.setText(R.string.no_past_data);
                ((ViewHeaderHolder) holder).emptyView.setVisibility(View.VISIBLE);
            }

            ((ViewHeaderHolder) holder).alphabeticalCheck.setChecked(((ManToSubProfileActivity) context).pastEAFlag == 1);
            ((ViewHeaderHolder) holder).latestCheck.setChecked(((ManToSubProfileActivity) context).pastEAFlag == 2);


            ((ViewHeaderHolder) holder).alphabeticalCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((ViewHeaderHolder) holder).alphabeticalCheck.isChecked()) {

                        ((ManToSubProfileActivity) context).pastEAFlag = 1;
                        ((ViewHeaderHolder) holder).latestCheck.setChecked(false);
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));

                    } else {
                        ((ManToSubProfileActivity) context).pastEAFlag = 2;

                        ((ViewHeaderHolder) holder).latestCheck.setChecked(true);

                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));
                    }
                    ((ManToSubProfileActivity) context).getPastEAList();

                }
            });

            ((ViewHeaderHolder) holder).latestCheck.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (((ViewHeaderHolder) holder).latestCheck.isChecked()) {

                        ((ManToSubProfileActivity) context).pastEAFlag = 2;

                        ((ViewHeaderHolder) holder).alphabeticalCheck.setChecked(false);
                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));


                    } else {

                        ((ManToSubProfileActivity) context).pastEAFlag = 1;

                        ((ViewHeaderHolder) holder).alphabeticalCheck.setChecked(true);
                        ((ViewHeaderHolder) holder).alphabeticalCheck.setTextColor(ContextCompat.getColor(context, R.color.colorAccent));
                        ((ViewHeaderHolder) holder).latestCheck.setTextColor(ContextCompat.getColor(context, R.color.check_grey));


                    }
                    ((ManToSubProfileActivity) context).getPastEAList();
                }
            });

        } else if (holder instanceof ProgressViewHolder) {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);

        }

    }

    @Override
    public int getItemCount() {
        return data.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (context instanceof ManagerMainActivity)
            return data.get(position) != null ? VIEW_ITEM : VIEW_PROG;
        else
            return position == 0 ? TYPE_HEADER : (data.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG);

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final TextView txtEA;
        private final ExpandableTextView txtEAdescription;
        private final TextView txtTime_Stamp;
        private final TextView txtMin;
        private final TextView txtMinTime;
        private final TextView txtMaxTime;
        private final TextView txtMax;
        private final TextView txtAvg;
        private final TextView txtAvgTime;
        private final TextView txtToday;
        private final TextView txtTodayTime;
        private final LinearLayout chartEa;
        private final RelativeLayout rel_subordinateDetail;
        private final ImageView profileImage;

        public ViewHolder(View itemView) {
            super(itemView);

            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            profileImage = (ImageView) itemView.findViewById(R.id.imgManagerImage);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            txtEA = (TextView) itemView.findViewById(R.id.txtEA);
            txtEAdescription = (ExpandableTextView) itemView.findViewById(R.id.txtEAdescription);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            txtMin = (TextView) itemView.findViewById(R.id.txtMin);
            txtMinTime = (TextView) itemView.findViewById(R.id.txtMinTime);
            txtMax = (TextView) itemView.findViewById(R.id.txtMax);
            txtMaxTime = (TextView) itemView.findViewById(R.id.txtMaxTime);
            txtAvg = (TextView) itemView.findViewById(R.id.txtAvg);
            txtAvgTime = (TextView) itemView.findViewById(R.id.txtAvgTime);
            txtToday = (TextView) itemView.findViewById(R.id.txtToday);
            txtTodayTime = (TextView) itemView.findViewById(R.id.txtTodayTime);

            chartEa = (LinearLayout) itemView.findViewById(R.id.chart_ea);
            rel_subordinateDetail = (RelativeLayout) itemView.findViewById(R.id.rel_subordinateDetail);

        }
    }

    public static class ViewHeaderHolder extends RecyclerView.ViewHolder {


        private final CheckBox alphabeticalCheck;
        private final CheckBox latestCheck;
        private final TextView emptyView;


        public ViewHeaderHolder(View itemView) {
            super(itemView);

            alphabeticalCheck = (CheckBox) itemView.findViewById(R.id.alphabetical);
            latestCheck = (CheckBox) itemView.findViewById(R.id.lastAssigned);
            emptyView = (TextView) itemView.findViewById(R.id.empty_view);

        }
    }

    ///////////////////

    private void deletePost(final int position) {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(context))
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<GenericResponse> ip = service.DeletePastEA(getPayload(position));


        ip.enqueue(new ErrorCallback.MyCallback<GenericResponse>() {
            @Override
            public void success(Response<GenericResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        data.remove(data.get(position));
                        notifyDataSetChanged();
                    }
                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, context, true);

    }

    private DeletePastEAReq getPayload(int position) {


        DeletePastEAReq req = new DeletePastEAReq();
        req.setDeviceId(Utils.getDeviceId(context));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setTeamId(userVO.getTeamId());
        req.setSubordinateId(userVO.getUser().getUserId());
        req.setEffectiveActionId(data.get(position).getEffectiveActionId());

        return req;
    }


//

}
