package com.appster.growthcard.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.appster.growthcard.ui.activities.ManagerMainActivity;
import com.appster.growthcard.ui.activities.SubMainActivity;
import com.appster.growthcard.model.NavItems;
import com.appster.growthcard.R;
import com.appster.growthcard.utils.Utils;

import java.util.ArrayList;


/**
 * Created by himanshu on 24/02/16.
 */
public class NavigationDrawerAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<NavItems> mNavItems;

    public NavigationDrawerAdapter(ArrayList<NavItems> navItems, Context context) {
        mContext = context;
        mNavItems = navItems;
    }

    @Override
    public int getCount() {
        return mNavItems.size();
    }

    @Override
    public NavItems getItem(int position) {
        return mNavItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.drawer_list_item, null);
            viewHolder = new ViewHolder();

            viewHolder.menuTitle = (TextView) convertView.findViewById(R.id.tv_nav_title);
            viewHolder.menuImage = (ImageView) convertView.findViewById(R.id.menu_image);
            viewHolder.notifyImage = (ImageView) convertView.findViewById(R.id.notification_icon);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }


        viewHolder.menuTitle.setText(mNavItems.get(position).mTitle);

        if (mNavItems.get(position).isNotifications)
            viewHolder.notifyImage.setVisibility(View.VISIBLE);
        else
            viewHolder.notifyImage.setVisibility(View.INVISIBLE);


        if (mContext instanceof SubMainActivity) {
            if (getItem(position).isSelected()) {
                viewHolder.menuTitle.setAlpha(1);
                Utils.setTextStyle(viewHolder.menuTitle, mContext, Utils.REGULAR);
                viewHolder.menuImage.setImageResource(Utils.subMenuActiveDrawableRes[position]);
            } else {
                viewHolder.menuTitle.setAlpha(0.5f);
                Utils.setTextStyle(viewHolder.menuTitle, mContext, Utils.REGULAR);
                viewHolder.menuImage.setImageResource(Utils.subMenuDrawableRes[position]);


            }
        } else if (mContext instanceof ManagerMainActivity) {
            if (getItem(position).isSelected()) {
                viewHolder.menuTitle.setAlpha(1);
                Utils.setTextStyle(viewHolder.menuTitle, mContext, Utils.REGULAR);
                viewHolder.menuImage.setImageResource(Utils.manMenuActiveDrawableRes[position]);

            } else {


                viewHolder.menuTitle.setAlpha(0.5f);
                Utils.setTextStyle(viewHolder.menuTitle, mContext, Utils.REGULAR);
                viewHolder.menuImage.setImageResource(Utils.manMenuDrawableRes[position]);

            }
        }


        return convertView;
    }

    class ViewHolder {

        public TextView menuTitle;
        public ImageView menuImage;
        public ImageView notifyImage;
    }

}