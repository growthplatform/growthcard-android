package com.appster.growthcard.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.appster.growthcard.R;
import com.appster.growthcard.network.ErrorCallback;
import com.appster.growthcard.network.GCApi;
import com.appster.growthcard.network.NetworkUtil;
import com.appster.growthcard.network.requests.DeletePostReq;
import com.appster.growthcard.network.response.GenericResponse;
import com.appster.growthcard.network.response.PostListResponse;
import com.appster.growthcard.ui.activities.SubToSubProfileActivity;
import com.appster.growthcard.ui.customviews.ExpandableTextView;
import com.appster.growthcard.utils.ApiConstants;
import com.appster.growthcard.utils.AppPrefrences;
import com.appster.growthcard.utils.SweetAlert.SweetAlertDialog;
import com.appster.growthcard.utils.Utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by himanshukathuria on 22/03/16.
 */
public class PostListAdapter extends EndlessAdapter<PostListResponse.Post> {
    private final Context context;
    private static final int TYPE_HEADER = 0;

    private final ArrayList<PostListResponse.Post> myDataSet;
    private final int flag;
    private View behindView;
    private Retrofit retrofit;
    private AppPrefrences appPref;


    public PostListAdapter(ArrayList<PostListResponse.Post> myDataSet, RecyclerView recyclerView, Context context, int flag) {
        super(myDataSet, recyclerView);
        this.context = context;
        this.myDataSet = myDataSet;
        appPref = new AppPrefrences(context);
        this.flag = flag;


    }


    public PostListAdapter(ArrayList<PostListResponse.Post> myDataSet, RecyclerView recyclerView, Context context, int flag, View behindView) {
        super(myDataSet, recyclerView);
        this.context = context;
        this.myDataSet = myDataSet;
        appPref = new AppPrefrences(context);
        this.flag = flag;
        this.behindView = behindView;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        RecyclerView.ViewHolder vh;
        if (viewType == VIEW_ITEM) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.lst_post, parent, false);
            vh = new ViewHolder(view);
        } else if (viewType == TYPE_HEADER) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.transparent_header, parent, false);
            VHHeader viewHolder = new VHHeader(view);
            return viewHolder;
        } else {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.progress_layout, parent, false);
            vh = new ProgressViewHolder(view);

        }
        return vh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position) == VIEW_ITEM) {
            final ViewHolder vholder = (ViewHolder) holder;

            PostListResponse.Post post = getItem(position);


            Utils.setTextStyle(vholder.txtSubordinateName, context, Utils.MEDIUM);
            Utils.setTextStyle(vholder.txtSubordinateDesignation, context, Utils.REGULAR);
            Utils.setTextStyle(vholder.txtPost, context, Utils.REGULAR);

            Utils.setTextStyle(vholder.txtTime_Stamp, context, Utils.REGULAR);

            if (post.getUser().getUserId() != appPref.getUserId()) {
                vholder.txtSubordinateName.setText(post.getUser().getFirstName() + " " + post.getUser().getLastName());


            } else {
                vholder.txtSubordinateName.setText(R.string.me);
            }

            if (post.getMember() != null) {
                vholder.txtSubordinateDesignation.setText(post.getMember().getDesignation().getDesignationTitle());
                vholder.txtPost.setText(post.getContent());
                Utils.loadUrlImage(post.getUser().getProfileImage(), vholder.imgSubordinateImage);

//                if (post.getContent().length() > 200) {
//                    UtileClass.makeTextViewResizable(context, vholder.txtPost, 4, "Read More", true);
//                }

                vholder.txtPost.collapseText();
                vholder.relPostDetail.setTag(position);
                vholder.relPostDetail.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View v) {

                        final int pos = (int) v.getTag();
                        if (flag == 2) {
                            new SweetAlertDialog(context, SweetAlertDialog.NORMAL_TYPE)
                                    .setTitleText("Alert")
                                    .showCancelButton(true)
                                    .setCancelText("No")
                                    .setConfirmText("Yes")
                                    .setContentText("Are you sure you want to Delete?")
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                        }
                                    })
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.dismiss();
                                            deletePost(pos);
                                        }
                                    })
                                    .show();

                        }
                        return false;

                    }
                });


                try {
                    SimpleDateFormat outputFmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    outputFmt.setTimeZone(TimeZone.getTimeZone("UTC"));
                    //  ((VHHeader) holder).eaDate.setText(UtileClass.getDate(outputFmt.parse(response.getResult().getMember().getEffective().getAssignEffectiveActionDate().getDate()), "dd MMMM, yyyy"));


                    long l = outputFmt.parse(post.getCreatedAt().getDate()).getTime();

                    String str = DateUtils.getRelativeDateTimeString(

                            context, // Suppose you are in an activity or other Context subclass

                            l, // The time to display

                            DateUtils.SECOND_IN_MILLIS, // The resolution. This will display only
                            // minutes (no "3 seconds ago")


                            DateUtils.YEAR_IN_MILLIS, // The maximum resolution at which the time will switch
                            // to default date instead of spans. This will not
                            // display "3 weeks ago" but a full date instead

                            0).toString();
                    if (str.contains(",")) {
                        vholder.txtTime_Stamp.setText(str.split(",")[0]);
                    } else {
                        vholder.txtTime_Stamp.setText(str);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }


        } else if (holder instanceof VHHeader) {

            ((View) ((VHHeader) holder).header.getParent()).setVisibility(View.INVISIBLE);

            if (context instanceof SubToSubProfileActivity) {
                ViewGroup.LayoutParams params = ((VHHeader) holder).transHead.getLayoutParams();
                params.height = Utils.dpToPx(context, 240);

                ((VHHeader) holder).transHead.setLayoutParams(params);
            }


        } else {
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemCount() {


        if (behindView != null)
            return myDataSet.size() + 1;
        else
            return myDataSet.size();
    }

    public PostListResponse.Post getItem(int position) {
        if (behindView != null)
            return myDataSet.get(position - 1);

        return myDataSet.get(position);
    }


    @Override
    public int getItemViewType(int position) {

        if (behindView != null) {
            if (isPositionHeader(position))
                return TYPE_HEADER;

            return myDataSet.get(position - 1) != null ? VIEW_ITEM : VIEW_PROG;

        }

        return myDataSet.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {


        private final TextView txtSubordinateName;
        private final TextView txtSubordinateDesignation;
        private final CircleImageView imgSubordinateImage;
        private final ExpandableTextView txtPost;
        private final TextView txtTime_Stamp;
        private final RelativeLayout relPostDetail;

        public ViewHolder(View itemView) {
            super(itemView);

            txtSubordinateName = (TextView) itemView.findViewById(R.id.txtSubordinateName);
            txtSubordinateDesignation = (TextView) itemView.findViewById(R.id.txtSubordinateDesignation);
            txtPost = (ExpandableTextView) itemView.findViewById(R.id.txtPost);
            txtTime_Stamp = (TextView) itemView.findViewById(R.id.txtTime_Stamp);
            imgSubordinateImage = (CircleImageView) itemView.findViewById(R.id.imgSubordinateImage);
            relPostDetail = (RelativeLayout) itemView.findViewById(R.id.relPostDetail);

        }
    }

    private void deletePost(final int position) {
        retrofit = new Retrofit.Builder()
                .baseUrl(ApiConstants.BASE_URL)
                .addCallAdapterFactory(new ErrorCallback.ErrorHandlingCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create())
                .client(NetworkUtil.addHeader(context))
                .build();
        GCApi service = retrofit.create(GCApi.class);
        ErrorCallback.MyCall<GenericResponse> ip = service.deletepost(getPayload(position));


        ip.enqueue(new ErrorCallback.MyCallback<GenericResponse>() {
            @Override
            public void success(Response<GenericResponse> response) {
                Log.d("Tag", "SUCCESS! " + response.message());


                ((Activity) context).runOnUiThread(new Runnable() {
                    public void run() {
                        myDataSet.remove(myDataSet.get(position));
                        notifyDataSetChanged();
                    }
                });


            }

            @Override
            public void error(String errorMessage) {
                Log.d("Tag", "Error! " + errorMessage);

            }


        }, context, true);

    }

    private DeletePostReq getPayload(int position) {


        DeletePostReq req = new DeletePostReq();
        req.setDeviceId(Utils.getDeviceId(context));
        req.setUserToken(appPref.getUserToken());
        req.setUserId(appPref.getUserId());
        req.setPostId(myDataSet.get(position).getPostId());
        return req;
    }


    class VHHeader extends RecyclerView.ViewHolder {

        public View transHead;
        public TextView header;
        private int touchActionDownY;
        private int touchActionDownX;
        private boolean touchActionMoveStatus;
        private int touchActionMoveX;
        private int touchActionMoveY;


        public VHHeader(View rootView) {
            super(rootView);

            transHead = (View) rootView.findViewById(R.id.trans_head);
            transHead.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // http://stackoverflow.com/questions/8121491/is-it-possible-to-add-a-scrollable-textview-to-a-listview
//                    v.getParent().requestDisallowInterceptTouchEvent(true); // needed for complex gestures
                    // simple tap works without the above line as well
//                    return behindView.dispatchTouchEvent(event); // onTouchEvent won't work

//                    int threshold = 5;//10;
//
//
//                    switch (event.getAction()) {
//
//                        case MotionEvent.ACTION_DOWN:
//                            Log.i("test", "Down");
//
//                            touchActionDownX = (int) event.getX();
//                            touchActionDownY = (int) event.getY();
//                            touchActionMoveStatus = true;
//
//
//                            break;
//
//                        case MotionEvent.ACTION_POINTER_UP:
//
//                            touchActionMoveStatus = false;
//
//                            break;
//
//                        case MotionEvent.ACTION_MOVE:
//                            //Log.i("test","Move");
//
//
//                            if (touchActionMoveStatus) {
//
//                                touchActionMoveX = (int) event.getX();
//                                touchActionMoveY = (int) event.getY();
//
//                                if (touchActionMoveX < (touchActionDownX - threshold) && (touchActionMoveY > (touchActionDownY - threshold)) && (touchActionMoveY < (touchActionDownY + threshold))) {
//                                    Log.i("test", "Move Left");//If the move left was greater than the threshold and not greater than the threshold up or down
//                                    touchActionMoveStatus = false;
//                                    v.getParent().requestDisallowInterceptTouchEvent(true); // needed for complex gestures
//                                    // simple tap works without the above line as well
//                                    return behindView.dispatchTouchEvent(event);
//                                } else if (touchActionMoveX > (touchActionDownX + threshold) && (touchActionMoveY > (touchActionDownY - threshold)) && (touchActionMoveY < (touchActionDownY + threshold))) {
//                                    Log.i("test", "Move Right");//If the move right was greater than the threshold and not greater than the threshold up or
//                                    touchActionMoveStatus = false;
//                                    v.getParent().requestDisallowInterceptTouchEvent(true); // needed for complex gestures
//                                    // simple tap works without the above line as well
//                                    return behindView.dispatchTouchEvent(event);
//                                } else if (touchActionMoveY < (touchActionDownY - threshold) && (touchActionMoveX > (touchActionDownX - threshold)) && (touchActionMoveX < (touchActionDownX + threshold))) {
//                                    Log.i("test", "Move Up");//If the move up was greater than the threshold and not greater than the threshold left or right
//                                    touchActionMoveStatus = false;
//                                    v.getParent().getParent().requestDisallowInterceptTouchEvent(false); // needed for complex gestures
//                                    return v.dispatchTouchEvent(event);
//
//                                } else if (touchActionMoveY > (touchActionDownY + threshold) && (touchActionMoveX > (touchActionDownX - threshold)) && (touchActionMoveX < (touchActionDownX + threshold))) {
//                                    Log.i("test", "Move Down");//If the move down was greater than the threshold and not greater than the threshold left or right
//                                    touchActionMoveStatus = false;
//                                    v.getParent().getParent().requestDisallowInterceptTouchEvent(false); // needed for complex gestures
//                                    return v.dispatchTouchEvent(event);
//                                }
//                            }
//
//                            break;
//
//                    }
                    v.getParent().requestDisallowInterceptTouchEvent(true); // needed for complex gestures
                    // simple tap works without the above line as well
                    return behindView.dispatchTouchEvent(event);
                }
            });
            header = (TextView) rootView.findViewById(R.id.ea_header);


        }
    }


}